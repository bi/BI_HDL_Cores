BE-BI HDL Coding Convention
=

Use only English language and full/explicit words for all names given: it simplifies the transfer of your design to other parties involved. An exception should be made for very common and widely accepted words' abbreviations in the electronic world like Rst (reset), Clr (clear), En (enable), Cnt (counter) or Clk (clock). In general, you should avoid any special characters inside the name of an element because you might run into trouble with the compiler and/or synthesis tool.

The identifiers should be written using the "camel" rule: each separate word in the name should start with a capital letter.

Signals, Variables, Constants, Types, Generics, Files
-

These elements should have names containing only the letters (A-Z, a-z), comma (,) and numbers (0-9). Do not use any other characters. Further, there are various prefixes and suffixes shown in Table 1 and Table 2. Suffix can be stacked and this should be done omitting the underscore between them (ex: PulseCnt_eo).

They should be used to clearly identify the type of the user defined name. It is recommended to use the same signal or generic name through the whole design hierarchy. This helps you identifying a signal or generic at a low level which has been set at the top level entity. Further, you should propagate every generic to the top because so you are able to configure the whole design in a centralized way.

**Table 1: Mandatory extensions**

| Description                   | Extension   | Example       |
|-------------------------------|-------------|---------------|
| instance                      | prefix i    | i_MyModule    |
| variable                      | prefix v    | v_Buffer      |
| alias                         | prefix a    | a_Bit5        |
| constant                      | prefix c    | c_Length      |
| type definition               | prefix t    | t_MyType      |
| generics                      | prefix g    | g_Width       |
| file identifier               | prefix f    | f_RomContent  |
| FSM state constant prefix     | prefix s    | s_Idle        |
| low active signal or variable | suffix n    | Rst_n         |
| tri-stated                    | suffix z    | DataBus_z     |
| unit input signals            | suffix i    | Din_i         |
| unit output signals           | suffix o    | Led_o         |
| unit bus signals              | suffix b[n] | Data_b4       |
| interface instance            | suffix t    | MySpi_t       |

**Table 2: Optional extensions**

| Description               | Extension                | Example             |
|---------------------------|--------------------------|---------------------|
| asynchronous              | suffix a                 | State_a             |
| registered                | suffix q                 | State_q             |
| delayed of n clock cycles | suffix d[n] (n optional) | Pulse_d2            |
| counter                   | suffix c                 | Seconds_c           |
| pulse                     | suffix p                 | Done_p              |
| memory block              | suffix m[n]              | ShiftReg_m512b8     |
| synchronization logic     | suffix x                 | ScintillatorPulse_x |
| enable                    | suffix e                 | PulseCnt_e          |
| reset                     | suffix r                 | PulseCnt_r          |
| clock                     | suffix k                 | Clk40MHz_k          |
| enum instance             | suffix l                 | NextState_l         |
| signed                    | suffix s                 | Data_sb4            |

Code Examples
-

```cpp
// CASE 1: enums

typedef enum {
    s_Idle, // prefix s_ for FSM states
    s_DoingSomething,
    s_Drinking
} t_State; // prefix t_ for types

t_State State_l, NextState_l; // suffix _l for "instances" of enum

typedef enum {
    Nop, // not all enums has to be used for FSM, hence no s_ here
    Add,
    Sub,
    Div,
    Mult
} t_Operation;

// CASE 2: interfaces
// new suffix: _t - inTerface instances - mandatory

interface t_SpiInterface(); // interface is some sort of type, hence t_ prefix
    // ...
endinterface

t_SpiInterface MySpi_t(); // suffix _t for interface instance

module WbSpiMaster (
    t_WbInterface Wb_iot, // here suffix _t is used also for port names with addition of generic _io
                          // as interfaces are usually bidirectional (with no distinct direction
                          // defined, as "modport" is not supported by Quartus)
    t_SpiInterface Spi_iot
);

WbSpiMaster i_SpiMaster ( // prefix i_ for module instances
    .Wb_iot(MyWb_t), // the suffix _t is used analogically to _b in ".Data_ib(Data_b)"
    .Spi_iot(MySpi_t)
);
```
