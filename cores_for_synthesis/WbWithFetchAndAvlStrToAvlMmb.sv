/*
The DDR3 memory on the VFC-HD is 4Gb (per chip).
On the wishbone this is arranged as 128M Long Words (32 bits)
To address all the space 28 (27 for the prototype) bits of address are needed
The address bits are split between a page address (PageSelector input)
and a pointer to the LW in the page that we want to access via WishBone
*/
module WbWithFetchAndAvlStrToAvlMmb
#(
    parameter g_Log2PageSize  = 6, // Comment: in 32bit words. Min is 2 as it must be 128bits alligned. Max is 12 as the longest burst is 1024
    parameter g_Log2BurstSize = 10 // Determines the lenght of the data (128b) FIFO (2*2**g_Log2BurstSize)
)
(
  t_WbInterface                  WbSlave_iot,     // Only g_Log2PageSize bits of address are used
  input  [28-g_Log2PageSize-1:0] PageSelector_ib,
  output                         WriteFifoEmpty_o,
  input                          FetchPage_ip,
  output reg                     PageFetched_o
  t_AvalonSt_tt                  AvlStD128_ts,
  input                          LoadStreamerBaseAddress_ip,
  input  [25:0]                  StreamerBaseAddress_ib26, //The address in the DDR3 from which we start the streaming 'dump'
  output [25:0]                  StreamerWriteAddress_ob26,
  t_AvalonMMb_tt                 AvlMmBA26D128_tm
);

parameter [10:0] c_PageMemDepth     = 2**(g_Log2PageSize-2);
parameter [10:0] c_WriteBurstLenght = 2**(g_Log2BurstSize);

typedef enum {
    s_AvlIdle,
    s_AvlPageRead,
    s_AvlSingleWrite,
    s_AvlPacketWrite
} t_AvlState;

logic [127:0] PageMem_mb128 [c_PageMemDepth-1:0];
logic WriteFifoFull, WeWriteFifo;
t_AvlState AvlState_l = s_AvlIdle, AvlNextState_l, AvlState_ld;
logic FetchPage_x = 0;
logic [2:0] FetchPage_xd3 = 0;
logic FetchPage_p = 0;
logic FetchDone_x = 0;
logic [2:0] FetchDone_xd3 = 0;
logic FetchRequest;
logic [28-g_Log2PageSize-1:0] PageToFetch_b;
logic [(g_Log2PageSize-2)-1:0] MemWrPointer_c;
logic [25:0] MemoryWriteAddress_b26;
logic [1:0] MemoryWriteLwSelect_b2;
logic [31:0] MemoryWriteLw_b32;
logic ReWriteFifo;
logic FetchDone_p = 0;
logic StreamerDataFifoFull, StreamerDataFifoEmpty;
logic StreamerDataFifoRe, StreamerDataFifoWe;
logic [127:0] StreamerDataFifoOut_b128;
logic StreamerPacketInfoFifoFull, StreamerPacketInfoFifoEmpty;
logic StreamerPacketInfoFifoRe, StreamerPacketInfoFifoWe;
logic [25:0] PacketBaseAddressWr_b26, PacketBaseAddressRd_b26;
logic [10:0] PacketSizeWr_b11, PacketSizeRd_b11;
logic [10:0] StoredStremerWords_b11;
logic ClosePacket;
logic LoadStreamerBaseAddress_d;
logic [10:0] WordsInPacketSent_c11;

wire a_WbClk_k     = WbSlave_iot.Clk_k;
wire a_AvlMMbClk_k = AvlMmBA26D128_tm.Clk_k;
wire a_AvlStClk_k  = AvlStD128_ts.Clk_k;

//=== WishBone interface
always @(posedge a_WbClk_k)
    if (WbSlave_iot.Cyc && WbSlave_iot.Stb) begin //Wishbone access
        if (WbSlave_iot.We) begin //write access
            if (~WriteFifoFull && ~WbSlave_iot.Ack) begin
                WeWriteFifo     <= 1'b1;
                WbSlave_iot.Ack <= 1'b1;
            end else begin
                WeWriteFifo <= 1'b0;
            end
        end else begin //read access
            case (WbSlave_iot.Adr_b[1:0])
                2'd0: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][ 31: 0];
                2'd1: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][ 63:32];
                2'd2: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][ 95:64];
                2'd3: WbSlave_iot.DatMiSo_b <= PageMem_mb128[WbSlave_iot.Adr_b[g_Log2PageSize-1:2]][127:96];
            endcase
            WbSlave_iot.Ack <= 1'b1;
        end
    end else begin
        WbSlave_iot.Ack <= 1'b0;
        WeWriteFifo     <= 1'b0;
    end//Wb access closed or simply no access

generic_fifo_dc_gray_mod #(.dw(60), .aw(4))
    i_WriteFifo(
        .rd_clk(a_AvlMMbClk_k),
        .wr_clk(a_WbClk_k),
        .rst(1'b1),
        .clr(1'b0),
        .din({PageSelector_ib, WbSlave_iot.Adr_b[g_Log2PageSize-1:0], WbSlave_iot.DatMoSi_b}),
        .we(WeWriteFifo),
		.dout({MemoryWriteAddress_b26, MemoryWriteLwSelect_b2, MemoryWriteLw_b32}),
        .re(ReWriteFifo),
        .full(WriteFifoFull),
        .empty(WriteFifoEmpty_o),
        .wr_level(/* Not Connected */),
        .rd_level(/* Not Connected */)
        );

//=== Synchronization logic

always_ff @(posedge a_WbClk_k)
    if (FetchPage_ip) begin
        FetchPage_x   <= ~FetchPage_x;
        PageToFetch_b <= PageSelector_ib;
    end

always_ff @(posedge a_AvlMMbClk_k)
  FetchPage_xd3 <= {FetchPage_xd3[1:0], FetchPage_x};

wire FetchPageAvl_p = ^FetchPage_xd3[2:1];

always_ff @(posedge a_AvlMMbClk_k)
    if (FetchDone_p)
        FetchDone_x <= ~FetchDone_x;

always_ff @(posedge a_WbClk_k)
    FetchDone_xd3 <= {FetchDone_xd3[1:0], FetchDone_x};

wire FetchDoneWb_p = ^FetchDone_xd3[2:1];

always_ff @(posedge a_WbClk_k)
    if (FetchPage_ip)
        PageFetched_o <= 1'b0;
    else if (FetchDoneWb_p)
        PageFetched_o <= 1'b1;

//=== Avalon Streaming Interface
alias a_AvlStrClock_ik   = AvlStD128_ts.Clk_k;
alias a_AvlStrData_ib128 = AvlStD128_ts.DataMoSi_b;
alias a_AvlStrValid_i    = AvlStD128_ts.ValidMoSi;
alias a_AvlStrReady_o    = AvlStD128_ts.ReadyMiSo;

assign a_AvlStrReady_o    = ~StreamerDataFifoFull && ~StreamerPacketInfoFifoFull;
assign StreamerDataFifoWe = a_AvlStrValid_i&&a_AvlStrReady_o;

generic_fifo_dc_gray_mod #(.dw(128), .aw(g_Log2BurstSize+1))
    i_StreamerDataFifo(
        .rd_clk(a_AvlMMbClk_k),
        .wr_clk(a_AvlStrClock_ik),
        .rst(1'b1),
        .clr(1'b0),
        .din(a_AvlStrData_ib128),
        .we(StreamerDataFifoWe),
		.dout(StreamerDataFifoOut_b128),
        .re(StreamerDataFifoRe),
        .full(StreamerDataFifoFull),
        .empty(StreamerDataFifoEmpty),
        .wr_level(/* Not Connected */),
        .rd_level(/* Not Connected */)
        );

always_ff @ (posedge a_AvlStrClock_ik)
    LoadStreamerBaseAddress_d <= LoadStreamerBaseAddress_ip;

assign ClosePacket = (StoredStremerWords_b11==c_WriteBurstLenght)  ||
                     (LoadStreamerBaseAddress_d && |StoredStremerWords_b11) ||
                     (~a_AvlStrValid_i && |StoredStremerWords_b11);

always_ff @ (posedge a_AvlStrClock_ik)
    if (LoadStreamerBaseAddress_ip)
        StreamerWriteAddress_ob26 <= StreamerBaseAddress_ib26;
    else if (a_AvlStrValid_i && a_AvlStrReady_o)
        StreamerWriteAddress_ob26 <= StreamerWriteAddress_ob26 + 1'b1;

always_ff @ (posedge a_AvlStrClock_ik) begin
    StreamerPacketInfoFifoWe <= 1'b0;
    if (ClosePacket) begin
        PacketSizeWr_b11 <= StoredStremerWords_b11;
        StreamerPacketInfoFifoWe <= 1'b1;
        StoredStremerWords_b11 <= {10'b0, StreamerDataFifoWe};
        if (StreamerDataFifoWe)
            PacketBaseAddressWr_b26 <= StreamerWriteAddress_ob26;
    end else if (StreamerDataFifoWe && ~|StoredStremerWords_b11)
        PacketBaseAddressWr_b26 <= StreamerWriteAddress_ob26;
end

generic_fifo_dc_gray_mod #(.dw(37), .aw(5)) // Up to 32 packet queue
    i_StreamerPacketInfoFifo(
        .rd_clk(a_AvlMMbClk_k),
        .wr_clk(a_AvlStrClock_ik),
        .rst(1'b1),
        .clr(1'b0),
        .din({PacketSizeWr_b11, PacketBaseAddressWr_b26}),
        .we(StreamerPacketInfoFifoWe),
		.dout({PacketSizeRd_b11, PacketBaseAddressRd_b26}),
        .re(StreamerPacketInfoFifoRe),
        .full(StreamerPacketInfoFifoFull),
        .empty(StreamerPacketInfoFifoEmpty),
        .wr_level(/* Not Connected */),
        .rd_level(/* Not Connected */)
        );

//=== Avalon Burst MM interface
always_ff @(posedge a_AvlMMbClk_k)
  AvlState_l <= AvlNextState_l;

always_comb begin
    AvlNextState_l = AvlState_l;
    case (AvlState_l)
    s_AvlIdle:
        if (~WriteFifoEmpty_o && ~ReWriteFifo)   AvlNextState_l = s_AvlSingleWrite;
        else if (FetchRequest)                   AvlNextState_l = s_AvlPageRead;
        else if (~StreamerPacketInfoFifoRe)      AvlNextState_l = s_AvlPacketWrite;
    s_AvlSingleWrite:
        if (~AvlMmBA26D128_tm.WaitRequestMiSo)
            AvlNextState_l = s_AvlIdle;
    s_AvlPageRead:
        if (&MemWrPointer_c && ~AvlMmBA26D128_tm.WaitRequestMiSo)
            AvlNextState_l = s_AvlIdle;
    s_AvlPacketWrite:
        if (WordsInPacketSent_c11!=AvlMmBA26D128_tm.BurstCountMoSi_b
        &&  AvlMmBA26D128_tm.WriteMoSi
        && ~AvlMmBA26D128_tm.WaitRequestMiSo)
            AvlNextState_l = s_AvlIdle;
    default:
        AvlNextState_l = s_AvlIdle;
    endcase
end

always_ff @(posedge a_AvlMMbClk_k) begin
    FetchRequest             <= FetchPageAvl_p;
    ReWriteFifo              <= 1'b0;
    AvlState_ld              <= AvlState_l;
    FetchDone_p              <= 1'b0;
    case (AvlState_l)
    s_AvlIdle: begin
        MemWrPointer_c             <= 'h0;
        AvlMmBA26D128_tm.ReadMoSi  <= 1'b0;
        AvlMmBA26D128_tm.WriteMoSi <= 1'b0;
        StreamerDataFifoRe         <= 1'b0;
        StreamerPacketInfoFifoRe   <= 1'b0;
        WordsInPacketSent_c11      <= 11'd1;
    end
    s_AvlPageRead: begin
        FetchRequest                                            <= 1'b0;
        AvlMmBA26D128_tm.BurstCountMoSi_b                       <= c_PageMemDepth;
        AvlMmBA26D128_tm.ByteEnableMoSi                         <= 16'hFFFF;
        AvlMmBA26D128_tm.AddressMoSi_b[26-1:(g_Log2PageSize-2)] <= PageToFetch_b;
        AvlMmBA26D128_tm.AddressMoSi_b[(g_Log2PageSize-2)-1:0]  <= 'b0;
        if (AvlState_ld==s_AvlIdle) //first cycle in state
            AvlMmBA26D128_tm.ReadMoSi <= 1'b1;
        else if (~AvlMmBA26D128_tm.WaitRequestMiSo) //Request accepted
            AvlMmBA26D128_tm.ReadMoSi <= 1'b0;
        if (AvlNextState_l == s_AvlIdle)
            FetchDone_p  <= 1'b1;
        if (AvlMmBA26D128_tm.ReadDataValidMiSo) begin
            PageMem_mb128[MemWrPointer_c] <= AvlMmBA26D128_tm.DataReadMiSo_b;
            MemWrPointer_c                <= MemWrPointer_c + 1'b1;
        end
    end
    s_AvlSingleWrite: begin
        if (AvlState_ld==s_AvlIdle) begin//first cycle in state
            ReWriteFifo                       <= 1'b1;
            AvlMmBA26D128_tm.WriteMoSi        <= 1'b1;
            AvlMmBA26D128_tm.BurstCountMoSi_b <= 11'd1;
            AvlMmBA26D128_tm.AddressMoSi_b    <= MemoryWriteAddress_b26;
            case (MemoryWriteLwSelect_b2)
                2'd0: begin
                    AvlMmBA26D128_tm.DataWriteMoSi_b <= {96'h0, MemoryWriteLw_b32};
                    AvlMmBA26D128_tm.ByteEnableMoSi <= 16'h000F;
                end
                2'd1: begin
                    AvlMmBA26D128_tm.DataWriteMoSi_b <= {64'h0, MemoryWriteLw_b32, 32'h0};
                    AvlMmBA26D128_tm.ByteEnableMoSi <= 16'h00F0;
                end
                2'd2: begin
                    AvlMmBA26D128_tm.DataWriteMoSi_b <= {32'h0, MemoryWriteLw_b32, 64'h0};
                    AvlMmBA26D128_tm.ByteEnableMoSi <= 16'h0F00;
                end
                2'd3: begin
                    AvlMmBA26D128_tm.DataWriteMoSi_b <= {MemoryWriteLw_b32, 96'h0};
                    AvlMmBA26D128_tm.ByteEnableMoSi <= 16'hF000;
                end
            endcase
        end else begin
            ReWriteFifo         <= 1'b0;
            if (~AvlMmBA26D128_tm.WaitRequestMiSo) AvlMmBA26D128_tm.WriteMoSi <= 1'b0;
        end
    end
    s_AvlPacketWrite: begin
        if (AvlState_ld==s_AvlIdle) begin//first cycle in state
            StreamerPacketInfoFifoRe           <= 1'b1;
            StreamerDataFifoRe                 <= 1'b1;
            AvlMmBA26D128_tm.WriteMoSi         <= 1'b1;
            AvlMmBA26D128_tm.BurstCountMoSi_b  <= PacketSizeRd_b11;
            AvlMmBA26D128_tm.AddressMoSi_b     <= PacketBaseAddressRd_b26;
            AvlMmBA26D128_tm.DataWriteMoSi_b   <= StreamerDataFifoOut_b128;
            AvlMmBA26D128_tm.ByteEnableMoSi    <= 16'hFFFF;
        end else begin
            StreamerPacketInfoFifoRe <= 1'b0;
            StreamerDataFifoRe       <= 1'b0;
            if (AvlMmBA26D128_tm.WriteMoSi&&~AvlMmBA26D128_tm.WaitRequestMiSo) begin
                WordsInPacketSent_c11            <= WordsInPacketSent_c11 + 1'b1;
                AvlMmBA26D128_tm.DataWriteMoSi_b <= StreamerDataFifoOut_b128;
                if (WordsInPacketSent_c11!=AvlMmBA26D128_tm.BurstCountMoSi_b)
                    StreamerDataFifoRe <= 1'b1;
                else
                    AvlMmBA26D128_tm.WriteMoSi <= 1'b0;
            end
        end
    end
    default: begin
        MemWrPointer_c <= 'h0;
        AvlMmBA26D128_tm.ReadMoSi  <= 1'b0;
        AvlMmBA26D128_tm.WriteMoSi <= 1'b0;
    end
    endcase
end

endmodule
