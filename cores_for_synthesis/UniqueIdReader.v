`timescale 1ns/100ps

module UniqueIdReader
# (parameter g_OneUsClkCycles = 8'd100)
(   input   Rst_irq,
    input   Clk_ik,
    input   Cyc_i,
    input   Stb_i,
    input   We_i,
    input   [1:0] Adr_ib2,
    input   [31:0] Dat_ib32,
    output  reg [31:0] Dat_oab32,
    output  Ack_oa,

    inout OneWireBus_io);

localparam dly = 1;

reg [31:0]  CommandReg_b32 = 32'h0000_abcd;

localparam  c_StatusRegAddress_b2  = 2'd0,
            c_CommandRegAddress_b2 = 2'd1;

wire  [7:0] a_CommandCode_b8 = CommandReg_b32[31:24];
wire  [4:0] a_ReadWriteLenght_b5 = CommandReg_b32[20:16];
wire [15:0] a_CommandData_b16 = CommandReg_b32[15:0];

reg [4:0] BitCounter_c5;
reg [15:0] ShReg_b16 = 0;

reg [8:0] UsCounter_c9 = 0;
reg [7:0] ClkCounter_c8 = 0;

localparam  c_InitCommand = 8'h1,
            c_WriteCommand = 8'h2,
            c_ReadCommand = 8'h3;

localparam  s_Idle              = 4'h0,
            s_InitResetPulse    = 4'h1,
            s_InitWait1         = 4'h2,
            s_InitPresenceFlag  = 4'h3,
            s_InitClose         = 4'h4,
            s_WriteStart        = 4'h5,
            s_WriteBit          = 4'h6,
            s_WriteWait1        = 4'h7,
            s_ReadStart         = 4'h8,
            s_ReadWaitSample    = 4'h9,
            s_ReadWait1         = 4'ha;


reg [3:0]   NextState_a,
            State_q = s_Idle;

reg OneWireBus_x = 0, OneWireBus_q = 0;
reg OeBusDriver = 0,
    OneWireBusSynch_q=0;
reg [5:0] OneWireBusSynch_c6 = 0;

reg PresentFlag_q = 0;

always @(posedge Clk_ik)
    {OneWireBus_q, OneWireBus_x} <= #dly {OneWireBus_x, OneWireBus_io};

always @(posedge Clk_ik)
    if (OneWireBus_q==OneWireBusSynch_q) OneWireBusSynch_c6 <= #dly 'h0;
    else OneWireBusSynch_c6 <= #dly OneWireBusSynch_c6 + 1'b1;

always @(posedge Clk_ik)
    if (&OneWireBusSynch_c6) OneWireBusSynch_q  <= #dly OneWireBus_q;

assign  OneWireBus_io = OeBusDriver ? 1'b0 : 1'bz;

always @(posedge Clk_ik)
    if (Rst_irq) CommandReg_b32 <= #dly 'h0000_abcd;
    else if (Cyc_i && We_i && Stb_i && Adr_ib2==c_CommandRegAddress_b2) CommandReg_b32 <= #dly Dat_ib32;
    else CommandReg_b32[31] <= #dly 1'b1;

assign Ack_oa = Stb_i&&Cyc_i;

always @(posedge Clk_ik) State_q <= #dly  Rst_irq? s_Idle : NextState_a;

always @* begin
    NextState_a = State_q;
    case(State_q)
    s_Idle:
        if (a_CommandCode_b8 == c_InitCommand) NextState_a = s_InitResetPulse;
        else if (a_CommandCode_b8 == c_WriteCommand) NextState_a = s_WriteStart;
        else if (a_CommandCode_b8 == c_ReadCommand) NextState_a = s_ReadStart;
    s_InitResetPulse:
        if (&UsCounter_c9) NextState_a = s_InitWait1;
    s_InitWait1:
        if (OneWireBusSynch_q) NextState_a =  s_InitPresenceFlag;
    s_InitPresenceFlag:
        if (!OneWireBusSynch_q) NextState_a = s_InitClose;
        else if (&UsCounter_c9) NextState_a =  s_Idle;
    s_InitClose:
        if (&UsCounter_c9) NextState_a =  s_Idle;
    s_WriteStart:
        if (UsCounter_c9==9'd2) NextState_a = s_WriteBit;
    s_WriteBit:
        if (UsCounter_c9==9'd70) NextState_a = s_WriteWait1;
    s_WriteWait1:
        if (UsCounter_c9==9'd2) NextState_a = (BitCounter_c5==a_ReadWriteLenght_b5) ?  s_Idle : s_WriteStart;
    s_ReadStart:
        if (UsCounter_c9==9'd2) NextState_a = s_ReadWaitSample;
    s_ReadWaitSample:
        if (UsCounter_c9==9'd13) NextState_a =  s_ReadWait1;
    s_ReadWait1:
        if (UsCounter_c9>9'd63) NextState_a = (BitCounter_c5==a_ReadWriteLenght_b5) ?  s_Idle :  s_ReadStart;
    default:   NextState_a = s_Idle;
    endcase
end

always @(posedge Clk_ik)
    case(State_q)
    s_Idle:
    begin
        BitCounter_c5   <= #dly 'h1;
        UsCounter_c9    <= #dly 'h0;
        ClkCounter_c8   <= #dly 'h0;
        OeBusDriver     <= #dly 'h0;
        if (NextState_a==s_WriteStart) ShReg_b16 <= #dly a_CommandData_b16;
        else if (NextState_a==s_ReadStart) ShReg_b16 <= #dly 'h0;
    end
    s_InitResetPulse: begin
        PresentFlag_q <= #dly !OneWireBusSynch_q;
        if (NextState_a==State_q) begin
            OeBusDriver     <= #dly 'h1;
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        end else begin
            UsCounter_c9    <= #dly 'h0;
            OeBusDriver     <= #dly 'h0;
        end
    end
    s_InitPresenceFlag:
        PresentFlag_q <= #dly !OneWireBusSynch_q;
    s_InitClose:
        if (NextState_a==State_q)
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        else UsCounter_c9    <= #dly 'h0;
    s_WriteStart:
        if (NextState_a==State_q) begin
            OeBusDriver     <= #dly 'h1;
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        end else begin
            UsCounter_c9    <= #dly 'h0;
            OeBusDriver     <= #dly !ShReg_b16[0];
        end
    s_WriteBit:
        if (NextState_a==State_q)
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        else begin
            UsCounter_c9    <= #dly 'h0;
        end
    s_WriteWait1: begin
        OeBusDriver     <= #dly 'h0;
        if (NextState_a==State_q)
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else
                ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        else begin
            ShReg_b16   <= # dly {1'b0, ShReg_b16[15:1]};
            BitCounter_c5 <= # dly BitCounter_c5 + 1'b1;
            UsCounter_c9    <= #dly 'h0;
        end
    end
    s_ReadStart:
        if (NextState_a==State_q) begin
            OeBusDriver     <= #dly 'h1;
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        end
    s_ReadWaitSample: begin
        OeBusDriver     <= #dly 'h0;
        if (NextState_a==State_q)
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
        else  ShReg_b16   <= # dly {OneWireBusSynch_q, ShReg_b16[15:1]};
    end
    s_ReadWait1:
        if (NextState_a==State_q)
            if (ClkCounter_c8==g_OneUsClkCycles) begin
                ClkCounter_c8   <= #dly 'h0;
                UsCounter_c9    <= #dly UsCounter_c9 + 1'b1;
            end else begin
                ClkCounter_c8   <= #dly ClkCounter_c8 + 1'b1;
            end
        else begin
            BitCounter_c5 <= # dly BitCounter_c5 + 1'b1;
            UsCounter_c9    <= #dly 'h0;
        end
	endcase

always @* case (Adr_ib2)
    c_StatusRegAddress_b2:  Dat_oab32 = {PresentFlag_q, OeBusDriver, OneWireBusSynch_q, OneWireBus_io, State_q, UsCounter_c9[7:0], ShReg_b16};
    c_CommandRegAddress_b2: Dat_oab32 = CommandReg_b32;
    default:                Dat_oab32 = 32'hdeadbeef;
endcase


endmodule
