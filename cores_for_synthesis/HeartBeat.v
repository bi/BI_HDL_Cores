//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: HeartBeat.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 08/09/16      1.0          M. Barros Marin    First module definition.              
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     - Module for toggling an LED with a periodicity of 1s. 
//                                                                                              
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 100ps / 10ps

module HeartBeat 
//====================================  Global Parameters  ===================================//
#( parameter g_ClkFrequency_b32 = 32'h02625A00) // Comment: Toggling period: 1s@40MHz.
//========================================  I/O ports  =======================================//
(  
    //==== Clock & Reset ====//

    input      Clk_ik,
    input      Reset_ira,

    //==== Control Path ====//

    output reg HeartBeat_oq
    
);

//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

reg [31:0] HeartBeatCntr_c32;

//=======================================  User Logic  =======================================//

//======================//
//==== Control Path ====//
//======================//

always @(posedge Clk_ik or posedge Reset_ira)
    if (Reset_ira) begin
        HeartBeatCntr_c32     <= #1 32'h0;
        HeartBeat_oq          <= #1  1'b0;
    end else begin
        if (HeartBeatCntr_c32 == g_ClkFrequency_b32-1) begin
            HeartBeatCntr_c32 <= #1 32'h0;
            HeartBeat_oq      <= #1 ~HeartBeat_oq;
        end else begin
            HeartBeatCntr_c32 <= #1 HeartBeatCntr_c32+1;
        end
    end

endmodule