/*
* This is a SPI master for up to 32 independent slaves
* The module support the following configurations: 
* - Any CPol CPha combination
* - The length of the reg is programmable up to 256 allowing
*   the concatenation of several slaves, if the access is longer 
*   than 32 bits between 2 registers the communication is paused
* - MSB or LSB first
* - The speed of the clock is settable
* - It is possible to put a wait state between the falling edge 
*   of SS (Slave Select) and the 1st edge of SClk as requested by some 
*   ADC
*
*  Configuration:
*
*  CONFIG1
*    +--------+--------+----------+-------+-----------------+-------+----------------------+
*    | a_CPol | a_CPha | a_Lsb1St |       | a_SpiChannel_b5 |       | a_RegisterLenght_b12 |
*    +--------+--------+----------+-------+-----------------+-------+----------------------+
*        31       30       29       28:21       20:16         15:12        11:0
*
*    a_CPol - default state of SClk_o
*    a_CPha - SClk_o phase:
*       0 - data are sampled on the first SClk_o edge
*       1 - data are sampled on the second SClk_o edge
*    a_Lsb1St - whether to send/receive LSB first
*    a_SpiChannel_b5 - SS_onb32 and MiSo_ib32 channel concerned
*    a_RegisterLenght_b12 - bit length of the data being send/receive
*
*  CONFIG2
*    +---------------------+----------------+
*    | a_ClkSemiPeriod_b16 | a_WaitTime_b16 |
*    +---------------------+----------------+
*            31:16              15:0
*
*    a_ClkSemiPeriod_b16 - # of Clk_ik cycles for half of the SClk_o period
*       Example: Clk_ik frequency = 125 MHz, a_ClkSemiPeriod_b16 = 2, SClk_o frequency = 31.25 MHz
*    a_WaitTime_b16 - minimum # of Clk_ik cycles of idle state before start of transaction
*
*/

`timescale 1ns/100ps 

 module SpiMasterWb (
    input   Clk_ik,
    input   Rst_irq,
    input   Cyc_i,
    input   Stb_i,
    input   We_i,
    input   [2:0] Adr_ib3,
    input   [31:0] Dat_ib32,
    output  reg [31:0] Dat_oab32 = 32'hDEAD_BEEF,
    output  reg Ack_oa = 0,

    output  reg WaitingNewData_o = 0,
    output  reg ModuleIdle_o = 0,

    output  reg SClk_o = 0,
    output  MoSi_o,
    input   [31:0] MiSo_ib32,
    output  reg [31:0] SS_onb32 = 32'hFFFF_FFFF);

localparam	c_AddrStatus = 3'd0, 
			c_AddrConfig1 = 3'd1,
			c_AddrConfig2 = 3'd2,
			c_AddrShiftOut = 3'd3,
			c_AddrShiftIn = 3'd4;

reg [31:0]  Config2_qb32 = 0,
            ShiftOut_qb32 = 0,
            ShiftIn_qb32 = 0,
            Config1_qb32 = 0;

wire a_CPol = Config1_qb32[31];
wire a_CPha = Config1_qb32[30];
wire a_Lsb1St = Config1_qb32[29];
wire [4:0] a_SpiChannel_b5 = Config1_qb32[20:16];
wire [11:0] a_RegisterLenght_b12 = Config1_qb32[11:0];


wire [15:0] a_ClkSemiPeriod_b16 = Config2_qb32[31:16];
wire [15:0] a_WaitTime_b16 = Config2_qb32[15:0];
   
localparam	s_Idle = 3'd0,     
			s_WaitBefore1StEdge = 3'd1,    
			s_EdgeTypeOne = 3'd2,     
			s_EdgeTypeTwo = 3'd3, 
			s_TxPause = 3'd4,     
			s_EndOfTx = 3'd5;

reg [2:0]   State_a = s_Idle,
            State_q = s_Idle;

reg [15:0] TimeCounter_cb16 = 0;
reg [11:0] TxCounter_cb12 = 0;
reg WriteAck_q = 0;
reg StartTx_q = 0;

always @(posedge Clk_ik)
    if (Rst_irq) State_q    <= s_Idle;
    else State_q    <= State_a;

always @* begin
    State_a = State_q;     
    case (State_q)
        s_Idle: if (StartTx_q) State_a = s_WaitBefore1StEdge;
        s_WaitBefore1StEdge: if (TimeCounter_cb16==a_WaitTime_b16) State_a = s_EdgeTypeOne; 
        s_EdgeTypeOne: if (TimeCounter_cb16==a_ClkSemiPeriod_b16) State_a =  s_EdgeTypeTwo;
        s_EdgeTypeTwo: if (TimeCounter_cb16==a_ClkSemiPeriod_b16) 
            if (TxCounter_cb12==a_RegisterLenght_b12) State_a = s_EndOfTx;
            else if (|TxCounter_cb12[4:0]) State_a = s_EdgeTypeOne;
            else State_a = s_TxPause;
        s_TxPause: if (StartTx_q) State_a = s_EdgeTypeOne;
        s_EndOfTx: if (TimeCounter_cb16==a_ClkSemiPeriod_b16) State_a =  s_Idle;
        default: State_a = s_Idle;
    endcase
end

assign MoSi_o = a_Lsb1St ? ShiftOut_qb32[0] : ShiftOut_qb32[31];

always @(posedge Clk_ik) begin
    WriteAck_q          <= WriteAck_q && Stb_i; // Default clear condition valid for all the states
    ModuleIdle_o        <= 'h0;
    WaitingNewData_o    <= 'h0;     
    if (Rst_irq) begin
        StartTx_q           <= 'b0;
        WriteAck_q          <= 'b0;
        SClk_o              <= 'b0;
        SS_onb32            <= 32'hFFFF_FFFF;
        TimeCounter_cb16    <= 'h0;
        TxCounter_cb12      <= 'h0;
        Config1_qb32        <= 'h0;
        Config2_qb32        <= 'h0;
        ShiftOut_qb32       <= 'h0;
        ShiftIn_qb32        <= 'h0;
        ModuleIdle_o        <= 'h0;
        WaitingNewData_o    <= 'h0;
    end else case (State_q)
        s_Idle: begin
            SClk_o              <= a_CPol;
            SS_onb32            <= 32'hFFFF_FFFF;
            TimeCounter_cb16    <= 'h0;
            TxCounter_cb12      <= 'h0;
            ModuleIdle_o        <= State_a==s_Idle;
            if (Cyc_i && We_i && Stb_i && Adr_ib3==c_AddrShiftOut) begin
                ShiftOut_qb32   <= Dat_ib32;
                ShiftIn_qb32    <= 'h0;
                WriteAck_q      <= 1'b1;       
                StartTx_q       <= 1'b1;
            end else if (Cyc_i && We_i && Stb_i && Adr_ib3==c_AddrConfig1) begin
                Config1_qb32        <= Dat_ib32;
                WriteAck_q          <= 1'b1;       
            end else if (Cyc_i && We_i && Stb_i && Adr_ib3==c_AddrConfig2) begin
                Config2_qb32        <= Dat_ib32;
                WriteAck_q          <= 1'b1;     
            end
        end
        s_WaitBefore1StEdge: begin
            TimeCounter_cb16            <= TimeCounter_cb16 + 1'b1;
            SS_onb32[a_SpiChannel_b5]   <= 1'b0;    
            if (State_a==s_EdgeTypeOne) TimeCounter_cb16 <= 'h0;  
        end
        s_EdgeTypeOne: begin
            TimeCounter_cb16    <= TimeCounter_cb16 + 1'b1;
            if (State_a==s_EdgeTypeTwo) begin
                TimeCounter_cb16    <= 'h0;
                SClk_o              <= ~SClk_o;
                TxCounter_cb12      <= TxCounter_cb12 + 1'b1;
                StartTx_q           <= 'h0;
                if (a_CPha && ~StartTx_q) ShiftOut_qb32   <= a_Lsb1St ? {ShiftOut_qb32[0], ShiftOut_qb32[31:1]} : {ShiftOut_qb32[30:0], ShiftOut_qb32[31]};
                else if (~a_CPha) ShiftIn_qb32 <= a_Lsb1St ? {MiSo_ib32[a_SpiChannel_b5], ShiftIn_qb32[31:1]} : {ShiftIn_qb32[30:0], MiSo_ib32[a_SpiChannel_b5]};
            end             
        end 
        s_EdgeTypeTwo: begin
            TimeCounter_cb16    <= TimeCounter_cb16 + 1'b1;     
            if (State_a!=s_EdgeTypeTwo) begin
                SClk_o              <= ~SClk_o;
                TimeCounter_cb16    <= 'h0;
                if (~a_CPha)ShiftOut_qb32   <= a_Lsb1St ? {ShiftOut_qb32[0], ShiftOut_qb32[31:1]} : {ShiftOut_qb32[30:0], ShiftOut_qb32[31]};
                else ShiftIn_qb32 <= a_Lsb1St ? {MiSo_ib32[a_SpiChannel_b5], ShiftIn_qb32[31:1]} : {ShiftIn_qb32[30:0], MiSo_ib32[a_SpiChannel_b5]};
            end                
        end 
        s_TxPause: begin
            WaitingNewData_o    <= State_a==s_TxPause;
            if (Cyc_i && We_i && Stb_i && Adr_ib3==c_AddrShiftOut) begin
                ShiftOut_qb32   <= Dat_ib32;
                ShiftIn_qb32    <= 'h0;    
                WriteAck_q      <= 1'b1;       
                StartTx_q       <= 1'b1;
            end
        end
        s_EndOfTx: begin
            TimeCounter_cb16    <= TimeCounter_cb16 + 1'b1;
            if (State_a==s_Idle) SS_onb32   <= 32'hFFFF_FFFF;    
        end
        default: begin
            StartTx_q           <= 'b0;
            WriteAck_q          <= 'b0;
            SClk_o              <= 'b0;
            SS_onb32            <= 32'hFFFF_FFFF;
            TimeCounter_cb16    <= 'h0;
            TxCounter_cb12      <= 'h0;
            Config1_qb32        <= 'h0;
            Config2_qb32        <= 'h0;
            ShiftOut_qb32       <= 'h0;
            ShiftIn_qb32        <= 'h0;
            ModuleIdle_o        <= 'h0;
            WaitingNewData_o    <= 'h0;  
        end
    endcase
end

always @* begin
    Ack_oa   <= (Stb_i&&Cyc_i&&~We_i) || WriteAck_q;
    case (Adr_ib3)
        c_AddrStatus:   Dat_oab32   <= {12'h0, 1'b0, State_q, 4'b0, TxCounter_cb12};
        c_AddrConfig1:  Dat_oab32   <= Config1_qb32;
        c_AddrConfig2:  Dat_oab32   <= Config2_qb32;
        c_AddrShiftOut: Dat_oab32   <= ShiftOut_qb32;
        c_AddrShiftIn:  Dat_oab32   <= ShiftIn_qb32;
        default: Dat_oab32 <= 32'hDEAD_BEEF;
    endcase
end

endmodule