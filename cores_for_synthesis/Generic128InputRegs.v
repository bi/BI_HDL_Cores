`timescale 1ns/100ps 

// Added by M. Barros Marin (07/12/16)

module Generic128InputRegs (
	input	           Clk_ik,
    input              Rst_irq,
    input              Cyc_i,
    input              Stb_i,
    input      [  6:0] Adr_ib7,
    output reg [ 31:0] Dat_oab32,
    output reg         Ack_oa,
    input      [ 31:0] Reg0Value_ib32,
    input      [ 31:0] Reg1Value_ib32,
    input      [ 31:0] Reg2Value_ib32,
    input      [ 31:0] Reg3Value_ib32,
    input      [ 31:0] Reg4Value_ib32,
    input      [ 31:0] Reg5Value_ib32,
    input      [ 31:0] Reg6Value_ib32,
    input      [ 31:0] Reg7Value_ib32,
    input      [ 31:0] Reg8Value_ib32,
    input      [ 31:0] Reg9Value_ib32,
    input      [ 31:0] Reg10Value_ib32,
    input      [ 31:0] Reg11Value_ib32,
    input      [ 31:0] Reg12Value_ib32,
    input      [ 31:0] Reg13Value_ib32,
    input      [ 31:0] Reg14Value_ib32,
    input      [ 31:0] Reg15Value_ib32,
    input      [ 31:0] Reg16Value_ib32,
    input      [ 31:0] Reg17Value_ib32,
    input      [ 31:0] Reg18Value_ib32,
    input      [ 31:0] Reg19Value_ib32,
    input      [ 31:0] Reg20Value_ib32,
    input      [ 31:0] Reg21Value_ib32,
    input      [ 31:0] Reg22Value_ib32,
    input      [ 31:0] Reg23Value_ib32,
    input      [ 31:0] Reg24Value_ib32,
    input      [ 31:0] Reg25Value_ib32,
    input      [ 31:0] Reg26Value_ib32,
    input      [ 31:0] Reg27Value_ib32,
    input      [ 31:0] Reg28Value_ib32,
    input      [ 31:0] Reg29Value_ib32,
    input      [ 31:0] Reg30Value_ib32,
    input      [ 31:0] Reg31Value_ib32,
    input      [ 31:0] Reg32Value_ib32,
    input      [ 31:0] Reg33Value_ib32,
    input      [ 31:0] Reg34Value_ib32,
    input      [ 31:0] Reg35Value_ib32,
    input      [ 31:0] Reg36Value_ib32,
    input      [ 31:0] Reg37Value_ib32,
    input      [ 31:0] Reg38Value_ib32,
    input      [ 31:0] Reg39Value_ib32,
    input      [ 31:0] Reg40Value_ib32,
    input      [ 31:0] Reg41Value_ib32,
    input      [ 31:0] Reg42Value_ib32,
    input      [ 31:0] Reg43Value_ib32,
    input      [ 31:0] Reg44Value_ib32,
    input      [ 31:0] Reg45Value_ib32,
    input      [ 31:0] Reg46Value_ib32,
    input      [ 31:0] Reg47Value_ib32,
    input      [ 31:0] Reg48Value_ib32,
    input      [ 31:0] Reg49Value_ib32,
    input      [ 31:0] Reg50Value_ib32,
    input      [ 31:0] Reg51Value_ib32,
    input      [ 31:0] Reg52Value_ib32,
    input      [ 31:0] Reg53Value_ib32,
    input      [ 31:0] Reg54Value_ib32,
    input      [ 31:0] Reg55Value_ib32,
    input      [ 31:0] Reg56Value_ib32,
    input      [ 31:0] Reg57Value_ib32,
    input      [ 31:0] Reg58Value_ib32,
    input      [ 31:0] Reg59Value_ib32,
    input      [ 31:0] Reg60Value_ib32,
    input      [ 31:0] Reg61Value_ib32,
    input      [ 31:0] Reg62Value_ib32,
    input      [ 31:0] Reg63Value_ib32,
    input      [ 31:0] Reg64Value_ib32,
    input      [ 31:0] Reg65Value_ib32,
    input      [ 31:0] Reg66Value_ib32,
    input      [ 31:0] Reg67Value_ib32,
    input      [ 31:0] Reg68Value_ib32,
    input      [ 31:0] Reg69Value_ib32,
    input      [ 31:0] Reg70Value_ib32,
    input      [ 31:0] Reg71Value_ib32,
    input      [ 31:0] Reg72Value_ib32,
    input      [ 31:0] Reg73Value_ib32,
    input      [ 31:0] Reg74Value_ib32,
    input      [ 31:0] Reg75Value_ib32,
    input      [ 31:0] Reg76Value_ib32,
    input      [ 31:0] Reg77Value_ib32,
    input      [ 31:0] Reg78Value_ib32,
    input      [ 31:0] Reg79Value_ib32,
    input      [ 31:0] Reg80Value_ib32,
    input      [ 31:0] Reg81Value_ib32,
    input      [ 31:0] Reg82Value_ib32,
    input      [ 31:0] Reg83Value_ib32,
    input      [ 31:0] Reg84Value_ib32,
    input      [ 31:0] Reg85Value_ib32,
    input      [ 31:0] Reg86Value_ib32,
    input      [ 31:0] Reg87Value_ib32,
    input      [ 31:0] Reg88Value_ib32,
    input      [ 31:0] Reg89Value_ib32,
    input      [ 31:0] Reg90Value_ib32,
    input      [ 31:0] Reg91Value_ib32,
    input      [ 31:0] Reg92Value_ib32,
    input      [ 31:0] Reg93Value_ib32,
    input      [ 31:0] Reg94Value_ib32,
    input      [ 31:0] Reg95Value_ib32,
    input      [ 31:0] Reg96Value_ib32,
    input      [ 31:0] Reg97Value_ib32,
    input      [ 31:0] Reg98Value_ib32,
    input      [ 31:0] Reg99Value_ib32,
    input      [ 31:0] Reg100Value_ib32,
    input      [ 31:0] Reg101Value_ib32,
    input      [ 31:0] Reg102Value_ib32,
    input      [ 31:0] Reg103Value_ib32,
    input      [ 31:0] Reg104Value_ib32,
    input      [ 31:0] Reg105Value_ib32,
    input      [ 31:0] Reg106Value_ib32,
    input      [ 31:0] Reg107Value_ib32,
    input      [ 31:0] Reg108Value_ib32,
    input      [ 31:0] Reg109Value_ib32,
    input      [ 31:0] Reg110Value_ib32,
    input      [ 31:0] Reg111Value_ib32,
    input      [ 31:0] Reg112Value_ib32,
    input      [ 31:0] Reg113Value_ib32,
    input      [ 31:0] Reg114Value_ib32,
    input      [ 31:0] Reg115Value_ib32,
    input      [ 31:0] Reg116Value_ib32,
    input      [ 31:0] Reg117Value_ib32,
    input      [ 31:0] Reg118Value_ib32,
    input      [ 31:0] Reg119Value_ib32,
    input      [ 31:0] Reg120Value_ib32,
    input      [ 31:0] Reg121Value_ib32,
    input      [ 31:0] Reg122Value_ib32,
    input      [ 31:0] Reg123Value_ib32,
    input      [ 31:0] Reg124Value_ib32,
    input      [ 31:0] Reg125Value_ib32,
    input      [ 31:0] Reg126Value_ib32,
    input      [ 31:0] Reg127Value_ib32
);
	
always @(posedge Clk_ik)
    if (Rst_irq) begin
        Dat_oab32                  <= #1 32'h0;
        Ack_oa                     <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib7)
                7'd  0:  Dat_oab32 <= #1 Reg0Value_ib32;
                7'd  1:  Dat_oab32 <= #1 Reg1Value_ib32;
                7'd  2:  Dat_oab32 <= #1 Reg2Value_ib32;
                7'd  3:  Dat_oab32 <= #1 Reg3Value_ib32;
                7'd  4:  Dat_oab32 <= #1 Reg4Value_ib32;
                7'd  5:  Dat_oab32 <= #1 Reg5Value_ib32;
                7'd  6:  Dat_oab32 <= #1 Reg6Value_ib32;
                7'd  7:  Dat_oab32 <= #1 Reg7Value_ib32;
                7'd  8:  Dat_oab32 <= #1 Reg8Value_ib32;
                7'd  9:  Dat_oab32 <= #1 Reg9Value_ib32;
                7'd 10:  Dat_oab32 <= #1 Reg10Value_ib32;
                7'd 11:  Dat_oab32 <= #1 Reg11Value_ib32;
                7'd 12:  Dat_oab32 <= #1 Reg12Value_ib32;
                7'd 13:  Dat_oab32 <= #1 Reg13Value_ib32;
                7'd 14:  Dat_oab32 <= #1 Reg14Value_ib32;
                7'd 15:  Dat_oab32 <= #1 Reg15Value_ib32;
                7'd 16:  Dat_oab32 <= #1 Reg16Value_ib32;
                7'd 17:  Dat_oab32 <= #1 Reg17Value_ib32;
                7'd 18:  Dat_oab32 <= #1 Reg18Value_ib32;
                7'd 19:  Dat_oab32 <= #1 Reg19Value_ib32;
                7'd 20:  Dat_oab32 <= #1 Reg20Value_ib32;
                7'd 21:  Dat_oab32 <= #1 Reg21Value_ib32;
                7'd 22:  Dat_oab32 <= #1 Reg22Value_ib32;
                7'd 23:  Dat_oab32 <= #1 Reg23Value_ib32;
                7'd 24:  Dat_oab32 <= #1 Reg24Value_ib32;
                7'd 25:  Dat_oab32 <= #1 Reg25Value_ib32;
                7'd 26:  Dat_oab32 <= #1 Reg26Value_ib32;
                7'd 27:  Dat_oab32 <= #1 Reg27Value_ib32;
                7'd 28:  Dat_oab32 <= #1 Reg28Value_ib32;
                7'd 29:  Dat_oab32 <= #1 Reg29Value_ib32;
                7'd 30:  Dat_oab32 <= #1 Reg30Value_ib32;
                7'd 31:  Dat_oab32 <= #1 Reg31Value_ib32;
                7'd 32:  Dat_oab32 <= #1 Reg32Value_ib32;
                7'd 33:  Dat_oab32 <= #1 Reg33Value_ib32;
                7'd 34:  Dat_oab32 <= #1 Reg34Value_ib32;
                7'd 35:  Dat_oab32 <= #1 Reg35Value_ib32;
                7'd 36:  Dat_oab32 <= #1 Reg36Value_ib32;
                7'd 37:  Dat_oab32 <= #1 Reg37Value_ib32;
                7'd 38:  Dat_oab32 <= #1 Reg38Value_ib32;
                7'd 39:  Dat_oab32 <= #1 Reg39Value_ib32;
                7'd 40:  Dat_oab32 <= #1 Reg40Value_ib32;
                7'd 41:  Dat_oab32 <= #1 Reg41Value_ib32;
                7'd 42:  Dat_oab32 <= #1 Reg42Value_ib32;
                7'd 43:  Dat_oab32 <= #1 Reg43Value_ib32;
                7'd 44:  Dat_oab32 <= #1 Reg44Value_ib32;
                7'd 45:  Dat_oab32 <= #1 Reg45Value_ib32;
                7'd 46:  Dat_oab32 <= #1 Reg46Value_ib32;
                7'd 47:  Dat_oab32 <= #1 Reg47Value_ib32;
                7'd 48:  Dat_oab32 <= #1 Reg48Value_ib32;
                7'd 49:  Dat_oab32 <= #1 Reg49Value_ib32;
                7'd 50:  Dat_oab32 <= #1 Reg50Value_ib32;
                7'd 51:  Dat_oab32 <= #1 Reg51Value_ib32;
                7'd 52:  Dat_oab32 <= #1 Reg52Value_ib32;
                7'd 53:  Dat_oab32 <= #1 Reg53Value_ib32;
                7'd 54:  Dat_oab32 <= #1 Reg54Value_ib32;
                7'd 55:  Dat_oab32 <= #1 Reg55Value_ib32;
                7'd 56:  Dat_oab32 <= #1 Reg56Value_ib32;
                7'd 57:  Dat_oab32 <= #1 Reg57Value_ib32;
                7'd 58:  Dat_oab32 <= #1 Reg58Value_ib32;
                7'd 59:  Dat_oab32 <= #1 Reg59Value_ib32;
                7'd 60:  Dat_oab32 <= #1 Reg60Value_ib32;
                7'd 61:  Dat_oab32 <= #1 Reg61Value_ib32;
                7'd 62:  Dat_oab32 <= #1 Reg62Value_ib32;
                7'd 63:  Dat_oab32 <= #1 Reg63Value_ib32;
                7'd 64:  Dat_oab32 <= #1 Reg64Value_ib32;
                7'd 65:  Dat_oab32 <= #1 Reg65Value_ib32;
                7'd 66:  Dat_oab32 <= #1 Reg66Value_ib32;
                7'd 67:  Dat_oab32 <= #1 Reg67Value_ib32;
                7'd 68:  Dat_oab32 <= #1 Reg68Value_ib32;
                7'd 69:  Dat_oab32 <= #1 Reg69Value_ib32;
                7'd 70:  Dat_oab32 <= #1 Reg70Value_ib32;
                7'd 71:  Dat_oab32 <= #1 Reg71Value_ib32;
                7'd 72:  Dat_oab32 <= #1 Reg72Value_ib32;
                7'd 73:  Dat_oab32 <= #1 Reg73Value_ib32;
                7'd 74:  Dat_oab32 <= #1 Reg74Value_ib32;
                7'd 75:  Dat_oab32 <= #1 Reg75Value_ib32;
                7'd 76:  Dat_oab32 <= #1 Reg76Value_ib32;
                7'd 77:  Dat_oab32 <= #1 Reg77Value_ib32;
                7'd 78:  Dat_oab32 <= #1 Reg78Value_ib32;
                7'd 79:  Dat_oab32 <= #1 Reg79Value_ib32;
                7'd 80:  Dat_oab32 <= #1 Reg80Value_ib32;
                7'd 81:  Dat_oab32 <= #1 Reg81Value_ib32;
                7'd 82:  Dat_oab32 <= #1 Reg82Value_ib32;
                7'd 83:  Dat_oab32 <= #1 Reg83Value_ib32;
                7'd 84:  Dat_oab32 <= #1 Reg84Value_ib32;
                7'd 85:  Dat_oab32 <= #1 Reg85Value_ib32;
                7'd 86:  Dat_oab32 <= #1 Reg86Value_ib32;
                7'd 87:  Dat_oab32 <= #1 Reg87Value_ib32;
                7'd 88:  Dat_oab32 <= #1 Reg88Value_ib32;
                7'd 89:  Dat_oab32 <= #1 Reg89Value_ib32;
                7'd 90:  Dat_oab32 <= #1 Reg90Value_ib32;
                7'd 91:  Dat_oab32 <= #1 Reg91Value_ib32;
                7'd 92:  Dat_oab32 <= #1 Reg92Value_ib32;
                7'd 93:  Dat_oab32 <= #1 Reg93Value_ib32;
                7'd 94:  Dat_oab32 <= #1 Reg94Value_ib32;
                7'd 95:  Dat_oab32 <= #1 Reg95Value_ib32;
                7'd 96:  Dat_oab32 <= #1 Reg96Value_ib32;
                7'd 97:  Dat_oab32 <= #1 Reg97Value_ib32;
                7'd 98:  Dat_oab32 <= #1 Reg98Value_ib32;
                7'd 99:  Dat_oab32 <= #1 Reg99Value_ib32;
                7'd100:  Dat_oab32 <= #1 Reg100Value_ib32;
                7'd101:  Dat_oab32 <= #1 Reg101Value_ib32;
                7'd102:  Dat_oab32 <= #1 Reg102Value_ib32;
                7'd103:  Dat_oab32 <= #1 Reg103Value_ib32;
                7'd104:  Dat_oab32 <= #1 Reg104Value_ib32;
                7'd105:  Dat_oab32 <= #1 Reg105Value_ib32;
                7'd106:  Dat_oab32 <= #1 Reg106Value_ib32;
                7'd107:  Dat_oab32 <= #1 Reg107Value_ib32;
                7'd108:  Dat_oab32 <= #1 Reg108Value_ib32;
                7'd109:  Dat_oab32 <= #1 Reg109Value_ib32;
                7'd110:  Dat_oab32 <= #1 Reg110Value_ib32;
                7'd111:  Dat_oab32 <= #1 Reg111Value_ib32;
                7'd112:  Dat_oab32 <= #1 Reg112Value_ib32;
                7'd113:  Dat_oab32 <= #1 Reg113Value_ib32;
                7'd114:  Dat_oab32 <= #1 Reg114Value_ib32;
                7'd115:  Dat_oab32 <= #1 Reg115Value_ib32;
                7'd116:  Dat_oab32 <= #1 Reg116Value_ib32;
                7'd117:  Dat_oab32 <= #1 Reg117Value_ib32;
                7'd118:  Dat_oab32 <= #1 Reg118Value_ib32;
                7'd119:  Dat_oab32 <= #1 Reg119Value_ib32;
                7'd120:  Dat_oab32 <= #1 Reg120Value_ib32;
                7'd121:  Dat_oab32 <= #1 Reg121Value_ib32;
                7'd122:  Dat_oab32 <= #1 Reg122Value_ib32;
                7'd123:  Dat_oab32 <= #1 Reg123Value_ib32;
                7'd124:  Dat_oab32 <= #1 Reg124Value_ib32;
                7'd125:  Dat_oab32 <= #1 Reg125Value_ib32;
                7'd126:  Dat_oab32 <= #1 Reg126Value_ib32;
                7'd127:  Dat_oab32 <= #1 Reg127Value_ib32;
                default: Dat_oab32 <= #1 Reg0Value_ib32;
            endcase
        end
        Ack_oa                     <= #1 Stb_i && Cyc_i;
    end

endmodule