`timescale 1ns/100ps 

module I2cMasterGeneric #(  parameter    g_CycleLenght = 10'h3ff)
(   input   Clk_ik,
    input   Rst_irq,
    
    input   SendStartBit_ip,
    input   SendByte_ip,
    input   GetByte_ip,
    input   SendStopBit_ip,
    input   [7:0] Byte_ib8,
    input   AckToSend_i,
    output  [7:0] Byte_ob8,
    output  AckReceived_o,
    output  Done_o,
    
    inout Scl_ioz,
    inout Sda_ioz);


 
reg  [3:0] BitCounter_c4; 
reg  [9:0] CycleCounter_c10;   
reg SclOe_e , SdaOe_e;
reg [7:0] ShReg_b8;
reg I2CAck;

localparam  s_Idle              = 4'h0,
            s_StartSda1         = 4'h1,
            s_StartScl1         = 4'h2,
            s_StartSda0         = 4'h3,
            s_StartScl0         = 4'h4,
            s_SendScl0          = 4'h5,
            s_SendScl1          = 4'h6,
            s_GetScl0           = 4'h7,
            s_GetScl1           = 4'h8,
            s_StopSda0          = 4'h9,
            s_StopScl1          = 4'ha,
            s_StopSda1          = 4'hb;
            
reg [3:0] State_qb4, NextState_ab4;
            
always @(posedge Clk_ik) State_qb4 <= #1 Rst_irq ?  s_Idle :  NextState_ab4;

always @* begin
    NextState_ab4 =  State_qb4;
    case(State_qb4)
    s_Idle             : if (SendStartBit_ip) NextState_ab4 = s_StartSda1;
                         else if (SendByte_ip) NextState_ab4 = s_SendScl0;
                         else if (GetByte_ip) NextState_ab4 = s_GetScl0;
                         else if (SendStopBit_ip) NextState_ab4 = s_StopSda0;
    
    s_StartSda1        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StartScl1;
    s_StartScl1        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StartSda0;
    s_StartSda0        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StartScl0;
    s_StartScl0        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_Idle;

    s_SendScl0         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = (BitCounter_c4==4'd9) ? s_Idle : s_SendScl1;
    s_SendScl1         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_SendScl0;

    s_GetScl0          : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = (BitCounter_c4==4'd9) ? s_Idle : s_GetScl1;
    s_GetScl1          : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_GetScl0;

    s_StopSda0         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StopScl1;
    s_StopScl1         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StopSda1;
    s_StopSda1         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_Idle;

    default: NextState_ab4 = s_Idle;
    endcase 
end         

   
always @(posedge Clk_ik)
    if  (Rst_irq) begin
        SclOe_e             <= #1 1'b0;
        SdaOe_e             <= #1 1'b0;        
        BitCounter_c4       <= #1 'h0;
        CycleCounter_c10    <= #1 'h0;
        I2CAck              <= #1 AckToSend_i;  
        ShReg_b8            <= #1 Byte_ib8;      
    end else case(State_qb4)
        s_Idle : begin
            BitCounter_c4       <= #1 'h0;
            CycleCounter_c10    <= #1 'h0;
            if (NextState_ab4==s_SendScl0)  ShReg_b8    <= #1 Byte_ib8;   
            if (NextState_ab4==s_GetScl0)   I2CAck      <= #1 AckToSend_i;      
        end     
                
        s_StartSda1 : begin
            SdaOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;
        end             
        s_StartScl1 : begin
            SclOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end          
        s_StartSda0 : begin
            SdaOe_e <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end             
        s_StartScl0 : begin
            SclOe_e <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end   
                  
        s_SendScl0 : begin
            SclOe_e             <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0; 
            if  (CycleCounter_c10[8:0]==g_CycleLenght[9:1])  SdaOe_e <= #1 BitCounter_c4[3] ? 1'b0 : !ShReg_b8[7];                
        end
        s_SendScl1 : begin
            SclOe_e             <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) begin
                BitCounter_c4       <= #1 BitCounter_c4 + 1'b1;
                CycleCounter_c10    <= #1 'h0;
                ShReg_b8[7:1]       <= #1 ShReg_b8[6:0];
                I2CAck              <= #1 Sda_ioz; 
            end else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;                          
        end              
        
        s_GetScl0 : begin
            SclOe_e             <= #1 1'b1;
            SdaOe_e             <= #1 (BitCounter_c4==4'd8) ? !I2CAck : 1'b0;        
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;         
        end                              
        s_GetScl1 : begin
            SclOe_e             <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) begin
                BitCounter_c4       <= #1 BitCounter_c4 + 1'b1;
                CycleCounter_c10    <= #1 'h0;
                ShReg_b8            <= #1 (BitCounter_c4==4'd8) ? ShReg_b8 : {ShReg_b8[6:0], Sda_ioz};         
            end else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;                
        end  
                     
        s_StopSda0 : begin
            SdaOe_e <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;  
        end              
        s_StopScl1 : begin
            SclOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end              
        s_StopSda1 : begin
            SdaOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end  
           
        default: begin
            SclOe_e             <= #1 1'b0;
            SdaOe_e             <= #1 1'b0;        
            BitCounter_c4       <= #1 'h0;
            CycleCounter_c10    <= #1 'h0;
            I2CAck              <= #1 AckToSend_i;  
            ShReg_b8            <= #1 Byte_ib8; 
        end
        endcase
 
 
assign Byte_ob8 = ShReg_b8;
assign AckReceived_o = I2CAck;
assign Done_o = State_qb4==s_Idle;
    
assign Scl_ioz = SclOe_e ? 1'b0 : 1'bz;
assign Sda_ioz = SdaOe_e ? 1'b0 : 1'bz;    
                  
                  
endmodule
