//----------------------------------------------------------------------
// Title      : OpenCores I2C Wrapper
// Project    : Multi-Band Instability Monitor
//----------------------------------------------------------------------
// File       : OcI2CWrapper.sv
// Author     : T. Levens
// Company    : CERN BE-BI-QP
// Platform   : FPGA-generic
// Standard   : SystemVerilog
//----------------------------------------------------------------------
// Description:
//
// Wrapper around OpenCores I2C master to make non-Wishbone interfacing
// simpler.
//
// After reset the master is configured by setting up the registers
// PRERlo, PRERhi, CTR and IFR.
//
// After that, a I2C transaction can be started by the `Start` pin.
// For each transaction the following sequence is followed:
//
//   1. Write portion:
//      a) The `SlaveAddress` is sent (start) with the R/W bit set to 0.
//      b) `WrCnt` bytes are written to the device from the `WrData`
//         bus, LSB first. For example, if writing 3 bytes (AA, BB, CC)
//         then WrData=XX_CC_BB_AA and the MSB is don't care.
//
//   2. Read portion:
//      a) The `SlaveAddress` is (re-)sent (repeated-)start with the R/W
//         bit set to 1.
//      b) `RdCnt` bytes are read from the device to the `RdData` bus,
//         LSB first. For example, if reading  2 bytes (AA, BB) then
//         RdData=XX_XX_BB_AA and the two MSB are don't care.
//
// If either `WrCnt` or `RdCnt` is zero then that portion of the
// transaction is skipped. During the transaction `Busy` is held high.
// Error conditions are signaled by the outputs:
//   `Nack`: the slave did not acknowlege
//   `Al`:   the arbitration of the I2C bus was lost
//
//----------------------------------------------------------------------

`timescale 1ns/100ps

module OcI2CWrapper #(
    parameter         g_ClkFrequency,
    parameter         g_I2CFrequency = 100000,
    parameter         g_IFR = 0
) (
    input             Clk_ik,
    input             Rst_ir,

    input       [7:1] SlaveAddr_ib7,
    input             Start_i,
    input      [31:0] WrData_ib32,
    output reg [31:0] RdData_ob32,
    input      [ 3:0] WrCnt_ib4,
    input      [ 3:0] RdCnt_ib4,
    output reg        Busy_o,
    output reg        Done_o,
    output reg        Nack_o,
    output reg        Al_o,

    output reg  [2:0] WbAdr_ob3,
    output reg  [7:0] WbDatMosi_ob8,
    input       [7:0] WbDatMiso_ib8,
    output            WbCyc_o,
    output reg        WbStb_o,
    output reg        WbWe_o,
    input             WbAck_i
);

// Calculate PRER
localparam c_PRER = ((g_ClkFrequency / (5 * g_I2CFrequency)) - 1);

// Local registers
wire [15:0] PRER_b16 = (16)'(c_PRER);
wire  [7:0] IFR_b8   = (8)'(g_IFR);

// CR register bits
localparam  C_CR_STA  = 8'h80,
            C_CR_STO  = 8'h40,
            C_CR_RD   = 8'h20,
            C_CR_WR   = 8'h10,
            C_CR_ACK  = 8'h08,
            C_CR_IACK = 8'h01;

// State encoding
typedef enum {
    // General states
    S_INIT,
    S_PRERLO,
    S_PRERHI,
    S_CTR,
    S_IFR,
    S_IDLE,
    S_WAIT_BUSY,
    S_NACK,
    S_AL,
    S_DONE,

    // Write specific states
    S_WR_START,
    S_WR_DATA_SHIFT,
    S_WR_TXR,
    S_WR_CR,
    S_WR_WAIT_TIP_H,
    S_WR_WAIT_TIP_L,
    S_WR_DONE,
    S_WR_COUNT_INC,

    // Read specific states
    S_RD_START,
    S_RD_TXR,
    S_RD_CR,
    S_RD_WAIT_TIP_H,
    S_RD_WAIT_TIP_L,
    S_RD_DONE,
    S_RD_RXR,
    S_RD_LATCH,
    S_RD_COUNT_INC,
    S_RD_DATA_SHIFT
} t_State;

// State register
t_State SeqCurrentState,
        SeqNextState;

always @(posedge Clk_ik) begin
    if (Rst_ir)
        SeqCurrentState <= S_INIT;
    else
        SeqCurrentState <= SeqNextState;
end

// Read/write counter
reg [3:0] SeqCnt_b4;

// Shift register for write data
reg [31:0] SeqWrData_b32;

// Sequencer status signals
wire SeqIsAddr   = (SeqCnt_b4 == 4'd0);
wire SeqIsLastRd = (SeqCnt_b4 == RdCnt_ib4);
wire SeqIsLastWr = (SeqCnt_b4 == WrCnt_ib4);
wire SeqIsMaxRd  = (SeqCnt_b4 == 4'd4);

wire SeqRdReq    = (RdCnt_ib4 != 4'd0);
wire SeqWrReq    = (WrCnt_ib4 != 4'd0);

// Next state logic
always @(*) begin
    SeqNextState <= SeqCurrentState;
    case (SeqCurrentState)
        S_INIT: begin
            if (!Rst_ir)
                SeqNextState <= S_PRERLO;
        end
        S_PRERLO: begin
            if (WbAck_i)
                SeqNextState <= S_PRERHI;
        end
        S_PRERHI: begin
            if (WbAck_i)
                SeqNextState <= S_CTR;
        end
        S_CTR: begin
            if (WbAck_i)
                SeqNextState <= S_IFR;
        end
        S_IFR: begin
            if (WbAck_i)
                SeqNextState <= S_IDLE;
        end
        S_IDLE: begin
            if (Start_i)
                SeqNextState <= S_WAIT_BUSY;
        end
        S_WAIT_BUSY: begin
            if (WbAck_i && !WbDatMiso_ib8[6]) begin
                if (SeqWrReq)
                    SeqNextState <= S_WR_START;
                else if (SeqRdReq)
                    SeqNextState <= S_RD_START;
                else
                    SeqNextState <= S_IDLE;
            end
        end
        S_WR_START: begin
            SeqNextState <= S_WR_TXR;
        end
        S_RD_START: begin
            SeqNextState <= S_RD_TXR;
        end
        S_WR_TXR: begin
            if (WbAck_i)
                SeqNextState <= (SeqIsAddr) ? S_WR_CR : S_WR_DATA_SHIFT;
        end
        S_RD_TXR: begin
            if (WbAck_i)
                SeqNextState <= S_RD_CR;
        end
        S_WR_CR: begin
            if (WbAck_i)
                SeqNextState <= S_WR_WAIT_TIP_H;
        end
        S_RD_CR: begin
            if (WbAck_i)
                SeqNextState <= S_RD_WAIT_TIP_H;
        end
        S_WR_WAIT_TIP_H: begin
            if (WbAck_i && WbDatMiso_ib8[1])
                SeqNextState <= S_WR_WAIT_TIP_L;
        end
        S_RD_WAIT_TIP_H: begin
            if (WbAck_i && WbDatMiso_ib8[1])
                SeqNextState <= S_RD_WAIT_TIP_L;
        end
        S_WR_WAIT_TIP_L: begin
            if (WbAck_i && !WbDatMiso_ib8[1]) begin
                if (WbDatMiso_ib8[5])
                    SeqNextState <= S_AL;
                else if (WbDatMiso_ib8[7])
                    SeqNextState <= S_NACK;
                else
                    SeqNextState <= S_WR_DONE;
            end
        end
        S_RD_WAIT_TIP_L: begin
            if (WbAck_i && !WbDatMiso_ib8[1]) begin
                if (WbDatMiso_ib8[5])
                    SeqNextState <= S_AL;
                else if (!SeqIsAddr)
                    SeqNextState <= S_RD_RXR;
                else if (WbDatMiso_ib8[7])
                    SeqNextState <= S_NACK;
                else
                    SeqNextState <= S_RD_DONE;
            end
        end
        S_RD_RXR: begin
            if (WbAck_i)
                SeqNextState <= S_RD_LATCH;
        end
        S_RD_LATCH: begin
            SeqNextState <= S_RD_DONE;
        end
        S_WR_DONE: begin
            if (SeqIsLastWr)
                SeqNextState <= (SeqRdReq) ? S_RD_START : S_DONE;
            else
                SeqNextState <= S_WR_COUNT_INC;
        end
        S_RD_DONE: begin
            if (SeqIsLastRd)
                SeqNextState <= (SeqIsMaxRd) ? S_DONE : S_RD_DATA_SHIFT;
            else
                SeqNextState <= S_RD_COUNT_INC;
        end
        S_WR_DATA_SHIFT: begin
            SeqNextState <= S_WR_CR;
        end
        S_RD_DATA_SHIFT: begin
            if (SeqIsMaxRd)
                SeqNextState <= S_DONE;
        end
        S_WR_COUNT_INC: begin
            SeqNextState <= S_WR_TXR;
        end
        S_RD_COUNT_INC: begin
            SeqNextState <= S_RD_TXR;
        end
        S_NACK: begin
            SeqNextState <= S_IDLE;
        end
        S_AL: begin
            SeqNextState <= S_IDLE;
        end
        S_DONE: begin
            SeqNextState <= S_IDLE;
        end
        default: begin
            SeqNextState <= S_IDLE;
        end
    endcase
end

// Output assignments
always @(posedge Clk_ik) begin
    if (Rst_ir) begin
        WbAdr_ob3       <= 3'h0;
        WbDatMosi_ob8   <= 8'h00;
        WbStb_o         <= 1'b0;
        WbWe_o          <= 1'b0;
        SeqCnt_b4       <= 4'd0;
        SeqWrData_b32   <= 32'h0;
        RdData_ob32     <= 32'h0;
        Busy_o          <= 1'b1;
        Done_o          <= 1'b0;
        Nack_o          <= 1'b0;
        Al_o            <= 1'b0;
    end else begin
        WbAdr_ob3       <= 3'h0;
        WbDatMosi_ob8   <= 8'h0;
        WbStb_o         <= 1'b0;
        WbWe_o          <= 1'b0;
        Busy_o          <= 1'b1;
        Done_o          <= 1'b0;
        case (SeqNextState)
            S_INIT: begin
                // Do nothing
            end
            S_PRERLO: begin
                WbAdr_ob3       <= 3'h0;
                WbDatMosi_ob8   <= PRER_b16[7:0];
                WbStb_o         <= 1'b1;
                WbWe_o          <= 1'b1;
            end
            S_PRERHI: begin
                WbAdr_ob3       <= 3'h1;
                WbDatMosi_ob8   <= PRER_b16[15:8];
                WbStb_o         <= 1'b1;
                WbWe_o          <= 1'b1;
            end
            S_CTR: begin
                WbAdr_ob3       <= 3'h2;
                WbDatMosi_ob8   <= 8'h80;
                WbStb_o         <= 1'b1;
                WbWe_o          <= 1'b1;
            end
            S_IFR: begin
                WbAdr_ob3       <= 3'h5;
                WbDatMosi_ob8   <= IFR_b8;
                WbStb_o         <= 1'b1;
                WbWe_o          <= 1'b1;
            end
            S_IDLE: begin
                Busy_o          <= 1'b0;
            end
            S_WAIT_BUSY: begin
                WbAdr_ob3       <= 3'h4;
                WbStb_o         <= 1'b1;
            end
            S_WR_START: begin
                SeqCnt_b4       <= 4'h0;
                Nack_o          <= 1'b0;
                Al_o            <= 1'b0;
                SeqWrData_b32   <= WrData_ib32;
            end
            S_RD_START: begin
                SeqCnt_b4   <= 4'h0;
                Nack_o      <= 1'b0;
                Al_o        <= 1'b0;
                RdData_ob32 <= 32'h0;
            end
            S_WR_TXR: begin
                WbAdr_ob3 <= 3'h3;
                WbStb_o   <= 1'b1;
                WbWe_o    <= 1'b1;
                if (SeqIsAddr) begin
                    WbDatMosi_ob8 <= {SlaveAddr_ib7, 1'b0};
                end else begin
                    WbDatMosi_ob8 <= SeqWrData_b32[7:0];
                end
            end
            S_RD_TXR: begin
                WbAdr_ob3 <= 3'h3;
                WbStb_o   <= 1'b1;
                WbWe_o    <= 1'b1;
                if (SeqIsAddr)
                    WbDatMosi_ob8 <= {SlaveAddr_ib7, 1'b1};
                else
                    WbDatMosi_ob8 <= 8'h00;
            end
            S_WR_CR: begin
                WbAdr_ob3 <= 3'h4;
                WbStb_o   <= 1'b1;
                WbWe_o    <= 1'b1;
                if (SeqIsAddr)
                    WbDatMosi_ob8 <= C_CR_STA | C_CR_WR;
                else if (SeqIsLastWr && !SeqRdReq)
                    WbDatMosi_ob8 <= C_CR_STO | C_CR_WR;
                else
                    WbDatMosi_ob8 <= C_CR_WR;
            end
            S_RD_CR: begin
                WbAdr_ob3 <= 3'h4;
                WbStb_o   <= 1'b1;
                WbWe_o    <= 1'b1;
                if (SeqIsAddr)
                    WbDatMosi_ob8 <= C_CR_STA | C_CR_WR;
                else if (SeqIsLastRd)
                    WbDatMosi_ob8 <= C_CR_STO | C_CR_ACK | C_CR_RD;
                else
                    WbDatMosi_ob8 <= C_CR_RD;
            end
            S_WR_WAIT_TIP_H, S_RD_WAIT_TIP_H: begin
                WbAdr_ob3 <= 3'h4;
                WbStb_o   <= 1'b1;
            end
            S_WR_WAIT_TIP_L, S_RD_WAIT_TIP_L: begin
                WbAdr_ob3 <= 3'h4;
                WbStb_o   <= 1'b1;
            end
            S_RD_RXR: begin
                WbAdr_ob3 <= 3'h3;
                WbStb_o   <= 1'b1;
            end
            S_RD_LATCH: begin
                RdData_ob32 <= {WbDatMiso_ib8, RdData_ob32[31:8]};
            end
            S_WR_DONE, S_RD_DONE: begin
                // Do nothing
            end
            S_WR_DATA_SHIFT: begin
                SeqWrData_b32   <= {8'h0, SeqWrData_b32[31:8]};
            end
            S_RD_DATA_SHIFT: begin
                RdData_ob32 <= {8'h0, RdData_ob32[31:8]};
                SeqCnt_b4 <= SeqCnt_b4 + 4'h1;
            end
            S_WR_COUNT_INC, S_RD_COUNT_INC: begin
                SeqCnt_b4 <= SeqCnt_b4 + 4'h1;
            end
            S_NACK: begin
                Done_o <= 1'b1;
                Nack_o <= 1'b1;
            end
            S_AL: begin
                Done_o <= 1'b1;
                Al_o   <= 1'b1;
            end
            S_DONE: begin
                Done_o <= 1'b1;
            end
            default: begin
                // Do nothing
            end
        endcase
    end
end

assign WbCyc_o = WbStb_o;

endmodule
