library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dmdt_phase_meas_tb is
end dmdt_phase_meas_tb;

architecture sim of dmdt_phase_meas_tb is

signal tb_clk_a	       : std_logic:='1';
signal tb_clk_b	       : std_logic:='1';
signal tb_clk_sys      : std_logic:='1';
signal tb_clk_dmdt     : std_logic:='1';
signal tb_rst_n        : std_logic:='0';
signal tb_phase_meas   : std_logic_vector(31 downto 0):= (others => '0');
signal tb_clk_s_ticks  : std_logic_vector(31 downto 0):= (others => '0');
signal tb_clk_a_ticks  : std_logic_vector(31 downto 0):= (others => '0');
signal tb_dv           : std_logic:= '0';


--=======================================--
procedure clk_gen_t
--=======================================--
( 
    signal   clk : out std_logic; 
    constant PERIOD : time;
    constant PHASE  : time
) is

constant HIGH_TIME : time := PERIOD / 2;          -- High time
constant LOW_TIME  : time := PERIOD - HIGH_TIME;  -- Low time; always >= HIGH_TIME
begin
clk <= '0';
wait for PHASE;
loop
clk <= '0';
wait for LOW_TIME;
clk <= '1';
wait for HIGH_TIME;
end loop;
end procedure;
--=======================================--


--=======================================--
procedure clk_gen_f
--=======================================--
( 
    signal   clk : out std_logic; 
    constant FREQ : real; 
    constant OFFSET: real
) is

constant PERIOD    : time := 1 sec / FREQ;        -- Full period
constant PHASE     : time := 1 ns * OFFSET;        -- Full period
constant HIGH_TIME : time := PERIOD / 2;          -- High time
constant LOW_TIME  : time := PERIOD - HIGH_TIME;  -- Low time; always >= HIGH_TIME
begin
clk <= '0';
wait for PHASE;
loop
clk <= '0';
wait for LOW_TIME;
clk <= '1';
wait for HIGH_TIME;
end loop;
end procedure;
--=======================================--

begin

tb_rst_n    <= '1' after 30 ns;

clk_gen_f(tb_clk_sys,  31.250000E6, 0.0);  
clk_gen_f(tb_clk_dmdt, 40.076994E6, 0.0); 
clk_gen_f(tb_clk_a,    40.080000E6, 0.0); 
clk_gen_f(tb_clk_b,    40.080000E6, 1.0); 
--clk_gen_t(tb_clk_sys,  32000 ps, 0.0 ns);
--clk_gen_t(tb_clk_dmdt, 25001.875 ps, 0.0 ns);
--clk_gen_t(tb_clk_a,    25000 ps, 0.0 ns); 
--clk_gen_t(tb_clk_b,    25000 ps, 26.0 ns);


--=======================================--
uut: entity work.dmtd_phase_meas
--=======================================--
generic map ( g_deglitcher_threshold => 2000, g_counter_bits => 14 )
port map 
(
    rst_n_i       => tb_rst_n,
    clk_sys_i     => tb_clk_sys, 
    clk_a_i       => tb_clk_a,
    clk_b_i       => tb_clk_b,
    clk_dmtd_i    => tb_clk_dmdt,
    en_i           => '1',
    navg_i         => x"010",
    phase_meas_o   => tb_phase_meas,
    clk_s_ticks_o  => tb_clk_s_ticks,
    clk_a_ticks_o  => tb_clk_a_ticks,
    dv_o           => tb_dv
    );
--=======================================--


        
end architecture;