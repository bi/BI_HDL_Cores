set CORES_SYN ../../..
set CORES_SIM ../../../../cores_for_simulation
set TB DpramGenericToWb_tb

if {[file exists work]} {vdel -all -lib work} 
vlib work

vlog -reportprogress 300 -work work $CORES_SYN/ip_open_cores/generic_dpram_mod.v
vlog -reportprogress 300 -work work $CORES_SYN/DpramGenericToWb.v
vlog -reportprogress 300 -work work $CORES_SIM/ClockGenerator.sv
vlog -reportprogress 300 -work work $CORES_SIM/wb_master_sim/WbMasterSim.sv
vlog -reportprogress 300 -work work ../$TB.sv

vopt work.$TB +acc -o _design_optimized

vsim -gui _design_optimized

add wave -noupdate sim:/$TB/*
add wave -noupdate -expand -group DUT sim:/$TB/i_Dut/*

configure wave -signalnamewidth 1

run -all

wave zoom full