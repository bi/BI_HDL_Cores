module DpramGenericToWb_tb;

    timeunit 1ns / 1ps;

    localparam longint unsigned c_FrequencyWrite = 123e6;
    localparam longint unsigned c_FrequencyRead = 100e6;
    localparam int c_DataWidth = 32;
    localparam int c_AddressWidth = 10;
    localparam int c_TestSize = 10;
    localparam int c_AckDelay = 10;

    logic [c_DataWidth-1:0] TestData [0:c_TestSize-1];
    logic [c_DataWidth-1:0] ReadData;
    // DUT: Generic interface (Write)
    logic WrClk_ik;
    logic WrRst_ir;
    logic WrEn_i;
    logic [c_AddressWidth-1:0] WrAddr_ib;
    logic [c_DataWidth-1:0] WrData_ib;
    // DUT: Wishbone interface (Read)
    logic Rst_ir;
    logic Clk_ik;
    logic Cyc_i;
    logic Stb_i;
    logic [c_AddressWidth-1:0] Adr_ib;
    logic [c_DataWidth-1:0] Dat_oqb;
    logic Ack_oq;

    ClockGenerator #(
        .g_Frequency(c_FrequencyWrite)
    ) i_ClkGeneratorWrite (
        .Clk_ok(WrClk_ik),
        .Enable_i(1'b1)
    );

    ClockGenerator #(
        .g_Frequency(c_FrequencyRead)
    ) i_ClkGeneratorRead (
        .Clk_ok(Clk_ik),
        .Enable_i(1'b1)
    );

    WbMasterSim #(
        .g_VerboseDefault(1)
    ) i_WbMasterSim (
        .Rst_orq(Rst_ir),
        .Clk_ik(Clk_ik),
        .Adr_obq32(Adr_ib),
        .Dat_obq32(),
        .Dat_ib32(Dat_oqb),
        .We_oq(),
        .Cyc_oq(Cyc_i),
        .Stb_oq(Stb_i),
        .Ack_i(Ack_oq)
    );

    DpramGenericToWb #(
        .g_AddrWidth(c_AddressWidth),
        .g_DataWidth(c_DataWidth)
    ) i_Dut (
        .*
    );

    task WrReset();
        WrRst_ir <= 1;
        repeat(5) @(posedge WrClk_ik);
        WrRst_ir <= 0;
    endtask

    task WrWrite(input logic [c_AddressWidth-1:0] Address_ib, input logic [c_DataWidth-1:0] Data_ib);
        WrEn_i <= 0;
        WrAddr_ib <= Address_ib;
        WrData_ib <= Data_ib;
        @(posedge WrClk_ik);
        WrEn_i <= 1;
        @(posedge WrClk_ik);
        WrEn_i <= 0;
    endtask

    initial begin
        // resets
        i_WbMasterSim.Reset();
        WrReset();
        repeat(10) @(posedge Clk_ik);
        // prepare test data and write them into memory
        for (int i = 0; i < c_TestSize; i = i + 1) begin
            TestData[i] = $urandom;
            WrWrite(i, TestData[i]);
        end
        // verify test data from memory
        for (int i = 0; i < c_TestSize; i = i + 1) begin
            i_WbMasterSim.WbRead(i, ReadData);
            assert (ReadData == TestData[i]) else $error("WB output does not match! [0x%h] = 0x%h != 0x%h", i, ReadData, TestData[i]);
        end
        $stop();
    end

endmodule
