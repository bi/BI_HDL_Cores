/*
 * Extract "extreme" value from stream of words consisting of one or more
 * samples
 * Extreme can be set to minimum or maximum
 * Comparison can be done signed or un-signed
 * Number of samples per word and sample width parameterisable
 * Masking is implemented per sample - 1 = sample masked, 0 = sample included
 * Re-start re-initiates a search process; it also generates valid flag after
 * g_PipeDepth cycles (can be 0, which means direct path between restart and
 * valid)
 * Pipeline is done primitively - multiple registers at combinational logic
 * output - tool's register pipelining should be enabled
*/

module GetExtreme #(
    g_SampleWidth = 2,      // sample data width
    g_WordWidth = 2,        // how many parallel samples
    g_DataSigned = 1'b0,    // is data signed? 0 = unsigned, 1 = signed
    g_Min = 1'b0,           // do we want to extract minimum? 0 = maximum, 1 = minimum
    g_PipeDepth = 0,        // how many registers at output of combinational logic

    c_cmpStages = $clog2(g_WordWidth)
)(
    input clk_ik,
    input restart_i,
    input [g_WordWidth -1:0] mask_ib,                       // input sample mask
    input [g_WordWidth -1:0][g_SampleWidth -1:0] data_ib,   // data input word
    output vld_o,
    output [g_SampleWidth -1:0] val_obq                     // "extreme" sample output
);

wire [c_cmpStages:0][g_WordWidth -1:0][g_SampleWidth -1:0] stageData_b;
wire [g_PipeDepth:0][g_SampleWidth -1:0] outPipeData_b;
wire [g_PipeDepth:0] outPipeRestart_b;

assign outPipeData_b[0] = stageData_b[c_cmpStages][0];
assign outPipeRestart_b[0] = restart_i;

genvar gv_sampleNum, gv_stageNum, gv_cmpNum;

generate
    for (gv_sampleNum = 0; gv_sampleNum < g_WordWidth; gv_sampleNum++) begin:InMaskBlk
        assign stageData_b[0][gv_sampleNum] = mask_ib[gv_sampleNum] ? cf_default() : data_ib[gv_sampleNum];
    end

    for (gv_stageNum = 1; gv_stageNum <= c_cmpStages; gv_stageNum++) begin:StageBlk
        localparam c_numInputs = cf_numStageInputs(gv_stageNum);
        localparam c_numOutputs = (c_numInputs +1) /2;

        assign stageData_b[gv_stageNum][g_WordWidth -1:c_numOutputs] = '{default: cf_default()};

        for (gv_cmpNum = 0; gv_cmpNum < (c_numInputs /2); gv_cmpNum++) begin:CmprBlk
            assign stageData_b[gv_stageNum][gv_cmpNum] = f_cmpAndSel(stageData_b[gv_stageNum -1][gv_cmpNum *2+:2]);
        end

        // handle odd-one out
        if (c_numInputs %2) begin:OddFeedThroughBlk
            assign stageData_b[gv_stageNum][c_numOutputs -1] = stageData_b[gv_stageNum -1][c_numInputs -1];
        end
    end

    for (gv_stageNum = 1; gv_stageNum <= g_PipeDepth; gv_stageNum++) begin:OutPipeBlk
        reg pipeRestart_q;
        reg [g_SampleWidth -1:0] pipeData_qb;
        always_ff @(posedge clk_ik) begin
            pipeRestart_q <= outPipeRestart_b[gv_stageNum -1];
            pipeData_qb <= outPipeData_b[gv_stageNum -1];
        end
        assign outPipeRestart_b[gv_stageNum] = pipeRestart_q;
        assign outPipeData_b[gv_stageNum] = pipeData_qb;
    end
endgenerate

assign vld_o = outPipeRestart_b[g_PipeDepth];

always_ff @(posedge clk_ik) begin
    // restart is implemented as severed connection with past values
    val_obq <= f_cmpAndSel({vld_o ? cf_default() : val_obq, outPipeData_b[g_PipeDepth]});
end

function [g_SampleWidth -1:0] f_cmpAndSel (input [1:0][g_SampleWidth -1:0] cmpData_ib);
    logic cmpMax, cmp;

    if (g_DataSigned) begin
        cmpMax = $signed(cmpData_ib[1]) > $signed(cmpData_ib[0]);
    end else begin
        cmpMax = cmpData_ib[1] > cmpData_ib[0];
    end

    cmp = cmpMax ^ g_Min; // default is search for maximum, if minimum required, flip comparison

    return (cmpData_ib[cmp]);
endfunction

function int cf_numStageInputs(input int stageNum);
    int iter, numInputs;

    numInputs = g_WordWidth;
    for (iter = 1; iter < stageNum; iter++) begin
        numInputs = (numInputs +1) /2;
    end

    return numInputs;
endfunction

function logic [g_SampleWidth -1:0] cf_default();
    logic [g_SampleWidth -1:0] val;

    // for max unsigned search, default = min = 0
    val = 0;
    // for max signed search, default = min = largest negative; add 1 to MS
    // bit
    if (g_DataSigned) val[g_SampleWidth -1] = 1'b1;

    // for minimum search, negate maximum search default
    if (g_Min) val = ~val;

    return val;
endfunction

endmodule

