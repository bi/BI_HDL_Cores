# Module extracting extreme value from stream of samples

Parameterizable:

- minimum/maximum
- signed/unsigned
- data width bits
- word width samples
- timing pipeline depth - stages of registers added behind single word min/max search

Individual samples can be masked from comparison for more control.

Configurations in use:

- No masking - all samples always un-masked
```
    .g_SampleWidth (14),
    .g_WordWidth (8),
    .g_DataSigned (1'b0),
    .g_Min (1'b0),
    .g_PipeDepth (0)
```
- No masking - all samples always un-masked
```
    .g_SampleWidth (14),
    .g_WordWidth (8),
    .g_DataSigned (1'b0),
    .g_Min (1'b1),
    .g_PipeDepth (0)
```

Please add your configuration above, if you use the core - to help other people see what was tested before.

There is not testbench at the moment.

