`timescale 1ns/100ps
module PeriodCounter 
#(
	parameter g_PeriodRangeTopLog2 = 8, 
	parameter g_OutputBits = 16,
	parameter g_NBits = 8
)(
	input Input_a,
	input ReferenceClock_ik,
	input Rst_ir,
	output [g_OutputBits - 1: 0] Output_ob,
	output NewDataOut_o
);
	
wire Clk_k = ReferenceClock_ik;

reg [2:0] Synch_xb3 = 0;

always @(posedge Clk_k) Synch_xb3 <= #1 {Synch_xb3[1:0], Input_a}; 

wire PosEdge_a = Synch_xb3[2:1]==2'b01;

reg [g_PeriodRangeTopLog2 -1 :0] PeriodCounter_c = 0, Data_b = 0;
reg NewData = 0;

always @(posedge Clk_k) begin
	if (PosEdge_a) begin
		Data_b <= #1 PeriodCounter_c;
		NewData <= #1 1'b1;
		PeriodCounter_c <= #1 3'b1;
	end else begin
		NewData <= #1 1'b0;
		PeriodCounter_c <= #1 PeriodCounter_c + 1'b1;		
	end
end
	
wire Dummy;	
	
Iir1stOrderLp  #(.g_InputBits(g_PeriodRangeTopLog2+1), .g_OutputBits(g_OutputBits), .g_NBits(g_NBits))
	i_Iir(
		.Clk_ik(Clk_k),
		.Rst_ir(Rst_ir),
		.NewDataIn_i(NewData),
		.Input_ib({1'b0, Data_b}),
		.NewDataOut_o(NewDataOut_o),
		.Output_ob({Dummy, Output_ob}));

endmodule