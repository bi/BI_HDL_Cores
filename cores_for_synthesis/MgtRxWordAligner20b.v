`timescale 1ns / 1ps

//Manoel Barros Marin, BE-BI-QP (CERN) - 14/12/16

module MgtRxWordAligner20b #(

//***********   
//Parameters:
//***********

    parameter         g_AlignPattern_b20 = 20'hCAAAA,
    parameter         g_Words2Align_b8   =  8'h10,
    parameter         g_Words2Bitslip_b8 =  8'h10) (

//*****   
//I/Os:
//*****   
   
    input             Clk_ik,
    input             Reset_ir,
    input             Enable_i,
    
    input      [19:0] RxData_ib20,
    
    output reg        RxAligned_o,
    output     [19:0] RxDataAligned_ob20
   
);

//*************   
//Declarations:
//*************   
    
    localparam  s_Idle     = 3'b001,
                s_Aligning = 3'b010,
                s_Aligned  = 3'b100;
               
    reg  [ 2:0] State_qb3, NextState_qb3;
    reg  [ 1:0] Enable_xb2;
    reg         Enable_q;
    reg  [19:0] PreviousData_qb20, ShiftedData_qb20;
    reg  [ 4:0] SrlAddress_qb5;
    reg  [ 7:0] Words2AlignCntr_cb8;
    reg  [ 7:0] Words2BitslipCntr_cb8;            

//***********      
//User Logic:
//***********   
   
    //Secuential logic:
    always @(posedge Clk_ik)
        if (Reset_ir) begin
            Enable_xb2               <= #1  2'h0;           
            Enable_q                 <= #1  1'b0;           
            //---                    
            PreviousData_qb20        <= #1 20'h0;
            ShiftedData_qb20         <= #1 20'h0;
        end else begin               
            Enable_xb2               <= #1 {Enable_xb2[0], Enable_i};
            Enable_q                 <= #1  Enable_xb2[1];      
            //---                    
            PreviousData_qb20        <= #1 RxData_ib20;            
            case(SrlAddress_qb5)     
                0: begin             
                    ShiftedData_qb20 <= #1 RxData_ib20;
                end                  
                1: begin             
                    ShiftedData_qb20 <= #1 {RxData_ib20[18:0],PreviousData_qb20[19:19]};
                end                                              
                2: begin                                         
                    ShiftedData_qb20 <= #1 {RxData_ib20[17:0],PreviousData_qb20[19:18]};
                end                                                                  
                3: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[16:0],PreviousData_qb20[19:17]};
                end                                                                  
                4: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[15:0],PreviousData_qb20[19:16]};
                end                                                                  
                5: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[14:0],PreviousData_qb20[19:15]};
                end                                                                  
                6: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[13:0],PreviousData_qb20[19:14]};
                end                                                                  
                7: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[12:0],PreviousData_qb20[19:13]};
                end                                                                  
                8: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[11:0],PreviousData_qb20[19:12]};
                end                                                                  
                9: begin                                                             
                    ShiftedData_qb20 <= #1 {RxData_ib20[10:0],PreviousData_qb20[19:11]};
                end                                                                  
                10: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 9:0],PreviousData_qb20[19:10]};
                end                                                                  
                11: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 8:0],PreviousData_qb20[19: 9]};
                end                                                                  
                12: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 7:0],PreviousData_qb20[19: 8]};
                end                                                                  
                13: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 6:0],PreviousData_qb20[19: 7]};
                end                                                                  
                14: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 5:0],PreviousData_qb20[19: 6]};
                end                                                                  
                15: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 4:0],PreviousData_qb20[19: 5]};
                end                                                                  
                16: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 3:0],PreviousData_qb20[19: 4]};
                end                                                                  
                17: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 2:0],PreviousData_qb20[19: 3]};
                end                                                                  
                18: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 1:0],PreviousData_qb20[19: 2]};
                end                                                                  
                19: begin                                                            
                    ShiftedData_qb20 <= #1 {RxData_ib20[ 0:0],PreviousData_qb20[19: 1]};
                end                                                                  
                default: begin                                                       
                    ShiftedData_qb20 <= #1 RxData_ib20;                         
                end                              
            endcase                              
        end                                      
                                                 
    assign RxDataAligned_ob20 = ShiftedData_qb20;
   
    //FSM:
    always @( posedge Clk_ik) State_qb3 <= #1 Reset_ir ? s_Idle : NextState_qb3;
    always @* begin
        NextState_qb3                              = State_qb3;
        case(State_qb3)
            s_Idle: begin
                if (Enable_q) begin
                    NextState_qb3                  = s_Aligning;
                end
            end         
            s_Aligning: begin
                if (Words2AlignCntr_cb8 == g_Words2Align_b8-1) begin
                    NextState_qb3                  = s_Aligned;
                end                         
            end                            
            s_Aligned: begin 
                if (Enable_q) begin    
                    NextState_qb3                  = s_Aligned;
                end else begin
                    NextState_qb3                  = s_Idle;
                end            
            end                            
            default: begin
                NextState_qb3                      = s_Idle;
            end                            
        endcase                           
    end  
    always @(posedge Clk_ik)             
        if (Reset_ir) begin               
            Words2AlignCntr_cb8                   <= #1 8'b0;
            Words2BitslipCntr_cb8                 <= #1 8'b0;
            SrlAddress_qb5                        <= #1 5'b0;
            RxAligned_o                           <= #1 1'b0;
        end else begin                    
            case(State_qb3)                
                s_Idle: begin               
                    Words2AlignCntr_cb8           <= #1 8'b0;   
                    Words2BitslipCntr_cb8         <= #1 8'b0;   
                    SrlAddress_qb5                <= #1 5'b0;
                    RxAligned_o                   <= #1 1'b0;
                end                         
                s_Aligning: begin           
                    if (ShiftedData_qb20 == g_AlignPattern_b20) begin
                        Words2AlignCntr_cb8       <= #1 Words2AlignCntr_cb8+1;                       
                    end else begin
                        Words2AlignCntr_cb8       <= #1 8'b0;
                        if (Words2BitslipCntr_cb8 == g_Words2Bitslip_b8-1) begin
                            Words2BitslipCntr_cb8 <= #1 0;
                            SrlAddress_qb5        <= #1 SrlAddress_qb5+1;
                        end else begin
                            Words2BitslipCntr_cb8 <= #1 Words2BitslipCntr_cb8+1;
                        end
                    end
                end
                s_Aligned: begin
                    RxAligned_o                   <= #1 1'b1;
                end
         endcase
      end
   
endmodule