--============================================================================================
--##################################   Module Information   ##################################
--============================================================================================
--
-- Company: CERN (BE-BI)
--
-- File Name: EdgeDetector.vhd
--
-- File versions history:
--
--  DATE        VERSION  AUTHOR                            DESCRIPTION
--  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
--
-- Language: VHDL
--
-- Targeted device:
--
--     - Vendor:  agnostic
--     - Model:   agnostic
--
-- Description:
--      Edge detection module
--
--============================================================================================
--############################################################################################
--============================================================================================

library ieee;
use ieee.std_logic_1164.all;

entity EdgeDetector is
    port (
        Clk_ik: in std_logic;
        Signal_i: in std_logic;
        Edge_o: out std_logic;        -- rising OR falling
        RisingEdge_o: out std_logic;  -- only rising
        FallingEdge_o: out std_logic  -- only falling
    );
end entity;

architecture syn of EdgeDetector is

    signal History: std_logic := '0';

begin
    
    pDelay: process (Clk_ik) is begin
        if rising_edge(Clk_ik) then
            History <= Signal_i;
        end if;
    end process;
    
    Edge_o <= Signal_i xor History;
    RisingEdge_o <= Signal_i and not History;
    FallingEdge_o <= not Signal_i and History;

end architecture;