//----------------------------------------------------------------------
// Title      : BST based pattern generator Test Bench
// Project    : Generic
//----------------------------------------------------------------------
// File       : tb_BstPatternGen.sv
// Author     : A. Boccardi
// Company    : CERN BE-BI-QP
// Created    : 2017-03-30
// Last update: 2017-03-30
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
//
//----------------------------------------------------------------------

`timescale 1ns/1ps

module tb_BstPatternGen;

//----- Declarations ---------------------------------------------------
//--- Local Parameters
localparam c_PatternMemAddr          = 13'h1000,
           c_LeadTimeAddr            = 13'h0000,
           c_GatePeriodAddr          = 13'h0001,
           c_NumberOfGatesAddr       = 13'h0002,
           c_NumberOfBunchesAddr     = 13'h0003,           
           c_UseExtLeadTimeRefAddr   = 13'h0004,
           c_UseExtGatePeriodRefAddr = 13'h0005;
 
 
//--- Variables
integer v_LeadTimeRefPeriod   = 1000;
integer v_GatePeriodRefPeriod = 1500;
integer v_NumberOfBunches     = 20;

//--- Signals
wire        WbReset, WbCyc, WbWe, WbStbMaster, WbAckMaster;
wire [31:0] WbAdr_b32, WbDataMoSi_b32, WbDataMiSo_b32;
wire        WbStbRegs, WbStbMem;
wire        WbAckRegs, WbAckMem; 
wire [31:0] WbDataMem_b32, WbDataRegs_b32;
reg         WbClk_k = 0;
reg         BstClk_k = 0;
reg         TurnClockFlag = 0;
reg         ExtLeadTimtRef, ExtGatePeriodRef; 
wire [ 1:0] SDRGate_b2; 
wire        DDRGate;
wire        GateGenRunning;
event       e_Start, e_Stop;
reg         Start = 0;
reg         Stop  = 0;
reg  [15:0] LeadTime_b16 = 0;        
reg  [15:0] GatePeriod_b16 = 0;     
reg  [15:0] NumberOfGates_b16 = 0;
reg  [14:0] TckDelay_b15 = 0;
reg         UseExtLeadTimeRef = 0;
reg         UseExtGatePeriodRef = 0;
reg  [ 7:0] Pattern_m4096b8 [4095:0];   
reg  [11:0] NumberOfBunches_b12 = 0; 


//----- Tasks and Event driven Actions ---------------------------------
task InitLocalMem;
    input [7:0] InitValue_b8;
begin   
    for (int i = 0; i<=4095; i++) Pattern_m4096b8[i] = InitValue_b8;
end    
endtask

task ConfigMem;
begin
    for (int i = 0; i<=4095; i++) i_WbMaster.WbWrite(c_PatternMemAddr+i, Pattern_m4096b8[i]);
end
endtask

task ConfigRegs;
begin
    i_WbMaster.WbWrite(c_LeadTimeAddr            , LeadTime_b16);
    i_WbMaster.WbWrite(c_GatePeriodAddr          , GatePeriod_b16);
    i_WbMaster.WbWrite(c_NumberOfGatesAddr       , NumberOfGates_b16);
    i_WbMaster.WbWrite(c_NumberOfBunchesAddr     , NumberOfBunches_b12);
    i_WbMaster.WbWrite(c_UseExtLeadTimeRefAddr   , UseExtLeadTimeRef);
    i_WbMaster.WbWrite(c_UseExtGatePeriodRefAddr , UseExtGatePeriodRef);   
end
endtask

always @(e_Start) begin 
    Start = 1'b1;
    #1 Start = 1'b0;
end
 
always @(e_Stop) begin 
    Stop = 1'b1;
    #1 Stop = 1'b0;
end

//----- Test Vector ----------------------------------------------------
initial begin
// Reset
    #100 i_WbMaster.Reset();
    #100;
// Starting Parameter definition
    v_NumberOfBunches     = 20;
    v_LeadTimeRefPeriod   = 1000;
    v_GatePeriodRefPeriod = 1500;  
    LeadTime_b16          = 0;        
    GatePeriod_b16        = 0;     
    NumberOfGates_b16     = 3;
    NumberOfBunches_b12   = v_NumberOfBunches - 1;
    UseExtLeadTimeRef     = 0;
    UseExtGatePeriodRef   = 0;
    InitLocalMem           (0);
    $display("------------------------------------------------------------------------");
    $display("------------- 1 slot gate all parameters to 0");
    $display("------------------------------------------------------------------------");
    Pattern_m4096b8[0] = 8'b00_00_00_01;
    ConfigMem();
    ConfigRegs();
    #1000; 
    -> e_Start;
    repeat(4) @(posedge TurnClockFlag);
    
    $display("------------------------------------------------------------------------");
    $display("------------- 2 slot gate all parameters to 0");
    $display("------------------------------------------------------------------------");
    Pattern_m4096b8[ 0] = 8'b00_00_00_01;
    Pattern_m4096b8[19] = 8'b10_00_00_00;
    ConfigMem();
    ConfigRegs();
    #1000; 
    -> e_Start;
    repeat(10) @(posedge TurnClockFlag);
    -> e_Stop;
    repeat(10) @(posedge TurnClockFlag);
    $stop();    
    
    
    
end

//----- Checkers -------------------------------------------------------

//----- Test Logic -----------------------------------------------------

always #4 WbClk_k = ~WbClk_k;

WbMasterSim 
    i_WbMaster (
        .Rst_orq(WbReset),
        .Clk_ik(WbClk_k),
        .Adr_obq32(WbAdr_b32),
        .Dat_obq32(WbDataMoSi_b32),
        .Dat_ib32(WbDataMiSo_b32),
        .We_oq(WbWe),
        .Cyc_oq(WbCyc),
        .Stb_oq(WbStbMaster),
        .Ack_i(WbAckMaster)
    );

assign WbStbRegs      = WbAdr_b32[12] ? 1'b0          : WbStbMaster;
assign WbStbMem       = WbAdr_b32[12] ? WbStbMaster   : 1'b0       ;
assign WbAckMaster    = WbAdr_b32[12] ? WbAckMem      : WbAckRegs  ; 
assign WbDataMiSo_b32 = WbAdr_b32[12] ? WbDataMem_b32 : WbDataRegs_b32;
 
always #3.125 BstClk_k = ~BstClk_k; 

always begin
    TurnClockFlag = 1'b0;
    repeat(v_NumberOfBunches*4-1) @(posedge BstClk_k);
    TurnClockFlag = 1'b1;
    @(posedge BstClk_k);
end

always begin
    ExtLeadTimtRef = 1'b0;
    #(v_LeadTimeRefPeriod-1);
    ExtLeadTimtRef = 1'b1;
    #1;
end 

always begin
    ExtGatePeriodRef = 1'b0;
    #(v_GatePeriodRefPeriod-1);
    ExtGatePeriodRef = 1'b1;
    #1;
end  

//----- Device Under Test ----------------------------------------------    

BstPatternGen 
    i_BstPatternGen(
        .Rst_iar(WbReset),
        .BstClk_ik(BstClk_k),
        .TurnClkFlag_i(TurnClockFlag),
        .ExtLeadTimeRef_iap(ExtLeadTimtRef),
        .ExtGatePeriodRef_iap(ExtGatePeriodRef),       
        .Start_iap(Start),
        .Stop_iap(Stop),
        .Running_o(GateGenRunning),   
        .WbClk_ik(WbClk_k),
        .WbCyc_i(WbCyc),
        .WbWe_i(WbWe),
        .WbAdr_ib12(WbAdr_b32[11:0]),
        .WbDataMoSi_ib32(WbDataMoSi_b32),
        .WbStbMem_i(WbStbMem),
        .WbDataMiSoMem_ob7(WbDataMem_b32[7:0]),
        .WbAckMem_o(WbAckMem),
        .WbStbRegs_i(WbStbRegs),
        .WbDataMiSoRegs_ob32(WbDataRegs_b32),
        .WbAckRegs_o(WbAckRegs),      
        .SDRGate_ob2(SDRGate_b2),   
        .DDRGate_o(DDRGate)
    );

 
assign WbDataMem_b32[31:0] = 24'h0;


    
endmodule    
    
    