# Cleaning the transcript window:
.main clear

###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

vsim -gui -novopt work.tb_BstPatternGen
do wave.do

do InteractiveWbTransactions.tcl

echo ""
echo ""
