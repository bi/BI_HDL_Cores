# Defining Quartus II path:
set QUARTUS_II_PATH C:\altera\15.1\quartus

# Define here the paths used for the different folders:
set DUT_PATH ../..
set BI_HDL_CORES_PATH ../../../..
set TB_PATH ..

# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"

###############################################
# Verilog Compile Process
###############################################

proc compile_verilog {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vlog -work $lib $name]
	}
}

###############################################
# VHDL Compile Process
###############################################

proc compile_vhdl {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vcom -work $lib $name]
	}
}

###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
if {![info exists sc]} {
    set sc 0
    set no_sc 1;
} 

if "$no_sc == 1" {
	echo "Smart compilation not possible or not enabled. Running full compilation..."
	# Deleting pre-existing libraries
	if {[file exists work]} {vdel -all -lib work}	
	# Creating and mapping working directory:
	vlib work
    vmap work work
	# Maping Altera libraries:
	#vmap altera      "\$QUARTUS_II_PATH/eda/sim_lib/altera"
    #vmap altera_mf   "\$QUARTUS_II_PATH/eda/sim_lib/altera_mf"
    #vmap arriav      "\$QUARTUS_II_PATH/eda/sim_lib/arriav"
    #vmap arriav_hssi "\$QUARTUS_II_PATH/eda/sim_lib/arriav_hssi"
	set no_sc 0;
} else {
	echo "Smart compilation enabled. Only out of date files will be compiled..."
	puts [clock format $sc -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""
    
# Device Under Test files:
compile_verilog 0 work $DUT_PATH/BstPatternGen.sv

# BI HDL CORES files (synthesis):
compile_verilog 0 work $BI_HDL_CORES_PATH/cores_for_synthesis/APulseSync.v
compile_verilog 0 work $BI_HDL_CORES_PATH/cores_for_synthesis/PulseSync.v

# BI HDL CORES files (simulation):
compile_verilog 0 work $BI_HDL_CORES_PATH/cores_for_simulation/wb_master_sim/WbMasterSim.v

# Test Bench files:
compile_verilog 0 work $TB_PATH/tb_BstPatternGen.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

set top_level work.tb_BstPatternGen

###############################################
# Acquiring Compilation Time
###############################################

echo ""
set sc [clock seconds];
puts [clock format $sc -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

echo ""
echo "-> Compilation Done..."
echo ""
echo ""