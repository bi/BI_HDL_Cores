onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/Start_iap
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/Stop_iap
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/Running_o
add wave -noupdate -radix unsigned /tb_BstPatternGen/i_BstPatternGen/LeadTime_b16
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/GateOn_e
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/State
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/TurnClkFlag_i
add wave -noupdate -radix unsigned /tb_BstPatternGen/i_BstPatternGen/GatePeriod_b16
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/DDRGate_o
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/SDRGate_ob2
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[13]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[12]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[11]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[10]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[9]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[8]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[7]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[6]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[5]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[4]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[3]}
add wave -noupdate -group MemoryPointer {/tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14[2]}
add wave -noupdate /tb_BstPatternGen/i_BstPatternGen/MemoryPointerX4_c14
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {165990625 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 340
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {165755801 ps} {166291713 ps}
