//----------------------------------------------------------------------
// Title      : BST based pattern generator
// Project    : Generic
//----------------------------------------------------------------------
// File       : BstPatternGen.sv
// Author     : A. Boccardi
// Company    : CERN BE-BI-QP
// Created    : 2017-03-29
// Last update: 2017-03-29
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
// Generate a pattern alligned to the turn (BST) with a resolution of
// 3.125ns in DDR mode or 6.25ns in SDR mode.
// The Pattern is programmed in a memory, the turn clock can be shifted
// of an additional 3.125ns, the start can be delayed by a lead time 
// (turns or number of external pulses), the patterb can be produced each 
// turn or set to have a "dead time" (period) based on turns or external 
// pulses like the lead time. The number of generated patterns can be 
// indefinite or programmed to an exact number up to 64k.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module BstPatternGen
(
    input             Rst_iar,

    input             BstClk_ik,
    input             TurnClkFlag_i,
    input             ExtLeadTimeRef_iap,
    input             ExtGatePeriodRef_iap, 
    input             AddHalfClockDelay_i,
    
    input             Start_iap,
    input             Stop_iap,
    output reg        Running_o,
    
    input             WbClk_ik,
    input             WbCyc_i,
    input             WbWe_i,
    input      [11:0] WbAdr_ib12,
    input      [31:0] WbDataMoSi_ib32,
    input             WbStbMem_i,
    output reg [7:0]  WbDataMiSoMem_ob7,
    output reg        WbAckMem_o,
    input             WbStbRegs_i,
    output reg [31:0] WbDataMiSoRegs_ob32,
    output reg        WbAckRegs_o,    
    
    output reg [ 1:0] SDRGate_ob2,   //Comment: Gate in SDR mode on 2 bits
    output            DDRGate_o
    );

reg  [ 7:0] GateShapeMem_m4096b8 [4095:0];
reg  [ 7:0] MemOut_b8 = 0;
reg  [ 1:0] MemOut_b2 = 0;
reg  [13:0] MemoryPointerX4_c14 = 0;
reg  [ 1:0] PreGate_b2 = 0;
reg         RamWe = 0;
reg         RamRe = 0;
reg  [13:0] TckDelayCounter_c14 = 0;
reg  [15:0] LeadTimeCntr_c16 = 0;
reg  [15:0] GateCntr_c16 = 0;
reg  [15:0] PeriodCntr_c16 = 0;
reg  [15:0] LeadTime_b16 = 0;      
reg         UseExtLeadTimeRef = 0;
reg  [15:0] GatePeriod_b16 = 0;      
reg         UseExtGatePeriodRef = 0;
reg  [15:0] NumberOfGates_b16 = 0;
reg  [11:0] NumberOfBunches_b12 = 0;   
wire        ConfigChangeBst;
wire        ExtReset_r;   
reg         FSMReset_r = 0;
wire        Start_qp, Stop_qp;
wire        ExtLeadTimeRef_qp, ExtGatePeriodRef_qp;
enum {  
        s_Idle, 
        s_LeadTime, 
        s_WaitTckToGoOn, 
        s_GateOn, 
        s_GateOff 
     }     State, NextState;


//------------------------------------------------------------------------------
//  Synch Blocks
//------------------------------------------------------------------------------ 

APulseSync 
    i_StartSynch(
        .APulse_iap(Start_iap),
        .Clk_ik(BstClk_ik),
        .Pulse_op(Start_qp));    
        
APulseSync 
    i_StopSynch(
        .APulse_iap(Stop_iap),
        .Clk_ik(BstClk_ik),
        .Pulse_op(Stop_qp));        
        
APulseSync 
    i_LeadRefSynch(
        .APulse_iap(ExtLeadTimeRef_iap),
        .Clk_ik(BstClk_ik),
        .Pulse_op(ExtLeadTimeRef_qp));         
 
APulseSync 
    i_GateRefSynch(
        .APulse_iap(ExtGatePeriodRef_iap),
        .Clk_ik(BstClk_ik),
        .Pulse_op(ExtGatePeriodRef_qp));      
 
APulseSync 
    i_ResetSynch(
        .APulse_iap(Rst_iar),
        .Clk_ik(BstClk_ik),
        .Pulse_op(ExtReset_r));

wire ConfigChange = WbCyc_i && WbWe_i && (WbStbRegs_i || WbStbMem_i);  
 
PulseSync 
    i_ConfigChangeSynch(
        .ClkIn_ik(WbClk_ik),
        .PulseIn_i(ConfigChange),
        .ClkOut_ik(BstClk_ik),
        .PulseOut_o(ConfigChangeBst));    
          
always @(posedge BstClk_ik)  
    FSMReset_r <= #1 ExtReset_r || ConfigChangeBst; 
 
//------------------------------------------------------------------------------
//  WishBone Registers (nop need to synch as we reset on change)
//------------------------------------------------------------------------------     

localparam c_LeadTimeAddr            = 3'd0,
           c_GatePeriodAddr          = 3'd1,
           c_NumberOfGatesAddr       = 3'd2,
           c_NumberOfBunchesAddr     = 3'd3,
           c_UseExtLeadTimeRefAddr   = 3'd4,
           c_UseExtGatePeriodRefAddr = 3'd5;

always @(posedge WbClk_ik) 
    WbAckRegs_o <= WbCyc_i && WbStbRegs_i;           
           
always @(posedge WbClk_ik)  
    if (WbCyc_i && WbWe_i && WbStbRegs_i && ~WbAckRegs_o) case(WbAdr_ib12[2:0])
        c_LeadTimeAddr            : LeadTime_b16        <= #1 WbDataMoSi_ib32[15:0];
        c_GatePeriodAddr          : GatePeriod_b16      <= #1 WbDataMoSi_ib32[15:0];
        c_NumberOfGatesAddr       : NumberOfGates_b16   <= #1 WbDataMoSi_ib32[15:0];
        c_NumberOfBunchesAddr     : NumberOfBunches_b12 <= #1 WbDataMoSi_ib32[11:0];
        c_UseExtLeadTimeRefAddr   : UseExtLeadTimeRef   <= #1 WbDataMoSi_ib32[0];
        c_UseExtGatePeriodRefAddr : UseExtGatePeriodRef <= #1 WbDataMoSi_ib32[0];
    endcase    

always @(posedge WbClk_ik)  
    if (~WbAckRegs_o) case(WbAdr_ib12[2:0])
        c_LeadTimeAddr            : WbDataMiSoRegs_ob32 <= #1 {16'h0, LeadTime_b16};
        c_GatePeriodAddr          : WbDataMiSoRegs_ob32 <= #1 {16'h0, GatePeriod_b16};
        c_NumberOfGatesAddr       : WbDataMiSoRegs_ob32 <= #1 {16'h0, NumberOfGates_b16};
        c_UseExtLeadTimeRefAddr   : WbDataMiSoRegs_ob32 <= #1 {31'h0, UseExtLeadTimeRef};
        c_UseExtGatePeriodRefAddr : WbDataMiSoRegs_ob32 <= #1 {31'h0, UseExtGatePeriodRef};
    endcase    

//------------------------------------------------------------------------------
//  Memory Pointer
//------------------------------------------------------------------------------ 
        
always @(posedge BstClk_ik)   
    if (TurnClkFlag_i)
        MemoryPointerX4_c14 <= 14'h2;
    else if (MemoryPointerX4_c14 == {NumberOfBunches_b12, 2'b11})
        MemoryPointerX4_c14 <= 14'h0; 
    else
        MemoryPointerX4_c14<= MemoryPointerX4_c14 + 1'b1;
        
//------------------------------------------------------------------------------
//  Gate Shape Memory
//------------------------------------------------------------------------------ 

always @(posedge WbClk_ik) 
    if (RamRe || RamWe)
        WbAckMem_o <= 1'b1;
    else if (~WbCyc_i || ~WbStbMem_i)
        WbAckMem_o <= 1'b0;
  
always @(posedge WbClk_ik)
    RamRe <= WbCyc_i && ~WbWe_i && WbStbMem_i && ~WbAckMem_o;
  
always @(posedge WbClk_ik) 
    RamWe <= WbCyc_i && WbWe_i && WbStbMem_i && ~WbAckMem_o;
 

always @(posedge WbClk_ik) begin
    if (RamWe) GateShapeMem_m4096b8[WbAdr_ib12] <= #1 WbDataMoSi_ib32[7:0];
    WbDataMiSoMem_ob7 <= GateShapeMem_m4096b8[WbAdr_ib12];
end
    
always @(posedge BstClk_ik)
    MemOut_b8 <= GateShapeMem_m4096b8 [MemoryPointerX4_c14/4];

always @*
 case (MemoryPointerX4_c14%4)                //comment: recovering the 1 ppl lvl
    2'b00: MemOut_b2 = MemOut_b8[7:6];
    2'b01: MemOut_b2 = MemOut_b8[1:0];
    2'b10: MemOut_b2 = MemOut_b8[3:2];
    2'b11: MemOut_b2 = MemOut_b8[5:4];
    default: MemOut_b2 = MemOut_b8[1:0];
endcase    

//------------------------------------------------------------------------------
//  Finite State Machine
//------------------------------------------------------------------------------

always @(posedge BstClk_ik) State <= #1 NextState;

always @* 
    if (FSMReset_r) 
        NextState = s_Idle;
    else case (State)
        s_Idle: 
            if (Start_qp) 
                NextState = s_LeadTime;    
        s_LeadTime:
            if (Stop_qp)
                NextState = s_Idle;
            else if (LeadTimeCntr_c16==LeadTime_b16 || ~|LeadTime_b16) 
                NextState = TurnClkFlag_i ? s_GateOn : s_WaitTckToGoOn;
        s_WaitTckToGoOn:   
            if (Stop_qp)
                NextState = s_Idle;
            else if (TurnClkFlag_i) 
                NextState = s_GateOn;
        s_GateOn:
            if (Stop_qp)
                NextState = s_Idle;
            else if (TurnClkFlag_i) begin
                if (|NumberOfGates_b16 && GateCntr_c16==NumberOfGates_b16) 
                    NextState = s_Idle;
                else if (|GatePeriod_b16) 
                    NextState = s_GateOff;
            end        
        s_GateOff: 
            if (Stop_qp)
                NextState = s_Idle;
            else if (PeriodCntr_c16==GatePeriod_b16)
                NextState = TurnClkFlag_i ? s_GateOn : s_WaitTckToGoOn;
        default: NextState = s_Idle;
    endcase
    
always @(posedge BstClk_ik) begin
    Running_o <= #1 ~(State == s_Idle);
    case(State)
        s_Idle: begin
            LeadTimeCntr_c16 <= #1 1'b1;
            GateCntr_c16     <= #1 1'b1;
            PeriodCntr_c16   <= #1 1'b1;
        end
        s_LeadTime: begin
            if (UseExtLeadTimeRef) 
                LeadTimeCntr_c16 <= #1 LeadTimeCntr_c16 + ExtLeadTimeRef_qp;
            else
                LeadTimeCntr_c16 <= #1 LeadTimeCntr_c16 + TurnClkFlag_i;
        end
        //s_WaitTckToGoOn: //Doing nothing here
        s_GateOn: begin
            GateCntr_c16     <= #1 GateCntr_c16 + TurnClkFlag_i;
            PeriodCntr_c16   <= #1 1'b1;
        end
        s_GateOff: begin
            if (UseExtGatePeriodRef) 
                PeriodCntr_c16 <= #1 PeriodCntr_c16 + ExtGatePeriodRef_qp;
            else
                PeriodCntr_c16 <= #1 PeriodCntr_c16 + TurnClkFlag_i;
        end
    endcase
end
   
//------------------------------------------------------------------------------
//  Gate SDR and DDR output stages
//------------------------------------------------------------------------------ 

wire GateOn_e = NextState == s_GateOn;

reg [1:0] DDRReg_b2 = 0;
reg MemOutMsb_d = 0; 
 
always @(posedge BstClk_ik) 
    MemOutMsb_d <= GateOn_e ? MemOut_b2[1] : 2'b0; 
 
always @(posedge BstClk_ik) 
    SDRGate_ob2 <= GateOn_e ? MemOut_b2 : 2'b0;
        
always @(posedge BstClk_ik)
    if (AddHalfClockDelay_i) begin
        DDRReg_b2[0] <= MemOutMsb_d;    
        DDRReg_b2[1] <= GateOn_e ? MemOut_b2[0] : 2'b0;
    end else    
        DDRReg_b2 <= GateOn_e ? MemOut_b2 : 2'b0;
    
assign DDRGate_o = BstClk_ik ? DDRReg_b2[0] : DDRReg_b2[1];
   
endmodule








































