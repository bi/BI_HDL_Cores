`timescale 1ns/1ns

/*
This module implements a non prioritized WishBone bus arbiter.
It allows to connect 2 masters to a WishBone bus
*/

module WbBus2M1S #( 
    parameter g_DataWidth = 32, 
              g_AddrWidth = 32)
(  
    input                      Clk_ik,
    input                      CycM1_i,
    input                      StbM1_i,
    input                      WeM1_i,
    input  [g_AddrWidth-1 : 0] AddrM1_ib,
    input  [g_DataWidth-1 : 0] DataM1_ib,
    output                     AckM1_o,
    output [g_DataWidth-1 : 0] DataM1_ob,    
    input                      CycM2_i,
    input                      StbM2_i,
    input                      WeM2_i,    
    input  [g_AddrWidth-1 : 0] AddrM2_ib,
    input  [g_DataWidth-1 : 0] DataM2_ib,
    output                     AckM2_o,
    output [g_DataWidth-1 : 0] DataM2_ob,
    output                     CycS_o,
    output                     StbS_o,
    output                     WeS_o,
    output [g_AddrWidth-1 : 0] AddrS_ob,
    output [g_DataWidth-1 : 0] DataS_ob,
    input                      AckS_i,
    input  [g_DataWidth-1 : 0] DataS_ib);
    
reg SelM1 = 1'b1;

always @(posedge Clk_ik) 
    if (!AckS_i) begin // master can changed only at the end of transactions
        if      ({CycM1_i,CycM2_i} == 2'b10) SelM1 <= 1'b1;
        else if ({CycM1_i,CycM2_i} == 2'b01) SelM1 <= 1'b0;
    end
    
assign AckM1_o =    SelM1 ? AckS_i :   1'b0;
assign AckM2_o =    SelM1 ?   1'b0 : AckS_i;

assign DataM1_ob = DataS_ib;
assign DataM2_ob = DataS_ib;

assign CycS_o   = SelM1 ?  CycM1_i  :  CycM2_i ;
assign StbS_o   = SelM1 ?  StbM1_i  :  StbM2_i ;
assign WeS_o    = SelM1 ?   WeM1_i  :   WeM2_i;
assign AddrS_ob = SelM1 ? AddrM1_ib : AddrM2_ib;
assign DataS_ob = SelM1 ? DataM1_ib : DataM2_ib;

endmodule

         
