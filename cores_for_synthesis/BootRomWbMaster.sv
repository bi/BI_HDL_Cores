/*

WB initial configuration master. The module acts as Wishbone master after reset. It plays
commands pre-programmed into a ROM after the release of reset signal. After the reset,
the sequence pre-programmed into a ROM is executed.

The maximum time for which the BOOT ROM master waits for slave ack is configurable via p_MaxAckWaitTimeMicrosec
and can be significantly larger than the one on the VME bus. This allows to wait for chip resets, etc.
There is h1000 clocks delay between commands, also before the first command.

The maximum time for which the BOOT ROM can request the bus is configured via p_MaxBootTimeMilisec.

Default p_MaxBootTimeMilisec is 3s, given in milisecond
Default p_MaxAckWaitTimeMicrosec is 10ms, given in microseconds

The ROM data is composed as follows:
[2'h flag, 8'h not used, 22'h Wishbone address, 32'h0 Wishbone data]

The flag is either:
2'h0 for commands to be passed to WB
2'h2 marking a pause. The 32 LSB in the line are used as the delay value in microseconds.
2'h3 marking end of configuration. This flag must be the last active line of MIF, other bits in the line
are neglected.

For the currenct version of the VfcHD-System, set the MSB of the 22-bit Wb address to 1 to send commands to
application part; to 0 to send commands to system part.

Relation between the Wishbone and VME address is defined by the VME module. Currently it is: Wishbone = VME >> 2.

Last change: 17/01/2019

Author: Jiri Kral
jiri.kral@cern.ch

MIF example: -----------------------------------------------------
WIDTH=64;
DEPTH=256;

ADDRESS_RADIX=HEX;
DATA_RADIX=HEX;

CONTENT BEGIN
    00  :   0000000100000002; -- write 0x2 to address 0x1 of system part
    01  :   0000000200000007; -- write 0x7 to address 0x2 of system part
    02  :   0020000500000003; -- write 0x3 to address 0x5 of application part
    03  :   0020000600000010; -- write 0x10 to address 0x6 of application part
    04  :   80000000000003E8; -- delay 1000 us
    05  :   002000070000001A; -- write 0x1A to address 0x7 of application part
    06  :   002000080000001B; -- write 0x1B to address 0x8 of application part
    07  :   002000090000001C; -- write 0x1C to address 0x9 of application part
    08  :   0020000A0000001D; -- write 0x1D to address 0xA of application part
    09  :   0020000B0000001E; -- write 0x1E to address 0xB of application part
    0A  :   C000000000000000; -- end flag
    [0B..FF]  :   C000000000000000;
END;
end of MIF example -----------------------------------------------

*/

`timescale 1ns/100ps

module BootRomWbMaster #(
    parameter g_ClkFrequency,
    parameter g_InitFile,
    parameter p_MaxBootTimeMicrosec = 3000000,  // 3s
    parameter p_MaxAckWaitTimeMicrosec = 10000 ) // 10ms
(
    // Clock, reset
    input   Clk_ik,
    input   Rst_irq,

    output  reg SelBoot_oq,  // bus takeover flag
    // Wishbone interface
    output  reg Cyc_oq,
    output  reg Stb_oq,
    output  reg We_oq,
    output  reg [21:0] Adr_oqb22,
    output  reg [31:0] Dat_oqb32,
    input  Ack_i
);

localparam int p_MicrosecCnt = ( g_ClkFrequency / 1e6 ) - 1;

// neg reset
wire rst_rn;

// rom signals
reg [7:0] RomAddress_qb8;
wire [63:0] RomData_b64;

// state machine
reg [1:0] ConfigState = 2'h0;
reg [31:0] Counter_qb32;
reg MemOverrun_q;

localparam s_wait = 2'h0;
localparam s_write = 2'h1;
localparam s_waitack = 2'h2;
localparam s_done = 2'h3;

// Total boot timeout
reg [$clog2( p_MaxBootTimeMicrosec + 1 ) - 1:0] BootTimeoutCounter_c;
// Ack timeout
reg [$clog2( p_MaxAckWaitTimeMicrosec + 1 ) - 1:0] TimeoutCounter_c;
// Boot master select
reg SelBoot_q;

// Microsecond counter and flag
reg [$clog2( p_MicrosecCnt + 1 ) - 1:0] MicrosecCounter_c;
reg MicrosecFlag_q;

// initial reset
assign rst_rn = ~Rst_irq;

// config ROM
altsyncram #(
    .address_aclr_a("NONE"),
    .clock_enable_input_a("BYPASS"),
    .clock_enable_output_a("BYPASS"),
    .init_file(g_InitFile),
    .intended_device_family("Arria V"),
    .lpm_hint("ENABLE_RUNTIME_MOD=NO"),
    .lpm_type("altsyncram"),
    .numwords_a(256),
    .operation_mode("ROM"),
    .outdata_aclr_a("NONE"),
    .outdata_reg_a("CLOCK0"),
    .widthad_a(8),
    .width_a(64),
    .width_byteena_a(1))
i_boot_rom (
    .address_a (RomAddress_qb8),
    .clock0 (Clk_ik),
    .q_a (RomData_b64),
    .aclr0 (1'b0),
    .aclr1 (1'b0),
    .address_b (1'b1),
    .addressstall_a (1'b0),
    .addressstall_b (1'b0),
    .byteena_a (1'b1),
    .byteena_b (1'b1),
    .clock1 (1'b1),
    .clocken0 (1'b1),
    .clocken1 (1'b1),
    .clocken2 (1'b1),
    .clocken3 (1'b1),
    .data_a ({64{1'b1}}),
    .data_b (1'b1),
    .eccstatus (),
    .q_b (),
    .rden_a (1'b1),
    .rden_b (1'b1),
    .wren_a (1'b0),
    .wren_b (1'b0));

// microsecond flag generator
// generates 1 clock lasting flag every microsecond
always @(posedge Clk_ik or negedge rst_rn )
begin
    if( rst_rn == 1'b0 )
    begin
        MicrosecFlag_q <= 1'b0;
        MicrosecCounter_c <= 0;
    end
    else begin
        // generate microsecond flag
        if( MicrosecCounter_c == p_MicrosecCnt )
        begin
            MicrosecCounter_c <= 0;
            MicrosecFlag_q <= 1'b1;
        end
        else begin
            MicrosecCounter_c <= MicrosecCounter_c + 1'b1;
            MicrosecFlag_q <= 1'b0;
        end
    end
end

// total boot time guard
always @(posedge Clk_ik or negedge rst_rn )
begin
    if( rst_rn == 1'b0 )
    begin
        SelBoot_oq <= 1'b0;
        BootTimeoutCounter_c <= 0;
    end
    else begin
        // do not request to be master after total timeout
        if( BootTimeoutCounter_c == p_MaxBootTimeMicrosec )
            SelBoot_oq <= 1'b0;
        else begin
            // timeout counter
            if( MicrosecFlag_q == 1'b1 )
                BootTimeoutCounter_c <= BootTimeoutCounter_c + 1'b1;
            // forward master request
            SelBoot_oq <= SelBoot_q;
        end
    end
end


// initial config state machine
always @(posedge Clk_ik or negedge rst_rn )
begin

    if( rst_rn == 1'b0 )
    begin
        RomAddress_qb8 <= 8'h0;
        Cyc_oq <= 1'b0;
        Stb_oq <= 1'b0;
        We_oq <= 1'b0;
        Adr_oqb22 <= 22'h0;
        Dat_oqb32 <= 32'h0;
        ConfigState <= s_wait;
        Counter_qb32 <= 32'h20; // give 20us initial wait
        SelBoot_q <= 1'b0;
        TimeoutCounter_c <= 0;
        MemOverrun_q <= 1'b0;
    end
    else begin

        case( ConfigState )
        // wait, release last commands
        s_wait:
        begin
            Cyc_oq <= 1'b0;
            Stb_oq <= 1'b0;
            We_oq <= 1'b0;
            SelBoot_q <= 1'b1;
            TimeoutCounter_c <= 0;

            // protection against running the memory content multiple times
            if( MemOverrun_q == 1'b1 )
                ConfigState <= s_done;
            // some slaves may keep the ack up for quite long
            // wait until it goes down before proceeding
            else if( Ack_i == 1'b0 )
            begin
                // wait specified time
                // decrement counter on every microsecond
                if( MicrosecFlag_q == 1'b1 )
                    Counter_qb32 <= Counter_qb32 - 32'h1;

                // done waiting
                if( Counter_qb32 == 32'h0 )
                    ConfigState <= s_write;
            end
        end

        // one WB write command
        s_write:
        begin
            // increment ROM address
            RomAddress_qb8 <= RomAddress_qb8 + 8'h1;

            // reached end of ROM
            if( RomAddress_qb8 == 8'hFF )
                MemOverrun_q <= 1'b1;

            // check if end of init mark
            // go to done if yes
            if( RomData_b64[63:62] == 2'b11 )
                ConfigState <= s_done;
            // Pause command
            else if ( RomData_b64[63:62] == 2'b10 )
            begin
                Counter_qb32 <= RomData_b64[31:0];
                ConfigState <= s_wait;
            end
            // write command otherwise
            else begin
                Cyc_oq <= 1'b1;
                Stb_oq <= 1'b1;
                We_oq <= 1'b1;
                Adr_oqb22 <= RomData_b64[53:32];
                Dat_oqb32 <= RomData_b64[31:0];
                ConfigState <= s_waitack;
            end
        end

        // wait for slave ack
        s_waitack:
        begin
            // prepare 20us delay before next write
            Counter_qb32 <= 32'h20;
            if( MicrosecFlag_q == 1'b1 )
                TimeoutCounter_c <= TimeoutCounter_c + 1'b1;

            // wait for slave ack or timeout (prevention of wrong addresses or dead slaves)
            if( Ack_i == 1'b1 || TimeoutCounter_c == p_MaxAckWaitTimeMicrosec )
                ConfigState <= s_wait;
            else
                ConfigState <= s_waitack;
        end

        // done state, stay here`
        s_done:
        begin
            ConfigState <= s_done;
            SelBoot_q <= 1'b0;
        end

        endcase
    end
end

endmodule
