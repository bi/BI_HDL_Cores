// Example usage:
//
//      t_WbInterface #(.g_DataWidth(32), .g_AddressWidth(16)) Wb_t(Clk_k, Reset_r);
//

interface t_WbInterface #(
    int g_DataWidth = 32,
    int g_AddressWidth = 32
) (
    input logic Clk_k,
    input logic Reset_r
);
    // Master 2 Slave
    logic Cyc;
    logic Stb;
    logic [g_AddressWidth-1:0] Adr_b;
    logic [(g_DataWidth/8)-1:0] Sel_b;
    logic We;
    logic [g_DataWidth-1:0] DatMoSi_b;
    // Slave 2 Master
    logic Ack;
    logic Err;
    logic Rty;
    logic Stall;
    logic [g_DataWidth-1:0] DatMiSo_b;
endinterface