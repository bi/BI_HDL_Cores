//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbCdc.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      Cross Domain Crossing for Wishbone bus (standard mode only - NOT for pipelined)
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

// TODO: investigate why this warning is issued
// altera message_off 10665
module WbCdc (
    t_WbInterface Master_iot, // towards master with some clock
    t_WbInterface Slave_iot   // towards slave with another clock
);

    localparam c_SignalDelay = 3; // delay for main controlling signals
    localparam c_OtherDelay = 2;  // delay for data-like signals
    localparam c_AddressWidth = $bits(Master_iot.Adr_b);
    localparam c_DataWidth = $bits(Master_iot.DatMoSi_b);
    localparam c_SelWidth = $bits(Master_iot.Sel_b);

    initial begin
        // check both interfaces have same data/address/sel width
        assert (c_AddressWidth == $bits(Slave_iot.Adr_b))
            else $error("Address widths of master/slave interfaces are not the same!");
        assert (c_DataWidth == $bits(Slave_iot.DatMoSi_b))
            else $error("Data widths of master/slave interfaces are not the same!");
        assert (c_SelWidth == $bits(Slave_iot.Sel_b))
            else $error("Sel widths of master/slave interfaces are not the same!");
    end

    // controlling signals

    SignalSyncer #(
        .g_Width(2),
        .g_Delay(c_SignalDelay)
    ) i_Master2SlaveCtrlSyncer (
        .Clk_ik(Slave_iot.Clk_k),
        .Data_ib({Master_iot.Cyc, Master_iot.Stb}),
        .Data_ob({Slave_iot.Cyc, Slave_iot.Stb})
    );

    SignalSyncer #(
        .g_Width(3),
        .g_Delay(c_SignalDelay)
    ) i_Slave2MasterCtrlSyncer (
        .Clk_ik(Master_iot.Clk_k),
        .Data_ib({Slave_iot.Ack, Slave_iot.Err, Slave_iot.Rty}),
        .Data_ob({Master_iot.Ack, Master_iot.Err, Master_iot.Rty})
    );
    
    // other signals
    
    SignalSyncer #(
        .g_Width(c_AddressWidth),
        .g_Delay(c_OtherDelay)
    ) i_Master2SlaveAddressSyncer (
        .Clk_ik(Slave_iot.Clk_k),
        .Data_ib(Master_iot.Adr_b),
        .Data_ob(Slave_iot.Adr_b)
    );

    SignalSyncer #(
        .g_Width(c_DataWidth),
        .g_Delay(c_OtherDelay)
    ) i_Master2SlaveDataSyncer (
        .Clk_ik(Slave_iot.Clk_k),
        .Data_ib(Master_iot.DatMoSi_b),
        .Data_ob(Slave_iot.DatMoSi_b)
    );

    SignalSyncer #(
        .g_Width(c_SelWidth+1),
        .g_Delay(c_OtherDelay)
    ) i_Master2SlaveOtherSyncer (
        .Clk_ik(Slave_iot.Clk_k),
        .Data_ib({Master_iot.Sel_b, Master_iot.We}),
        .Data_ob({Slave_iot.Sel_b, Slave_iot.We})
    );

    // signals Slave -> Master
    assign Master_iot.Stall = 1'b0;

    SignalSyncer #(
        .g_Width(c_DataWidth),
        .g_Delay(c_OtherDelay)
    ) i_Slave2MasterDataSyncer (
        .Clk_ik(Master_iot.Clk_k),
        .Data_ib(Slave_iot.DatMiSo_b),
        .Data_ob(Master_iot.DatMiSo_b)
    );
    
endmodule