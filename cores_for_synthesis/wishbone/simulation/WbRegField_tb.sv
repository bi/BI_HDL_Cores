module WbRegField_tb;

    timeunit 1ns / 1ps;

    localparam longint unsigned c_Frequency = 100e6;
    localparam int c_DataWidth = 32;
    localparam int c_NumberOfRegisters = 4;
    localparam logic [c_NumberOfRegisters-1:0] c_DirectionMiso_b = 4'b0011;

    logic Clk_k;
    logic Reset_r;
    logic [c_DataWidth-1:0] Register_imb [0:c_NumberOfRegisters-1];
    logic [c_DataWidth-1:0] Register_omb [0:c_NumberOfRegisters-1];
    logic [c_DataWidth-1:0] TestData;
    logic [c_DataWidth-1:0] ReadData;
    
    t_WbInterface #(.g_DataWidth(c_DataWidth), .g_AddressWidth(32)) Wb_iot(Clk_k, Reset_r);

    ClockGenerator #(
        .g_Frequency(c_Frequency)
    ) i_ClkGenerator (
        .Clk_ok(Clk_k)
    );

    WbMasterSim i_WbMasterSim (
        .Rst_orq(Reset_r),
        .Clk_ik(Clk_k),
        .Adr_obq32(Wb_iot.Adr_b),
        .Dat_obq32(Wb_iot.DatMoSi_b),
        .Dat_ib32(Wb_iot.DatMiSo_b),
        .We_oq(Wb_iot.We),
        .Cyc_oq(Wb_iot.Cyc),
        .Stb_oq(Wb_iot.Stb),
        .Ack_i(Wb_iot.Ack)
    );
    assign Wb_iot.Sel_b = 4'hf;

    WbRegField #(
        .g_NumberOfRegisters(c_NumberOfRegisters),
        .g_DataWidth(c_DataWidth),
        .g_DirectionMiso_b(c_DirectionMiso_b)//,
        // .g_DefaultValue_mb() = {((g_NumberOfRegisters*g_DataWidth)-1){1'b0}},
        // .g_AutoClrMask_mb() = {((g_NumberOfRegisters*g_DataWidth)-1){1'b0}},
    ) i_Dut (
        .*
    );
    
    task TestRegister(input int RegisterNumber);
        TestData = $urandom; // 32 bit!
        $display("Testing register %d with data %h...", RegisterNumber, TestData);
        // write to the register
        if (c_DirectionMiso_b[RegisterNumber] == 1'b0) begin
            i_WbMasterSim.WbWrite(RegisterNumber, TestData);
        end else begin
            Register_imb[RegisterNumber] = TestData;
            repeat(2) @(posedge Clk_k);
        end
        // read and check        
        assert (Register_omb[RegisterNumber] == TestData) else $error("GW output does not match!");
        i_WbMasterSim.WbRead(RegisterNumber, ReadData);
        assert (ReadData == TestData) else $error("WB output does not match!");
    endtask

    initial begin
        i_WbMasterSim.Reset();
        repeat(10) @(posedge Clk_k);
        for (int i = 0; i < c_NumberOfRegisters; i = i + 1) begin
            TestRegister(i);
        end
        $stop();
    end
       
endmodule