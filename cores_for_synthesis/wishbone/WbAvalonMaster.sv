//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbAvalonMaster.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//     Wishbone pipelined slave <-> Avalon-MM master bridge
//     For connecting Avalon slave to the WB pipelined bus.
//
//     No Avalon burst support.
//
//     !! Both interfaces have to have same clock/reset !!
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

// TODO: investigate why this warning is issued
// altera message_off 10665
module WbAvalonMaster (
    t_WbInterface WbSlave_iot, // acts as WB slave - pipelined version
    t_AvalonMmInterface AvalonMaster_iot // acts as Avalon master
);

    localparam c_WbAddressWidth = $bits(WbSlave_iot.Adr_b);
    localparam c_WbDataWidth = $bits(WbSlave_iot.DatMoSi_b);

    initial begin
        assert (c_WbAddressWidth == $bits(AvalonMaster_iot.Address_b))
            else $error("Different address width between Wishbone and Avalon interfaces!");
        assert (c_WbDataWidth == $bits(AvalonMaster_iot.DataRead_b))
            else $error("Different data width between Wishbone and Avalon interfaces!");
    end
    
    logic WbTransaction;
    logic WaitingForAck = 0;
    logic WbLastTransactionWrite = 0;
    
    assign WbTransaction = WbSlave_iot.Cyc && WbSlave_iot.Stb;

    // Master 2 Slave
    assign AvalonMaster_iot.Address_b = WbSlave_iot.Adr_b;
    assign AvalonMaster_iot.ByteEnable = WbSlave_iot.Sel_b;
    assign AvalonMaster_iot.Read = WbTransaction && ~WbSlave_iot.We;
    assign AvalonMaster_iot.Write = WbTransaction && WbSlave_iot.We;
    assign AvalonMaster_iot.DataWrite_b = WbSlave_iot.DatMoSi_b;
    
    // Slave 2 Master
    assign WbSlave_iot.Ack = WaitingForAck & (WbLastTransactionWrite | (~WbLastTransactionWrite & AvalonMaster_iot.ReadDataValid));
    assign WbSlave_iot.Err = 1'b0;
    assign WbSlave_iot.Rty = 1'b0;
    assign WbSlave_iot.Stall = AvalonMaster_iot.WaitRequest;
    assign WbSlave_iot.DatMiSo_b = AvalonMaster_iot.DataRead_b;

    always_ff @(posedge WbSlave_iot.Clk_k)
        if (WbTransaction)
            WbLastTransactionWrite <= WbSlave_iot.We;
    
    always_ff @(posedge WbSlave_iot.Clk_k)
        if (WbTransaction)
            WaitingForAck <= 1;
        else if (WbSlave_iot.Ack)
            WaitingForAck <= 0;
    
endmodule
