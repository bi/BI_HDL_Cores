
//----------------------------------------------------------------------
// Title      : Generate Wishbone transactions from Cheby generated
//              register interface
// Project    : BBQ-VFC (https://gitlab.cern.ch/bi/BI_HDL_Cores/)
//----------------------------------------------------------------------
// File       : Reg2WbIf.sv
// Author     : M. Dolenc
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Receive required transaction information from register interface,
// generate the transaction and return status/data back to register
// interface.
//----------------------------------------------------------------------

module Reg2WbIf #(
    AW_REG  = 32,
    AW_WB   = 32,
    DW_REG  = 32,
    DW_WB   = 32
)
(
    input           [AW_REG -1:0]   Addr_ib,        // address register - note this is assumed to be byte address
    input           [DW_REG -1:0]   Data_ib,        // write data register
    output  reg     [DW_REG -1:0]   Data_ob,        // read data register
    input           [DW_REG/8 -1:0] Sel_ib,         // Byte select input
    input                           Write_i,        // write trigger input - qualified with Control_wr_i
    output                          Write_o,        // write in progress output
    input                           Read_i,         // read trigger input - qualified with Control_wr_i
    output                          Read_o,         // read in progress output
    input                           Control_wr_i,   // Strobe for Write_i/Read_i/Sel_i
    output  reg                     Error_o,        // Complete with error
    output  reg                     Retry_o,        // Complete with retry
    output  reg                     Ack_o,          // Complete with acknowledge

    output                          wbm_cyc_o,
    output                          wbm_stb_o,
    output  reg                     wbm_we_o,
    output  reg     [AW_WB -1:0]    wbm_adr_o,
    output  reg     [DW_WB/8 -1:0]  wbm_sel_o,
    output  reg     [DW_WB -1:0]    wbm_dat_o,
    input           [DW_WB -1:0]    wbm_dat_i,
    input                           wbm_ack_i,
    input                           wbm_rty_i,
    input                           wbm_err_i,

    input   Clk_ik,
    input   Rst_ir
);

reg CycStb_q;
assign wbm_cyc_o = CycStb_q;
assign wbm_stb_o = CycStb_q;

wire TermRec = wbm_ack_i || wbm_rty_i || wbm_err_i;
always_ff @(posedge Clk_ik) begin
    if (Rst_ir) begin
        CycStb_q <= 1'b0;
        wbm_we_o <= 1'b0;
    end else begin
        CycStb_q <=
                (Control_wr_i && (Write_i || Read_i))
            ||  (CycStb_q && !TermRec);

        if (Control_wr_i) wbm_we_o <= Write_i;
    end
end

always_ff @(posedge Clk_ik) begin
    if (CycStb_q) begin
        Error_o <= wbm_err_i;
        Retry_o <= wbm_rty_i;
        Ack_o   <= wbm_ack_i;
        Data_ob <= wbm_dat_i;
    end

    if (!CycStb_q) begin
        wbm_adr_o <= Addr_ib;
        wbm_sel_o <= Sel_ib;
        wbm_dat_o <= Data_ib;
    end
end

assign Write_o = CycStb_q && wbm_we_o;
assign Read_o = CycStb_q && !wbm_we_o;

endmodule

