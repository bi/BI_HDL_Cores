//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbCrossbar.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//     Wishbone crossbar
//
//
// Example usage:
//
//    localparam c_WbAddressWidth = 25;
//
//    localparam c_WbSlaves = 6;
//    
//    localparam c_WbSlaveA = 0;
//    localparam c_WbSlaveB = 1;
//    localparam c_WbSlaveC = 2;
//    localparam c_WbSlaveD = 3;
//    localparam c_WbSlaveE = 4;
//    localparam c_WbSlaveF = 5;
//    
//    t_WbInterface #(.g_DataWidth(32), .g_AddressWidth(c_WbAddressWidth)) WbMaster_t(Clk_k, Reset_r);
//    t_WbInterface #(.g_DataWidth(32), .g_AddressWidth(c_WbAddressWidth)) WbSlaves_t[0:c_WbSlaves-1](Clk_k, Reset_r);
//    
//    WbCrossbar #(
//        .g_AddressWidth(c_WbAddressWidth),
//        .g_Slaves(c_WbSlaves),
//        .g_SlaveAddresses_mb('{
//            (c_WbAddressWidth)'('h0_0000), // c_WbSlaveA
//            (c_WbAddressWidth)'('h0_0010), // c_WbSlaveB
//            (c_WbAddressWidth)'('h0_0018), // c_WbSlaveC
//            // 0x00_0019: free
//            (c_WbAddressWidth)'('h0_0020), // c_WbSlaveD
//            // 0x00_0024-0x00_00FF: free
//            (c_WbAddressWidth)'('h0_0100), // c_WbSlaveE
//            // 0x00_0200-0x03_FFFF: free
//            (c_WbAddressWidth)'('h4_0000)  // c_WbSlaveF
//            // 0x08_0000-0x1F_FFFF: free
//        }),
//        .g_SlaveMasks_mb('{
//            (c_WbAddressWidth)'('h0_000F), // c_WbSlaveA
//            (c_WbAddressWidth)'('h0_0007), // c_WbSlaveB
//            (c_WbAddressWidth)'('h0_0001), // c_WbSlaveC
//            (c_WbAddressWidth)'('h0_0003), // c_WbSlaveD
//            (c_WbAddressWidth)'('h0_00FF), // c_WbSlaveE
//            (c_WbAddressWidth)'('h3_FFFF)  // c_WbSlaveF
//        })
//    ) i_WbCrossbar (
//        .Master_iot(WbMaster_t),
//        .Slaves_iot(WbSlaves_t)
//    );
//
//    WbSpiMaster i_Spi (
//        .Wb_iot(WbSlaves_t[c_WbSlaveB]),
//        .Spi_iot(Spi_t)
//    );
//
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

// TODO: registered?
// TODO: g_SlaveMasks_mb by number of bits
// TODO: g_SlaveAddresses_mb automatic

// TODO: investigate why this warning is issued
// altera message_off 10665
module WbCrossbar #(
    int g_AddressWidth = 32,
    int g_Slaves,
    logic [g_AddressWidth-1:0] g_SlaveAddresses_mb [0:g_Slaves-1],
    logic [g_AddressWidth-1:0] g_SlaveMasks_mb [0:g_Slaves-1]
) (
    t_WbInterface Master_iot,
    t_WbInterface Slaves_iot [0:g_Slaves-1]
);

    localparam c_DataWidth = $bits(Master_iot.DatMoSi_b);
    localparam c_DefaultSlave = g_Slaves;
    localparam c_SlavesInclDefault = g_Slaves + 1; // plus default slave

    function automatic [c_DataWidth-1:0] f_DataOr;
        input [c_DataWidth-1:0] InputData [0:c_SlavesInclDefault-1];
        f_DataOr = 0;
        for (int k = 0; k < c_SlavesInclDefault; k++) begin
            f_DataOr |= InputData[k];
        end
    endfunction
    
    function automatic int maskValid(logic [g_AddressWidth-1:0] mask);
        // check validity of the mask, i.e. if the mask is in this format: 0..01..1
        int inMask = mask[0];
        maskValid = 1;
        for (int k = 1; k < g_AddressWidth; k++) begin
            if (mask[k] == 0)
                inMask = 0;
            if ((mask[k] == 1) && (inMask == 0))
                maskValid = 0;
        end
    endfunction

    genvar i, j;
    
    logic [c_SlavesInclDefault-1:0] SlaveActive_b;
    logic [c_SlavesInclDefault-1:0] SlaveAck_b, SlaveErr_b, SlaveRty_b, SlaveStall_b;
    logic [c_DataWidth-1:0] SlaveDatMiSo_mb [0:c_SlavesInclDefault-1];

    initial begin
        // check parameters
        assert (g_AddressWidth == $bits(Master_iot.Adr_b))
            else $error("Parameter g_AddressWidth has to be the same as address width of the master WB interface!");
    end

    generate
        for (i = 0; i < g_Slaves; i++) begin: generateForSlaves
            initial begin
                // check all interfaces have same data/address width
                assert (g_AddressWidth == $bits(Slaves_iot[i].Adr_b))
                    else $error("Address width of the slave interface #%0d is not the same as of the master interface!", i);
                assert (c_DataWidth == $bits(Slaves_iot[i].DatMoSi_b))
                    else $error("Data width of the slave interface #%0d is not the same as of the master interface!", i);
                // check all slaves are address aligned according to their size
                assert ((g_SlaveAddresses_mb[i]&(~g_SlaveMasks_mb[i])) == g_SlaveAddresses_mb[i])
                    else $error("Address of the slave #%0d is not aligned according to its size!", i);
                // check masks are valid
                assert (maskValid(g_SlaveMasks_mb[i]) == 1)
                    else $error("Mask of the slave #%0d is not valid!", i);
            end
            for (j = i+1; j < g_Slaves; j++) begin: generateForSlavesInner
                initial begin
                    // check no two slaves overlap
                    assert (((g_SlaveAddresses_mb[i]^g_SlaveAddresses_mb[j]) & (~(g_SlaveMasks_mb[i]|g_SlaveMasks_mb[j]))) != 0)
                        else $error("Address spaces of slaves #%0d and #%0d overlap!", i, j);
                end
            end
            // Address decoder
            assign SlaveActive_b[i] = (Master_iot.Adr_b & (~g_SlaveMasks_mb[i])) == (g_SlaveAddresses_mb[i] & (~g_SlaveMasks_mb[i]));
            // Master -> Slave
            assign Slaves_iot[i].Adr_b = Master_iot.Adr_b & g_SlaveMasks_mb[i];
            assign Slaves_iot[i].DatMoSi_b = Master_iot.DatMoSi_b;
            assign Slaves_iot[i].Sel_b = Master_iot.Sel_b;
            assign Slaves_iot[i].Cyc = Master_iot.Cyc & SlaveActive_b[i];
            assign Slaves_iot[i].Stb = Master_iot.Stb & SlaveActive_b[i];
            assign Slaves_iot[i].We = Master_iot.We & SlaveActive_b[i];
            // Slave -> Master
            assign SlaveAck_b[i] = Slaves_iot[i].Ack & SlaveActive_b[i];
            assign SlaveErr_b[i] = Slaves_iot[i].Err & SlaveActive_b[i];
            assign SlaveRty_b[i] = Slaves_iot[i].Rty & SlaveActive_b[i];
            assign SlaveStall_b[i] = Slaves_iot[i].Stall & SlaveActive_b[i];
            assign SlaveDatMiSo_mb[i] = SlaveActive_b[i] ? Slaves_iot[i].DatMiSo_b : (c_DataWidth)'(0);
        end
    endgenerate
    
    // default action - read from unspecified address
    assign SlaveActive_b[c_DefaultSlave] = ~|SlaveActive_b[0+:c_DefaultSlave];
    assign SlaveAck_b[c_DefaultSlave] = Master_iot.Stb & Master_iot.Cyc & SlaveActive_b[c_DefaultSlave]; // TODO: rather Err than Ack?
    assign SlaveErr_b[c_DefaultSlave] = 0;
    assign SlaveRty_b[c_DefaultSlave] = 0;
    assign SlaveStall_b[c_DefaultSlave] = 0;
    assign SlaveDatMiSo_mb[c_DefaultSlave] = SlaveActive_b[c_DefaultSlave] ? (c_DataWidth)'('hFEEDBEEF) : (c_DataWidth)'(0);
    
    // Slave -> Master (reduction)
    assign Master_iot.Ack = |SlaveAck_b;
    assign Master_iot.Err = |SlaveErr_b;
    assign Master_iot.Rty = |SlaveRty_b;
    assign Master_iot.Stall = |SlaveStall_b;
    assign Master_iot.DatMiSo_b = f_DataOr(SlaveDatMiSo_mb);

endmodule