//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbPipelined2Standard.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      Converts pipelined and standard Wishbone mode: allows to connect a pipelined slave to
//      a standard master.
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

// TODO: investigate why this warning is issued
// altera message_off 10665
module WbPipelined2Standard (
    t_WbInterface Master_iot, // towards standard master
    t_WbInterface Slave_iot   // towards pipelined slave
);

    localparam c_DataWidth = $bits(Master_iot.DatMoSi_b);

    initial begin
        // check both interfaces have same data/address/sel width
        assert ($bits(Master_iot.Adr_b) == $bits(Slave_iot.Adr_b))
            else $error("Address widths of master/slave interfaces are not the same!");
        assert (c_DataWidth == $bits(Slave_iot.DatMoSi_b))
            else $error("Data widths of master/slave interfaces are not the same!");
        assert ($bits(Master_iot.Sel_b) == $bits(Slave_iot.Sel_b))
            else $error("Sel widths of master/slave interfaces are not the same!");
    end

    // signals Master -> Slave
    assign Slave_iot.Cyc = Master_iot.Cyc;
    assign Slave_iot.Adr_b = Master_iot.Adr_b;
    assign Slave_iot.Sel_b = Master_iot.Sel_b;
    assign Slave_iot.We = Master_iot.We;
    assign Slave_iot.DatMoSi_b = Master_iot.DatMoSi_b;

    // signals Slave -> Master
    assign Master_iot.Stall = 1'b0;
    
    // controlling logic
    
    logic [2:0] SlaveAck, MasterAck = 3'b0;
    assign SlaveAck = {Slave_iot.Ack, Slave_iot.Err, Slave_iot.Rty};
    assign {Master_iot.Ack, Master_iot.Err, Master_iot.Rty} = MasterAck;
    logic [c_DataWidth-1:0] SlaveData_b = (c_DataWidth)'(0);
    
    typedef enum {
        s_NoCyc,
        s_WaitForStb,
        s_WaitForAck,
        s_WaitForStbNeg
    } t_State;
    
    t_State State_l, NextState_l;
    localparam t_State InitialState_l = s_NoCyc;
    
    always_ff @(posedge Master_iot.Clk_k) begin
        if (Master_iot.Reset_r)
            State_l <= InitialState_l;
        else
            State_l <= NextState_l;
    end
    
    always_comb begin
        NextState_l <= State_l;
        case (State_l)
            s_NoCyc:
                if (Master_iot.Cyc && ~Master_iot.Stb)
                    NextState_l <= s_WaitForStb;
                else if (Master_iot.Cyc && Master_iot.Stb)
                    NextState_l <= s_WaitForAck;
            s_WaitForStb:
                if (Master_iot.Stb)
                    NextState_l <= s_WaitForAck;
            s_WaitForAck:
                if (|SlaveAck) begin
                    NextState_l <= s_WaitForStbNeg;
                end
            s_WaitForStbNeg:
                if (~Master_iot.Stb) begin
                    NextState_l <= s_WaitForStb;
                end
            default: begin
                NextState_l <= InitialState_l;
            end
        endcase
        if (~Master_iot.Cyc) begin
            NextState_l <= s_NoCyc;
        end;
    end
    
    always_ff @(posedge Master_iot.Clk_k) begin
        if (Master_iot.Reset_r) begin
            MasterAck <= 3'b0;
            SlaveData_b <= (c_DataWidth)'(0);
        end else begin
            MasterAck <= MasterAck;
            SlaveData_b <= SlaveData_b;
            case (State_l)
                s_WaitForAck:
                    if (|SlaveAck) begin
                        MasterAck <= SlaveAck;
                        SlaveData_b <= Slave_iot.DatMiSo_b;
                    end
                s_WaitForStbNeg:
                    if (~Master_iot.Stb) begin
                        MasterAck <= 3'b0;
                        SlaveData_b <= (c_DataWidth)'(0);
                    end
                default: begin
                    MasterAck <= 3'b0;
                    SlaveData_b <= (c_DataWidth)'(0);
                end
            endcase
            if (~Master_iot.Cyc) begin
                MasterAck <= 3'b0;
                SlaveData_b <= (c_DataWidth)'(0);
            end;
        end
    end
    
    assign Slave_iot.Stb = (State_l == s_NoCyc || State_l == s_WaitForStb) ? (Master_iot.Stb & Master_iot.Cyc) : 1'b0;
    assign Master_iot.DatMiSo_b = SlaveData_b;
    
endmodule