//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbRegField.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//  Wishbone Register Field
//  ^^^^^^^^^^^^^^^^^^^^^^^
//
//  Parameters:
//    g_NumberOfRegisters - number of registers
//    g_DataWidth - width of the WB data bus
//    g_DirectionMiso_b - vector, one bit for each register
//      0 - direction MOSI - slave input register (only WB master can write register, default)
//      1 - direction MISO - slave output register (only gateware can write register)
//    g_DefaultValue_mb - array of default values (one for each register)
//    g_AutoClrMask_mb - array of automatic bit-wise clear masks (one for each register)
//      0 - corresponding bit in a register is written and it persist (default)
//      1 - corresponding bit in a register is written and cleared next clock cycle
//          (pulse generator mode)
//
//
// Example usage:
//
//    localparam c_AdrStatus = 0;
//    localparam c_AdrControl = 1;
//    localparam c_AdrConfig = 2;
//    localparam c_AdrDelay = 3;
//
//    localparam c_WbDataWidth = $bits(Wb_t.DatMoSi_b);
//    localparam c_Registers = 4;
//    
//    logic [c_WbDataWidth-1:0] RegisterMiso [0:c_Registers-1];
//    logic [c_WbDataWidth-1:0] RegisterMosi [0:c_Registers-1];
//
//    WbRegField #(
//        .g_NumberOfRegisters(c_Registers),
//        .g_DataWidth(c_WbDataWidth),
//        .g_DirectionMiso_b(4'b0001),
//        .g_AutoClrMask_mb('{(c_WbDataWidth)'(0), (c_WbDataWidth)'(1), (c_WbDataWidth)'(0), (c_WbDataWidth)'(0)})
//    ) i_WbRegField (
//        .Wb_iot(Wb_t),
//        .Register_imb(RegisterMiso),
//        .Register_omb(RegisterMosi)
//    );
//    
//    assign SourcesAnded_o = RegisterMosi[c_AdrControl][1];
//    assign DelayValue_ob = RegisterMosi[c_AdrDelay];
//    
//    always_comb begin
//        RegisterMiso <= '{(c_Registers){(c_WbDataWidth)'(0)}};
//        RegisterMiso[c_AdrStatus][0] <= FlagA;
//        RegisterMiso[c_AdrStatus][1] <= FlagB;
//    end
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

module WbRegField #(
    int g_NumberOfRegisters,
    int g_DataWidth,
    logic [g_NumberOfRegisters-1:0] g_DirectionMiso_b,
    logic [g_DataWidth-1:0] g_DefaultValue_mb [0:g_NumberOfRegisters-1] = '{g_NumberOfRegisters{(g_DataWidth)'(0)}},
    logic [g_DataWidth-1:0] g_AutoClrMask_mb [0:g_NumberOfRegisters-1] = '{g_NumberOfRegisters{(g_DataWidth)'(0)}}
) (
    // Wishbone interface
    t_WbInterface Wb_iot,
    // register inputs
    input [g_DataWidth-1:0] Register_imb [0:g_NumberOfRegisters-1],
    // register outputs
    output [g_DataWidth-1:0] Register_omb [0:g_NumberOfRegisters-1]
);

    logic [g_DataWidth-1:0] Register_mb [0:g_NumberOfRegisters-1] = g_DefaultValue_mb;
    logic WbTransaction, WbTransactionStrobe;
    
    initial begin
        assert (g_DataWidth == $bits(Wb_iot.DatMoSi_b))
            else $error("Parameter g_DataWidth has to be the same as data width of the WB interface!");
    end

    assign WbTransaction = Wb_iot.Cyc & Wb_iot.Stb;
    
    EdgeDetector i_WbTransStrobe (
        .Clk_ik(Wb_iot.Clk_k),
        .Signal_i(WbTransaction),
        .Edge_o(),
        .RisingEdge_o(WbTransactionStrobe),
        .FallingEdge_o()
    );

    always @(posedge Wb_iot.Clk_k)
        if (Wb_iot.Reset_r) begin
            Register_mb <= g_DefaultValue_mb;
        end else begin
            for (int i = 0; i < g_NumberOfRegisters; i = i + 1) begin
                if (g_DirectionMiso_b[i] == 1'b0) begin // MOSI
                    Register_mb[i] <= Register_mb[i] & ~g_AutoClrMask_mb[i];
                    if (WbTransactionStrobe && Wb_iot.We && (Wb_iot.Adr_b == i))
                        Register_mb[i] <= Wb_iot.DatMoSi_b;
                end else begin // MISO
                    Register_mb[i] <= Register_imb[i];
                end
            end
        end

    assign Register_omb = Register_mb;
    
    always_ff @(posedge Wb_iot.Clk_k)
        Wb_iot.Ack <= Wb_iot.Stb & Wb_iot.Cyc;

    assign Wb_iot.Err = 1'b0;
    assign Wb_iot.Rty = 1'b0;
    assign Wb_iot.Stall = 1'b0;

    always_ff @(posedge Wb_iot.Clk_k)
        if (WbTransaction & ~Wb_iot.We) begin
            Wb_iot.DatMiSo_b <= Register_mb[Wb_iot.Adr_b];
        end

endmodule