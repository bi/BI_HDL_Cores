//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbDummy.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      Dummy Wishbone slave
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

module WbDummy (
    t_WbInterface WbSlave_iot
);

    localparam c_DataWidth = $bits(WbSlave_iot.DatMiSo_b);
    localparam c_DefaultData = (c_DataWidth)'('hDEADBEEF);

    assign WbSlave_iot.DatMiSo_b = c_DefaultData;
    assign WbSlave_iot.Ack = WbSlave_iot.Cyc && WbSlave_iot.Stb;
    assign WbSlave_iot.Err = 'b0;
    assign WbSlave_iot.Rty = 'b0;
    assign WbSlave_iot.Stall = 'b0;

endmodule
