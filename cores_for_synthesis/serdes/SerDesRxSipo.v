//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesRxSipo.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 10/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Serial Input Parallel Output (SIPO) of the Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesRxSipo
//====================================  Global Parameters  ===================================//
#(  // Sampler:
    parameter g_RxEdges2Lock_b8              = 8'h20, // Comment: 32 Edges. 
              g_RxNoEdges2LossOfLock_b8      = 8'hFF, // Comment: 256/16 = 16 Unit Intervals with NO transition.
    // Control:                        
    parameter g_IdleNotLocked_b8             = 8'h1C, // Comment: The Idle Not Locked is the K character K28.0.     
              g_IdleLocked_b8                = 8'hBC, // Comment: The Idle Locked is the K character K28.5.     
              g_Header_b4                    = 4'h 5, // Comment: The header does not need to be DC balanced.
              g_CorrectBytes2Lock_b8         = 8'h20,      
              g_NoCorrectBytes2LossOfLock_b8 = 8'h20,
              g_BytesPerFrame_b3             = 3'h 6)  
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input        ClkSerDes_ik,
    input        Reset_ira,
    
    //==== Control Path ====//
    
    output       RxLocked_o,
    output       SerialLinkUp_o,
    output       EnRxParallel_o,    
    output       HeaderFound_o,    
    
    //==== Data Path ====//
    
    input        Rx_i,
    output [7:0] RxByte_ob8
    
); 
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

// Sampler:
wire       RxBitLocked;
wire       EnRxSerial;
wire       RxBit;
// Aligner:
wire [9:0] Rx8b10b_b10;
// 8b10b Decoder:   
wire       RunningDisparity8b10b;
wire [7:0] RxByte_b8;
wire       KCharacterFlag;
// Control:  
wire [3:0] Rx8b10bAddr_cb4;
    
//=======================================  User Logic  =======================================//

// Sampler:
SerDesRxSipoSampler #(
    .g_RxEdges2Lock_b8         (g_RxEdges2Lock_b8),
    .g_RxNoEdges2LossOfLock_b8 (g_RxNoEdges2LossOfLock_b8))
i_Sampler (
    .ClkSerDes_ik              (ClkSerDes_ik),
    .Reset_ira                 (Reset_ira), 
    .RxBitLocked_oq            (RxBitLocked),
    .EnRxSerial_o              (EnRxSerial),
    .Rx_i                      (Rx_i),
    .RxBit_oq                  (RxBit));

// Aligner:
SerDesRxSipoAligner i_Aligner (
    .ClkSerDes_ik     (ClkSerDes_ik),
    .Reset_ira        (Reset_ira || ~RxBitLocked), 
    .EnRxSerial_i     (EnRxSerial),
    .EnRxParallel_i   (EnRxParallel_o),
    .Rx8b10bAddr_icb4 (Rx8b10bAddr_cb4),
    .RxBit_i          (RxBit),
    .Rx8b10b_oqb10    (Rx8b10b_b10));    
    
// 8b10b Decoder:
Decoder8b10b i_8b10Dec (
    .clk         (ClkSerDes_ik),
    .rst         (Reset_ira || ~RxBitLocked),
    .din_ena     (EnRxParallel_o),                            
    .din_dat     (Rx8b10b_b10),   
    .din_rd      (RunningDisparity8b10b),      
    .dout_val    (), 
    .dout_kerr   (), 
    .dout_dat    (RxByte_b8),  
    .dout_k      (KCharacterFlag),
    .dout_rderr  (),
    .dout_rdcomb (),
    .dout_rdreg  (RunningDisparity8b10b));     

// Control:
SerDesRxSipoControl #(
    .g_IdleNotLocked_b8             (g_IdleNotLocked_b8),
    .g_IdleLocked_b8                (g_IdleLocked_b8),
    .g_Header_b4                    (g_Header_b4),
    .g_CorrectBytes2Lock_b8         (g_CorrectBytes2Lock_b8),
    .g_NoCorrectBytes2LossOfLock_b8 (g_NoCorrectBytes2LossOfLock_b8),
    .g_BytesPerFrame_b3             (g_BytesPerFrame_b3))
i_Control (
    .ClkSerDes_ik                   (ClkSerDes_ik),
    .Reset_ira                      (Reset_ira || ~RxBitLocked),     
    .EnRxSerial_i                   (EnRxSerial),
    .KCharacterFlag_i               (KCharacterFlag),
    .Rx8b10bAddr_ocb4               (Rx8b10bAddr_cb4),
    .RxLocked_oq                    (RxLocked_o),
    .SerialLinkUp_oq                (SerialLinkUp_o),
    .EnRxParallel_o                 (EnRxParallel_o),
    .HeaderFound_oq                 (HeaderFound_o),
    .RxByte_ib8                     (RxByte_b8),
    .RxByte_oqb8                    (RxByte_ob8));
    
endmodule