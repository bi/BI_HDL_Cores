//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDes.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 07/09/16      1.3          M. Barros Marin    Removed TX BERT port.
//     - 31/08/16      1.2          M. Barros Marin    Added Loss Of Lock (LOL) check in BERT.
//     - 26/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDes
//====================================  Global Parameters  ===================================//
#( // Common:    
    parameter g_IdleNotLocked_b8             =  8'h1C, // Comment: The Idle Not Locked is the K character K28.0.     
              g_IdleLocked_b8                =  8'hBC, // Comment: The Idle Locked is the K character K28.5.         
              g_Header_b4                    =  4'h 5, // Comment: The header does not need to be DC balanced.
    // Transmitter specific:    
    parameter g_TxFifoAddWith                =  4,     // Comment: Default: 16 LW positions (64 Bytes).   
    // Receiver specific:    
    parameter g_RxEdges2Lock_b8              =  8'h20, // Comment: 32 Edges. 
              g_RxNoEdges2LossOfLock_b8      =  8'hFF, // Comment: 256/16 = 16 Unit Intervals with NO transition. 
              g_CorrectBytes2Lock_b8         =  8'h20,
              g_NoCorrectBytes2LossOfLock_b8 =  8'h20, 
              g_RxFifoAddWith                =  4)     // Comment: Default: 16 LW positions (64 Bytes).                
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input         ClkUser_ik,
    input         ClkSerDes_ik,
    input         Reset_ira,
    
    //==== Transmitter====//

    input         TxWrEnFifo_i,
    output        TxEmptyFifo_o,
    output        TxFullFifo_o,
    output        TxBusy_o,
    input         TxErrInjBert_i,
    input  [31:0] TxDataFifo_ib32,
    output        Tx_o,
    
    //==== Receiver ====//
    
    output        RxLocked_o,
    input         RxRdEnFifo_i,
    output        RxEmptyFifo_o,
    output        RxFullFifo_o,
    output        RxCrcChkError_o,
    output        RxBusy_o,
    input         RxEnBert_i, // Comment: Byte Error Ratio Test (BERT): Tests only 8b10b->PISO->SIPO->8b10b.
    output [63:0] RxBertByteErrors_oc64,
    output [63:0] RxBertBytesReceived_oc64,
    output        RxBertLosFlag_o,
    output [15:0] RxBertLosCntr_oc16,
    input         Rx_i,
    output [31:0] RxDataFifo_ob32,
    output        SerialLinkUp_o
); 
//=======================================  User Logic  =======================================//

// Transmitter:
SerDesTx #(
    .g_IdleNotLocked_b8             (g_IdleNotLocked_b8),
    .g_IdleLocked_b8                (g_IdleLocked_b8),
    .g_Header_b4                    (g_Header_b4),
    .g_TxFifoAddWith                (g_TxFifoAddWith))
i_SerDesTx (
    .ClkUser_ik                     (ClkUser_ik),
    .ClkSerDes_ik                   (ClkSerDes_ik),
    .Reset_ira                      (Reset_ira), 
    .RxLocked_i                     (RxLocked_o),
    .TxWrEnFifo_i                   (TxWrEnFifo_i),
    .TxEmptyFifo_o                  (TxEmptyFifo_o),
    .TxFullFifo_o                   (TxFullFifo_o),
    .TxBusy_o                       (TxBusy_o),
    .TxErrInjBert_i                 (TxErrInjBert_i),
    .TxDataFifo_ib32                (TxDataFifo_ib32),
    .Tx_o                           (Tx_o));

// Receiver:
SerDesRx #(
    .g_RxEdges2Lock_b8              (g_RxEdges2Lock_b8),
    .g_RxNoEdges2LossOfLock_b8      (g_RxNoEdges2LossOfLock_b8),
    .g_IdleNotLocked_b8             (g_IdleNotLocked_b8),
    .g_IdleLocked_b8                (g_IdleLocked_b8),
    .g_Header_b4                    (g_Header_b4),
    .g_CorrectBytes2Lock_b8         (g_CorrectBytes2Lock_b8),
    .g_NoCorrectBytes2LossOfLock_b8 (g_NoCorrectBytes2LossOfLock_b8),   
    .g_RxFifoAddWith                (g_RxFifoAddWith))
i_SerDesRx (
    .ClkUser_ik                     (ClkUser_ik),
    .ClkSerDes_ik                   (ClkSerDes_ik),
    .Reset_ira                      (Reset_ira), 
    .RxLocked_o                     (RxLocked_o),
    .RxRdEnFifo_i                   (RxRdEnFifo_i),
    .RxEmptyFifo_o                  (RxEmptyFifo_o),
    .RxFullFifo_o                   (RxFullFifo_o),
    .RxCrcChkError_o                (RxCrcChkError_o),
    .RxBusy_o                       (RxBusy_o),
    .RxEnBert_i                     (RxEnBert_i),
    .RxBertByteErrors_oc64          (RxBertByteErrors_oc64),
    .RxBertBytesReceived_oc64       (RxBertBytesReceived_oc64),
    .RxBertLosFlag_oq               (RxBertLosFlag_o),  
    .RxBertLosCntr_oc16             (RxBertLosCntr_oc16),    
    .SerialLinkUp_o                 (SerialLinkUp_o),
    .Rx_i                           (Rx_i),
    .RxDataFifo_ob32                (RxDataFifo_ob32));
    
endmodule