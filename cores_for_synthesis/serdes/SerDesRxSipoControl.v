//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesRxSipoControl.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 26/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Serial Input Parallel Output (SIPO) Control of the Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesRxSipoControl
//====================================  Global Parameters  ===================================//
#(  parameter g_IdleNotLocked_b8             = 8'h1C, // Comment: The Idle Not Locked is the K character K28.0.
              g_IdleLocked_b8                = 8'hBC, // Comment: The Idle Locked is the K character K28.5.    
              g_Header_b4                    = 4'h 5, // Comment: The header does not need to be DC balanced.
              g_CorrectBytes2Lock_b8         = 8'h20,
              g_NoCorrectBytes2LossOfLock_b8 = 8'h20,
              g_BytesPerFrame_b3             = 3'h 6)            
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input            ClkSerDes_ik,
    input            Reset_ira,
    
    //==== Control Path ====//
    
    input            EnRxSerial_i,
    input            KCharacterFlag_i,
    output reg [3:0] Rx8b10bAddr_ocb4,
    output reg       RxLocked_oq,
    output reg       SerialLinkUp_oq,
    output           EnRxParallel_o,
    output reg       HeaderFound_oq,
        
    //==== Data Path ====//
    
    input      [7:0] RxByte_ib8,
    output reg [7:0] RxByte_oqb8
    
); 
//=======================================  Declarations  =====================================//

//==== Local parameters ====//

localparam g_DeSerialiationFactor_b5 = 5'hA; // Comment: 1 to 10 bits.

// FSM states:
localparam s_Idle      = 2'h0,
           s_Alignning = 2'h1,
           s_Aligned   = 2'h2;

//==== Wires & Regs ====//

// Control path:
reg  [4:0] EnRxSerialCntr_c5;
wire       EnRxParallel;
reg  [1:0] EnRxParallel_db2;
reg  [1:0] State_qb2, NextState_ab2;
reg  [7:0] CorrectBytes2LockCntr_c8;
reg  [7:0] NoCorrectBytes2LossOfLockCntr_c8;
reg  [2:0] RxFrameBytesCntr_c3;
    
//=======================================  User Logic  =======================================//
 
//==== Data Path ====//
 
// Registered RX Byte output:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)      RxByte_oqb8 <= #1 8'h0;
    else if (EnRxParallel_o) RxByte_oqb8 <= #1 RxByte_ib8;
 
//==== Control Path ====//

// Enable RX Parallel generation:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)                                           EnRxSerialCntr_c5 <= #1 5'h0;
    else if (EnRxSerial_i)
            if (EnRxSerialCntr_c5 == g_DeSerialiationFactor_b5-1) EnRxSerialCntr_c5 <= #1 5'h0;
            else                                                  EnRxSerialCntr_c5 <= #1 EnRxSerialCntr_c5+1;

assign EnRxParallel = (EnRxSerialCntr_c5 == g_DeSerialiationFactor_b5-1) ? 1'b1 : 1'b0;
    
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) EnRxParallel_db2 <= #1 2'h0;
    else             EnRxParallel_db2 <= #1 {EnRxParallel_db2[0],EnRxParallel};
    
assign EnRxParallel_o = (EnRxParallel_db2 == 2'b01) ? 1'b1 : 1'b0;
    
// Serial Input Parallel Output (SIPO) control FSM:
always @(posedge ClkSerDes_ik or posedge Reset_ira) State_qb2 <= #1 Reset_ira ? s_Idle : NextState_ab2;
always @* begin
    NextState_ab2 = State_qb2;
    case(State_qb2)
        s_Idle:                                                                              NextState_ab2 = s_Alignning;
        s_Alignning: if (CorrectBytes2LockCntr_c8         == g_CorrectBytes2Lock_b8)         NextState_ab2 = s_Aligned;
        s_Aligned:   if (NoCorrectBytes2LossOfLockCntr_c8 == g_NoCorrectBytes2LossOfLock_b8) NextState_ab2 = s_Idle;
        default:                                                                             NextState_ab2 = s_Idle;
    endcase
end
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin
        CorrectBytes2LockCntr_c8                             <= #1 8'h0;
        NoCorrectBytes2LossOfLockCntr_c8                     <= #1 8'h0;
        Rx8b10bAddr_ocb4                                     <= #1 4'h0;
        RxLocked_oq                                          <= #1 1'b0;    
        SerialLinkUp_oq                                      <= #1 1'b0;    
        HeaderFound_oq                                       <= #1 1'b0; 
        RxFrameBytesCntr_c3                                  <= #1 3'h0;
    end else begin
        case(State_qb2) 
            s_Idle: begin
                CorrectBytes2LockCntr_c8                     <= #1 8'h0;
                NoCorrectBytes2LossOfLockCntr_c8             <= #1 8'h0;
                Rx8b10bAddr_ocb4                             <= #1 4'h0;
                RxLocked_oq                                  <= #1 1'b0;    
                SerialLinkUp_oq                              <= #1 1'b0;    
                HeaderFound_oq                               <= #1 1'b0;
                RxFrameBytesCntr_c3                          <= #1 3'h0;
            end
            s_Alignning: begin
                if (EnRxParallel_o) begin
                    // Comment: The RX Byte aligner only locks over Idle characters (K28.0 & K28.5):
                    if ((KCharacterFlag_i) && ((RxByte_ib8 == g_IdleNotLocked_b8) || (RxByte_ib8 == g_IdleLocked_b8))) begin
                        CorrectBytes2LockCntr_c8             <= #1 CorrectBytes2LockCntr_c8+1;
                        NoCorrectBytes2LossOfLockCntr_c8     <= #1 8'h0;
                    end else begin
                        CorrectBytes2LockCntr_c8             <= #1 8'h0;
                        if (NoCorrectBytes2LossOfLockCntr_c8 == g_NoCorrectBytes2LossOfLock_b8) begin
                            Rx8b10bAddr_ocb4                 <= #1 Rx8b10bAddr_ocb4+1;
                            NoCorrectBytes2LossOfLockCntr_c8 <= #1 8'h0;
                        end else begin
                            NoCorrectBytes2LossOfLockCntr_c8 <= #1 NoCorrectBytes2LossOfLockCntr_c8+1;
                        end
                    end
                end
            end
            s_Aligned: begin
                if (EnRxParallel_o) begin
                    RxLocked_oq                              <= #1 1'b1;                    
                    HeaderFound_oq                           <= #1 1'b0;                    
                    if (RxFrameBytesCntr_c3 < g_BytesPerFrame_b3-1) RxFrameBytesCntr_c3 <= #1 RxFrameBytesCntr_c3+1;
                    NoCorrectBytes2LossOfLockCntr_c8         <= #1 NoCorrectBytes2LossOfLockCntr_c8+1;
                    // Comment: The RX Byte aligner keeps the lock over Idle characters (K28.0 & K28.5) OR Header (4'h5).
                    if (KCharacterFlag_i) begin
                        if (RxByte_ib8 == g_IdleNotLocked_b8) begin
                            SerialLinkUp_oq                  <= #1 1'b0;  
                            NoCorrectBytes2LossOfLockCntr_c8 <= #1 8'h0;
                        end else if (RxByte_ib8 == g_IdleLocked_b8) begin   
                            SerialLinkUp_oq                  <= #1 1'b1;  
                            NoCorrectBytes2LossOfLockCntr_c8 <= #1 8'h0;
                        end
                    end else begin
                        // Comment: Waits at least the number of Bytes of the RX Frame until finding a new Header
                        //          in order spurious triggers of the RX SerDes FSM.
                        if (~KCharacterFlag_i && (RxByte_ib8[7:4] == g_Header_b4) && (RxFrameBytesCntr_c3 == g_BytesPerFrame_b3-1)) begin                            
                            HeaderFound_oq                       <= #1 1'b1;
                            NoCorrectBytes2LossOfLockCntr_c8     <= #1 8'h0;
                            RxFrameBytesCntr_c3                  <= #1 3'h0;
                        end
                    end
                end
            end
            default: begin
            end
        endcase
    end
    
endmodule