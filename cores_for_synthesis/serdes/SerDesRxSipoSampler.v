//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesRxSipoSampler.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 10/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     RX Sampler of the Serializer/Deserializer (SerDes) module. This module samples the 
//     incoming serial bits.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesRxSipoSampler 
//====================================  Global Parameters  ===================================//
#(  parameter g_RxEdges2Lock_b8         = 8'h20, // Comment: 32 Edges. 
              g_RxNoEdges2LossOfLock_b8 = 8'hFF) // Comment: 256/16 = 16 Unit Intervals with NO transition.     
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input      ClkSerDes_ik,
    input      Reset_ira,
    
    //==== Control Path ====//
    
    output reg RxBitLocked_oq,
    output     EnRxSerial_o,    
    
    //==== Data Path ====//
    
    input      Rx_i,
    output reg RxBit_oq
    
); 
//=======================================  Declarations  =====================================//

//==== Local parameters ====//

localparam c_SamplingPoint_b4 = 4'h6; 

// FSM states:
localparam s_Idle    = 2'h0,
           s_Locking = 2'h1,
           s_Locked  = 2'h2;

//==== Wires & Regs ====//

// Data path:
reg  [1:0] Rx_xb2;
wire       RxGlitchFree;
reg  [1:0] Rx_db2;
// Control path:
wire       RxEdgeDetected;
reg  [3:0] SamplingPointCntr_c4;
reg        EnRxSerial_q;
reg  [7:0] EnRxSerial_db8;
reg  [1:0] State_qb2, NextState_ab2;
reg  [7:0] RxEdges2LockCntr_c8;
reg  [7:0] RxNoEdges2LossOfLockCntr_c8;
    
//=======================================  User Logic  =======================================//

//==== Data Path ====//

// Serial input data synchronizer:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) Rx_xb2 <= #1 2'h0;
    else             Rx_xb2 <= #1 {Rx_i,Rx_xb2[1]};

// Serial input data glitch filter:
GlitchFilter i_GlitchFilter (
    .Clk_ik    (ClkSerDes_ik),
    .Reset_ira (Reset_ira),
    .Input_i   (Rx_xb2[0]),
    .Output_oq (RxGlitchFree)); 

// Serial input data edge detector Right Shift Register (RSR):
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) Rx_db2 <= #1 2'h0;
    else             Rx_db2 <= #1 {RxGlitchFree,Rx_db2[1]};    
    
// Serial input data sample Register:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)    RxBit_oq <= #1 1'b0;
    else if (EnRxSerial_q) RxBit_oq <= #1 Rx_db2[0];

//==== Control Path ====//

// Serial input data edge detector control:
assign RxEdgeDetected = Rx_db2[1]^Rx_db2[0];

// Sampling Point Counter generation:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if        (Reset_ira)      SamplingPointCntr_c4 <= #1 4'h0;
    else if   (RxEdgeDetected) SamplingPointCntr_c4 <= #1 4'h0;  
         else                  SamplingPointCntr_c4 <= #1 SamplingPointCntr_c4+1;
    
// Enable RX Serial generation:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira)  EnRxSerial_q <= #1 1'b0;
    else              EnRxSerial_q <= #1 ((SamplingPointCntr_c4 == c_SamplingPoint_b4) && RxBitLocked_oq);

// Enable RX Serial delay Right Shift Register (RSR):
// Comment: Delay of 8 clock cycled with the purpose of sampling the output bit in the middle of the 
//          sampling window by the flip-flops of the next module.
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) EnRxSerial_db8 <= #1 8'h0;
    else             EnRxSerial_db8 <= #1 {EnRxSerial_q,EnRxSerial_db8[7:1]};
    
assign EnRxSerial_o = EnRxSerial_db8[0];
    
// SerDes RX bit lock control FSM:
always @(posedge ClkSerDes_ik or posedge Reset_ira) State_qb2 <= #1 Reset_ira ? s_Idle : NextState_ab2;
always @* begin
    NextState_ab2 = State_qb2;
    case(State_qb2)
        s_Idle:                                                                  NextState_ab2 = s_Locking;
        s_Locking: if (RxEdges2LockCntr_c8         == g_RxEdges2Lock_b8)         NextState_ab2 = s_Locked;
        s_Locked:  if (RxNoEdges2LossOfLockCntr_c8 == g_RxNoEdges2LossOfLock_b8) NextState_ab2 = s_Idle;
        default:                                                                 NextState_ab2 = s_Idle;
    endcase
end
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin      
        RxBitLocked_oq                              <= #1 1'b0;
        RxEdges2LockCntr_c8                         <= #1 8'h0;
        RxNoEdges2LossOfLockCntr_c8                 <= #1 8'h0;
    end else begin
        case(State_qb2) 
            s_Idle: begin
                RxBitLocked_oq                      <= #1 1'b0;
                RxEdges2LockCntr_c8                 <= #1 8'h0;
                RxNoEdges2LossOfLockCntr_c8         <= #1 8'h0;
            end
            s_Locking: begin
                if (RxEdgeDetected) begin
                    RxEdges2LockCntr_c8             <= #1 RxEdges2LockCntr_c8+1;
                    RxNoEdges2LossOfLockCntr_c8     <= #1 8'h0;
                end else begin
                    if (RxNoEdges2LossOfLockCntr_c8 == g_RxNoEdges2LossOfLock_b8) begin
                        RxEdges2LockCntr_c8         <= #1 8'h0;
                        RxNoEdges2LossOfLockCntr_c8 <= #1 8'h0;
                    end else begin
                        RxNoEdges2LossOfLockCntr_c8 <= #1 RxNoEdges2LossOfLockCntr_c8+1;
                    end
                end
            end
            s_Locked: begin
                RxBitLocked_oq                      <= #1 1'b1;
                if (RxEdgeDetected) RxNoEdges2LossOfLockCntr_c8 <= #1 8'h0;
                else                RxNoEdges2LossOfLockCntr_c8 <= #1 RxNoEdges2LossOfLockCntr_c8+1;
            end
            default: begin
            end
        endcase
    end
    
endmodule