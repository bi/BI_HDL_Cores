//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesRx.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 08/12/16      1.3          M. Barros Marin    IP cores update,
//     - 31/08/16      1.2          M. Barros Marin    Added Loss Of Lock (LOL) check in BERT.
//     - 15/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Receiver of the Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesRx 
//====================================  Global Parameters  ===================================//
#( 
    parameter g_RxEdges2Lock_b8              = 8'h20, // Comment: 32 Edges. 
              g_RxNoEdges2LossOfLock_b8      = 8'hFF, // Comment: 256/16 = 16 Unit Intervals with NO transition.               
              g_IdleNotLocked_b8             = 8'h1C, // Comment: The Idle Not Locked is the K character K28.0.       
              g_IdleLocked_b8                = 8'hBC, // Comment: The Idle Locked is the K character K28.5.           
              g_Header_b4                    = 4'h 5, // Comment: The header does not need to be DC balanced.
              g_CorrectBytes2Lock_b8         = 8'h20,
              g_NoCorrectBytes2LossOfLock_b8 = 8'h20,  
              g_RxFifoAddWith                = 4)     // Comment: Default: 16 LW positions (64 Bytes).  
    
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input             ClkUser_ik,
    input             ClkSerDes_ik,
    input             Reset_ira,
    
    //==== Control Path ====//
    
    output            RxLocked_o,
    input             RxRdEnFifo_i,
    output            RxEmptyFifo_o,
    output            RxFullFifo_o,
    output            RxCrcChkError_o,
    output            RxBusy_o,
    //--
    input             RxEnBert_i, // Comment: Byte Error Ratio Test (BERT): Tests only 8b10b->PISO->SIPO->8b10b.
    output reg [63:0] RxBertByteErrors_oc64,
    output reg [63:0] RxBertBytesReceived_oc64,
    output reg        RxBertLosFlag_oq,
    output reg [15:0] RxBertLosCntr_oc16,
    //--
    output            SerialLinkUp_o,
    
    //==== Data Path ====//
    
    input             Rx_i,
    output     [31:0] RxDataFifo_ob32
    
); 
//=======================================  Declarations  =====================================//

integer    i;

//==== Local parameters ====//

localparam c_BytesPerFrame   = 6;

// SerDes RX control FSM:
localparam s_Idle            = 3'h0,
           s_StoreRxFrame    = 3'h1,
           s_AssemblyRxFrame = 3'h2,
           s_CheckCrc        = 3'h3,
           s_WriteFifo       = 3'h4;
           
//==== Wires & Regs ====//

// Data path:
wire [ 7:0] RxByte_b8;
reg  [ 7:0] RxFrameBytesRegs_q6b8 [0:5];
reg  [43:0] RxDataCrc_qb44;
wire [11:0] CrcChk_b12;
reg  [31:0] RxData_db32;
wire [35:0] DinFifo_b36;
wire [35:0] DoutFifo_b36;
reg  [35:0] DoutFifo_qb36;
// Control path:
wire        RxLocked;
reg  [ 2:0] RxLocked_xb3;
wire [ 1:0] a_RxLocked_db2;
wire        EnRxParallel;
wire        HeaderFound;
wire        Reset_ra;
reg  [ 1:0] EnRxBert_xb2;
wire        FullRxFifo;
reg  [ 1:0] FullRxFifo_xb2;
reg  [ 2:0] State_qb3, NextState_ab3;
reg         RxBusy_q;
reg  [ 1:0] RxBusy_xb2;
reg         EnRxFrameBytesRegs_q;
reg  [ 2:0] RxFrameBytesRegsAddr_c3;
reg         EnRxDataCrcReg_q;
reg         EnCrcChk_q;
reg         CrcChkError_q;
reg         RstCrcChk_q;
reg  [ 1:0] StatusCheckCrc_c2;
reg         WrEnRxFifo_q;
    
//=======================================  User Logic  =======================================//

//==== Data Path ====//

// Serial Input Parallel Output (SIPO):
SerDesRxSipo #(
    .g_RxEdges2Lock_b8              (g_RxEdges2Lock_b8),
    .g_RxNoEdges2LossOfLock_b8      (g_RxNoEdges2LossOfLock_b8),
    .g_IdleNotLocked_b8             (g_IdleNotLocked_b8),
    .g_IdleLocked_b8                (g_IdleLocked_b8),
    .g_Header_b4                    (g_Header_b4),
    .g_CorrectBytes2Lock_b8         (g_CorrectBytes2Lock_b8),
    .g_NoCorrectBytes2LossOfLock_b8 (g_NoCorrectBytes2LossOfLock_b8),
    .g_BytesPerFrame_b3             (c_BytesPerFrame))
i_Sipo (
    .ClkSerDes_ik                   (ClkSerDes_ik),
    .Reset_ira                      (Reset_ira),
    .RxLocked_o                     (RxLocked),
    .SerialLinkUp_o                 (SerialLinkUp_o),
    .EnRxParallel_o                 (EnRxParallel),
    .HeaderFound_o                  (HeaderFound),
    .Rx_i                           (Rx_i),
    .RxByte_ob8                     (RxByte_b8));

// RX Data Bytes bank of registers:
always @(posedge ClkSerDes_ik or posedge Reset_ra)
    if      (Reset_ra)             for (i=0;i<6;i=i+1) RxFrameBytesRegs_q6b8[i]   <= #1 8'h0;
    else if (EnRxFrameBytesRegs_q) RxFrameBytesRegs_q6b8[RxFrameBytesRegsAddr_c3] <= #1 RxByte_b8;

// Registered RX Data Long Word (LW) & CRC assembly (de-inverted):
// Comment; The RX Frame is 48 bits long with the following fields: |Header(4-bit)|Data(32-bit)|CRC(12-bit)|. 
always @(posedge ClkSerDes_ik or posedge Reset_ra)
    if      (Reset_ra)         RxDataCrc_qb44 <= #1 44'h0;
    else if (EnRxDataCrcReg_q) RxDataCrc_qb44 <= #1 {RxFrameBytesRegs_q6b8[0][3:0],RxFrameBytesRegs_q6b8[1],
                                                     RxFrameBytesRegs_q6b8[2],     RxFrameBytesRegs_q6b8[3],
                                                     RxFrameBytesRegs_q6b8[4],     RxFrameBytesRegs_q6b8[5]}; 

// Dummy signal for Error Injection test:
//wire   [43:0] DummyErrorRxDataCrc_b44;
//assign        DummyErrorRxDataCrc_b44[43:1] =  RxDataCrc_qb44[43:1];
//assign        DummyErrorRxDataCrc_b44[   0] = ~RxDataCrc_qb44[   0];

// CRC Checker:    
Crc12LwChk i_CrcChk (
    .data_in (RxDataCrc_qb44),//DummyErrorRxDataCrc_b44),//
    .crc_en  (EnCrcChk_q),
    .crc_out (CrcChk_b12),
    .rst     (Reset_ra || RstCrcChk_q),
    .clk     (ClkSerDes_ik)); 
  
// RX Data delay register:
// Comment: This register add a clock cycle of delay for matching the RX Data with the CRC Checker output.
always @(posedge ClkSerDes_ik or posedge Reset_ra)
    if   (Reset_ra) RxData_db32 <= #1 32'h0;
    else            RxData_db32 <= #1 RxDataCrc_qb44[43:12];    
  
// RX FIFO:
assign DinFifo_b36 = {CrcChkError_q, 3'b000,RxData_db32};

generic_fifo_dc_gray_mod #(
    .dw       (36),
    .aw       (g_RxFifoAddWith))
i_RxFifo (
    .rd_clk   (ClkUser_ik),
    .wr_clk   (ClkSerDes_ik),
    .rst      (~Reset_ra),
    .clr      (1'b0),
    .din      (DinFifo_b36),
    .we       (WrEnRxFifo_q && ~FullRxFifo),
    .dout     (DoutFifo_b36),
    .re       (RxRdEnFifo_i && ~RxEmptyFifo_o),
    .full     (FullRxFifo),
    .empty    (RxEmptyFifo_o),
    .wr_level (),
    .rd_level ());
    
// Registered RX FIFO output:
always @(posedge ClkUser_ik or posedge Reset_ra)
    if   (Reset_ra) DoutFifo_qb36 <= #1 36'h0;
    else            DoutFifo_qb36 <= #1 DoutFifo_b36;

// Output assignments:    
assign RxCrcChkError_o = DoutFifo_qb36[35];
assign RxDataFifo_ob32 = DoutFifo_qb36[31:0];    
    
//==== Control Path ====//

// Reset generation:
assign Reset_ra = Reset_ira || ~RxLocked;        

// RX locked Synchronizer & Edge detector:
always @(posedge ClkUser_ik or posedge Reset_ra)
    if   (Reset_ra) RxLocked_xb3 <= #1 3'h0;
    else            RxLocked_xb3 <= #1 {RxLocked_xb3[1:0],RxLocked};

assign a_RxLocked_db2 = RxLocked_xb3[2:1];
assign RxLocked_o     = a_RxLocked_db2[1];

// RX Byte Error Ratio Test (BERT) enable synchronizer:
always @(posedge ClkSerDes_ik or posedge Reset_ra)
    if   (Reset_ra) EnRxBert_xb2 <= #1 2'h0;
    else            EnRxBert_xb2 <= #1 {EnRxBert_xb2[0],RxEnBert_i};

// RX FIFO full synchronizer:
always @(posedge ClkUser_ik or posedge Reset_ra)
    if   (Reset_ra) FullRxFifo_xb2 <= #1 2'h0;
    else            FullRxFifo_xb2 <= #1 {FullRxFifo_xb2[0],FullRxFifo};

assign RxFullFifo_o = FullRxFifo_xb2[1];

// RX busy synchronizer:
always @(posedge ClkUser_ik or posedge Reset_ra)
    if   (Reset_ra) RxBusy_xb2 <= #1 2'h0;
    else            RxBusy_xb2 <= #1 {RxBusy_xb2[0],RxBusy_q};

assign RxBusy_o = RxBusy_xb2[1];    

// SerDes RX control FSM:
always @(posedge ClkSerDes_ik or posedge Reset_ra) State_qb3 <= #1 Reset_ra ? s_Idle : NextState_ab3;
always @* begin
    NextState_ab3 = State_qb3;
    case(State_qb3)
        s_Idle:            if (HeaderFound)                                NextState_ab3 = s_StoreRxFrame;
        s_StoreRxFrame:    if (RxFrameBytesRegsAddr_c3 == c_BytesPerFrame) NextState_ab3 = s_AssemblyRxFrame;
        s_AssemblyRxFrame:                                                 NextState_ab3 = s_CheckCrc;
        s_CheckCrc:        if (&StatusCheckCrc_c2)                         NextState_ab3 = s_WriteFifo;
        s_WriteFifo:                                                       NextState_ab3 = s_Idle;
        default:                                                           NextState_ab3 = s_Idle;
    endcase
end
always @(posedge ClkSerDes_ik or posedge Reset_ra)
    if (Reset_ra) begin      
        RxBusy_q                            <= #1 1'b0;
        EnRxFrameBytesRegs_q                <= #1 1'b0;
        RxFrameBytesRegsAddr_c3             <= #1 3'h0;
        EnRxDataCrcReg_q                    <= #1 1'b0;
        EnCrcChk_q                          <= #1 1'b0;
        CrcChkError_q                       <= #1 1'b0;
        RstCrcChk_q                         <= #1 1'b0;
        StatusCheckCrc_c2                   <= #1 2'h0;
        WrEnRxFifo_q                        <= #1 1'b0;
    end else begin
        case(State_qb3) 
            s_Idle: begin
                RxBusy_q                    <= #1 1'b0;
                CrcChkError_q               <= #1 1'b0;
                WrEnRxFifo_q                <= #1 1'b0;
                if (NextState_ab3 != State_qb3) begin
                    RxBusy_q                <= #1 1'b1; 
                    EnRxFrameBytesRegs_q    <= #1 1'b1;                    
                end
            end
            s_StoreRxFrame: begin
                if (EnRxParallel) begin
                    RxFrameBytesRegsAddr_c3 <= #1 RxFrameBytesRegsAddr_c3+1;
                end
                if (NextState_ab3 != State_qb3) begin
                    EnRxFrameBytesRegs_q    <= #1 1'b0;
                    RxFrameBytesRegsAddr_c3 <= #1 2'h0;
                    EnRxDataCrcReg_q        <= #1 1'b1;
                end
            end
            s_AssemblyRxFrame: begin
                EnRxDataCrcReg_q            <= #1 1'b0;
            end
            s_CheckCrc: begin                 
                case(StatusCheckCrc_c2)
                    0: EnCrcChk_q           <= #1 1'b1;
                    1: EnCrcChk_q           <= #1 1'b0;                     
                    2: ; // Comment: CRC Checker busy.
                    3: CrcChkError_q        <= #1 (|CrcChk_b12) ? 1'b1 : 1'b0; 
                endcase
                StatusCheckCrc_c2           <= #1 StatusCheckCrc_c2+1;
                if (NextState_ab3 != State_qb3) begin
                    StatusCheckCrc_c2       <= #1 2'h0;
                    RstCrcChk_q             <= #1 1'b1;
                end
            end
            s_WriteFifo: begin
                RstCrcChk_q                 <= #1 1'b0;
                WrEnRxFifo_q                <= #1 1'b1;
            end
            default: begin
            end
        endcase
    end

// RX Byte Error Ratio Test (BERT) control:
always @(posedge ClkSerDes_ik or posedge Reset_ra)
    if (Reset_ra) begin
        RxBertByteErrors_oc64             <= #1 64'h0;
        RxBertBytesReceived_oc64          <= #1 64'h0;
        RxBertLosFlag_oq                  <= #1  1'b0;
        RxBertLosCntr_oc16                <= #1 16'h0;
    end else begin
        if (EnRxParallel) begin
            if (EnRxBert_xb2[1]) begin
                if ((RxByte_b8 != g_IdleNotLocked_b8) && (RxByte_b8 != g_IdleLocked_b8)) begin
                    RxBertByteErrors_oc64 <= #1 RxBertByteErrors_oc64+1;
                end
                RxBertBytesReceived_oc64  <= #1 RxBertBytesReceived_oc64+1;        
                if (a_RxLocked_db2 == 2'b10) begin
                    RxBertLosFlag_oq      <= #1 1'b1;
                    RxBertLosCntr_oc16    <= #1 RxBertLosCntr_oc16+1;
                end
            end else begin
                RxBertByteErrors_oc64     <= #1 64'h0;
                RxBertBytesReceived_oc64  <= #1 64'h0;
                RxBertLosFlag_oq          <= #1  1'b0;
                RxBertLosCntr_oc16        <= #1 16'h0;
            end
        end
    end
  
endmodule