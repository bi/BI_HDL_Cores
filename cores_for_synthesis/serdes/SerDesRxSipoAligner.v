//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesRxSipoAligner.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 26/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     RX Aligner of the Serializer/Deserializer (SerDes) module.  This module aligns the 
//     incoming 8b10b characters.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesRxSipoAligner    
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input            ClkSerDes_ik,
    input            Reset_ira,
    
    //==== Control Path ====//
    
    input            EnRxSerial_i,
    input            EnRxParallel_i,
    input      [3:0] Rx8b10bAddr_icb4,
        
    //==== Data Path ====//
    
    input            RxBit_i,
    output reg [9:0] Rx8b10b_oqb10
    
); 
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

// Data path:
reg  [9:0] RxRsr_qb10;
reg  [9:0] InputRx8b10b_qb10;
reg  [9:0] InputRx8b10b_db10;
    
//=======================================  User Logic  =======================================//

//==== Data Path ====//

// Input data Right Shift Register (RSR):
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)    RxRsr_qb10 <= #1 10'h0;
    else if (EnRxSerial_i) RxRsr_qb10 <= #1 {RxBit_i,RxRsr_qb10[9:1]};       

// Input Byte register:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)      InputRx8b10b_qb10 <= #1 10'h0;
    else if (EnRxParallel_i) InputRx8b10b_qb10 <= #1 RxRsr_qb10;
    
// Byte aligner barrel shifter:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin
        InputRx8b10b_db10 <= #1 10'h0;
        Rx8b10b_oqb10     <= #1 10'h0;
    end else begin
        if (EnRxParallel_i) begin
            InputRx8b10b_db10 <= #1 InputRx8b10b_qb10;
            case(Rx8b10bAddr_icb4)
                4'h0:    Rx8b10b_oqb10 <= #1                         InputRx8b10b_db10;
                4'h1:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[  0],InputRx8b10b_db10[9:1]};
                4'h2:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[1:0],InputRx8b10b_db10[9:2]};
                4'h3:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[2:0],InputRx8b10b_db10[9:3]};
                4'h4:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[3:0],InputRx8b10b_db10[9:4]};
                4'h5:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[4:0],InputRx8b10b_db10[9:5]};
                4'h6:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[5:0],InputRx8b10b_db10[9:6]};
                4'h7:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[6:0],InputRx8b10b_db10[9:7]};
                4'h8:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[7:0],InputRx8b10b_db10[9:8]};
                4'h9:    Rx8b10b_oqb10 <= #1 {InputRx8b10b_qb10[8:0],InputRx8b10b_db10[9  ]};
                default: Rx8b10b_oqb10 <= #1  10'h0;               
            endcase
        end
    end
    
endmodule