//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesTxPiso.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 04/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Parallel Input Serial Output (PISO) of the Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesTxPiso 
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input            ClkSerDes_ik,
    input            Reset_ira,
    
    //==== Control Path ====//
    
    input            EnTxParallel_i,
    input            EnTxSerial_i,
    
    //==== Data Path ====//
    
    input      [9:0] TxData_ib10,
    output reg       Tx_oq
    
); 
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

// Data path:
reg [9:0] TxDataReg_qb10;
// Control path:
reg [3:0] TxBitAdd_c4;
    
//=======================================  User Logic  =======================================//

//==== Data Path ====//

// Parallel data register:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)      TxDataReg_qb10 <= #1 10'h0;
    else if (EnTxParallel_i) TxDataReg_qb10 <= #1 TxData_ib10;

// Serial data output register:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) Tx_oq <= #1 1'b0;
    else             Tx_oq <= #1 TxDataReg_qb10[TxBitAdd_c4];  

//==== Control Path ====//
    
// Bit address counter:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin
        TxBitAdd_c4 <= #1 4'h0;
    end else begin
        if (EnTxSerial_i) begin
            if   (TxBitAdd_c4 == 9) TxBitAdd_c4 <= #1 4'h0;  
            else                    TxBitAdd_c4 <= #1 TxBitAdd_c4+1;  
        end
    end
    
endmodule