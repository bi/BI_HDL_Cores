//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesPrescaler.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 04/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Prescaler of the Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesTxPrescaler
//====================================  Global Parameters  ===================================//
#(  parameter g_SerialPrescaler_b8    = 8'h10,      
              g_SerialiationFactor_b5 = 5'h 8)      
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input      ClkSerDes_ik,
    input      Reset_ira,
    
    //==== Control Path ====//
    
    output reg EnTxParallel_oq,
    output reg EnTxSerial_oq
    
); 
//=======================================  Declarations  =====================================//

//==== Local parameters ====//

localparam c_ParallelPrescaler_b8 = g_SerialPrescaler_b8*g_SerialiationFactor_b5;  

//==== Wires & Regs ====//

// Control path:
reg  [7:0] EnTxSerialCntr_c8;
reg  [7:0] EnTxParallelCntr_c8;
    
//=======================================  User Logic  =======================================//

//==== Control Path ====//

always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin
        EnTxParallelCntr_c8     <= #1 8'h0;
        EnTxParallel_oq         <= #1 1'b0;
    end else begin
        EnTxParallelCntr_c8     <= #1 EnTxParallelCntr_c8+1;
        EnTxParallel_oq         <= #1 1'b0;        
        if (EnTxParallelCntr_c8 == c_ParallelPrescaler_b8-1) begin
            EnTxParallelCntr_c8 <= #1 8'h0;
            EnTxParallel_oq     <= #1 1'b1;
        end
    end

always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin
        EnTxSerialCntr_c8     <= #1 8'h0;
        EnTxSerial_oq         <= #1 1'b0;
    end else begin
        EnTxSerialCntr_c8     <= #1 EnTxSerialCntr_c8+1;
        EnTxSerial_oq         <= #1 1'b0;        
        if (EnTxSerialCntr_c8 == g_SerialPrescaler_b8-1) begin
            EnTxSerialCntr_c8 <= #1 8'h0;
            EnTxSerial_oq     <= #1 1'b1;
        end
    end
    
endmodule