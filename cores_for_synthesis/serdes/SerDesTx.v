//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: SerDesTx.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 08/12/16      1.3          M. Barros Marin    IP cores update,
//     - 07/09/16      1.1          M. Barros Marin    Removed TX BERT port.
//     - 04/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Transmitter of the Serializer/Deserializer (SerDes) module.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module SerDesTx 
//====================================  Global Parameters  ===================================//

#(  parameter g_IdleNotLocked_b8 = 8'h1C, // Comment: The Idle Not Locked is the K character K28.0.     
              g_IdleLocked_b8    = 8'hBC, // Comment: The Idle Locked is the K character K28.5.          
              g_Header_b4        = 4'h 5, // Comment: The header does not need to be DC balanced.
              g_TxFifoAddWith    = 4)     // Comment: Default: 16 LW positions (64 Bytes).               

//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input         ClkUser_ik,
    input         ClkSerDes_ik,
    input         Reset_ira,
    
    //==== Control Path ====//

    input         RxLocked_i,
    input         TxWrEnFifo_i,
    output        TxEmptyFifo_o,
    output        TxFullFifo_o,
    output        TxBusy_o,
    input         TxErrInjBert_i, 
    
    //==== Data Path ====//
    
    input  [31:0] TxDataFifo_ib32,
    output        Tx_o
    
); 
//=======================================  Declarations  =====================================//

//==== Local parameters ====//

localparam c_BytesPerFrame = 6;

// SerDes TX control FSM:
localparam s_Idle          = 3'h0,
           s_ReadFifo      = 3'h1,
           s_EnCrcGen      = 3'h2,
           s_EnTxFrameReg  = 3'h3,
           s_SendTxFrame   = 3'h4;

//==== Wires & Regs ====//

// Data path:
wire [ 7:0] IdleByte_b8;
wire [ 7:0] BertIdleByte_b8;
wire [31:0] TxDataFifo_b32;
reg  [31:0] TxDataFifo_qb32;
wire [11:0] Crc_b12;
reg  [47:0] TxFrame_qb48;
reg  [ 7:0] InvTxFrameOrIdleMux_qb8;
wire        RunningDisparity8b10b;
wire [ 9:0] Tx8b10b_10b;

// Control path:
wire        EnTxParallel;
wire        EnTxSerial;
reg  [ 1:0] ErrInjTxBert_x2;
wire        ErrInjTxBert;
wire        EmptyTxFifo;
reg  [ 1:0] EmptyTxFifo_xb2;
reg  [ 1:0] TxBusy_xb2;
reg  [ 2:0] State_qb3, NextState_ab3;  
reg         TxBusy_q;
reg         SendKCharacter_q;
reg  [ 2:0] InvTxFrameOrIdleAddr_c3;
reg         RdEnTxFifo_q;
reg         EnCrcGen_q;
reg         RstCrcGen_q;
reg         EnTxFrameReg_q;
    
//=======================================  User Logic  =======================================//

//==== Data Path ====//

// Idle character type multiplexor:
assign IdleByte_b8 = RxLocked_i ? g_IdleLocked_b8 : g_IdleNotLocked_b8; 

// Byte Error Ration Test (BERT) error injection:
assign BertIdleByte_b8 = ErrInjTxBert ? {IdleByte_b8[3:0],IdleByte_b8[7:4]} : IdleByte_b8;

// TX FIFO:
generic_fifo_dc_gray_mod #(
    .dw       (32),
    .aw       (g_TxFifoAddWith))
i_TxFifo (
    .rd_clk   (ClkSerDes_ik),
    .wr_clk   (ClkUser_ik),
    .rst      (~Reset_ira),
    .clr      (1'b0),
    .din      (TxDataFifo_ib32),
    .we       (TxWrEnFifo_i && ~TxFullFifo_o),
    .dout     (TxDataFifo_b32),
    .re       (RdEnTxFifo_q && ~EmptyTxFifo),
    .full     (TxFullFifo_o),
    .empty    (EmptyTxFifo),
    .wr_level (),
    .rd_level ());

// Registered TX FIFO output:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) TxDataFifo_qb32 <= #1 32'h0;
    else             TxDataFifo_qb32 <= #1 TxDataFifo_b32;    

// CRC Generator:
Crc12LwGen i_CrcGen (
    .data_in (TxDataFifo_qb32),
    .crc_en  (EnCrcGen_q),
    .crc_out (Crc_b12),
    .rst     (Reset_ira || RstCrcGen_q),
    .clk     (ClkSerDes_ik));

// Registered frame assembler:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if      (Reset_ira)      TxFrame_qb48 <= #1 48'h0;
    else if (EnTxFrameReg_q) TxFrame_qb48 <= #1 {g_Header_b4, TxDataFifo_qb32, Crc_b12};
    
// Registered TX Frame Byte multiplexor:
// Comment: Note!! The Bytes of the TX Frame are inverted in order to send the Byte with the Header first.
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin
        InvTxFrameOrIdleMux_qb8              <= #1 8'h0;
    end else begin
        case (InvTxFrameOrIdleAddr_c3)
            3'b000:  InvTxFrameOrIdleMux_qb8 <= #1 BertIdleByte_b8;     // Comment: Note!! TX Frame Bytes inverted.
            3'b001:  InvTxFrameOrIdleMux_qb8 <= #1 TxFrame_qb48[47:40]; // Comment: Note!! TX Frame Bytes inverted.
            3'b010:  InvTxFrameOrIdleMux_qb8 <= #1 TxFrame_qb48[39:32]; // Comment: Note!! TX Frame Bytes inverted.
            3'b011:  InvTxFrameOrIdleMux_qb8 <= #1 TxFrame_qb48[31:24]; // Comment: Note!! TX Frame Bytes inverted.
            3'b100:  InvTxFrameOrIdleMux_qb8 <= #1 TxFrame_qb48[23:16]; // Comment: Note!! TX Frame Bytes inverted.
            3'b101:  InvTxFrameOrIdleMux_qb8 <= #1 TxFrame_qb48[15: 8]; // Comment: Note!! TX Frame Bytes inverted.
            3'b110:  InvTxFrameOrIdleMux_qb8 <= #1 TxFrame_qb48[ 7: 0]; // Comment: Note!! TX Frame Bytes inverted.
            default: InvTxFrameOrIdleMux_qb8 <= #1 BertIdleByte_b8;     // Comment: Note!! TX Frame Bytes inverted.
        endcase
    end

// 8b10b Encoder:
Encoder8b10b i_8b10Enc (
    .clk         (ClkSerDes_ik),
    .rst         (Reset_ira), 
    .kin_ena     (EnTxParallel && SendKCharacter_q),
    .ein_ena     (EnTxParallel),
    .ein_dat     (InvTxFrameOrIdleMux_qb8),
    .ein_rd      (RunningDisparity8b10b),
    .eout_val    (),
    .eout_dat    (Tx8b10b_10b),
    .eout_rdcomb (),
    .eout_rdreg  (RunningDisparity8b10b));

// Parallel Input Serial Output (PISO):    
SerDesTxPiso i_Piso (
    .ClkSerDes_ik   (ClkSerDes_ik),
    .Reset_ira      (Reset_ira), 
    .EnTxParallel_i (EnTxParallel),
    .EnTxSerial_i   (EnTxSerial),
    .TxData_ib10    (Tx8b10b_10b),
    .Tx_oq          (Tx_o));

//==== Control Path ====//

// Prescaler:
SerDesTxPrescaler #(
    .g_SerialPrescaler_b8    (8'h10), // Comment: x16 oversampling.      
    .g_SerialiationFactor_b5 (5'hA))  // Comment: 10 to 1 bits.
i_Prescaler (
    .ClkSerDes_ik            (ClkSerDes_ik),
    .Reset_ira               (Reset_ira),  
    .EnTxParallel_oq         (EnTxParallel),
    .EnTxSerial_oq           (EnTxSerial)); 

// Byte Error Ratio Test (BERT) error injector synchronizer:
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if   (Reset_ira) ErrInjTxBert_x2 <= #1 2'h0;
    else             ErrInjTxBert_x2 <= #1 {ErrInjTxBert_x2[0],TxErrInjBert_i};

assign ErrInjTxBert = ErrInjTxBert_x2[1];

// TX FIFO empty synchronizer:
always @(posedge ClkUser_ik or posedge Reset_ira)
    if   (Reset_ira) EmptyTxFifo_xb2 <= #1 2'h3;
    else             EmptyTxFifo_xb2 <= #1 {EmptyTxFifo_xb2[0],EmptyTxFifo};

assign TxEmptyFifo_o = EmptyTxFifo_xb2[1];

// TX busy synchronizer:
always @(posedge ClkUser_ik or posedge Reset_ira)
    if   (Reset_ira) TxBusy_xb2 <= #1 2'h0;
    else             TxBusy_xb2 <= #1 {TxBusy_xb2[0],TxBusy_q};

assign TxBusy_o = TxBusy_xb2[1];

// SerDes TX control FSM:
always @(posedge ClkSerDes_ik or posedge Reset_ira) State_qb3 <= #1 Reset_ira ? s_Idle : NextState_ab3;
always @* begin
    NextState_ab3 = State_qb3;
    case(State_qb3)
        s_Idle:          if (EnTxParallel && ~EmptyTxFifo)               NextState_ab3 = s_ReadFifo;
        s_ReadFifo:                                                      NextState_ab3 = s_EnCrcGen;
        s_EnCrcGen:                                                      NextState_ab3 = s_EnTxFrameReg;
        s_EnTxFrameReg:                                                  NextState_ab3 = s_SendTxFrame;        
        s_SendTxFrame:   if (InvTxFrameOrIdleAddr_c3 == c_BytesPerFrame) NextState_ab3 = s_Idle;
        default:                                                         NextState_ab3 = s_Idle;
    endcase
end
always @(posedge ClkSerDes_ik or posedge Reset_ira)
    if (Reset_ira) begin      
        TxBusy_q                            <= #1 1'b0;
        SendKCharacter_q                    <= #1 1'b1;
        InvTxFrameOrIdleAddr_c3             <= #1 3'h0;
        RdEnTxFifo_q                        <= #1 1'b0;
        EnCrcGen_q                          <= #1 1'b0;
        RstCrcGen_q                         <= #1 1'b0;
        EnTxFrameReg_q                      <= #1 1'b0;
    end else begin
        case(State_qb3) 
            s_Idle: begin                                
                if (EnTxParallel && EmptyTxFifo) begin
                    TxBusy_q                <= #1 1'b0;
                    SendKCharacter_q        <= #1 1'b1;
                    InvTxFrameOrIdleAddr_c3 <= #1 3'h0;
                end
                if (NextState_ab3 != State_qb3) begin
                    TxBusy_q                <= #1 1'b1;
                    RdEnTxFifo_q            <= #1 1'b1;
                end
            end
            s_ReadFifo: begin
                RdEnTxFifo_q                <= #1 1'b0;
                if (NextState_ab3 != State_qb3) begin
                    EnCrcGen_q              <= #1 1'b1;
                end
            end
            s_EnCrcGen: begin
                EnCrcGen_q                  <= #1 1'b0;   
                if (NextState_ab3 != State_qb3) begin
                    EnTxFrameReg_q          <= #1 1'b1;
                end
            end
            s_EnTxFrameReg: begin 
                    RstCrcGen_q             <= #1 1'b1;
                    EnTxFrameReg_q          <= #1 1'b0;  
                if (NextState_ab3 != State_qb3) begin
                    SendKCharacter_q        <= #1 1'b0;
                    InvTxFrameOrIdleAddr_c3 <= #1 3'h1;
                end
            end
            s_SendTxFrame: begin
                RstCrcGen_q                 <= #1 1'b0;
                if (EnTxParallel) InvTxFrameOrIdleAddr_c3 <= #1 InvTxFrameOrIdleAddr_c3+1;
            end
            default: begin
            end
        endcase
    end
    
endmodule