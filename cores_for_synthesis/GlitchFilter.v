//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: GlitchFilter.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 10/08/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//    Module for filtering possible glitches in a serial data stream.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module GlitchFilter
//========================================  I/O ports  =======================================//
(   
    //==== Clocks & Resets ====//
    
    input      Clk_ik,
    input      Reset_ira,
    
    //==== Data Path ====//
    
    input      Input_i,
    output reg Output_oq
    
); 
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

// Data path:
reg [3:0] Lsr_qb4;
// Control path:
wire      SetOutput;
wire      ClrOutput;
    
//=======================================  User Logic  =======================================//

//==== Data Path ====//

// Serial data input Left Shift Register (LSR):
always @(posedge Clk_ik or posedge Reset_ira)
    if   (Reset_ira) Lsr_qb4 <= #1 4'h0;
    else             Lsr_qb4 <= #1 {Lsr_qb4[2:0],Input_i};

// Output Set/Clr register:
always @(posedge Clk_ik or posedge Reset_ira)
    if           (Reset_ira) Output_oq <= #1 1'b0;
    else if      (SetOutput) Output_oq <= #1 1'b1;  
         else if (ClrOutput) Output_oq <= #1 1'b0;  
         
//==== Control Path ====//
    
// Output Register Set control:
assign SetOutput = &Lsr_qb4;

// Output Register Clear control:
assign ClrOutput = ~|Lsr_qb4;

endmodule