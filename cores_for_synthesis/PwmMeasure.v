//----------------------------------------------------------------------
// Title      : PWM Measure
// Project    : Generic
//----------------------------------------------------------------------
// File       : PwmMeasure.v
// Author     : T. Levens
// Company    : CERN BE-BI-QP
// Created    : 2015-11-14
// Last update: 2015-11-14
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
// Measures high and low periods of PWM signal.
//----------------------------------------------------------------------

module PwmMeasure (
    input Clk_ik,
    input Input_i,
    output reg [31:0] HiCount_ob32,
    output reg [31:0] LoCount_ob32
);

localparam dly = 1;

reg Input_d;
always @(posedge Clk_ik) Input_d <= #dly Input_i;

reg [31:0] HiCount_b32;
reg [31:0] LoCount_b32;

always @(posedge Clk_ik) begin
    if (Input_i && ~Input_d) begin
        HiCount_ob32 <= #dly HiCount_b32;
        HiCount_b32  <= #dly 32'h00000000;
    end
    else if (~Input_i && Input_d) begin
        LoCount_ob32 <= #dly LoCount_b32;
        LoCount_b32  <= #dly 32'h00000000;
    end
    else if (Input_i)
        HiCount_b32 <= #dly HiCount_b32 + 32'h00000001;
    else
        LoCount_b32 <= #dly LoCount_b32 + 32'h00000001;
end

endmodule
