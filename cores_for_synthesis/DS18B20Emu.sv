//----------------------------------------------------------------------
// Title      : DS18B20 Emulator
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : DS18B20Emu.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Implements an emulated Maxim DS18B20 OneWire serial number and
// temperature sensor.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module DS18B20Emu #(parameter g_ClkFrequency) (
    input             Clk_ik,
    input             Rst_irq,

    input      [63:0] UniqueId_ib64,
    input      [15:0] Temp_ib16,

    input             OneWireBus_i,
    output            OneWireBus_oe
);

//----------------------------------------------------------------------
// Constants
//----------------------------------------------------------------------

// Timing
localparam int c_DlyCntBits = $clog2(int'((400 * g_ClkFrequency) / 1e6) + 1);
localparam c_Trstl = (c_DlyCntBits)'(int'((400 * g_ClkFrequency) / 1e6));
localparam c_Tpdh  = (c_DlyCntBits)'(int'(( 50 * g_ClkFrequency) / 1e6));
localparam c_Tpdl  = (c_DlyCntBits)'(int'((200 * g_ClkFrequency) / 1e6));
localparam c_Tsamp = (c_DlyCntBits)'(int'(( 30 * g_ClkFrequency) / 1e6));
localparam c_Trel  = (c_DlyCntBits)'(int'(( 30 * g_ClkFrequency) / 1e6));

// ROM commands
localparam c_ReadRom     = 8'h33;
localparam c_MatchRom    = 8'h55;
localparam c_SearchRom   = 8'hF0;
localparam c_AlarmSearch = 8'hEC;
localparam c_SkipRom     = 8'hCC;

// Function commands
localparam c_ConvTemp    = 8'h44;
localparam c_ReadSP      = 8'hBE;
localparam c_WriteSP     = 8'h4E;
localparam c_CopySP      = 8'h48;
localparam c_RecallE2    = 8'hB8;
localparam c_ReadPS      = 8'hB4;

//----------------------------------------------------------------------
// Commands
//----------------------------------------------------------------------

// Synchronisation of bus input
reg [2:0] OneWireBus_xb2;
always @(posedge Clk_ik) OneWireBus_xb2 <= #1 {OneWireBus_xb2[1:0], OneWireBus_i};
wire OneWireBus_q =  OneWireBus_xb2[1];

// Bus edge detection
wire BusPosEdge_p =  OneWireBus_xb2[1] && !OneWireBus_xb2[2];
wire BusNegEdge_p = !OneWireBus_xb2[1] &&  OneWireBus_xb2[2];

// Bus reset detection (bus low for >=480us)
reg [c_DlyCntBits-1:0] RstCounter_c;
reg BusRst_p;

always @(posedge Clk_ik) begin
    BusRst_p         <= #1 1'b0;
    if (BusNegEdge_p) begin
        RstCounter_c <= #1 'b1;
    end else if (BusPosEdge_p) begin
        RstCounter_c <= #1 'b0;
    end else if (RstCounter_c == c_Trstl) begin
        BusRst_p     <= #1 1'b1;
        RstCounter_c <= #1 'b0;
    end else if (RstCounter_c) begin
        RstCounter_c <= #1 RstCounter_c + 1'b1;
    end
end

//----------------------------------------------------------------------
// State machine
//----------------------------------------------------------------------

// State encoding
enum {
    s_Idle,
    s_Reset,
    s_WaiPrsnt,
    s_Prsnt,
    s_PreRomCmd,
    s_WaiRomCmd,
    s_RomCmd,
    s_DecRomCmd,
    s_WaiTxRomBit,
    s_TxRomBit,
    s_WaiTxNRomBit,
    s_TxNRomBit,
    s_WaiRxRomBit,
    s_RxRomBit,
    s_PreFncCmd,
    s_WaiFncCmd,
    s_FncCmd,
    s_DecFncCmd,
    s_WaiTxFncBit,
    s_TxFncBit,
    s_WaiRxSPBit,
    s_RxSPBit
} State_l;

reg [c_DlyCntBits-1:0] DlyCnt_c;
reg [ 7:0] BitCnt_c8;
reg [ 7:0] Command_b8;
reg [63:0] SR_b64;
reg [23:0] E2_b24;
reg [ 7:0] CRC_b8;
reg BusOut, SearchRom, SendCRC;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        State_l         <= #1 s_Idle;
        BusOut          <= #1  1'b1;
        DlyCnt_c        <= #1   'h0;
        BitCnt_c8       <= #1  8'h0;
        Command_b8      <= #1  8'h0;
        SR_b64          <= #1 64'h0;
        SearchRom       <= #1  1'b0;
        SendCRC         <= #1  1'b0;
        E2_b24          <= #1 24'h7F7F80;
        CRC_b8          <= #1  8'h0;
    end else if (BusRst_p) begin
        State_l         <= #1 s_Reset;
        BusOut          <= #1 1'b1;
    end else begin
        case (State_l)
            //----------------------------------------------------------
            // Bus reset & present generation
            //----------------------------------------------------------
            s_Reset: begin
                if (BusPosEdge_p) begin
                    State_l         <= #1 s_WaiPrsnt;
                    DlyCnt_c        <= #1 c_Tpdh;
                end
            end
            s_WaiPrsnt: begin
                if (!DlyCnt_c) begin
                    State_l         <= #1 s_Prsnt;
                    DlyCnt_c        <= #1 c_Tpdl;
                    BusOut          <= #1 1'b0;
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            s_Prsnt: begin
                if (!DlyCnt_c) begin
                    BusOut          <= #1 1'b1;
                    State_l         <= #1 s_PreRomCmd;
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            //----------------------------------------------------------
            // Read ROM command
            //----------------------------------------------------------
            s_PreRomCmd: begin
                State_l         <= #1 s_WaiRomCmd;
                BitCnt_c8       <= #1 8'h7;
                Command_b8      <= #1 8'h0;
            end
            s_WaiRomCmd: begin
                if (BusNegEdge_p) begin
                    State_l     <= #1 s_RomCmd;
                    DlyCnt_c    <= #1 c_Tsamp;
                end
            end
            s_RomCmd: begin
                if (!DlyCnt_c) begin
                    Command_b8      <= #1 {OneWireBus_q, Command_b8[7:1]};
                    if (!BitCnt_c8) begin
                        State_l     <= #1 s_DecRomCmd;
                    end else begin
                        State_l     <= #1 s_WaiRomCmd;
                        BitCnt_c8   <= #1 BitCnt_c8 - 8'h1;
                    end
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            s_DecRomCmd: begin
                case (Command_b8)
                    c_ReadRom: begin
                        State_l     <= #1 s_WaiTxRomBit;
                        SearchRom   <= #1 1'b0;
                        BitCnt_c8   <= #1 8'd63;
                        SR_b64      <= #1 UniqueId_ib64;
                    end
                    c_MatchRom: begin
                        State_l     <= #1 s_WaiRxRomBit;
                        SearchRom   <= #1 1'b0;
                        BitCnt_c8   <= #1 8'd63;
                        SR_b64      <= #1 UniqueId_ib64;
                    end
                    c_SearchRom, c_AlarmSearch: begin
                        State_l     <= #1 s_WaiTxRomBit;
                        SearchRom   <= #1 1'b1;
                        BitCnt_c8   <= #1 8'd63;
                        SR_b64      <= #1 UniqueId_ib64;
                    end
                    c_SkipRom: begin
                        State_l     <= #1 s_PreFncCmd;
                    end
                    default: begin
                        State_l     <= #1 s_Idle;
                    end
                endcase
            end
            //----------------------------------------------------------
            // Excecute ROM command
            //----------------------------------------------------------
            //
            // Depending on the command, the following sequence is
            // followed:
            //
            //     SearchRom: 64x[TX, TXN, RX]
            //       ReadRom: 64x[TX]
            //      MatchRom: 64x[RX]
            //
            // TX:
            s_WaiTxRomBit: begin
                if (BusNegEdge_p) begin
                    State_l         <= #1 s_TxRomBit;
                    DlyCnt_c        <= #1 c_Trel;
                    BusOut          <= #1 SR_b64[0];
                end
            end
            s_TxRomBit: begin
                if (!DlyCnt_c) begin
                    BusOut          <= #1 1'b1;
                    if (SearchRom) begin
                        State_l     <= #1 s_WaiTxNRomBit;
                    end else if (!BitCnt_c8) begin
                        State_l     <= #1 s_PreFncCmd;
                    end else begin
                        State_l     <= #1 s_WaiTxRomBit;
                        SR_b64      <= #1 {1'b0, SR_b64[63:1]};
                        BitCnt_c8   <= #1 BitCnt_c8 - 1'b1;
                    end
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            // TXN:
            s_WaiTxNRomBit: begin
                if (BusNegEdge_p) begin
                    State_l         <= #1 s_TxNRomBit;
                    DlyCnt_c        <= #1 c_Trel;
                    BusOut          <= #1 !SR_b64[0];
                end
            end
            s_TxNRomBit: begin
                if (!DlyCnt_c) begin
                    BusOut          <= #1 1'b1;
                    State_l         <= #1 s_WaiRxRomBit;
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            // RX:
            s_WaiRxRomBit: begin
                if (BusNegEdge_p) begin
                    State_l     <= #1 s_RxRomBit;
                    DlyCnt_c    <= #1 c_Tsamp;
                end
            end
            s_RxRomBit: begin
                if (!DlyCnt_c) begin
                    if (SR_b64[0] != OneWireBus_q) begin
                        State_l         <= #1 s_Idle;
                        BitCnt_c8       <= #1 8'd0;
                    end else begin
                        if (!BitCnt_c8) begin
                            State_l     <= #1 (SearchRom) ? s_Idle : s_PreFncCmd;
                        end else begin
                            State_l     <= #1 (SearchRom) ? s_WaiTxRomBit : s_WaiRxRomBit;
                            SR_b64      <= #1 {1'b0, SR_b64[63:1]};
                            BitCnt_c8   <= #1 BitCnt_c8 - 1'b1;
                        end
                    end
                end else begin
                    DlyCnt_c    <= #1 DlyCnt_c - 1'b1;
                end
            end
            //----------------------------------------------------------
            // Read function command
            //----------------------------------------------------------
            s_PreFncCmd: begin
                State_l         <= #1 s_WaiFncCmd;
                BitCnt_c8       <= #1 8'h7;
                Command_b8      <= #1 8'h0;
                BusOut          <= #1 1'b1;
                SearchRom       <= #1 1'b0;
            end
            s_WaiFncCmd: begin
                if (BusNegEdge_p) begin
                    State_l     <= #1 s_FncCmd;
                    DlyCnt_c    <= #1 c_Tsamp;
                end
            end
            s_FncCmd: begin
                if (!DlyCnt_c) begin
                    Command_b8      <= #1 {OneWireBus_q, Command_b8[7:1]};
                    if (!BitCnt_c8) begin
                        State_l     <= #1 s_DecFncCmd;
                    end else begin
                        State_l     <= #1 s_WaiFncCmd;
                        BitCnt_c8   <= #1 BitCnt_c8 - 8'h1;
                    end
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            s_DecFncCmd: begin
                case (Command_b8)
                    c_ConvTemp, c_RecallE2: begin
                        State_l     <= #1 s_WaiTxFncBit;
                        CRC_b8      <= #1  8'h0;
                        BitCnt_c8   <= #1  8'd3;
                        SR_b64      <= #1 63'b1000;
                        SendCRC     <= #1  1'b0;
                    end
                    c_ReadPS: begin
                        State_l     <= #1 s_WaiTxFncBit;
                        CRC_b8      <= #1  8'h0;
                        BitCnt_c8   <= #1  8'd0;
                        SR_b64      <= #1 63'b1;
                        SendCRC     <= #1  1'b0;
                    end
                    c_CopySP: begin
                        State_l     <= #1 s_Idle;
                    end
                    c_ReadSP: begin
                        State_l     <= #1 s_WaiTxFncBit;
                        CRC_b8      <= #1 8'h0;
                        BitCnt_c8   <= #1 8'd63;
                        SR_b64      <= #1 {8'h10, 8'h00, 8'hFF, E2_b24, Temp_ib16};
                        SendCRC     <= #1 1'b1;
                    end
                    c_WriteSP: begin
                        State_l     <= #1 s_WaiRxSPBit;
                        BitCnt_c8   <= #1 8'd23;
                    end
                    default: begin
                        State_l     <= #1 s_Idle;
                    end
                endcase
            end
            //----------------------------------------------------------
            // Excecute function command
            //----------------------------------------------------------
            s_WaiTxFncBit: begin
                if (BusNegEdge_p) begin
                    State_l         <= #1 s_TxFncBit;
                    DlyCnt_c        <= #1 c_Trel;
                    BusOut          <= #1 SR_b64[0];
                    CRC_b8[7]       <= #1 SR_b64[0] ^ CRC_b8[0];
                    CRC_b8[6:4]     <= #1 CRC_b8[7:5];
                    CRC_b8[3]       <= #1 SR_b64[0] ^ CRC_b8[0] ^ CRC_b8[4];
                    CRC_b8[2]       <= #1 SR_b64[0] ^ CRC_b8[0] ^ CRC_b8[3];
                    CRC_b8[1:0]     <= #1 CRC_b8[2:1];
                end
            end
            s_TxFncBit: begin
                if (!DlyCnt_c) begin
                    BusOut          <= #1 1'b1;
                    if (!BitCnt_c8) begin
                        if (SendCRC) begin
                            State_l     <= #1 s_WaiTxFncBit;
                            BitCnt_c8   <= #1 8'd7;
                            SR_b64      <= #1 {56'h0, CRC_b8};
                            SendCRC     <= #1 1'b0;
                        end else begin
                            State_l     <= #1 s_Idle;
                        end
                    end else begin
                        State_l     <= #1 s_WaiTxFncBit;
                        SR_b64      <= #1 {1'b0, SR_b64[63:1]};
                        BitCnt_c8   <= #1 BitCnt_c8 - 1'b1;
                    end
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            s_WaiRxSPBit: begin
                if (BusNegEdge_p) begin
                    State_l         <= #1 s_RxSPBit;
                    DlyCnt_c        <= #1 c_Tsamp;
                end
            end
            s_RxSPBit: begin
                if (!DlyCnt_c) begin
                    E2_b24          <= #1 {OneWireBus_q, E2_b24[23:1]};
                    if (!BitCnt_c8) begin
                        State_l     <= #1 s_Idle;
                    end else begin
                        State_l     <= #1 s_WaiRxSPBit;
                        BitCnt_c8   <= #1 BitCnt_c8 - 1'b1;
                    end
                end else begin
                    DlyCnt_c        <= #1 DlyCnt_c - 1'b1;
                end
            end
            //----------------------------------------------------------
            default: begin
                // Do nothing...
            end
        endcase
    end
end

assign OneWireBus_oe = !BusOut;

endmodule
