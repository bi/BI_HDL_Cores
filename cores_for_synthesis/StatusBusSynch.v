//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: StatusBusSynch.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    Andrea Boccardi    New Version.  
//                                                                    
//
// Language: Verilog 2001                                                              
//                                                                                                   
//
// Description:
//                                                                                                     
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module StatusBusSynch
//====================================  Global Parameters  ===================================//
#(  parameter g_BusWidth = 32)         
//========================================  I/O ports  =======================================//

(   
    input                       Clk_ik,
    input      [g_BusWidth-1:0] StatusBus_iab,
    output reg [g_BusWidth-1:0] StatusBus_oqb
);     
   
//==============================  Declarations & Initializations  =============================//

reg [g_BusWidth-1:0] StatusBus_xb = 0;
reg [g_BusWidth-1:0] StatusBus_db = 0;
   
//===========================================  Logic  ==========================================//

wire StatusStable  = StatusBus_xb === StatusBus_db;
wire StatusChanged = StatusBus_db !== StatusBus_oqb;

always @(posedge Clk_ik) StatusBus_xb <= #1 StatusBus_iab;
always @(posedge Clk_ik) StatusBus_db <= #1 StatusBus_xb;
always @(posedge Clk_ik) 
    if (StatusChanged && StatusStable) StatusBus_oqb <= #1 StatusBus_db;

endmodule        