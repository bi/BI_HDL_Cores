`timescale 1ns/100ps

// Mod by M. Barros Marin (28/06/18): Re-written

module Generic32OutputRegs
#(  parameter Reg0Default      = 32'h00000000,
              Reg0AutoClrMask  = 32'hFFFFFFFF,
              Reg1Default      = 32'h00000000,
              Reg1AutoClrMask  = 32'hFFFFFFFF,
              Reg2Default      = 32'h00000000,
              Reg2AutoClrMask  = 32'hFFFFFFFF,
              Reg3Default      = 32'h00000000,
              Reg3AutoClrMask  = 32'hFFFFFFFF,
              Reg4Default      = 32'h00000000,
              Reg4AutoClrMask  = 32'hFFFFFFFF,
              Reg5Default      = 32'h00000000,
              Reg5AutoClrMask  = 32'hFFFFFFFF,
              Reg6Default      = 32'h00000000,
              Reg6AutoClrMask  = 32'hFFFFFFFF,
              Reg7Default      = 32'h00000000,
              Reg7AutoClrMask  = 32'hFFFFFFFF,
              Reg8Default      = 32'h00000000,
              Reg8AutoClrMask  = 32'hFFFFFFFF,
              Reg9Default      = 32'h00000000,
              Reg9AutoClrMask  = 32'hFFFFFFFF,
              Reg10Default     = 32'h00000000,
              Reg10AutoClrMask = 32'hFFFFFFFF,
              Reg11Default     = 32'h00000000,
              Reg11AutoClrMask = 32'hFFFFFFFF,
              Reg12Default     = 32'h00000000,
              Reg12AutoClrMask = 32'hFFFFFFFF,
              Reg13Default     = 32'h00000000,
              Reg13AutoClrMask = 32'hFFFFFFFF,
              Reg14Default     = 32'h00000000,
              Reg14AutoClrMask = 32'hFFFFFFFF,
              Reg15Default     = 32'h00000000,
              Reg15AutoClrMask = 32'hFFFFFFFF,
              Reg16Default     = 32'h00000000,
              Reg16AutoClrMask = 32'hFFFFFFFF,
              Reg17Default     = 32'h00000000,
              Reg17AutoClrMask = 32'hFFFFFFFF,
              Reg18Default     = 32'h00000000,
              Reg18AutoClrMask = 32'hFFFFFFFF,
              Reg19Default     = 32'h00000000,
              Reg19AutoClrMask = 32'hFFFFFFFF,
              Reg20Default     = 32'h00000000,
              Reg20AutoClrMask = 32'hFFFFFFFF,
              Reg21Default     = 32'h00000000,
              Reg21AutoClrMask = 32'hFFFFFFFF,
              Reg22Default     = 32'h00000000,
              Reg22AutoClrMask = 32'hFFFFFFFF,
              Reg23Default     = 32'h00000000,
              Reg23AutoClrMask = 32'hFFFFFFFF,
              Reg24Default     = 32'h00000000,
              Reg24AutoClrMask = 32'hFFFFFFFF,
              Reg25Default     = 32'h00000000,
              Reg25AutoClrMask = 32'hFFFFFFFF,
              Reg26Default     = 32'h00000000,
              Reg26AutoClrMask = 32'hFFFFFFFF,
              Reg27Default     = 32'h00000000,
              Reg27AutoClrMask = 32'hFFFFFFFF,
              Reg28Default     = 32'h00000000,
              Reg28AutoClrMask = 32'hFFFFFFFF,
              Reg29Default     = 32'h00000000,
              Reg29AutoClrMask = 32'hFFFFFFFF,
              Reg30Default     = 32'h00000000,
              Reg30AutoClrMask = 32'hFFFFFFFF,
              Reg31Default     = 32'h00000000,
              Reg31AutoClrMask = 32'hFFFFFFFF
)(
    input              Clk_ik,
    input              Rst_irq,
    input              Cyc_i,
    input              Stb_i,
    input              We_i,
    input       [ 4:0] Adr_ib5,
    input       [31:0] Dat_ib32,
    output      [31:0] Dat_oab32,
    output  reg        Ack_oa,
    output      [31:0] Reg0Value_ob32,
    output      [31:0] Reg1Value_ob32,
    output      [31:0] Reg2Value_ob32,
    output      [31:0] Reg3Value_ob32,
    output      [31:0] Reg4Value_ob32,
    output      [31:0] Reg5Value_ob32,
    output      [31:0] Reg6Value_ob32,
    output      [31:0] Reg7Value_ob32,
    output      [31:0] Reg8Value_ob32,
    output      [31:0] Reg9Value_ob32,
    output      [31:0] Reg10Value_ob32,
    output      [31:0] Reg11Value_ob32,
    output      [31:0] Reg12Value_ob32,
    output      [31:0] Reg13Value_ob32,
    output      [31:0] Reg14Value_ob32,
    output      [31:0] Reg15Value_ob32,
    output      [31:0] Reg16Value_ob32,
    output      [31:0] Reg17Value_ob32,
    output      [31:0] Reg18Value_ob32,
    output      [31:0] Reg19Value_ob32,
    output      [31:0] Reg20Value_ob32,
    output      [31:0] Reg21Value_ob32,
    output      [31:0] Reg22Value_ob32,
    output      [31:0] Reg23Value_ob32,
    output      [31:0] Reg24Value_ob32,
    output      [31:0] Reg25Value_ob32,
    output      [31:0] Reg26Value_ob32,
    output      [31:0] Reg27Value_ob32,
    output      [31:0] Reg28Value_ob32,
    output      [31:0] Reg29Value_ob32,
    output      [31:0] Reg30Value_ob32,
    output      [31:0] Reg31Value_ob32
);

reg [31:0] Reg_q32b32 [31:0];

always @(posedge Clk_ik)
    if (Rst_irq) begin
        Reg_q32b32[ 0] <= #1 Reg0Default;
        Reg_q32b32[ 1] <= #1 Reg1Default;
        Reg_q32b32[ 2] <= #1 Reg2Default;
        Reg_q32b32[ 3] <= #1 Reg3Default;
        Reg_q32b32[ 4] <= #1 Reg4Default;
        Reg_q32b32[ 5] <= #1 Reg5Default;
        Reg_q32b32[ 6] <= #1 Reg6Default;
        Reg_q32b32[ 7] <= #1 Reg7Default;
        Reg_q32b32[ 8] <= #1 Reg8Default;
        Reg_q32b32[ 9] <= #1 Reg9Default;
        Reg_q32b32[10] <= #1 Reg10Default;
        Reg_q32b32[11] <= #1 Reg11Default;
        Reg_q32b32[12] <= #1 Reg12Default;
        Reg_q32b32[13] <= #1 Reg13Default;
        Reg_q32b32[14] <= #1 Reg14Default;
        Reg_q32b32[15] <= #1 Reg15Default;
        Reg_q32b32[16] <= #1 Reg16Default;
        Reg_q32b32[17] <= #1 Reg17Default;
        Reg_q32b32[18] <= #1 Reg18Default;
        Reg_q32b32[19] <= #1 Reg19Default;
        Reg_q32b32[20] <= #1 Reg20Default;
        Reg_q32b32[21] <= #1 Reg21Default;
        Reg_q32b32[22] <= #1 Reg22Default;
        Reg_q32b32[23] <= #1 Reg23Default;
        Reg_q32b32[24] <= #1 Reg24Default;
        Reg_q32b32[25] <= #1 Reg25Default;
        Reg_q32b32[26] <= #1 Reg26Default;
        Reg_q32b32[27] <= #1 Reg27Default;
        Reg_q32b32[28] <= #1 Reg28Default;
        Reg_q32b32[29] <= #1 Reg29Default;
        Reg_q32b32[30] <= #1 Reg30Default;
        Reg_q32b32[31] <= #1 Reg31Default;
        Ack_oa         <= #1 1'b0;
    end else begin
        Reg_q32b32[ 0] <= #1 Reg_q32b32[ 0] & Reg0AutoClrMask;
        Reg_q32b32[ 1] <= #1 Reg_q32b32[ 1] & Reg1AutoClrMask;
        Reg_q32b32[ 2] <= #1 Reg_q32b32[ 2] & Reg2AutoClrMask;
        Reg_q32b32[ 3] <= #1 Reg_q32b32[ 3] & Reg3AutoClrMask;
        Reg_q32b32[ 4] <= #1 Reg_q32b32[ 4] & Reg4AutoClrMask;
        Reg_q32b32[ 5] <= #1 Reg_q32b32[ 5] & Reg5AutoClrMask;
        Reg_q32b32[ 6] <= #1 Reg_q32b32[ 6] & Reg6AutoClrMask;
        Reg_q32b32[ 7] <= #1 Reg_q32b32[ 7] & Reg7AutoClrMask;
        Reg_q32b32[ 8] <= #1 Reg_q32b32[ 8] & Reg8AutoClrMask;
        Reg_q32b32[ 9] <= #1 Reg_q32b32[ 9] & Reg9AutoClrMask;
        Reg_q32b32[10] <= #1 Reg_q32b32[10] & Reg10AutoClrMask;
        Reg_q32b32[11] <= #1 Reg_q32b32[11] & Reg11AutoClrMask;
        Reg_q32b32[12] <= #1 Reg_q32b32[12] & Reg12AutoClrMask;
        Reg_q32b32[13] <= #1 Reg_q32b32[13] & Reg13AutoClrMask;
        Reg_q32b32[14] <= #1 Reg_q32b32[14] & Reg14AutoClrMask;
        Reg_q32b32[15] <= #1 Reg_q32b32[15] & Reg15AutoClrMask;
        Reg_q32b32[16] <= #1 Reg_q32b32[16] & Reg16AutoClrMask;
        Reg_q32b32[17] <= #1 Reg_q32b32[17] & Reg17AutoClrMask;
        Reg_q32b32[18] <= #1 Reg_q32b32[18] & Reg18AutoClrMask;
        Reg_q32b32[19] <= #1 Reg_q32b32[19] & Reg19AutoClrMask;
        Reg_q32b32[20] <= #1 Reg_q32b32[20] & Reg20AutoClrMask;
        Reg_q32b32[21] <= #1 Reg_q32b32[21] & Reg21AutoClrMask;
        Reg_q32b32[22] <= #1 Reg_q32b32[22] & Reg22AutoClrMask;
        Reg_q32b32[23] <= #1 Reg_q32b32[23] & Reg23AutoClrMask;
        Reg_q32b32[24] <= #1 Reg_q32b32[24] & Reg24AutoClrMask;
        Reg_q32b32[25] <= #1 Reg_q32b32[25] & Reg25AutoClrMask;
        Reg_q32b32[26] <= #1 Reg_q32b32[26] & Reg26AutoClrMask;
        Reg_q32b32[27] <= #1 Reg_q32b32[27] & Reg27AutoClrMask;
        Reg_q32b32[28] <= #1 Reg_q32b32[28] & Reg28AutoClrMask;
        Reg_q32b32[29] <= #1 Reg_q32b32[29] & Reg29AutoClrMask;
        Reg_q32b32[30] <= #1 Reg_q32b32[30] & Reg30AutoClrMask;
        Reg_q32b32[31] <= #1 Reg_q32b32[31] & Reg31AutoClrMask;
        if (Cyc_i && We_i && Stb_i) Reg_q32b32[Adr_ib5] <= #1 Dat_ib32;
        Ack_oa         <= #1 Stb_i && Cyc_i;
    end

assign Reg0Value_ob32  = Reg_q32b32[ 0];
assign Reg1Value_ob32  = Reg_q32b32[ 1];
assign Reg2Value_ob32  = Reg_q32b32[ 2];
assign Reg3Value_ob32  = Reg_q32b32[ 3];
assign Reg4Value_ob32  = Reg_q32b32[ 4];
assign Reg5Value_ob32  = Reg_q32b32[ 5];
assign Reg6Value_ob32  = Reg_q32b32[ 6];
assign Reg7Value_ob32  = Reg_q32b32[ 7];
assign Reg8Value_ob32  = Reg_q32b32[ 8];
assign Reg9Value_ob32  = Reg_q32b32[ 9];
assign Reg10Value_ob32 = Reg_q32b32[10];
assign Reg11Value_ob32 = Reg_q32b32[11];
assign Reg12Value_ob32 = Reg_q32b32[12];
assign Reg13Value_ob32 = Reg_q32b32[13];
assign Reg14Value_ob32 = Reg_q32b32[14];
assign Reg15Value_ob32 = Reg_q32b32[15];
assign Reg16Value_ob32 = Reg_q32b32[16];
assign Reg17Value_ob32 = Reg_q32b32[17];
assign Reg18Value_ob32 = Reg_q32b32[18];
assign Reg19Value_ob32 = Reg_q32b32[19];
assign Reg20Value_ob32 = Reg_q32b32[20];
assign Reg21Value_ob32 = Reg_q32b32[21];
assign Reg22Value_ob32 = Reg_q32b32[22];
assign Reg23Value_ob32 = Reg_q32b32[23];
assign Reg24Value_ob32 = Reg_q32b32[24];
assign Reg25Value_ob32 = Reg_q32b32[25];
assign Reg26Value_ob32 = Reg_q32b32[26];
assign Reg27Value_ob32 = Reg_q32b32[27];
assign Reg28Value_ob32 = Reg_q32b32[28];
assign Reg29Value_ob32 = Reg_q32b32[29];
assign Reg30Value_ob32 = Reg_q32b32[30];
assign Reg31Value_ob32 = Reg_q32b32[31];
assign Dat_oab32       = Reg_q32b32[Adr_ib5];

endmodule
