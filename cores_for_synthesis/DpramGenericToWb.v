//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: DpramGenericToWb.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 19/03/18      1.5          M. Barros Marin    Fixed bug in Ack.
//     - 22/02/17      1.4          M. Barros Marin    Renamed do to dout in dpram.
//     - 11/11/16      1.3          M. Barros Marin    Added Ack delay.
//     - dd/mm/yy      1.2          A. Boccardi        Added variable data width.
//     - 01/02/16      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005
//
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     DPRAM with Generic Write interface and Wishbone Read interface.
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module DpramGenericToWb
//====================================  Global Parameters  ===================================//
#(  parameter g_AddrWidth = 10, g_DataWidth = 32)
//========================================  I/O ports  =======================================//
(
    //==== Generic interface (Write) ====//

    input                         WrClk_ik,
    input                         WrRst_ir,
    input                         WrEn_i,
    input       [g_AddrWidth-1:0] WrAddr_ib,
    input       [g_DataWidth-1:0] WrData_ib,

    //==== Wishbone interface (Read) ====//

    input                         Rst_ir,
    input                         Clk_ik,
    input                         Cyc_i,
    input                         Stb_i,
    input       [g_AddrWidth-1:0] Adr_ib,
    output  reg [g_DataWidth-1:0] Dat_oqb,
    output  reg                   Ack_oq

);
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

wire [g_DataWidth-1:0] Dat_b;
reg                    Ack_q;

//=======================================  User Logic  =======================================//

// Generic DPRAM:
generic_dpram_mod #(
    .aw    (g_AddrWidth),
    .dw    (g_DataWidth))
i_Dpram (
    .rclk  (Clk_ik),
    .rrst  (Rst_ir),
    .rce   (1'b1),
    .oe    (1'b1),
    .raddr (Adr_ib),
    .dout  (Dat_b),
    //---
    .wclk  (WrClk_ik),
    .wrst  (~WrRst_ir),
    .wce   (1'b1),
    .we    (WrEn_i),
    .waddr (WrAddr_ib),
    .di    (WrData_ib));

// Wishbone interface:
always @(posedge Clk_ik)
    if (Rst_ir) begin
        Ack_q   <= 1'b0;
        Ack_oq  <= 1'b0;
        //--
        Dat_oqb <=  'h0;
    end else begin
        Ack_q   <= Cyc_i && Stb_i && ~Ack_q & ~Ack_oq; // Comment: Acknowledge delayed 1 clk cycle due to DPRAM latency
        Ack_oq  <= Ack_q;
        //--
        Dat_oqb <= ~Ack_oq ? Dat_b : Dat_oqb;
    end

endmodule
