`timescale 1ns/100ps

// Mod by M. Barros Marin (05/07/18): Re-written

module Generic16OutputRegs
#(  parameter Reg0Default      = 32'h00000000,
              Reg0AutoClrMask  = 32'hFFFFFFFF,
              Reg1Default      = 32'h00000000,
              Reg1AutoClrMask  = 32'hFFFFFFFF,
              Reg2Default      = 32'h00000000,
              Reg2AutoClrMask  = 32'hFFFFFFFF,
              Reg3Default      = 32'h00000000,
              Reg3AutoClrMask  = 32'hFFFFFFFF,
              Reg4Default      = 32'h00000000,
              Reg4AutoClrMask  = 32'hFFFFFFFF,
              Reg5Default      = 32'h00000000,
              Reg5AutoClrMask  = 32'hFFFFFFFF,
              Reg6Default      = 32'h00000000,
              Reg6AutoClrMask  = 32'hFFFFFFFF,
              Reg7Default      = 32'h00000000,
              Reg7AutoClrMask  = 32'hFFFFFFFF,
              Reg8Default      = 32'h00000000,
              Reg8AutoClrMask  = 32'hFFFFFFFF,
              Reg9Default      = 32'h00000000,
              Reg9AutoClrMask  = 32'hFFFFFFFF,
              Reg10Default     = 32'h00000000,
              Reg10AutoClrMask = 32'hFFFFFFFF,
              Reg11Default     = 32'h00000000,
              Reg11AutoClrMask = 32'hFFFFFFFF,
              Reg12Default     = 32'h00000000,
              Reg12AutoClrMask = 32'hFFFFFFFF,
              Reg13Default     = 32'h00000000,
              Reg13AutoClrMask = 32'hFFFFFFFF,
              Reg14Default     = 32'h00000000,
              Reg14AutoClrMask = 32'hFFFFFFFF,
              Reg15Default     = 32'h00000000,
              Reg15AutoClrMask = 32'hFFFFFFFF
)(
    input              Clk_ik,
    input              Rst_irq,
    input              Cyc_i,
    input              Stb_i,
    input              We_i,
    input       [ 3:0] Adr_ib4,
    input       [31:0] Dat_ib32,
    output      [31:0] Dat_oab32,
    output  reg        Ack_oa,
    output      [31:0] Reg0Value_ob32,
    output      [31:0] Reg1Value_ob32,
    output      [31:0] Reg2Value_ob32,
    output      [31:0] Reg3Value_ob32,
    output      [31:0] Reg4Value_ob32,
    output      [31:0] Reg5Value_ob32,
    output      [31:0] Reg6Value_ob32,
    output      [31:0] Reg7Value_ob32,
    output      [31:0] Reg8Value_ob32,
    output      [31:0] Reg9Value_ob32,
    output      [31:0] Reg10Value_ob32,
    output      [31:0] Reg11Value_ob32,
    output      [31:0] Reg12Value_ob32,
    output      [31:0] Reg13Value_ob32,
    output      [31:0] Reg14Value_ob32,
    output      [31:0] Reg15Value_ob32
);

reg [31:0] Reg_q16b32 [15:0];

always @(posedge Clk_ik)
    if (Rst_irq) begin
        Reg_q16b32[ 0] <= #1 Reg0Default;
        Reg_q16b32[ 1] <= #1 Reg1Default;
        Reg_q16b32[ 2] <= #1 Reg2Default;
        Reg_q16b32[ 3] <= #1 Reg3Default;
        Reg_q16b32[ 4] <= #1 Reg4Default;
        Reg_q16b32[ 5] <= #1 Reg5Default;
        Reg_q16b32[ 6] <= #1 Reg6Default;
        Reg_q16b32[ 7] <= #1 Reg7Default;
        Reg_q16b32[ 8] <= #1 Reg7Default;
        Reg_q16b32[ 9] <= #1 Reg7Default;
        Reg_q16b32[10] <= #1 Reg7Default;
        Reg_q16b32[11] <= #1 Reg7Default;
        Reg_q16b32[12] <= #1 Reg7Default;
        Reg_q16b32[13] <= #1 Reg7Default;
        Reg_q16b32[14] <= #1 Reg7Default;
        Reg_q16b32[15] <= #1 Reg7Default;
        Ack_oa         <= #1 1'b0;
    end else begin
        Reg_q16b32[ 0] <= #1 Reg_q16b32[ 0] & Reg0AutoClrMask;
        Reg_q16b32[ 1] <= #1 Reg_q16b32[ 1] & Reg1AutoClrMask;
        Reg_q16b32[ 2] <= #1 Reg_q16b32[ 2] & Reg2AutoClrMask;
        Reg_q16b32[ 3] <= #1 Reg_q16b32[ 3] & Reg3AutoClrMask;
        Reg_q16b32[ 4] <= #1 Reg_q16b32[ 4] & Reg4AutoClrMask;
        Reg_q16b32[ 5] <= #1 Reg_q16b32[ 5] & Reg5AutoClrMask;
        Reg_q16b32[ 6] <= #1 Reg_q16b32[ 6] & Reg6AutoClrMask;
        Reg_q16b32[ 7] <= #1 Reg_q16b32[ 7] & Reg7AutoClrMask;
        Reg_q16b32[ 8] <= #1 Reg_q16b32[ 8] & Reg8AutoClrMask;
        Reg_q16b32[ 9] <= #1 Reg_q16b32[ 9] & Reg9AutoClrMask;
        Reg_q16b32[10] <= #1 Reg_q16b32[10] & Reg10AutoClrMask;
        Reg_q16b32[11] <= #1 Reg_q16b32[11] & Reg11AutoClrMask;
        Reg_q16b32[12] <= #1 Reg_q16b32[12] & Reg12AutoClrMask;
        Reg_q16b32[13] <= #1 Reg_q16b32[13] & Reg13AutoClrMask;
        Reg_q16b32[14] <= #1 Reg_q16b32[14] & Reg14AutoClrMask;
        Reg_q16b32[15] <= #1 Reg_q16b32[15] & Reg15AutoClrMask;
        if (Cyc_i && We_i && Stb_i) Reg_q16b32[Adr_ib4] <= #1 Dat_ib32;
        Ack_oa         <= #1 Stb_i && Cyc_i;
    end

assign Reg0Value_ob32  = Reg_q16b32[ 0];
assign Reg1Value_ob32  = Reg_q16b32[ 1];
assign Reg2Value_ob32  = Reg_q16b32[ 2];
assign Reg3Value_ob32  = Reg_q16b32[ 3];
assign Reg4Value_ob32  = Reg_q16b32[ 4];
assign Reg5Value_ob32  = Reg_q16b32[ 5];
assign Reg6Value_ob32  = Reg_q16b32[ 6];
assign Reg7Value_ob32  = Reg_q16b32[ 7];
assign Reg8Value_ob32  = Reg_q16b32[ 8];
assign Reg9Value_ob32  = Reg_q16b32[ 9];
assign Reg10Value_ob32 = Reg_q16b32[10];
assign Reg11Value_ob32 = Reg_q16b32[11];
assign Reg12Value_ob32 = Reg_q16b32[12];
assign Reg13Value_ob32 = Reg_q16b32[13];
assign Reg14Value_ob32 = Reg_q16b32[14];
assign Reg15Value_ob32 = Reg_q16b32[15];
assign Dat_oab32       = Reg_q16b32[Adr_ib4];

endmodule
