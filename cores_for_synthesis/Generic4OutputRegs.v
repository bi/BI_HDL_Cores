`timescale 1ns/100ps

// Mod by M. Barros Marin (02/03/16): Re-written

module Generic4OutputRegs
#(  parameter          Reg0Default     = 32'h00000000,
                       Reg0AutoClrMask = 32'hFFFFFFFF,
                       Reg1Default     = 32'h00000000,
                       Reg1AutoClrMask = 32'hFFFFFFFF,
                       Reg2Default     = 32'h00000000,
                       Reg2AutoClrMask = 32'hFFFFFFFF,
                       Reg3Default     = 32'h00000000,
                       Reg3AutoClrMask = 32'hFFFFFFFF
)(
    input              Clk_ik,
    input              Rst_irq,
    input              Cyc_i,
    input              Stb_i,
    input              We_i,
    input       [ 1:0] Adr_ib2,
    input       [31:0] Dat_ib32,
    output      [31:0] Dat_oab32,
    output  reg        Ack_oa,
    output      [31:0] Reg0Value_ob32,
    output      [31:0] Reg1Value_ob32,
    output      [31:0] Reg2Value_ob32,
    output      [31:0] Reg3Value_ob32
);

reg [31:0] Reg_q4b32 [3:0];

always @(posedge Clk_ik)
    if (Rst_irq) begin
        Reg_q4b32[0] <= #1 Reg0Default;
        Reg_q4b32[1] <= #1 Reg1Default;
        Reg_q4b32[2] <= #1 Reg2Default;
        Reg_q4b32[3] <= #1 Reg3Default;
        Ack_oa       <= #1 1'b0;
    end else begin
        Reg_q4b32[0] <= #1 Reg_q4b32[0] & Reg0AutoClrMask;
        Reg_q4b32[1] <= #1 Reg_q4b32[1] & Reg1AutoClrMask;
        Reg_q4b32[2] <= #1 Reg_q4b32[2] & Reg2AutoClrMask;
        Reg_q4b32[3] <= #1 Reg_q4b32[3] & Reg3AutoClrMask;
        if (Cyc_i && We_i && Stb_i && ~Ack_oa) Reg_q4b32[Adr_ib2] <= #1 Dat_ib32;
        Ack_oa       <= #1 Stb_i&&Cyc_i;
    end

assign Reg0Value_ob32 = Reg_q4b32[0];
assign Reg1Value_ob32 = Reg_q4b32[1];
assign Reg2Value_ob32 = Reg_q4b32[2];
assign Reg3Value_ob32 = Reg_q4b32[3];
assign Dat_oab32      = Reg_q4b32[Adr_ib2];

endmodule
