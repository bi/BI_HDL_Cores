`timescale 1ns/100ps 

module I2cMasterWb #(  parameter    g_CycleLenght = 10'h3ff)
(   input   Clk_ik,
    input   Rst_irq,
    input   Cyc_i,
    input   Stb_i,
    input   We_i,
    input   [1:0] Adr_ib2,
    input   [31:0] Dat_ib32,
    output  reg [31:0] Dat_oab32,
    output  Ack_oa,
    
    inout Scl_ioz,
    inout Sda_ioz);


 
reg  [3:0] BitCounter_c4; 
reg  [9:0] CycleCounter_c10;   
reg  [31:0] CommandReg_qb32;
wire [7:0] a_Command_b8 = CommandReg_qb32[31:24]; 
reg SclOe_e , SdaOe_e;
reg [7:0] ShReg_b8;
reg I2CAck;


localparam  s_Idle              = 4'h0,
            s_StartSda1         = 4'h1,
            s_StartScl1         = 4'h2,
            s_StartSda0         = 4'h3,
            s_StartScl0         = 4'h4,
            s_SendScl0          = 4'h5,
            s_SendScl1          = 4'h6,
            s_GetScl0           = 4'h7,
            s_GetScl1           = 4'h8,
            s_StopSda0          = 4'h9,
            s_StopScl1          = 4'ha,
            s_StopSda1          = 4'hb;
            
reg [3:0] State_qb4, NextState_ab4;
 
localparam  c_SendStartBit  = 8'h01,
            c_SendByte      = 8'h02,
            c_GetByte       = 8'h03,
            c_SendStopBit   = 8'h04;             
 
always @(posedge Clk_ik) State_qb4 <= #1 Rst_irq ?  s_Idle :  NextState_ab4;

always @* begin
    NextState_ab4 =  State_qb4;
    case(State_qb4)
    s_Idle             : if (a_Command_b8 == c_SendStartBit) NextState_ab4 = s_StartSda1;
                         else if (a_Command_b8 == c_SendByte) NextState_ab4 = s_SendScl0;
                         else if (a_Command_b8 == c_GetByte) NextState_ab4 = s_GetScl0;
                         else if (a_Command_b8 == c_SendStopBit) NextState_ab4 = s_StopSda0;
    
    s_StartSda1        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StartScl1;
    s_StartScl1        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StartSda0;
    s_StartSda0        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StartScl0;
    s_StartScl0        : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_Idle;

    s_SendScl0         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = (BitCounter_c4==4'd9) ? s_Idle : s_SendScl1;
    s_SendScl1         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_SendScl0;

    s_GetScl0          : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = (BitCounter_c4==4'd9) ? s_Idle : s_GetScl1;
    s_GetScl1          : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_GetScl0;

    s_StopSda0         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StopScl1;
    s_StopScl1         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_StopSda1;
    s_StopSda1         : if (CycleCounter_c10==g_CycleLenght) NextState_ab4 = s_Idle;

    default: NextState_ab4 = s_Idle;
    endcase 
end         

   
always @(posedge Clk_ik)
    if  (Rst_irq) begin
        SclOe_e             <= #1 1'b0;
        SdaOe_e             <= #1 1'b0;        
        BitCounter_c4       <= #1 'h0;
        CycleCounter_c10    <= #1 'h0;
        I2CAck              <= #1 CommandReg_qb32[0];  
        ShReg_b8            <= #1 CommandReg_qb32[7:0];      
    end else case(State_qb4)
        s_Idle : begin
            BitCounter_c4       <= #1 'h0;
            CycleCounter_c10    <= #1 'h0;
            if (NextState_ab4==s_SendScl0)  ShReg_b8    <= #1 CommandReg_qb32[7:0];   
            if (NextState_ab4==s_GetScl0)   I2CAck      <= #1 CommandReg_qb32[0];      
        end     
                
        s_StartSda1 : begin
            SdaOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;
        end             
        s_StartScl1 : begin
            SclOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end          
        s_StartSda0 : begin
            SdaOe_e <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end             
        s_StartScl0 : begin
            SclOe_e <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end   
                  
        s_SendScl0 : begin
            SclOe_e             <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0; 
            if  (CycleCounter_c10[8:0]==g_CycleLenght[9:1])  SdaOe_e <= #1 BitCounter_c4[3] ? 1'b0 : !ShReg_b8[7];                
        end
        s_SendScl1 : begin
            SclOe_e             <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) begin
                BitCounter_c4       <= #1 BitCounter_c4 + 1'b1;
                CycleCounter_c10    <= #1 'h0;
                ShReg_b8[7:1]       <= #1 ShReg_b8[6:0];
                I2CAck              <= #1 Sda_ioz; 
            end else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;                          
        end              
        
        s_GetScl0 : begin
            SclOe_e             <= #1 1'b1;
            SdaOe_e             <= #1 (BitCounter_c4==4'd8) ? !I2CAck : 1'b0;        
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;         
        end                              
        s_GetScl1 : begin
            SclOe_e             <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) begin
                BitCounter_c4       <= #1 BitCounter_c4 + 1'b1;
                CycleCounter_c10    <= #1 'h0;
                ShReg_b8            <= #1 (BitCounter_c4==4'd8) ? ShReg_b8 : {ShReg_b8[6:0], Sda_ioz};         
            end else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;                
        end  
                     
        s_StopSda0 : begin
            SdaOe_e <= #1 1'b1;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (!Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;  
        end              
        s_StopScl1 : begin
            SclOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Scl_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end              
        s_StopSda1 : begin
            SdaOe_e <= #1 1'b0;
            if (NextState_ab4 !=  State_qb4) CycleCounter_c10    <= #1 'h0;
            else if (Sda_ioz) CycleCounter_c10 <= #1 CycleCounter_c10 + 1'b1;
            else CycleCounter_c10    <= #1 'h0;        
        end  
           
        default: begin
            SclOe_e             <= #1 1'b0;
            SdaOe_e             <= #1 1'b0;        
            BitCounter_c4       <= #1 'h0;
            CycleCounter_c10    <= #1 'h0;
            I2CAck              <= #1 CommandReg_qb32[0];  
            ShReg_b8            <= #1 CommandReg_qb32[7:0]; 
        end
        endcase
 
    
assign Scl_ioz = SclOe_e ? 1'b0 : 1'bz;
assign Sda_ioz = SdaOe_e ? 1'b0 : 1'bz;    
        
always @(posedge Clk_ik) 
   	if (Rst_irq) CommandReg_qb32 <= #1 'h8000_0000;
   	else if (Cyc_i && We_i && Stb_i && Adr_ib2==2'b01) CommandReg_qb32 <= #1 Dat_ib32;
   	else CommandReg_qb32[31] <= #1 1'b1;
    
assign Ack_oa = Stb_i&&Cyc_i;

always @* case (Adr_ib2)
	2'b00: Dat_oab32 = {State_qb4, 4'b0, 2'b0, Scl_ioz, ~SclOe_e, 2'b0, Sda_ioz, ~SdaOe_e, 4'h0, 3'h0, I2CAck, ShReg_b8};
	2'b01: Dat_oab32 = CommandReg_qb32;
	default: Dat_oab32 = 32'hDEAD_BEEF;
endcase            
          
endmodule
