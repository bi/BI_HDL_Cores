`timescale 1ns/100ps 

// Added by M. Barros Marin (05/12/16)

module Generic8InputRegs (
	input	          Clk_ik,
    input             Rst_irq,
    input             Cyc_i,
    input             Stb_i,
    input      [ 2:0] Adr_ib3,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,
    input      [31:0] Reg0Value_ib32,
    input      [31:0] Reg1Value_ib32,
    input      [31:0] Reg2Value_ib32,
    input      [31:0] Reg3Value_ib32,
    input      [31:0] Reg4Value_ib32,
    input      [31:0] Reg5Value_ib32,
    input      [31:0] Reg6Value_ib32,
    input      [31:0] Reg7Value_ib32
);
	
always @(posedge Clk_ik)
    if (Rst_irq) begin
        Dat_oab32                  <= #1 32'h0;
        Ack_oa                     <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib3)
                3'b000:  Dat_oab32 <= #1 Reg0Value_ib32;
                3'b001:  Dat_oab32 <= #1 Reg1Value_ib32;
                3'b010:  Dat_oab32 <= #1 Reg2Value_ib32;
                3'b011:  Dat_oab32 <= #1 Reg3Value_ib32;
                3'b100:  Dat_oab32 <= #1 Reg4Value_ib32;
                3'b101:  Dat_oab32 <= #1 Reg5Value_ib32;
                3'b110:  Dat_oab32 <= #1 Reg6Value_ib32;
                3'b111:  Dat_oab32 <= #1 Reg7Value_ib32;
                default: Dat_oab32 <= #1 Reg0Value_ib32;
            endcase
        end
        Ack_oa                     <= #1 Stb_i && Cyc_i;
    end

endmodule