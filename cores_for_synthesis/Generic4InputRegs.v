`timescale 1ns/100ps 

// Mod by M. Barros Marin (02/03/16): Re-written

module Generic4InputRegs (
	input	          Clk_ik,
    input             Rst_irq,
    input             Cyc_i,
    input             Stb_i,
    input      [ 1:0] Adr_ib2,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,
    input      [31:0] Reg0Value_ib32,
    input      [31:0] Reg1Value_ib32,
    input      [31:0] Reg2Value_ib32,
    input      [31:0] Reg3Value_ib32
);
	
always @(posedge Clk_ik) begin
    Ack_oa                     <= #1 Stb_i && Cyc_i;
    if (~Ack_oa) begin
        case (Adr_ib2)
            2'b00:   Dat_oab32 <= #1 Reg0Value_ib32;
            2'b01:   Dat_oab32 <= #1 Reg1Value_ib32;
            2'b10:   Dat_oab32 <= #1 Reg2Value_ib32;
            2'b11:   Dat_oab32 <= #1 Reg3Value_ib32;
            default: Dat_oab32 <= #1 Reg0Value_ib32;
        endcase
    end
end

endmodule