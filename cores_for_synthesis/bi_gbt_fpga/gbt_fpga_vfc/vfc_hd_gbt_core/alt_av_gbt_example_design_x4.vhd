--=================================================================================================--
--##################################   Module Information   #######################################--
--=================================================================================================--
--                                                                                         
-- Company:               CERN (PH-ESE-BE)                                                         
-- Engineer:              Manoel Barros Marin (manoel.barros.marin@cern.ch)
--                                                                                                 
-- Project Name:          GBT-FPGA                                                                
-- Module Name:           Altera Arria V - GBT Bank example design x4                                         
--                                                                                                 
-- Language:              VHDL'93                                                                  
--                                                                                                   
-- Target Device:         Altera Arria V                                                        
-- Tool version:          Quartus Prime 16.1                                                               
--                                                                                                   
-- Version:               4.0                                                                      
--
-- Description:            
--
-- Versions history:      DATE         VERSION   AUTHOR            DESCRIPTION
--
--                        21/04/2017   4.1       M. Barros Marin   Renamed and minor modifications.
--                        19/07/2016   4.0       M. Barros Marin   First .vhd module definition.
--
-- Description:
--
--    Wrapper to adapt the GBT-FPGA Example Design, featuring 4 GBT Link, for begin instantiated in Verilog.
--                                                                                              
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Altera devices library:
library altera; 
library altera_mf;
library lpm;
use altera.altera_primitives_components.all;   
use altera_mf.altera_mf_components.all;
use lpm.lpm_components.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_banks_user_setup.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity alt_av_gbt_example_design_x4 is 
   generic (
      SYNTHESIS                                 : integer := 1;
      GBT_BANK_ID                               : integer := 0;
      TX_OPTIMIZATION                           : integer range 0 to 1 := STANDARD;
      RX_OPTIMIZATION                           : integer range 0 to 1 := STANDARD;
      TX_ENCODING                               : integer range 0 to 1 := GBT_FRAME;
      RX_ENCODING                               : integer range 0 to 1 := GBT_FRAME;      
      -- Extended configuration --
      DATA_GENERATOR_ENABLE                     : integer range 0 to 1 := 1;
      DATA_CHECKER_ENABLE                       : integer range 0 to 1 := 1;
      MATCH_FLAG_ENABLE                         : integer range 0 to 1 := 1
   );
   port ( 

      --==============--
      -- Clocks       --
      --==============--
      
      TX_FRAMECLK_40MHZ_I                       : in  std_logic;
      MGT_REFCLK_120MHZ_I                       : in  std_logic;
      RX_FRAMECLK_1_O                           : out std_logic;
      RX_FRAMECLK_2_O                           : out std_logic;
      RX_FRAMECLK_3_O                           : out std_logic;
      RX_FRAMECLK_4_O                           : out std_logic;
      TX_WORDCLK_1_O                            : out std_logic;
      TX_WORDCLK_2_O                            : out std_logic;
      TX_WORDCLK_3_O                            : out std_logic;
      TX_WORDCLK_4_O                            : out std_logic;
      RX_WORDCLK_1_O                            : out std_logic;
      RX_WORDCLK_2_O                            : out std_logic;
      RX_WORDCLK_3_O                            : out std_logic;
      RX_WORDCLK_4_O                            : out std_logic;
      
      --==============--
      -- Reset        --
      --==============--
      
      GBTBANK_GENERAL_RESET_I                   : in  std_logic;
      GBTBANK_MANUAL_RESET_TX_1_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_TX_2_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_TX_3_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_TX_4_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_RX_1_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_RX_2_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_RX_3_I               : in  std_logic;
      GBTBANK_MANUAL_RESET_RX_4_I               : in  std_logic;
      
      --==============--
      -- Serial lanes --
      --==============--
      
      GBTBANK_MGT_RX_1_I                        : in  std_logic;
      GBTBANK_MGT_RX_2_I                        : in  std_logic;
      GBTBANK_MGT_RX_3_I                        : in  std_logic;
      GBTBANK_MGT_RX_4_I                        : in  std_logic;
      GBTBANK_MGT_TX_1_O                        : out std_logic;
      GBTBANK_MGT_TX_2_O                        : out std_logic;
      GBTBANK_MGT_TX_3_O                        : out std_logic;
      GBTBANK_MGT_TX_4_O                        : out std_logic;
      
      --==============--
      -- Data         --
      --==============--   
      
      GBTBANK_TX_DATA_1_I                       : in  std_logic_vector(115 downto 0);
      GBTBANK_TX_DATA_2_I                       : in  std_logic_vector(115 downto 0);
      GBTBANK_TX_DATA_3_I                       : in  std_logic_vector(115 downto 0);
      GBTBANK_TX_DATA_4_I                       : in  std_logic_vector(115 downto 0);
      GBTBANK_RX_DATA_1_O                       : out std_logic_vector(115 downto 0);
      GBTBANK_RX_DATA_2_O                       : out std_logic_vector(115 downto 0);
      GBTBANK_RX_DATA_3_O                       : out std_logic_vector(115 downto 0);
      GBTBANK_RX_DATA_4_O                       : out std_logic_vector(115 downto 0);
      
      --==============--
      -- Reconf.      --
      --==============--
      
      GBTBANK_RECONF_AVMM_RST_I                 : in  std_logic;
      GBTBANK_RECONF_AVMM_CLK_I                 : in  std_logic;
      GBTBANK_RECONF_AVMM_ADDR_I                : in  std_logic_vector(6 downto 0);
      GBTBANK_RECONF_AVMM_READ_I                : in  std_logic;
      GBTBANK_RECONF_AVMM_WRITE_I               : in  std_logic;
      GBTBANK_RECONF_AVMM_WRITEDATA_I           : in  std_logic_vector(31 downto 0);
      GBTBANK_RECONF_AVMM_READDATA_O            : out std_logic_vector(31 downto 0);
      GBTBANK_RECONF_AVMM_WAITREQUEST_O         : out std_logic;
         
      --==============--
      -- Control      --
      --==============--
      
      GBTBANK_TX_ISDATA_SEL_1_I                 : in  std_logic;
      GBTBANK_TX_ISDATA_SEL_2_I                 : in  std_logic;
      GBTBANK_TX_ISDATA_SEL_3_I                 : in  std_logic;
      GBTBANK_TX_ISDATA_SEL_4_I                 : in  std_logic;
      TEST_SCEC_CHECK_EN_1_I                    : in  std_logic;
      TEST_SCEC_CHECK_EN_2_I                    : in  std_logic;
      TEST_SCEC_CHECK_EN_3_I                    : in  std_logic;
      TEST_SCEC_CHECK_EN_4_I                    : in  std_logic;
      TOGGLING_DATA_CHECK_SYNC_1_I              : in  std_logic;
      TOGGLING_DATA_CHECK_SYNC_2_I              : in  std_logic;
      TOGGLING_DATA_CHECK_SYNC_3_I              : in  std_logic;
      TOGGLING_DATA_CHECK_SYNC_4_I              : in  std_logic;                                               
      GBTBANK_TEST_PATTERN_SEL_I                : in  std_logic_vector(2 downto 0);      
      GBTBANK_RESET_GBTRXREADY_LOST_FLAG_1_I    : in  std_logic;             
      GBTBANK_RESET_GBTRXREADY_LOST_FLAG_2_I    : in  std_logic;             
      GBTBANK_RESET_GBTRXREADY_LOST_FLAG_3_I    : in  std_logic;             
      GBTBANK_RESET_GBTRXREADY_LOST_FLAG_4_I    : in  std_logic;             
      GBTBANK_RESET_DATA_ERRORS_1_I             : in  std_logic;         
      GBTBANK_RESET_DATA_ERRORS_2_I             : in  std_logic;         
      GBTBANK_RESET_DATA_ERRORS_3_I             : in  std_logic;         
      GBTBANK_RESET_DATA_ERRORS_4_I             : in  std_logic;         
      
      --==============--
      -- Status       --
      --==============--
      
      GBTBANK_LINK_READY_1_O                    : out std_logic;
      GBTBANK_LINK_READY_2_O                    : out std_logic;
      GBTBANK_LINK_READY_3_O                    : out std_logic;
      GBTBANK_LINK_READY_4_O                    : out std_logic;
      GBTBANK_LINK_TX_READY_1_O                 : out std_logic;
      GBTBANK_LINK_TX_READY_2_O                 : out std_logic;
      GBTBANK_LINK_TX_READY_3_O                 : out std_logic;
      GBTBANK_LINK_TX_READY_4_O                 : out std_logic;
      GBTBANK_LINK_RX_READY_1_O                 : out std_logic;      
      GBTBANK_LINK_RX_READY_2_O                 : out std_logic;      
      GBTBANK_LINK_RX_READY_3_O                 : out std_logic;      
      GBTBANK_LINK_RX_READY_4_O                 : out std_logic;      
      GBTBANK_GBTRX_READY_1_O                   : out std_logic;
      GBTBANK_GBTRX_READY_2_O                   : out std_logic;
      GBTBANK_GBTRX_READY_3_O                   : out std_logic;
      GBTBANK_GBTRX_READY_4_O                   : out std_logic;
      GBTBANK_RX_ISDATA_FLAG_1_O                : out std_logic;
      GBTBANK_RX_ISDATA_FLAG_2_O                : out std_logic;
      GBTBANK_RX_ISDATA_FLAG_3_O                : out std_logic;
      GBTBANK_RX_ISDATA_FLAG_4_O                : out std_logic;
      GBTBANK_GBTRXREADY_LOST_FLAG_1_O          : out std_logic;
      GBTBANK_GBTRXREADY_LOST_FLAG_2_O          : out std_logic;
      GBTBANK_GBTRXREADY_LOST_FLAG_3_O          : out std_logic;
      GBTBANK_GBTRXREADY_LOST_FLAG_4_O          : out std_logic;
      GBTBANK_RXDATA_ERRORSEEN_FLAG_1_O         : out std_logic;
      GBTBANK_RXDATA_ERRORSEEN_FLAG_2_O         : out std_logic;
      GBTBANK_RXDATA_ERRORSEEN_FLAG_3_O         : out std_logic;
      GBTBANK_RXDATA_ERRORSEEN_FLAG_4_O         : out std_logic;
      GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_1_O : out std_logic;
      GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_2_O : out std_logic;
      GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_3_O : out std_logic;
      GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_4_O : out std_logic;
      GBTBANK_TX_MATCHFLAG_O                    : out std_logic;
      GBTBANK_RX_MATCHFLAG_1_O                  : out std_logic;
      GBTBANK_RX_MATCHFLAG_2_O                  : out std_logic;
      GBTBANK_RX_MATCHFLAG_3_O                  : out std_logic;
      GBTBANK_RX_MATCHFLAG_4_O                  : out std_logic;
      
      --==============--
      -- XCVR ctrl    --
      --==============--
      
      GBTBANK_LOOPBACK_1_I                      : in  std_logic;
      GBTBANK_LOOPBACK_2_I                      : in  std_logic;
      GBTBANK_LOOPBACK_3_I                      : in  std_logic;
      GBTBANK_LOOPBACK_4_I                      : in  std_logic;
      GBTBANK_TX_POL_1_I                        : in  std_logic;
      GBTBANK_TX_POL_2_I                        : in  std_logic;
      GBTBANK_TX_POL_3_I                        : in  std_logic;
      GBTBANK_TX_POL_4_I                        : in  std_logic;
      GBTBANK_RX_POL_1_I                        : in  std_logic;
      GBTBANK_RX_POL_2_I                        : in  std_logic;
      GBTBANK_RX_POL_3_I                        : in  std_logic;
      GBTBANK_RX_POL_4_I                        : in  std_logic;     
      GBTBANK_TXWORDCLKMON_EN_I                 : in  std_logic     
     
   );
end alt_av_gbt_example_design_x4;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture structural of alt_av_gbt_example_design_x4 is 
   
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--   
   
   i_GbtExmplDsgn: entity work.alt_av_gbt_example_design
      generic map (
         SYNTHESIS                                  => SYNTHESIS,
         GBT_BANK_ID                                => GBT_BANK_ID,    
         NUM_LINKS                                  => 4,   
         TX_OPTIMIZATION                            => TX_OPTIMIZATION,      
         RX_OPTIMIZATION                            => RX_OPTIMIZATION,
         TX_ENCODING                                => TX_ENCODING,
         RX_ENCODING                                => RX_ENCODING,    
         -- Extended configuration:                 
         DATA_GENERATOR_ENABLE                      => DATA_GENERATOR_ENABLE,
         DATA_CHECKER_ENABLE                        => DATA_CHECKER_ENABLE,  
         MATCH_FLAG_ENABLE                          => MATCH_FLAG_ENABLE)    
      port map (
         -- Clocks:
         TX_FRAMECLK_40MHZ_I                        => TX_FRAMECLK_40MHZ_I,
         MGT_REFCLK_120MHZ_I                        => MGT_REFCLK_120MHZ_I,
         RX_FRAMECLK_O(1)                           => RX_FRAMECLK_1_O,    
         RX_FRAMECLK_O(2)                           => RX_FRAMECLK_2_O,    
         RX_FRAMECLK_O(3)                           => RX_FRAMECLK_3_O,    
         RX_FRAMECLK_O(4)                           => RX_FRAMECLK_4_O,    
         TX_WORDCLK_O(1)                            => TX_WORDCLK_1_O,     
         TX_WORDCLK_O(2)                            => TX_WORDCLK_2_O,     
         TX_WORDCLK_O(3)                            => TX_WORDCLK_3_O,     
         TX_WORDCLK_O(4)                            => TX_WORDCLK_4_O,     
         RX_WORDCLK_O(1)                            => RX_WORDCLK_1_O,     
         RX_WORDCLK_O(2)                            => RX_WORDCLK_2_O,     
         RX_WORDCLK_O(3)                            => RX_WORDCLK_3_O,     
         RX_WORDCLK_O(4)                            => RX_WORDCLK_4_O,     
         -- Reset:
         GBTBANK_GENERAL_RESET_I                    => GBTBANK_GENERAL_RESET_I,   
         GBTBANK_MANUAL_RESET_TX_I(1)               => GBTBANK_MANUAL_RESET_TX_1_I,
         GBTBANK_MANUAL_RESET_TX_I(2)               => GBTBANK_MANUAL_RESET_TX_2_I,
         GBTBANK_MANUAL_RESET_TX_I(3)               => GBTBANK_MANUAL_RESET_TX_3_I,
         GBTBANK_MANUAL_RESET_TX_I(4)               => GBTBANK_MANUAL_RESET_TX_4_I,
         GBTBANK_MANUAL_RESET_RX_I(1)               => GBTBANK_MANUAL_RESET_RX_1_I,
         GBTBANK_MANUAL_RESET_RX_I(2)               => GBTBANK_MANUAL_RESET_RX_2_I,
         GBTBANK_MANUAL_RESET_RX_I(3)               => GBTBANK_MANUAL_RESET_RX_3_I,
         GBTBANK_MANUAL_RESET_RX_I(4)               => GBTBANK_MANUAL_RESET_RX_4_I,
         -- Serial lanes:
         GBTBANK_MGT_RX_I(1)                        => GBTBANK_MGT_RX_1_I,                    
         GBTBANK_MGT_RX_I(2)                        => GBTBANK_MGT_RX_2_I,                    
         GBTBANK_MGT_RX_I(3)                        => GBTBANK_MGT_RX_3_I,                    
         GBTBANK_MGT_RX_I(4)                        => GBTBANK_MGT_RX_4_I,                    
         GBTBANK_MGT_TX_O(1)                        => GBTBANK_MGT_TX_1_O,
         GBTBANK_MGT_TX_O(2)                        => GBTBANK_MGT_TX_2_O,
         GBTBANK_MGT_TX_O(3)                        => GBTBANK_MGT_TX_3_O,
         GBTBANK_MGT_TX_O(4)                        => GBTBANK_MGT_TX_4_O,
         -- Data:
         GBTBANK_GBT_TX_DATA_I(1)                   => GBTBANK_TX_DATA_1_I(115 downto 32),     
         GBTBANK_GBT_TX_DATA_I(2)                   => GBTBANK_TX_DATA_2_I(115 downto 32),     
         GBTBANK_GBT_TX_DATA_I(3)                   => GBTBANK_TX_DATA_3_I(115 downto 32),     
         GBTBANK_GBT_TX_DATA_I(4)                   => GBTBANK_TX_DATA_4_I(115 downto 32),     
         GBTBANK_WB_TX_DATA_I(1)                    => GBTBANK_TX_DATA_1_I, 
         GBTBANK_WB_TX_DATA_I(2)                    => GBTBANK_TX_DATA_2_I, 
         GBTBANK_WB_TX_DATA_I(3)                    => GBTBANK_TX_DATA_3_I, 
         GBTBANK_WB_TX_DATA_I(4)                    => GBTBANK_TX_DATA_4_I, 
         GBTBANK_GBT_RX_DATA_O(1)                   => open,
         GBTBANK_GBT_RX_DATA_O(2)                   => open,
         GBTBANK_GBT_RX_DATA_O(3)                   => open,
         GBTBANK_GBT_RX_DATA_O(4)                   => open,
         GBTBANK_WB_RX_DATA_O(1)                    => GBTBANK_RX_DATA_1_O, 
         GBTBANK_WB_RX_DATA_O(2)                    => GBTBANK_RX_DATA_2_O, 
         GBTBANK_WB_RX_DATA_O(3)                    => GBTBANK_RX_DATA_3_O, 
         GBTBANK_WB_RX_DATA_O(4)                    => GBTBANK_RX_DATA_4_O, 
         -- Reconf:
         GBTBANK_RECONF_AVMM_RST_I                  => GBTBANK_RECONF_AVMM_RST_I,        
         GBTBANK_RECONF_AVMM_CLK_I                  => GBTBANK_RECONF_AVMM_CLK_I,        
         GBTBANK_RECONF_AVMM_ADDR_I                 => GBTBANK_RECONF_AVMM_ADDR_I,       
         GBTBANK_RECONF_AVMM_READ_I                 => GBTBANK_RECONF_AVMM_READ_I,       
         GBTBANK_RECONF_AVMM_WRITE_I                => GBTBANK_RECONF_AVMM_WRITE_I,      
         GBTBANK_RECONF_AVMM_WRITEDATA_I            => GBTBANK_RECONF_AVMM_WRITEDATA_I,  
         GBTBANK_RECONF_AVMM_READDATA_O             => GBTBANK_RECONF_AVMM_READDATA_O,   
         GBTBANK_RECONF_AVMM_WAITREQUEST_O          => GBTBANK_RECONF_AVMM_WAITREQUEST_O,
         -- Control:
         GBTBANK_TX_ISDATA_SEL_I(1)                 => GBTBANK_TX_ISDATA_SEL_1_I,             
         GBTBANK_TX_ISDATA_SEL_I(2)                 => GBTBANK_TX_ISDATA_SEL_2_I,             
         GBTBANK_TX_ISDATA_SEL_I(3)                 => GBTBANK_TX_ISDATA_SEL_3_I,             
         GBTBANK_TX_ISDATA_SEL_I(4)                 => GBTBANK_TX_ISDATA_SEL_4_I,  
         TEST_SCEC_CHECK_EN_I(1)                    => TEST_SCEC_CHECK_EN_1_I,                 
         TEST_SCEC_CHECK_EN_I(2)                    => TEST_SCEC_CHECK_EN_2_I,                 
         TEST_SCEC_CHECK_EN_I(3)                    => TEST_SCEC_CHECK_EN_3_I,                 
         TEST_SCEC_CHECK_EN_I(4)                    => TEST_SCEC_CHECK_EN_4_I,                 
         TOGGLING_DATA_CHECK_SYNC_I(1)              => TOGGLING_DATA_CHECK_SYNC_1_I,
         TOGGLING_DATA_CHECK_SYNC_I(2)              => TOGGLING_DATA_CHECK_SYNC_2_I,
         TOGGLING_DATA_CHECK_SYNC_I(3)              => TOGGLING_DATA_CHECK_SYNC_3_I,
         TOGGLING_DATA_CHECK_SYNC_I(4)              => TOGGLING_DATA_CHECK_SYNC_4_I,
         GBTBANK_TEST_PATTERN_SEL_I                 => GBTBANK_TEST_PATTERN_SEL_I,          
         GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(1)    => GBTBANK_RESET_GBTRXREADY_LOST_FLAG_1_I,              
         GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(2)    => GBTBANK_RESET_GBTRXREADY_LOST_FLAG_2_I,              
         GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(3)    => GBTBANK_RESET_GBTRXREADY_LOST_FLAG_3_I,              
         GBTBANK_RESET_GBTRXREADY_LOST_FLAG_I(4)    => GBTBANK_RESET_GBTRXREADY_LOST_FLAG_4_I,              
         GBTBANK_RESET_DATA_ERRORS_I(1)             => GBTBANK_RESET_DATA_ERRORS_1_I, 
         GBTBANK_RESET_DATA_ERRORS_I(2)             => GBTBANK_RESET_DATA_ERRORS_2_I, 
         GBTBANK_RESET_DATA_ERRORS_I(3)             => GBTBANK_RESET_DATA_ERRORS_3_I, 
         GBTBANK_RESET_DATA_ERRORS_I(4)             => GBTBANK_RESET_DATA_ERRORS_4_I, 
         -- Status:
         GBTBANK_LINK_READY_O(1)                    => GBTBANK_LINK_READY_1_O,                   
         GBTBANK_LINK_READY_O(2)                    => GBTBANK_LINK_READY_2_O,                   
         GBTBANK_LINK_READY_O(3)                    => GBTBANK_LINK_READY_3_O,                   
         GBTBANK_LINK_READY_O(4)                    => GBTBANK_LINK_READY_4_O,                   
         GBTBANK_LINK_TX_READY_O(1)                 => GBTBANK_LINK_TX_READY_1_O,                
         GBTBANK_LINK_TX_READY_O(2)                 => GBTBANK_LINK_TX_READY_2_O,                
         GBTBANK_LINK_TX_READY_O(3)                 => GBTBANK_LINK_TX_READY_3_O,                
         GBTBANK_LINK_TX_READY_O(4)                 => GBTBANK_LINK_TX_READY_4_O,                
         GBTBANK_LINK_RX_READY_O(1)                 => GBTBANK_LINK_RX_READY_1_O,                
         GBTBANK_LINK_RX_READY_O(2)                 => GBTBANK_LINK_RX_READY_2_O,                
         GBTBANK_LINK_RX_READY_O(3)                 => GBTBANK_LINK_RX_READY_3_O,                
         GBTBANK_LINK_RX_READY_O(4)                 => GBTBANK_LINK_RX_READY_4_O,                
         GBTBANK_GBTRX_READY_O(1)                   => GBTBANK_GBTRX_READY_1_O,                  
         GBTBANK_GBTRX_READY_O(2)                   => GBTBANK_GBTRX_READY_2_O,                  
         GBTBANK_GBTRX_READY_O(3)                   => GBTBANK_GBTRX_READY_3_O,                  
         GBTBANK_GBTRX_READY_O(4)                   => GBTBANK_GBTRX_READY_4_O,                  
         GBTBANK_RX_ISDATA_FLAG_O(1)                => GBTBANK_RX_ISDATA_FLAG_1_O,                
         GBTBANK_RX_ISDATA_FLAG_O(2)                => GBTBANK_RX_ISDATA_FLAG_2_O,                
         GBTBANK_RX_ISDATA_FLAG_O(3)                => GBTBANK_RX_ISDATA_FLAG_3_O,                
         GBTBANK_RX_ISDATA_FLAG_O(4)                => GBTBANK_RX_ISDATA_FLAG_4_O,                
         GBTBANK_GBTRXREADY_LOST_FLAG_O(1)          => GBTBANK_GBTRXREADY_LOST_FLAG_1_O,         
         GBTBANK_GBTRXREADY_LOST_FLAG_O(2)          => GBTBANK_GBTRXREADY_LOST_FLAG_2_O,         
         GBTBANK_GBTRXREADY_LOST_FLAG_O(3)          => GBTBANK_GBTRXREADY_LOST_FLAG_3_O,         
         GBTBANK_GBTRXREADY_LOST_FLAG_O(4)          => GBTBANK_GBTRXREADY_LOST_FLAG_4_O,         
         GBTBANK_RXDATA_ERRORSEEN_FLAG_O(1)         => GBTBANK_RXDATA_ERRORSEEN_FLAG_1_O,        
         GBTBANK_RXDATA_ERRORSEEN_FLAG_O(2)         => GBTBANK_RXDATA_ERRORSEEN_FLAG_2_O,        
         GBTBANK_RXDATA_ERRORSEEN_FLAG_O(3)         => GBTBANK_RXDATA_ERRORSEEN_FLAG_3_O,        
         GBTBANK_RXDATA_ERRORSEEN_FLAG_O(4)         => GBTBANK_RXDATA_ERRORSEEN_FLAG_4_O,        
         GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_O(1) => GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_1_O,
         GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_O(2) => GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_2_O,
         GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_O(3) => GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_3_O,
         GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_O(4) => GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_4_O,
         GBTBANK_TX_MATCHFLAG_O                     => GBTBANK_TX_MATCHFLAG_O,
         GBTBANK_RX_MATCHFLAG_O(1)                  => GBTBANK_RX_MATCHFLAG_1_O,                 
         GBTBANK_RX_MATCHFLAG_O(2)                  => GBTBANK_RX_MATCHFLAG_2_O,                 
         GBTBANK_RX_MATCHFLAG_O(3)                  => GBTBANK_RX_MATCHFLAG_3_O,                 
         GBTBANK_RX_MATCHFLAG_O(4)                  => GBTBANK_RX_MATCHFLAG_4_O,                 
         -- XCVR ctrl:
         GBTBANK_LOOPBACK_I(1)                      => GBTBANK_LOOPBACK_1_I,                
         GBTBANK_LOOPBACK_I(2)                      => GBTBANK_LOOPBACK_2_I,                
         GBTBANK_LOOPBACK_I(3)                      => GBTBANK_LOOPBACK_3_I,                
         GBTBANK_LOOPBACK_I(4)                      => GBTBANK_LOOPBACK_4_I,                
         GBTBANK_TX_POL_I(1)                        => GBTBANK_TX_POL_1_I,         
         GBTBANK_TX_POL_I(2)                        => GBTBANK_TX_POL_2_I,         
         GBTBANK_TX_POL_I(3)                        => GBTBANK_TX_POL_3_I,         
         GBTBANK_TX_POL_I(4)                        => GBTBANK_TX_POL_4_I,         
         GBTBANK_RX_POL_I(1)                        => GBTBANK_RX_POL_1_I,         
         GBTBANK_RX_POL_I(2)                        => GBTBANK_RX_POL_2_I,         
         GBTBANK_RX_POL_I(3)                        => GBTBANK_RX_POL_3_I,         
         GBTBANK_RX_POL_I(4)                        => GBTBANK_RX_POL_4_I,
         GBTBANK_TXWORDCLKMON_EN_I                  => GBTBANK_TXWORDCLKMON_EN_I);

   --=====================================================================================--   
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--