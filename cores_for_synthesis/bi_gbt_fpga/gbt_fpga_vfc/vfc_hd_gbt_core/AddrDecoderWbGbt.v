//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: AddrDecoderWbGbt.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 24/03/17      1.0          M. Barros Marin    First module definition.
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     WishBone Address Decoder & CrossBar of the VFC-HD GBT-FPGA Core for VFC-HD
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module AddrDecoderWbGbt
//========================================  I/O ports  =======================================//
( 
    //==== WishBone Interface ====//

    input             Clk_ik,
    input      [ 6:0] Adr_ib7,
    input             Stb_i,
    output reg [31:0] Dat_oqb32,
    output reg        Ack_oq,

    //==== WishBone Slaves Interface ====//

    input      [31:0] DatGbtCtrlReg_ib32,
    input             AckGbtCtrlReg_i,
    output reg        StbGbtCtrlReg_oq,    
    input      [31:0] DatGbtStatReg_ib32,
    input             AckGbtStatReg_i,
    output reg        StbGbtStatReg_oq, 
    input      [31:0] DatTxGbtElinkAlignReg0_ib32,
    input             AckTxGbtElinkAlignReg0_i,
    output reg        StbTxGbtElinkAlignReg0_oq,
    input      [31:0] DatTxGbtElinkAlignReg1_ib32,
    input             AckTxGbtElinkAlignReg1_i,
    output reg        StbTxGbtElinkAlignReg1_oq,
    input      [31:0] DatTxGbtElinkAlignReg2_ib32,
    input             AckTxGbtElinkAlignReg2_i,
    output reg        StbTxGbtElinkAlignReg2_oq,
    input      [31:0] DatTxGbtElinkAlignReg3_ib32,
    input             AckTxGbtElinkAlignReg3_i,
    output reg        StbTxGbtElinkAlignReg3_oq,
    input      [31:0] DatRxGbtElinkAlignReg0_ib32,
    input             AckRxGbtElinkAlignReg0_i,
    output reg        StbRxGbtElinkAlignReg0_oq,
    input      [31:0] DatRxGbtElinkAlignReg1_ib32,
    input             AckRxGbtElinkAlignReg1_i,
    output reg        StbRxGbtElinkAlignReg1_oq,
    input      [31:0] DatRxGbtElinkAlignReg2_ib32,
    input             AckRxGbtElinkAlignReg2_i,
    output reg        StbRxGbtElinkAlignReg2_oq,
    input      [31:0] DatRxGbtElinkAlignReg3_ib32,
    input             AckRxGbtElinkAlignReg3_i,
    output reg        StbRxGbtElinkAlignReg3_oq
    
); 

//=======================================  Declarations  =====================================//

reg [7:0]  SelectedModule_b8;

localparam c_SelNothing             = 8'd0,
           c_SelGbtCtrlReg          = 8'd1,
           c_SelGbtStatReg          = 8'd2, 
           c_SelTxGbtElinkAlignReg0 = 8'd3, 
           c_SelTxGbtElinkAlignReg1 = 8'd4, 
           c_SelTxGbtElinkAlignReg2 = 8'd5, 
           c_SelTxGbtElinkAlignReg3 = 8'd6,
           c_SelRxGbtElinkAlignReg0 = 8'd7, 
           c_SelRxGbtElinkAlignReg1 = 8'd8, 
           c_SelRxGbtElinkAlignReg2 = 8'd9, 
           c_SelRxGbtElinkAlignReg3 = 8'd10; 

//=======================================  User Logic  =======================================//
 
always @* 
    casez(Adr_ib7)
        7'b000_00??: SelectedModule_b8 = c_SelGbtCtrlReg;          // FROM 00 TO 03 (WB) == FROM 000 TO 00C (VME) <- 4 regs (16B)
        7'b000_01??: SelectedModule_b8 = c_SelGbtStatReg;          // FROM 04 TO 07 (WB) == FROM 010 TO 01C (VME) <- 4 regs (16B)    
        7'b100_0???: SelectedModule_b8 = c_SelTxGbtElinkAlignReg0; // FROM 40 TO 47 (WB) == FROM 100 TO 11C (VME) <- 8 regs (32B)    
        7'b100_1???: SelectedModule_b8 = c_SelTxGbtElinkAlignReg1; // FROM 48 TO 4F (WB) == FROM 120 TO 13C (VME) <- 8 regs (32B)    
        7'b101_0???: SelectedModule_b8 = c_SelTxGbtElinkAlignReg2; // FROM 50 TO 57 (WB) == FROM 140 TO 15C (VME) <- 8 regs (32B)    
        7'b101_1???: SelectedModule_b8 = c_SelTxGbtElinkAlignReg3; // FROM 58 TO 5F (WB) == FROM 160 TO 17C (VME) <- 8 regs (32B)  
        7'b110_0???: SelectedModule_b8 = c_SelRxGbtElinkAlignReg0; // FROM 60 TO 67 (WB) == FROM 180 TO 19C (VME) <- 8 regs (32B)    
        7'b110_1???: SelectedModule_b8 = c_SelRxGbtElinkAlignReg1; // FROM 68 TO 6F (WB) == FROM 1A0 TO 1BC (VME) <- 8 regs (32B)    
        7'b111_0???: SelectedModule_b8 = c_SelRxGbtElinkAlignReg2; // FROM 70 TO 77 (WB) == FROM 1C0 TO 1DC (VME) <- 8 regs (32B)    
        7'b111_1???: SelectedModule_b8 = c_SelRxGbtElinkAlignReg3; // FROM 78 TO 7F (WB) == FROM 1E0 TO 1FC (VME) <- 8 regs (32B)
        default:     SelectedModule_b8 = c_SelNothing;
    endcase 
            
always @(posedge Clk_ik) begin
    Ack_oq                            <= #1  1'b0;
    Dat_oqb32                         <= #1 32'h0;
    StbGbtCtrlReg_oq                  <= #1  1'b0;
    StbGbtStatReg_oq                  <= #1  1'b0;
    StbTxGbtElinkAlignReg0_oq         <= #1  1'b0;
    StbTxGbtElinkAlignReg1_oq         <= #1  1'b0;
    StbTxGbtElinkAlignReg2_oq         <= #1  1'b0;
    StbTxGbtElinkAlignReg3_oq         <= #1  1'b0;
    StbRxGbtElinkAlignReg0_oq         <= #1  1'b0;
    StbRxGbtElinkAlignReg1_oq         <= #1  1'b0;
    StbRxGbtElinkAlignReg2_oq         <= #1  1'b0;
    StbRxGbtElinkAlignReg3_oq         <= #1  1'b0;
    case(SelectedModule_b8)         
        c_SelGbtCtrlReg: begin      
            StbGbtCtrlReg_oq          <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatGbtCtrlReg_ib32;
            Ack_oq                    <= #1 AckGbtCtrlReg_i;
        end                         
        c_SelGbtStatReg: begin      
            StbGbtStatReg_oq          <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatGbtStatReg_ib32;
            Ack_oq                    <= #1 AckGbtStatReg_i;        
        end                         
        c_SelTxGbtElinkAlignReg0: begin
            StbTxGbtElinkAlignReg0_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatTxGbtElinkAlignReg0_ib32;
            Ack_oq                    <= #1 AckTxGbtElinkAlignReg0_i;        
        end
        c_SelTxGbtElinkAlignReg1: begin
            StbTxGbtElinkAlignReg1_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatTxGbtElinkAlignReg1_ib32;
            Ack_oq                    <= #1 AckTxGbtElinkAlignReg1_i;        
        end
        c_SelTxGbtElinkAlignReg2: begin
            StbTxGbtElinkAlignReg2_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatTxGbtElinkAlignReg2_ib32;
            Ack_oq                    <= #1 AckTxGbtElinkAlignReg2_i;        
        end        
        c_SelTxGbtElinkAlignReg3: begin
            StbTxGbtElinkAlignReg3_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatTxGbtElinkAlignReg3_ib32;
            Ack_oq                    <= #1 AckTxGbtElinkAlignReg3_i;        
        end
        c_SelRxGbtElinkAlignReg0: begin
            StbRxGbtElinkAlignReg0_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatRxGbtElinkAlignReg0_ib32;
            Ack_oq                    <= #1 AckRxGbtElinkAlignReg0_i;        
        end
        c_SelRxGbtElinkAlignReg1: begin
            StbRxGbtElinkAlignReg1_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatRxGbtElinkAlignReg1_ib32;
            Ack_oq                    <= #1 AckRxGbtElinkAlignReg1_i;        
        end
        c_SelRxGbtElinkAlignReg2: begin
            StbRxGbtElinkAlignReg2_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatRxGbtElinkAlignReg2_ib32;
            Ack_oq                    <= #1 AckRxGbtElinkAlignReg2_i;        
        end        
        c_SelRxGbtElinkAlignReg3: begin
            StbRxGbtElinkAlignReg3_oq <= #1 Stb_i;
            Dat_oqb32                 <= #1 DatRxGbtElinkAlignReg3_ib32;
            Ack_oq                    <= #1 AckRxGbtElinkAlignReg3_i;        
        end
    endcase      
end

endmodule