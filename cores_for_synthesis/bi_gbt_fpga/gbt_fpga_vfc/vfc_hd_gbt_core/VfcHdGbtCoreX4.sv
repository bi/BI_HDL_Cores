//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: VfcHdGbtCoreX4.sv  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 11/10/17      1.1          M. Barros Marin    - Disabled internal patter gen/check
//                                                     - Improved resets scheme.               
//                                                     - Other minor modifications.               
//     - 19/07/16      1.0          M. Barros Marin    First module definition.               
//
// Language: SystemVerilog 2013                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Intel FPGA (Altera)
//     - Model:   Arria V GX
//
// Description:
//
//     Wrapper to instantiate the GBT-FPGA Example Design featuring 4 GBT Link on the VFC-HD.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module VfcHdGbtCoreX4  
//====================================  Global Parameters  ===================================//
#(  parameter g_Synthesis           = (1'b1), 
              g_GbtBankId           = (1'b0),
              g_TxLatOp             = (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
              g_RxLatOp             = (1'b0), // Comment: '0' -> Standard Latency   | '1' -> Latency Optimized
              g_TxWideBus           = (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
              g_RxWideBus           = (1'b0), // Comment: '0' -> GBT Frame Encoding | '1' -> WideBus Encoding
              g_TxElinksDataAlignEn = ({1'b1, 1'b1, 1'b1, 1'b1}),
              g_RxElinksDataAlignEn = ({1'b1, 1'b1, 1'b1, 1'b1}),
              g_TxRxMatchFlagsEn    = (1'b1))
//========================================  I/O ports  =======================================//
(
    //==== WishBone Interface ====//
    
    input         Clk_ik,   
    input         Rst_ir,
    input         Cyc_i,    
    input         Stb_i,    
    input         We_i,     
    input  [ 6:0] Adr_ib7,  
    input  [31:0] Dat_ib32, 
    output [31:0] Dat_ob32,
    output        Ack_o,

    //==== SFPs Control ====//
    
    input  [ 4:1] SfpPresent_ib4,         
    input  [ 4:1] SfpTxFault_ib4,         
    input  [ 4:1] SfpLos_ib4,             
    output [ 4:1] SfpTxDisable_oqb4,      
    output [ 4:1] SfpRateSelect_oqb4,     
    input  [ 4:1] StatusSfpTxDisable_ib4, 
    input  [ 4:1] StatusSfpRateSelect_ib4,     
    
    //==== Clocks Scheme ====//
    
    // GBT Bank Reference Clock (PllRef (Si5338)):
    input         GbtRefClk120MHz_ik,
    // User Clocks:
    inout         TxFrameClk40Mhz_ik,
    output [ 4:1] RxFrameClk40Mhz_okb4,
    
    //==== GBT Resets Scheme ====//
   
    input         GbtReset_ir,
    input  [ 4:1] TxGbtReset_irb4,
    input  [ 4:1] RxGbtReset_irb4,
   
    //==== Serial Lanes ====//
   
    output [ 4:1] MgtTx_ob4,
    input  [ 4:1] MgtRx_ib4,
   
    //==== Parallel Data ====//
   
    input  [ 1:0] TxDataScIc_i4b2 [4:1],     
    input  [ 1:0] TxDataScEc_i4b2 [4:1],     
    input  [79:0] TxData_i4b80    [4:1],     
    input  [31:0] TxDataWb_i4b32  [4:1],
    //--    
    output [ 1:0] RxDataScIc_o4b2 [4:1],
    output [ 1:0] RxDataScEc_o4b2 [4:1],
    output [79:0] RxData_o4b80    [4:1],
    output [31:0] RxDataWb_o4b32  [4:1],
    
    //==== Control ====//
    
    output [ 4:1] GbtTxReady_ob4,
    output [ 4:1] GbtRxReady_ob4,
    input  [ 4:1] TxIsDataSel_ib4,
    output [ 4:1] RxIsDataFlag_ob4,

    //==== Latency Test Flags ====//
    
    output        TxMatchFlag_o,
    output [ 4:1] RxMatchFlag_ob4
   
);

//=======================================  Declarations  =====================================//

genvar      i, j;
// GBT Resets Scheme:
reg [  1:0] GbtReset_xb2;
reg         GbtReset_rq;
reg [  1:0] TxGbtReset_x2b4 [3:0];
reg [  3:0] TxGbtReset_rqb4;
reg [  1:0] RxGbtReset_x2b4 [3:0];
reg [  3:0] RxGbtReset_rqb4;
// WishBone Address Decoder:
wire [ 31:0] WbDatGbtCtrlReg_b32;
wire         WbAckGbtCtrlReg;
wire         WbStbGbtCtrlReg;
wire [ 31:0] WbDatGbtStatReg_b32;
wire         WbAckGbtStatReg;
wire         WbStbGbtStatReg;
wire [ 31:0] WbDatTxGbtElinkAlignReg_4b32 [3:0];
wire [  3:0] WbAckTxGbtElinkAlignReg_b4;
wire [  3:0] WbStbTxGbtElinkAlignReg_b4;
wire [ 31:0] WbDatRxGbtElinkAlignReg_4b32 [3:0];
wire [  3:0] WbAckRxGbtElinkAlignReg_b4;
wire [  3:0] WbStbRxGbtElinkAlignReg_b4;
// GBT Bank Control:
wire [ 31:0] GbtCtrlReg_4xb32    [3:0];
wire [ 31:0] GbtCtrlRegCdc_4xb32 [3:0];
wire         GbtReset_r;
wire [  3:0] TxGbtReset_rb4;
wire [  3:0] RxGbtReset_rb4;
wire [  3:0] TxPolInv_b4;
wire [  3:0] RxPolInv_b4;   
wire [  3:0] LoopBackEn_b4; 
wire         TxWordClkMonEn;
wire [  3:0] TxIsDataSel_b4;
// GBT Bank Status:
reg  [ 31:0] GbtStatReg_4x32 [3:0];
wire [  3:0] MgtTxReady_b4;
reg  [  1:0] MgtTxReady_x4b2 [3:0];
reg  [  3:0] MgtTxReady_qb4;
wire [  3:0] MgtRxReady_b4;
reg  [  1:0] MgtRxReady_x4b2 [3:0];
reg  [  3:0] MgtRxReady_qb4;
reg  [  1:0] GbtTxReady_x4b2 [3:0];
reg  [  3:0] GbtTxReady_qb4;
reg  [  1:0] GbtRxReady_x4b2 [3:0];
reg  [  3:0] GbtRxReady_qb4;
wire [  3:0] RxIsDataFlag_b4;
reg  [  1:0] RxIsDataFlag_x4b2 [3:0];
reg  [  3:0] RxIsDataFlag_qb4;
// Elinks Data Alignment:
wire [115:0] TxDataNotAligned_4b116 [3:0];
wire [115:0] TxDataAligned_4b116    [3:0];
wire [ 31:0] TxElinksAlignCtrlReg_4x8b32    [3:0][ 7:0];
wire [ 31:0] TxElinksAlignCtrlRegCdc_4x8b32 [3:0][ 7:0];
wire [  2:0] TxElinksAlignCtrl_4x58b3       [3:0][57:0];
wire [115:0] RxDataNotAligned_4b116         [3:0];
wire [115:0] RxDataAligned_4b116            [3:0];
wire [  2:0] RxElinksAlignCtrl_4x58b3       [3:0][57:0];
wire [ 31:0] RxElinksAlignCtrlReg_4x8b32    [3:0][ 7:0];
wire [ 31:0] RxElinksAlignCtrlRegCdc_4x8b32 [3:0][ 7:0];

//=======================================  User Logic  =======================================//

//==== GBT Resets Scheme ====//

always @(posedge TxFrameClk40Mhz_ik) GbtReset_xb2 <= #1 {GbtReset_xb2[0], GbtReset_ir};
always @(posedge TxFrameClk40Mhz_ik) GbtReset_rq  <= #1  GbtReset_xb2[1];        

always @(posedge TxFrameClk40Mhz_ik) for (int i=0;i<=3;i=i+1) TxGbtReset_x2b4[i] <= #1 {TxGbtReset_x2b4[i][0], TxGbtReset_irb4[i+1]};
always @(posedge TxFrameClk40Mhz_ik) for (int i=0;i<=3;i=i+1) TxGbtReset_rqb4[i] <= #1  TxGbtReset_x2b4[i][1];        

always @(posedge TxFrameClk40Mhz_ik) for (int i=0;i<=3;i=i+1) RxGbtReset_x2b4[i] <= #1 {RxGbtReset_x2b4[i][0], RxGbtReset_irb4[i+1]};
always @(posedge TxFrameClk40Mhz_ik) for (int i=0;i<=3;i=i+1) RxGbtReset_rqb4[i] <= #1  RxGbtReset_x2b4[i][1];        

//==== Intel FPGA (Altera) Arria V - GBT Example Design ====//

alt_av_gbt_example_design_x4 #(
   .SYNTHESIS                                 (g_Synthesis),
   .GBT_BANK_ID                               (g_GbtBankId),
   .TX_OPTIMIZATION                           (g_TxLatOp),
   .RX_OPTIMIZATION                           (g_RxLatOp),
   .TX_ENCODING                               (g_TxWideBus),    
   .RX_ENCODING                               (g_RxWideBus),
   .DATA_GENERATOR_ENABLE                     (1'b0),
   .DATA_CHECKER_ENABLE                       (1'b0),
   .MATCH_FLAG_ENABLE                         (1'b0))
i_AltAvGbtExampleDesignX4                     (                   
   // Clocks Scheme:                        
   .TX_FRAMECLK_40MHZ_I                       (TxFrameClk40Mhz_ik),
   .MGT_REFCLK_120MHZ_I                       (GbtRefClk120MHz_ik),
   .RX_FRAMECLK_1_O                           (RxFrameClk40Mhz_okb4[1]),
   .RX_FRAMECLK_2_O                           (RxFrameClk40Mhz_okb4[2]),
   .RX_FRAMECLK_3_O                           (RxFrameClk40Mhz_okb4[3]),
   .RX_FRAMECLK_4_O                           (RxFrameClk40Mhz_okb4[4]),
   .TX_WORDCLK_1_O                            (),
   .TX_WORDCLK_2_O                            (),
   .TX_WORDCLK_3_O                            (),
   .TX_WORDCLK_4_O                            (),
   .RX_WORDCLK_1_O                            (),
   .RX_WORDCLK_2_O                            (),
   .RX_WORDCLK_3_O                            (),
   .RX_WORDCLK_4_O                            (),
   // Resets Scheme:                        
   .GBTBANK_GENERAL_RESET_I                   (GbtReset_r),
   .GBTBANK_MANUAL_RESET_TX_1_I               (TxGbtReset_rb4[0]),
   .GBTBANK_MANUAL_RESET_TX_2_I               (TxGbtReset_rb4[1]),
   .GBTBANK_MANUAL_RESET_TX_3_I               (TxGbtReset_rb4[2]),
   .GBTBANK_MANUAL_RESET_TX_4_I               (TxGbtReset_rb4[3]),
   .GBTBANK_MANUAL_RESET_RX_1_I               (RxGbtReset_rb4[0]),
   .GBTBANK_MANUAL_RESET_RX_2_I               (RxGbtReset_rb4[1]),
   .GBTBANK_MANUAL_RESET_RX_3_I               (RxGbtReset_rb4[2]),
   .GBTBANK_MANUAL_RESET_RX_4_I               (RxGbtReset_rb4[3]),
   // MGT Serial Lanes:                     
   .GBTBANK_MGT_RX_1_I                        (MgtRx_ib4[1]),                    
   .GBTBANK_MGT_RX_2_I                        (MgtRx_ib4[2]),                    
   .GBTBANK_MGT_RX_3_I                        (MgtRx_ib4[3]),                    
   .GBTBANK_MGT_RX_4_I                        (MgtRx_ib4[4]),                    
   .GBTBANK_MGT_TX_1_O                        (MgtTx_ob4[1]),
   .GBTBANK_MGT_TX_2_O                        (MgtTx_ob4[2]),
   .GBTBANK_MGT_TX_3_O                        (MgtTx_ob4[3]),
   .GBTBANK_MGT_TX_4_O                        (MgtTx_ob4[4]),
   // Data Paths:
   .GBTBANK_TX_DATA_1_I                       (TxDataAligned_4b116[0]),
   .GBTBANK_TX_DATA_2_I                       (TxDataAligned_4b116[1]),
   .GBTBANK_TX_DATA_3_I                       (TxDataAligned_4b116[2]),
   .GBTBANK_TX_DATA_4_I                       (TxDataAligned_4b116[3]),
   .GBTBANK_RX_DATA_1_O                       (RxDataNotAligned_4b116[0]),
   .GBTBANK_RX_DATA_2_O                       (RxDataNotAligned_4b116[1]),
   .GBTBANK_RX_DATA_3_O                       (RxDataNotAligned_4b116[2]),
   .GBTBANK_RX_DATA_4_O                       (RxDataNotAligned_4b116[3]),
   // MGT Reconfiguration:                  
   .GBTBANK_RECONF_AVMM_RST_I                 (GbtReset_r),
   .GBTBANK_RECONF_AVMM_CLK_I                 (GbtRefClk120MHz_ik),
   .GBTBANK_RECONF_AVMM_ADDR_I                ( 7'h0),
   .GBTBANK_RECONF_AVMM_READ_I                ( 1'b0),
   .GBTBANK_RECONF_AVMM_WRITE_I               ( 1'b0),
   .GBTBANK_RECONF_AVMM_WRITEDATA_I           (32'h0),
   .GBTBANK_RECONF_AVMM_READDATA_O            (),
   .GBTBANK_RECONF_AVMM_WAITREQUEST_O         (),
   // GBT Bank Control:                     
   .GBTBANK_TX_ISDATA_SEL_1_I                 (TxIsDataSel_b4[0]),
   .GBTBANK_TX_ISDATA_SEL_2_I                 (TxIsDataSel_b4[1]),
   .GBTBANK_TX_ISDATA_SEL_3_I                 (TxIsDataSel_b4[2]),
   .GBTBANK_TX_ISDATA_SEL_4_I                 (TxIsDataSel_b4[3]),
   .TEST_SCEC_CHECK_EN_1_I                    (1'b0),
   .TEST_SCEC_CHECK_EN_2_I                    (1'b0),
   .TEST_SCEC_CHECK_EN_3_I                    (1'b0),
   .TEST_SCEC_CHECK_EN_4_I                    (1'b0),
   .TOGGLING_DATA_CHECK_SYNC_1_I              (1'b0),
   .TOGGLING_DATA_CHECK_SYNC_2_I              (1'b0),
   .TOGGLING_DATA_CHECK_SYNC_3_I              (1'b0),
   .TOGGLING_DATA_CHECK_SYNC_4_I              (1'b0),
   .GBTBANK_TEST_PATTERN_SEL_I                (3'h7),
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_1_I    (1'b0),
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_2_I    (1'b0),
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_3_I    (1'b0),
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_4_I    (1'b0),
   .GBTBANK_RESET_DATA_ERRORS_1_I             (1'b0),
   .GBTBANK_RESET_DATA_ERRORS_2_I             (1'b0),
   .GBTBANK_RESET_DATA_ERRORS_3_I             (1'b0),
   .GBTBANK_RESET_DATA_ERRORS_4_I             (1'b0),
   // GBT Bank Status:
   .GBTBANK_LINK_READY_1_O                    (),
   .GBTBANK_LINK_READY_2_O                    (),
   .GBTBANK_LINK_READY_3_O                    (),
   .GBTBANK_LINK_READY_4_O                    (),
   .GBTBANK_LINK_TX_READY_1_O                 (MgtTxReady_b4[0]),
   .GBTBANK_LINK_TX_READY_2_O                 (MgtTxReady_b4[1]),
   .GBTBANK_LINK_TX_READY_3_O                 (MgtTxReady_b4[2]),
   .GBTBANK_LINK_TX_READY_4_O                 (MgtTxReady_b4[3]),
   .GBTBANK_LINK_RX_READY_1_O                 (MgtRxReady_b4[0]),
   .GBTBANK_LINK_RX_READY_2_O                 (MgtRxReady_b4[1]),
   .GBTBANK_LINK_RX_READY_3_O                 (MgtRxReady_b4[2]),
   .GBTBANK_LINK_RX_READY_4_O                 (MgtRxReady_b4[3]),
   .GBTBANK_GBTRX_READY_1_O                   (GbtRxReady_ob4[1]),
   .GBTBANK_GBTRX_READY_2_O                   (GbtRxReady_ob4[2]),
   .GBTBANK_GBTRX_READY_3_O                   (GbtRxReady_ob4[3]),
   .GBTBANK_GBTRX_READY_4_O                   (GbtRxReady_ob4[4]),
   .GBTBANK_RX_ISDATA_FLAG_1_O                (RxIsDataFlag_ob4[1]),
   .GBTBANK_RX_ISDATA_FLAG_2_O                (RxIsDataFlag_ob4[2]),
   .GBTBANK_RX_ISDATA_FLAG_3_O                (RxIsDataFlag_ob4[3]),
   .GBTBANK_RX_ISDATA_FLAG_4_O                (RxIsDataFlag_ob4[4]),
   .GBTBANK_GBTRXREADY_LOST_FLAG_1_O          (),
   .GBTBANK_GBTRXREADY_LOST_FLAG_2_O          (),
   .GBTBANK_GBTRXREADY_LOST_FLAG_3_O          (),
   .GBTBANK_GBTRXREADY_LOST_FLAG_4_O          (),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_1_O         (),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_2_O         (),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_3_O         (),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_4_O         (),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_1_O (),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_2_O (),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_3_O (),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_4_O (),
   .GBTBANK_TX_MATCHFLAG_O                    (),
   .GBTBANK_RX_MATCHFLAG_1_O                  (),
   .GBTBANK_RX_MATCHFLAG_2_O                  (),
   .GBTBANK_RX_MATCHFLAG_3_O                  (),
   .GBTBANK_RX_MATCHFLAG_4_O                  (),
   // MGT Control:                            
   .GBTBANK_LOOPBACK_1_I                      (LoopBackEn_b4[0]),
   .GBTBANK_LOOPBACK_2_I                      (LoopBackEn_b4[1]),
   .GBTBANK_LOOPBACK_3_I                      (LoopBackEn_b4[2]),
   .GBTBANK_LOOPBACK_4_I                      (LoopBackEn_b4[3]),
   .GBTBANK_TX_POL_1_I                        (TxPolInv_b4[0]),
   .GBTBANK_TX_POL_2_I                        (TxPolInv_b4[1]),
   .GBTBANK_TX_POL_3_I                        (TxPolInv_b4[2]),
   .GBTBANK_TX_POL_4_I                        (TxPolInv_b4[3]),
   .GBTBANK_RX_POL_1_I                        (RxPolInv_b4[0]),
   .GBTBANK_RX_POL_2_I                        (RxPolInv_b4[1]),
   .GBTBANK_RX_POL_3_I                        (RxPolInv_b4[2]),
   .GBTBANK_RX_POL_4_I                        (RxPolInv_b4[3]),
   .GBTBANK_TXWORDCLKMON_EN_I                 (TxWordClkMonEn));
   
//==== WishBone Address Decoder ====//
   
AddrDecoderWbGbt i_AddrDecoderWbGbt ( 
    .Clk_ik                      (Clk_ik),
    .Adr_ib7                     (Adr_ib7),
    .Stb_i                       (Stb_i),
    .Dat_oqb32                   (Dat_ob32),
    .Ack_oq                      (Ack_o),
    //--
    .DatGbtCtrlReg_ib32          (WbDatGbtCtrlReg_b32),
    .AckGbtCtrlReg_i             (WbAckGbtCtrlReg),
    .StbGbtCtrlReg_oq            (WbStbGbtCtrlReg),
    //--                      
    .DatGbtStatReg_ib32          (WbDatGbtStatReg_b32),
    .AckGbtStatReg_i             (WbAckGbtStatReg),
    .StbGbtStatReg_oq            (WbStbGbtStatReg),
    //--                      
    .DatTxGbtElinkAlignReg0_ib32 (WbDatTxGbtElinkAlignReg_4b32[0]),
    .AckTxGbtElinkAlignReg0_i    (WbAckTxGbtElinkAlignReg_b4  [0]),
    .StbTxGbtElinkAlignReg0_oq   (WbStbTxGbtElinkAlignReg_b4  [0]),      
    //--                      
    .DatTxGbtElinkAlignReg1_ib32 (WbDatTxGbtElinkAlignReg_4b32[1]),
    .AckTxGbtElinkAlignReg1_i    (WbAckTxGbtElinkAlignReg_b4  [1]),
    .StbTxGbtElinkAlignReg1_oq   (WbStbTxGbtElinkAlignReg_b4  [1]),      
    //--                      
    .DatTxGbtElinkAlignReg2_ib32 (WbDatTxGbtElinkAlignReg_4b32[2]),
    .AckTxGbtElinkAlignReg2_i    (WbAckTxGbtElinkAlignReg_b4  [2]),
    .StbTxGbtElinkAlignReg2_oq   (WbStbTxGbtElinkAlignReg_b4  [2]),      
    //--                      
    .DatTxGbtElinkAlignReg3_ib32 (WbDatTxGbtElinkAlignReg_4b32[3]),
    .AckTxGbtElinkAlignReg3_i    (WbAckTxGbtElinkAlignReg_b4  [3]),
    .StbTxGbtElinkAlignReg3_oq   (WbStbTxGbtElinkAlignReg_b4  [3]),      
    //--                          
    .DatRxGbtElinkAlignReg0_ib32 (WbDatRxGbtElinkAlignReg_4b32[0]),
    .AckRxGbtElinkAlignReg0_i    (WbAckRxGbtElinkAlignReg_b4  [0]),
    .StbRxGbtElinkAlignReg0_oq   (WbStbRxGbtElinkAlignReg_b4  [0]),
    //--                          
    .DatRxGbtElinkAlignReg1_ib32 (WbDatRxGbtElinkAlignReg_4b32[1]),
    .AckRxGbtElinkAlignReg1_i    (WbAckRxGbtElinkAlignReg_b4  [1]),
    .StbRxGbtElinkAlignReg1_oq   (WbStbRxGbtElinkAlignReg_b4  [1]),    
    //--                          
    .DatRxGbtElinkAlignReg2_ib32 (WbDatRxGbtElinkAlignReg_4b32[2]),
    .AckRxGbtElinkAlignReg2_i    (WbAckRxGbtElinkAlignReg_b4  [2]),
    .StbRxGbtElinkAlignReg2_oq   (WbStbRxGbtElinkAlignReg_b4  [2]),    
    //--                          
    .DatRxGbtElinkAlignReg3_ib32 (WbDatRxGbtElinkAlignReg_4b32[3]),
    .AckRxGbtElinkAlignReg3_i    (WbAckRxGbtElinkAlignReg_b4  [3]),
    .StbRxGbtElinkAlignReg3_oq   (WbStbRxGbtElinkAlignReg_b4  [3]));
   
//==== Control Registers ====//

// Control Registers Bank:
Generic4OutputRegs #(  
    .Reg0Default     (32'h000000F0), // Comment: Note!! Default value
    .Reg0AutoClrMask (32'hFFFFFFFF), 
    .Reg1Default     (32'h00000001), // Comment: Note!! Default value
    .Reg1AutoClrMask (32'hFFFFFFFF), 
    .Reg2Default     (32'h00000004), // Comment: Note!! Default value
    .Reg2AutoClrMask (32'hFFFFFFFF),
    .Reg3Default     (32'h00000000), 
    .Reg3AutoClrMask (32'hFFFFFFFF))
i_GbtControlRegs (
    .Clk_ik          (Clk_ik),
    .Rst_irq         (Rst_ir),
    .Cyc_i           (Cyc_i),
    .Stb_i           (WbStbGbtCtrlReg),
    .We_i            (We_i),
    .Adr_ib2         (Adr_ib7[1:0]),
    .Dat_ib32        (Dat_ib32),
    .Dat_oab32       (WbDatGbtCtrlReg_b32),
    .Ack_oa          (WbAckGbtCtrlReg),
    //--
    .Reg0Value_ob32  (GbtCtrlReg_4xb32[0]),
    .Reg1Value_ob32  (GbtCtrlReg_4xb32[1]),
    .Reg2Value_ob32  (GbtCtrlReg_4xb32[2]),
    .Reg3Value_ob32  (GbtCtrlReg_4xb32[3]));

// Clock Domain Crossing (CDC) DPRAMs:
generate for (i=0;i<=3;i=i+1) begin: Gen_GbtCtrlRegCdc        
    generic_dpram_mod #(
        .aw     ( 4), 
        .dw     (32))
    i_GbtCtrlRegCdc (
        .rclk  (TxFrameClk40Mhz_ik), 
        .rrst  (GbtReset_rq),
        .rce   (1'b1), 
        .oe    (1'b1),
        .raddr (4'h0), 
        .dout  (GbtCtrlRegCdc_4xb32[i]),
        .wclk  (Clk_ik), 
        .wrst  (Rst_ir),
        .wce   (1'b1),
        .we    (1'b1),
        .waddr (4'h0),
        .di    (GbtCtrlReg_4xb32[i]));
end endgenerate    
    
// Registers Mapping:
assign SfpTxDisable_oqb4 [1] = GbtCtrlRegCdc_4xb32[0][   0]; // Comment: '0' -> Enabled (default) | '1' -> Disabled
assign SfpTxDisable_oqb4 [2] = GbtCtrlRegCdc_4xb32[0][   1];
assign SfpTxDisable_oqb4 [3] = GbtCtrlRegCdc_4xb32[0][   2];
assign SfpTxDisable_oqb4 [4] = GbtCtrlRegCdc_4xb32[0][   3];
assign SfpRateSelect_oqb4[1] = GbtCtrlRegCdc_4xb32[0][   4]; // Comment: '0' -> Low bandwidth | '1' -> High bandwidth (default)
assign SfpRateSelect_oqb4[2] = GbtCtrlRegCdc_4xb32[0][   5];
assign SfpRateSelect_oqb4[3] = GbtCtrlRegCdc_4xb32[0][   6];
assign SfpRateSelect_oqb4[4] = GbtCtrlRegCdc_4xb32[0][   7];
//     Reserved                GbtCtrlRegCdc_4xb32[0][31:8]
//--
assign GbtReset_r            = GbtCtrlRegCdc_4xb32[1][    0] | GbtReset_rq;
//     Reserved                GbtCtrlRegCdc_4xb32[1][ 3: 1]
assign TxGbtReset_rb4        = GbtCtrlRegCdc_4xb32[1][ 7: 4] | TxGbtReset_rqb4;   
assign RxGbtReset_rb4        = GbtCtrlRegCdc_4xb32[1][11: 8] | RxGbtReset_rqb4;   
assign TxPolInv_b4           = GbtCtrlRegCdc_4xb32[1][15:12];
assign RxPolInv_b4           = GbtCtrlRegCdc_4xb32[1][19:16];
assign LoopBackEn_b4         = GbtCtrlRegCdc_4xb32[1][23:20];
assign TxWordClkMonEn        = GbtCtrlRegCdc_4xb32[1][   24];
//     Reserved                GbtCtrlRegCdc_4xb32[1][27:25]
assign TxIsDataSel_b4        = GbtCtrlRegCdc_4xb32[1][31:28] | TxIsDataSel_ib4;  
//--
//     Reserved                GbtCtrlRegCdc_4xb32[2]
//--
//     Reserved                GbtCtrlRegCdc_4xb32[3]

//==== Status Registers ====//

// Status Registers Bank:
Generic4InputRegs i_GbtStatusRegs (
    .Clk_ik         (Clk_ik),
    .Rst_irq        (Rst_ir),
    .Cyc_i          (Cyc_i),
    .Stb_i          (WbStbGbtStatReg),
    .Adr_ib2        (Adr_ib7[1:0]),
    .Dat_oab32      (WbDatGbtStatReg_b32),
    .Ack_oa         (WbAckGbtStatReg),
    //--          
    .Reg0Value_ib32 (GbtStatReg_4x32[0]),
    .Reg1Value_ib32 (GbtStatReg_4x32[1]),
    .Reg2Value_ib32 (GbtStatReg_4x32[2]),
    .Reg3Value_ib32 (GbtStatReg_4x32[3]));

// Clock Domain Crossing (CDC) Synchronizers
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) MgtTxReady_x4b2[i] <= #1 {MgtTxReady_x4b2[i][0], MgtTxReady_b4[i]};
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) MgtTxReady_qb4 [i] <= #1  MgtTxReady_x4b2[i][1];

always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) MgtRxReady_x4b2[i] <= #1 {MgtRxReady_x4b2[i][0], MgtRxReady_b4[i]};
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) MgtRxReady_qb4 [i] <= #1  MgtRxReady_x4b2[i][1];

assign GbtTxReady_ob4 = MgtTxReady_b4;
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) GbtTxReady_x4b2[i] <= #1 {GbtTxReady_x4b2[i][0], GbtTxReady_ob4[i+1]};
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) GbtTxReady_qb4 [i] <= #1  GbtTxReady_x4b2[i][1];

always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) GbtRxReady_x4b2[i] <= #1 {GbtRxReady_x4b2[i][0], GbtRxReady_ob4[i+1]};
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) GbtRxReady_qb4 [i] <= #1  GbtRxReady_x4b2[i][1];

always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) RxIsDataFlag_x4b2[i] <= #1 {RxIsDataFlag_x4b2[i][0], RxIsDataFlag_ob4[i+1]};
always @(posedge Clk_ik) for (int i=0;i<=3;i=i+1) RxIsDataFlag_qb4 [i] <= #1  RxIsDataFlag_x4b2[i][1];

// Registers Mapping:
assign GbtStatReg_4x32[0][    0] = SfpPresent_ib4[1]; 
assign GbtStatReg_4x32[0][    1] = SfpPresent_ib4[2];   
assign GbtStatReg_4x32[0][    2] = SfpPresent_ib4[3]; 
assign GbtStatReg_4x32[0][    3] = SfpPresent_ib4[4];    
assign GbtStatReg_4x32[0][    4] = SfpTxFault_ib4[1];  
assign GbtStatReg_4x32[0][    5] = SfpTxFault_ib4[2];
assign GbtStatReg_4x32[0][    6] = SfpTxFault_ib4[3];    
assign GbtStatReg_4x32[0][    7] = SfpTxFault_ib4[4];
assign GbtStatReg_4x32[0][    8] = SfpLos_ib4[1];
assign GbtStatReg_4x32[0][    9] = SfpLos_ib4[2];
assign GbtStatReg_4x32[0][   10] = SfpLos_ib4[3]; 
assign GbtStatReg_4x32[0][   11] = SfpLos_ib4[4];
assign GbtStatReg_4x32[0][   12] = StatusSfpTxDisable_ib4[1];  // Comment: '0' -> Enabled (default) | '1' -> Disabled
assign GbtStatReg_4x32[0][   13] = StatusSfpTxDisable_ib4[2];    
assign GbtStatReg_4x32[0][   14] = StatusSfpTxDisable_ib4[3];
assign GbtStatReg_4x32[0][   15] = StatusSfpTxDisable_ib4[4];
assign GbtStatReg_4x32[0][   16] = StatusSfpRateSelect_ib4[1]; // Comment: '0' -> Low bandwidth | '1' -> High bandwidth (default) 
assign GbtStatReg_4x32[0][   17] = StatusSfpRateSelect_ib4[2];
assign GbtStatReg_4x32[0][   18] = StatusSfpRateSelect_ib4[3];
assign GbtStatReg_4x32[0][   19] = StatusSfpRateSelect_ib4[4];
assign GbtStatReg_4x32[0][31:20] = 12'h0;
//--
assign GbtStatReg_4x32[1][    0] = g_GbtBankId;
assign GbtStatReg_4x32[1][ 3: 1] = 3'h0;  // Reserved
assign GbtStatReg_4x32[1][ 7: 4] = MgtTxReady_qb4;
assign GbtStatReg_4x32[1][11: 8] = MgtRxReady_qb4;
assign GbtStatReg_4x32[1][15:12] = GbtTxReady_qb4;
assign GbtStatReg_4x32[1][19:16] = GbtRxReady_qb4;
assign GbtStatReg_4x32[1][23:20] = RxIsDataFlag_qb4;
assign GbtStatReg_4x32[1][31:24] = 8'h0;  // Reserved
//--
assign GbtStatReg_4x32[2]        = 32'h0; // Reserved
//--
assign GbtStatReg_4x32[3]        = 32'h0; // Reserved

//==== Elinks Data Alignment ====//

// Comment: The Elinks of the GBTx do not take into account the alignment
//          of the data, which is done in the Back-End.

generate for (i=1;i<=4;i=i+1) begin: Gen_DataAssignment
    // Comment: Note!! TxDataScIc must be set to 2'b11 when not used
    assign TxDataNotAligned_4b116[i-1] = {TxDataScIc_i4b2[i], TxDataScEc_i4b2[i], TxData_i4b80[i], TxDataWb_i4b32[i]}; 
    //--
    assign RxDataScIc_o4b2[i] = RxDataAligned_4b116[i-1][115:114]; 
    assign RxDataScEc_o4b2[i] = RxDataAligned_4b116[i-1][113:112];
    assign RxData_o4b80   [i] = RxDataAligned_4b116[i-1][111: 32];
    assign RxDataWb_o4b32 [i] = RxDataAligned_4b116[i-1][ 31:  0];
end endgenerate

generate for (i=0;i<=3;i=i+1) begin: Gen_ElinksDataAlignement

    // TX Data Alignment:
    if (g_TxElinksDataAlignEn[i] == 1'b1) begin: Gen_TxElinksDataAligners
    
        GbtElinksDataAligner i_TxGbtElinksDataAligner (   
            .Clk_ik                   (TxFrameClk40Mhz_ik),
            .GbtElinksAlignCtrl_i58b3 (TxElinksAlignCtrl_4x58b3[i]),
            .GbtData_ib116            (TxDataNotAligned_4b116  [i]),   
            .GbtDataAligned_oqb116    (TxDataAligned_4b116     [i])); 
            
        Generic8OutputRegs #(
            // TX Data:  
            .Reg0Default      (32'h00000000), 
            .Reg0AutoClrMask  (32'hFFFFFFFF), 
            .Reg1Default      (32'h00000000),
            .Reg1AutoClrMask  (32'hFFFFFFFF), 
            .Reg2Default      (32'h00000000),
            .Reg2AutoClrMask  (32'hFFFFFFFF), 
            .Reg3Default      (32'h00000000),
            .Reg3AutoClrMask  (32'hFFFFFFFF),
            .Reg4Default      (32'h00000000), 
            .Reg4AutoClrMask  (32'hFFFFFFFF), 
            .Reg5Default      (32'h00000000),
            .Reg5AutoClrMask  (32'hFFFFFFFF), 
            // Reserved
            .Reg6Default      (32'h00000000),
            .Reg6AutoClrMask  (32'hFFFFFFFF), 
            .Reg7Default      (32'h00000000),
            .Reg7AutoClrMask  (32'hFFFFFFFF))
        i_TxGbtElinksAlignCtrlRegs (
            .Clk_ik           (Clk_ik),
            .Rst_irq          (Rst_ir),
            .Cyc_i            (Cyc_i),
            .Stb_i            (WbStbTxGbtElinkAlignReg_b4[i]),
            .We_i             (We_i),
            .Adr_ib3          (Adr_ib7[2:0]),
            .Dat_ib32         (Dat_ib32),
            .Dat_oab32        (WbDatTxGbtElinkAlignReg_4b32[i]),
            .Ack_oa           (WbAckTxGbtElinkAlignReg_b4  [i]),
            // TX Data:  
            .Reg0Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][0]),
            .Reg1Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][1]),
            .Reg2Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][2]),
            .Reg3Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][3]),    
            .Reg4Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][4]),
            .Reg5Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][5]),
            // Reserved:
            .Reg6Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][6]),
            .Reg7Value_ob32   (TxElinksAlignCtrlReg_4x8b32[i][7]));     

        for (j=0;j<6;j=j+1) begin: Gen_TxElinksAlignCtrlRegCdc // Comment: Note!! Only 6 DPRAMs instantiated
            generic_dpram_mod #(
                .aw     ( 1), 
                .dw     (32))
            i_TxGbtCtrlRegCdc (
                .rclk  (TxFrameClk40Mhz_ik), 
                .rrst  (GbtReset_ir),
                .rce   (1'b1), 
                .oe    (1'b1),
                .raddr (1'b0), 
                .dout  (TxElinksAlignCtrlRegCdc_4x8b32[i][j]),
                .wclk  (Clk_ik), 
                .wrst  (Rst_ir),
                .wce   (1'b1),
                .we    (1'b1),
                .waddr (1'b0),
                .di    (TxElinksAlignCtrlReg_4x8b32[i][j])); 
        end    

        assign TxElinksAlignCtrl_4x58b3[i][ 0] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][ 2: 0];               
        assign TxElinksAlignCtrl_4x58b3[i][ 1] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][ 5: 3];               
        assign TxElinksAlignCtrl_4x58b3[i][ 2] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][ 8: 6];               
        assign TxElinksAlignCtrl_4x58b3[i][ 3] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][11: 9];               
        assign TxElinksAlignCtrl_4x58b3[i][ 4] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][14:12];               
        assign TxElinksAlignCtrl_4x58b3[i][ 5] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][17:15];               
        assign TxElinksAlignCtrl_4x58b3[i][ 6] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][20:18];               
        assign TxElinksAlignCtrl_4x58b3[i][ 7] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][23:21];               
        assign TxElinksAlignCtrl_4x58b3[i][ 8] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][26:24];               
        assign TxElinksAlignCtrl_4x58b3[i][ 9] = TxElinksAlignCtrlRegCdc_4x8b32[i][0][29:27]; 
        //     Reserved                          TxElinksAlignCtrlRegCdc_4x8b32[i][0][31:30]
        assign TxElinksAlignCtrl_4x58b3[i][10] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][ 2: 0];               
        assign TxElinksAlignCtrl_4x58b3[i][11] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][ 5: 3];               
        assign TxElinksAlignCtrl_4x58b3[i][12] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][ 8: 6];               
        assign TxElinksAlignCtrl_4x58b3[i][13] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][11: 9];               
        assign TxElinksAlignCtrl_4x58b3[i][14] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][14:12];               
        assign TxElinksAlignCtrl_4x58b3[i][15] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][17:15];               
        assign TxElinksAlignCtrl_4x58b3[i][16] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][20:18];               
        assign TxElinksAlignCtrl_4x58b3[i][17] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][23:21];               
        assign TxElinksAlignCtrl_4x58b3[i][18] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][26:24];               
        assign TxElinksAlignCtrl_4x58b3[i][19] = TxElinksAlignCtrlRegCdc_4x8b32[i][1][29:27]; 
        //     Reserved                          TxElinksAlignCtrlRegCdc_4x8b32[i][1][31:30]              
        assign TxElinksAlignCtrl_4x58b3[i][20] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][ 2: 0];               
        assign TxElinksAlignCtrl_4x58b3[i][21] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][ 5: 3];               
        assign TxElinksAlignCtrl_4x58b3[i][22] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][ 8: 6];               
        assign TxElinksAlignCtrl_4x58b3[i][23] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][11: 9];               
        assign TxElinksAlignCtrl_4x58b3[i][24] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][14:12];               
        assign TxElinksAlignCtrl_4x58b3[i][25] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][17:15];               
        assign TxElinksAlignCtrl_4x58b3[i][26] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][20:18];               
        assign TxElinksAlignCtrl_4x58b3[i][27] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][23:21];               
        assign TxElinksAlignCtrl_4x58b3[i][28] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][26:24];               
        assign TxElinksAlignCtrl_4x58b3[i][29] = TxElinksAlignCtrlRegCdc_4x8b32[i][2][29:27];
        //     Reserved                          TxElinksAlignCtrlRegCdc_4x8b32[i][2][31:30]               
        assign TxElinksAlignCtrl_4x58b3[i][30] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][ 2: 0];               
        assign TxElinksAlignCtrl_4x58b3[i][31] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][ 5: 3];               
        assign TxElinksAlignCtrl_4x58b3[i][32] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][ 8: 6];               
        assign TxElinksAlignCtrl_4x58b3[i][33] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][11: 9];               
        assign TxElinksAlignCtrl_4x58b3[i][34] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][14:12];               
        assign TxElinksAlignCtrl_4x58b3[i][35] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][17:15];               
        assign TxElinksAlignCtrl_4x58b3[i][36] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][20:18];               
        assign TxElinksAlignCtrl_4x58b3[i][37] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][23:21];               
        assign TxElinksAlignCtrl_4x58b3[i][38] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][26:24];               
        assign TxElinksAlignCtrl_4x58b3[i][39] = TxElinksAlignCtrlRegCdc_4x8b32[i][3][29:27];
        //     Reserved                          TxElinksAlignCtrlRegCdc_4x8b32[i][3][31:30]               
        assign TxElinksAlignCtrl_4x58b3[i][40] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][ 2: 0];               
        assign TxElinksAlignCtrl_4x58b3[i][41] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][ 5: 3];               
        assign TxElinksAlignCtrl_4x58b3[i][42] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][ 8: 6];               
        assign TxElinksAlignCtrl_4x58b3[i][43] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][11: 9];               
        assign TxElinksAlignCtrl_4x58b3[i][44] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][14:12];               
        assign TxElinksAlignCtrl_4x58b3[i][45] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][17:15];               
        assign TxElinksAlignCtrl_4x58b3[i][46] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][20:18];               
        assign TxElinksAlignCtrl_4x58b3[i][47] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][23:21];               
        assign TxElinksAlignCtrl_4x58b3[i][48] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][26:24];               
        assign TxElinksAlignCtrl_4x58b3[i][49] = TxElinksAlignCtrlRegCdc_4x8b32[i][4][29:27];
        //     Reserved                          TxElinksAlignCtrlRegCdc_4x8b32[i][4][31:30]               
        assign TxElinksAlignCtrl_4x58b3[i][50] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][ 2: 0];               
        assign TxElinksAlignCtrl_4x58b3[i][51] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][ 5: 3];               
        assign TxElinksAlignCtrl_4x58b3[i][52] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][ 8: 6];               
        assign TxElinksAlignCtrl_4x58b3[i][53] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][11: 9];               
        assign TxElinksAlignCtrl_4x58b3[i][54] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][14:12];               
        assign TxElinksAlignCtrl_4x58b3[i][55] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][17:15];               
        assign TxElinksAlignCtrl_4x58b3[i][56] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][20:18];               
        assign TxElinksAlignCtrl_4x58b3[i][57] = TxElinksAlignCtrlRegCdc_4x8b32[i][5][23:21];
        //     Reserved                          TxElinksAlignCtrlRegCdc_4x8b32[i][5][31:24]
        
    end else begin: Gen_NoTxElinksDataAligners
        assign TxDataAligned_4b116         [i] = TxDataNotAligned_4b116[i];
        assign WbAckTxGbtElinkAlignReg_b4  [i] = Cyc_i & WbStbTxGbtElinkAlignReg_b4[i];
        assign WbDatTxGbtElinkAlignReg_4b32[i] = 32'hBEBECACA;       
    end
    
    // RX Data Alignment:        
    if (g_RxElinksDataAlignEn[i] == 1'b1) begin: Gen_RxElinksDataAligners            
        
        GbtElinksDataAligner i_RxGbtElinksDataAligner (   
            .Clk_ik                   (RxFrameClk40Mhz_okb4    [i+1]),
            .GbtElinksAlignCtrl_i58b3 (RxElinksAlignCtrl_4x58b3[i]),
            .GbtData_ib116            (RxDataNotAligned_4b116  [i]),   
            .GbtDataAligned_oqb116    (RxDataAligned_4b116     [i]));
        
        Generic8OutputRegs #(
            // Rx Data:  
            .Reg0Default      (32'h00000000), 
            .Reg0AutoClrMask  (32'hFFFFFFFF), 
            .Reg1Default      (32'h00000000),
            .Reg1AutoClrMask  (32'hFFFFFFFF), 
            .Reg2Default      (32'h00000000),
            .Reg2AutoClrMask  (32'hFFFFFFFF), 
            .Reg3Default      (32'h00000000),
            .Reg3AutoClrMask  (32'hFFFFFFFF),
            .Reg4Default      (32'h00000000), 
            .Reg4AutoClrMask  (32'hFFFFFFFF), 
            .Reg5Default      (32'h00000000),
            .Reg5AutoClrMask  (32'hFFFFFFFF), 
            // Reserved:
            .Reg6Default      (32'h00000000),
            .Reg6AutoClrMask  (32'hFFFFFFFF), 
            .Reg7Default      (32'h00000000),
            .Reg7AutoClrMask  (32'hFFFFFFFF))
        i_RxGbtElinksAlignCtrlRegs (
            .Clk_ik           (Clk_ik),
            .Rst_irq          (Rst_ir),
            .Cyc_i            (Cyc_i),
            .Stb_i            (WbStbRxGbtElinkAlignReg_b4[i]),
            .We_i             (We_i),
            .Adr_ib3          (Adr_ib7[2:0]),
            .Dat_ib32         (Dat_ib32),
            .Dat_oab32        (WbDatRxGbtElinkAlignReg_4b32[i]),
            .Ack_oa           (WbAckRxGbtElinkAlignReg_b4  [i]),
            // Rx Data:  
            .Reg0Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][0]),
            .Reg1Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][1]),
            .Reg2Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][2]),
            .Reg3Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][3]),    
            .Reg4Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][4]),
            .Reg5Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][5]),
            // Reserved:
            .Reg6Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][6]),
            .Reg7Value_ob32   (RxElinksAlignCtrlReg_4x8b32[i][7]));     

        for (j=0;j<=5;j=j+1) begin: Gen_RxElinksAlignCtrlRegCdc // Comment: Note!! Only 6 DPRAMs instantiated
            generic_dpram_mod #(
                .aw     ( 1), 
                .dw     (32))
            i_RxGbtCtrlRegCdc (
                .rclk  (RxFrameClk40Mhz_okb4[i+1]), 
                .rrst  (GbtReset_ir),
                .rce   (1'b1), 
                .oe    (1'b1),
                .raddr (1'b0), 
                .dout  (RxElinksAlignCtrlRegCdc_4x8b32[i][j]),
                .wclk  (Clk_ik), 
                .wrst  (Rst_ir),
                .wce   (1'b1),
                .we    (1'b1),
                .waddr (1'b0),
                .di    (RxElinksAlignCtrlReg_4x8b32[i][j]));
        end    

        assign RxElinksAlignCtrl_4x58b3[i][ 0] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][ 2: 0];               
        assign RxElinksAlignCtrl_4x58b3[i][ 1] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][ 5: 3];               
        assign RxElinksAlignCtrl_4x58b3[i][ 2] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][ 8: 6];               
        assign RxElinksAlignCtrl_4x58b3[i][ 3] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][11: 9];               
        assign RxElinksAlignCtrl_4x58b3[i][ 4] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][14:12];               
        assign RxElinksAlignCtrl_4x58b3[i][ 5] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][17:15];               
        assign RxElinksAlignCtrl_4x58b3[i][ 6] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][20:18];               
        assign RxElinksAlignCtrl_4x58b3[i][ 7] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][23:21];               
        assign RxElinksAlignCtrl_4x58b3[i][ 8] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][26:24];               
        assign RxElinksAlignCtrl_4x58b3[i][ 9] = RxElinksAlignCtrlRegCdc_4x8b32[i][0][29:27]; 
        //     Reserved                          RxElinksAlignCtrlRegCdc_4x8b32[i][0][31:30]
        assign RxElinksAlignCtrl_4x58b3[i][10] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][ 2: 0];               
        assign RxElinksAlignCtrl_4x58b3[i][11] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][ 5: 3];               
        assign RxElinksAlignCtrl_4x58b3[i][12] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][ 8: 6];               
        assign RxElinksAlignCtrl_4x58b3[i][13] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][11: 9];               
        assign RxElinksAlignCtrl_4x58b3[i][14] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][14:12];               
        assign RxElinksAlignCtrl_4x58b3[i][15] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][17:15];               
        assign RxElinksAlignCtrl_4x58b3[i][16] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][20:18];               
        assign RxElinksAlignCtrl_4x58b3[i][17] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][23:21];               
        assign RxElinksAlignCtrl_4x58b3[i][18] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][26:24];               
        assign RxElinksAlignCtrl_4x58b3[i][19] = RxElinksAlignCtrlRegCdc_4x8b32[i][1][29:27]; 
        //     Reserved                          RxElinksAlignCtrlRegCdc_4x8b32[i][1][31:30]              
        assign RxElinksAlignCtrl_4x58b3[i][20] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][ 2: 0];               
        assign RxElinksAlignCtrl_4x58b3[i][21] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][ 5: 3];               
        assign RxElinksAlignCtrl_4x58b3[i][22] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][ 8: 6];               
        assign RxElinksAlignCtrl_4x58b3[i][23] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][11: 9];               
        assign RxElinksAlignCtrl_4x58b3[i][24] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][14:12];               
        assign RxElinksAlignCtrl_4x58b3[i][25] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][17:15];               
        assign RxElinksAlignCtrl_4x58b3[i][26] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][20:18];               
        assign RxElinksAlignCtrl_4x58b3[i][27] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][23:21];               
        assign RxElinksAlignCtrl_4x58b3[i][28] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][26:24];               
        assign RxElinksAlignCtrl_4x58b3[i][29] = RxElinksAlignCtrlRegCdc_4x8b32[i][2][29:27];
        //     Reserved                          RxElinksAlignCtrlRegCdc_4x8b32[i][2][31:30]               
        assign RxElinksAlignCtrl_4x58b3[i][30] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][ 2: 0];               
        assign RxElinksAlignCtrl_4x58b3[i][31] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][ 5: 3];               
        assign RxElinksAlignCtrl_4x58b3[i][32] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][ 8: 6];               
        assign RxElinksAlignCtrl_4x58b3[i][33] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][11: 9];               
        assign RxElinksAlignCtrl_4x58b3[i][34] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][14:12];               
        assign RxElinksAlignCtrl_4x58b3[i][35] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][17:15];               
        assign RxElinksAlignCtrl_4x58b3[i][36] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][20:18];               
        assign RxElinksAlignCtrl_4x58b3[i][37] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][23:21];               
        assign RxElinksAlignCtrl_4x58b3[i][38] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][26:24];               
        assign RxElinksAlignCtrl_4x58b3[i][39] = RxElinksAlignCtrlRegCdc_4x8b32[i][3][29:27];
        //     Reserved                          RxElinksAlignCtrlRegCdc_4x8b32[i][3][31:30]               
        assign RxElinksAlignCtrl_4x58b3[i][40] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][ 2: 0];               
        assign RxElinksAlignCtrl_4x58b3[i][41] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][ 5: 3];               
        assign RxElinksAlignCtrl_4x58b3[i][42] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][ 8: 6];               
        assign RxElinksAlignCtrl_4x58b3[i][43] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][11: 9];               
        assign RxElinksAlignCtrl_4x58b3[i][44] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][14:12];               
        assign RxElinksAlignCtrl_4x58b3[i][45] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][17:15];               
        assign RxElinksAlignCtrl_4x58b3[i][46] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][20:18];               
        assign RxElinksAlignCtrl_4x58b3[i][47] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][23:21];               
        assign RxElinksAlignCtrl_4x58b3[i][48] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][26:24];               
        assign RxElinksAlignCtrl_4x58b3[i][49] = RxElinksAlignCtrlRegCdc_4x8b32[i][4][29:27];
        //     Reserved                          RxElinksAlignCtrlRegCdc_4x8b32[i][4][31:30]               
        assign RxElinksAlignCtrl_4x58b3[i][50] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][ 2: 0];               
        assign RxElinksAlignCtrl_4x58b3[i][51] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][ 5: 3];               
        assign RxElinksAlignCtrl_4x58b3[i][52] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][ 8: 6];               
        assign RxElinksAlignCtrl_4x58b3[i][53] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][11: 9];               
        assign RxElinksAlignCtrl_4x58b3[i][54] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][14:12];               
        assign RxElinksAlignCtrl_4x58b3[i][55] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][17:15];               
        assign RxElinksAlignCtrl_4x58b3[i][56] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][20:18];               
        assign RxElinksAlignCtrl_4x58b3[i][57] = RxElinksAlignCtrlRegCdc_4x8b32[i][5][23:21];
        //     Reserved                          RxElinksAlignCtrlRegCdc_4x8b32[i][5][31:24]
        
    end else begin: Gen_NoRxElinksDataAligners
        assign RxDataAligned_4b116         [i] = RxDataNotAligned_4b116[i];
        assign WbAckRxGbtElinkAlignReg_b4  [i] = Cyc_i & WbStbRxGbtElinkAlignReg_b4[i];
        assign WbDatRxGbtElinkAlignReg_4b32[i] = 32'hBABEAC1D;
    end
    
end endgenerate
   
//==== TX & RX Match Flags ====//

generate if (g_TxRxMatchFlagsEn == 1'b1) begin: Gen_TxRxMatchFlags

    gbt_pattern_matchflag #(
        .MATCH_PATTERN (8'hFF))
    i_TxMatchFlag (
        .RESET_I       (GbtReset_rq),
        .CLK_I         (TxFrameClk40Mhz_ik),
        .DATA_I        (TxData_i4b80[1]),
        .MATCHFLAG_O   (TxMatchFlag_o));

    for (i=1;i<=4;i=i+1) begin: Gen_RxMatchFlags
        gbt_pattern_matchflag #(
            .MATCH_PATTERN (8'hFF))
        i_RxMatchFlag (
            .RESET_I       (GbtReset_rq),
            .CLK_I         (RxFrameClk40Mhz_okb4[i]),
            .DATA_I        (RxData_o4b80[i]),
            .MATCHFLAG_O   (RxMatchFlag_ob4[i]));
    end
    
end else begin: Gen_NoTxRxMatchFlags
    assign TxMatchFlag_o   = 1'b0;
    assign RxMatchFlag_ob4 = 4'h0;    
end endgenerate
   
endmodule