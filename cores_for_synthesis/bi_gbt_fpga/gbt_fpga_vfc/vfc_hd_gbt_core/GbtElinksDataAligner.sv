//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: GbtElinksDataAligner.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR                  DESCRIPTION
//       29/11/2016    1.0          M. Barros Marin         First module definition
//                                                                    
// Language: System Verilog                                                     
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     Module for aligning the incoming data from the GBTx that has been misaligned
//     by the Elinks in DDR mode.
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module GbtElinksDataAligner (
//========================================  I/O ports  =======================================//
   
    input              Clk_ik,
    input      [  2:0] GbtElinksAlignCtrl_i58b3 [57:0],
    input      [115:0] GbtData_ib116,   
    output reg [115:0] GbtDataAligned_oqb116
    
);     
   
//=======================================  Declarations  =====================================//

genvar           i;
integer          m;

wire [  1:0] GbtElinksIn_58b2 [57:0];
wire [ 57:0] GbtElinksInA_b58;
wire [ 57:0] GbtElinksInB_b58;
reg  [ 57:0] GbtElinksInA_db58;
reg  [ 57:0] GbtElinksInB_db58;
wire [ 57:0] GbtElinksOutA_b58;
wire [ 57:0] GbtElinksOutB_b58;
wire [  1:0] GbtElinksOut_b58b2 [57:0];
wire [115:0] GbtDataAligned_b116;
   
//=======================================  User Logic  =======================================//

// Elinks data separation:
generate for (i=0; i<58; i=i+1) begin: GbtElinksIn_Gen
    assign GbtElinksIn_58b2[i] = GbtData_ib116[(2*i)+1:(2*i)];    
    assign GbtElinksInA_b58[i] = GbtElinksIn_58b2[i][0];
    assign GbtElinksInB_b58[i] = GbtElinksIn_58b2[i][1];
end endgenerate

// Elinks data delay:
always @(posedge Clk_ik)
    for (m=0; m<58; m=m+1) begin
        GbtElinksInA_db58[m] <= #1 GbtElinksInA_b58[m]; 
        GbtElinksInB_db58[m] <= #1 GbtElinksInB_b58[m];
    end

// Elinks alignment:
generate for (i=0; i<58; i=i+1) begin: GbtElinksOut_Gen
    assign GbtElinksOutA_b58 [i] = GbtElinksAlignCtrl_i58b3[i][0] ?  GbtElinksInA_db58[i] : GbtElinksInA_b58[i];
    assign GbtElinksOutB_b58 [i] = GbtElinksAlignCtrl_i58b3[i][1] ?  GbtElinksInB_db58[i] : GbtElinksInB_b58[i];
    assign GbtElinksOut_b58b2[i] = GbtElinksAlignCtrl_i58b3[i][2] ? {GbtElinksOutA_b58[i], GbtElinksOutB_b58[i]} : {GbtElinksOutB_b58[i], GbtElinksOutA_b58[i]};
end endgenerate

// GBT data assembly:
generate for (i=0; i<58; i=i+1) begin: GbtDataAligned_Gen
    assign GbtDataAligned_b116[(2*i)+1:(2*i)] = GbtElinksOut_b58b2[i];
end endgenerate

always @(posedge Clk_ik) GbtDataAligned_oqb116 <= #1 GbtDataAligned_b116;

endmodule        