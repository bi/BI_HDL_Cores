// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:42 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
jmyFBdQYeF5R5537yO3Rd8jC5ySlV2VGWYaVSFEVuMcleT46IlJglG8wvtR917b/
x7/FqqfHa9mRK2hm9bbaz0ONW2qH6+MQAs9q8hMPZxgaH+/yTRZnojIuAWqYyrdm
r7zYhEhdMTnWONniKxsRx/NhtakYJhZSEAmEXBD0jrE=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3584)
aVwj/L6m+XZfthB+TMB0BZm7pXzDKhzk4YOK1JX0YYlxPHTYq4wV1p7NGBgHLBrx
mxSeM5gh8y5h7A2zWawiJvSITWfs5NBZXAyjTCdpE0dELWNFiFHeP8AbMqxdAabI
9WWTXQNbxrsx9s6P2L7Gv97MpJ7LdmRnItddXRDjB65j1u/JeN03BxpuPHYq0bzM
kgIqRSDjr5F5BOrl1KQ2nCBUwsyUyv1zmMdLd/R+05XKx2+mSNeRAj3Y/Eh4YLfW
KyQWMDPKekjwtsqC6ltsTz/jU3aaq4jI19f9JJUHxwvkAuXgRriTp3FHuptwD/vR
+xcBj+AIAMD0P/SRgYKRSnLqb9nn2JHHJgjx85NnzuavHzW3bWmTGElTajr/FkSz
6cBARl2+aSvvYKr/aB7QFpGqU6vRI4mt/P2/wkIScI4SqjuTlWtcqVG0zNw6wmw4
2CSybjxyaHxsU8Elk1PhvGDpLXvavgRYcLGpg1a3wYYtV6hkIvuILmYblXboC+Ne
1tjmx5uJ5/2ekAc2Y6N6s9vRsZ9lW0t47+WN0oy8aX9hd4iV9OK3r4f+kBz6R6im
wxh7TKXxIGyuJBWvu7TzkCcBSYIh8/31ZCui+zsldVlvQIdy3tcXYLxsfuBdgxvK
0pIOlo6ZumkoSCxfpJKHm2kr4vfrM66QeNQl/POm6uUapUH6tTLH9NzvfevtFYQj
iB/bVy6GuETSNF4nRif+8lyxoSUonr6jqmZTyd506VRQEHoEgpaxXAsBqV8Tavbq
Xlq6mkjjeYwoCjhcfCz6iR+UvlQ3dtTJdehvG5f2DQoavRPa0MFgln+EVQRfkajS
KdgyVskc2eeMMokBhksQinUdSAs/i7jwFBj1djzLOUVOXncEheyzeiyLBTLej0Y9
qWCqt+KbZv4u11fnnBT6hlruifd+f2Fv65ug/oNTuuGB1CcaTycpd7UC725HpCEf
/Uaz/aWR7YAlFnk26fMPIkwAgXb224xowbkiDbjveidd81qtwGMva/fcKUjo5UFl
ZpJhVMJGdkeZ2zxqtqwIK9hAamur/NGA+kyapiWs+qLPLRAbbMKYiQVHioiQxl+l
mG1e5KwpTA8DpUj1MgRBw4SasR7nq5S5HNAgNEt7qrG2pLE7SpDIsbBFDPyPVOC0
GS+VEEmRByZkIvRMTqLn6EFRTz3ovWUVyIFR+hX/bkNL/MK1ayMCP89rkuWQY/JJ
yducvEomSC1ifY7cXB33a/hwk4UKhjb2OzDQvxxRtYqhqpL9u8W/t89vgVJV7sVi
jZhKbowJhkUT94Z2fX9xzPHjTCPGfHX+QUQTQVUEU+7QKZHlhmnTY4B4dx0GqnMg
D3GUWsrjeoUvfeI8z6e6bo7AUkOBNI/7gf7N7Fvtf8gsu18wu7Eg/u93TesGWkr2
BFwqmgsaDZ2RqjqZ1LtofIqsRaUuWsJk+UI9Ang0AxF59YzMGHFzC1miv58fN9V4
piyWLpD5m6J96T1i8GUttla29TnOqaLF840Cf1SsWUbzqC20Jq/AjxySpj8k3Zz7
1QmcjJoYCU7syU2kZIv5hUtqyfEuA4KauAv7nyF2M1jtBOwHMZYUvz/hjYMbkOIv
BnIzj+TI3FDfN1H9YxS78rZ60HAePKxVCeplprIQJ3kBxgpQtkXYet6AG81qj7yf
s79SMNNiFIQA94eIvwCajnM8JIvp4cbp6SiqB2JgnB3VBbZ1Mmv50Srh1tnEvclU
OuDRlaBsROvdmd+BryiNrXLuH8gW215VHgXOCrTjXSwoGYM8dKjXwHVAwg39pYLe
Ay6wCRXc3l756jCzw9ZnmHz4Qy6foThvZS7/APZ48EpKMCnL1VhPdTB05NaNkVCN
xFKNNMmabs7aX6oftpI7eFmVn2gslvjrP1LT/9US4kAe653Lqk+WJ20eptjTMHku
pxlfOIuNSnnGQCsWzj5G0jGepqWGn6oe+bNHrpmlNmLKVwfkR5OYf4LyIkjlXGlk
oDYwan5iHYqaJnhz7ShQ7Lg4feMtEaIqfHNIoKWb+seFYOo9a27ELO8f2deuwWGW
laOAeDUpNy8Hhg9tveIZM0Dey9MzuCkp+BIia0TWKMD1BtVYmDkhNbiQK6hpD+qQ
I0neMpgW7FoOuMF2XOxIAhXio/Smh5RwNW0+yfuh1Fo1iLTOwhr5c/GAYe10DZEM
1CDVgY+r6U5i9XgGj2s+sbc1sy7m/skfeqs92IzvmT1JQBpe9GeIgAX8CtCcStSG
gxPLU6/XpJ/mfXqJ+DcZXmeOdSHvklTHh8RVpq8N0P576Pywqcanc4POEswNCKVQ
O4KiWFCjg6IQ1RU3d3mfLmo8b0BG+UXYzI9/FMtUCZmLLPXpvTLG9ADzJL4F074g
cAOBJ8mF0LFlpPn+Z8bxDmktohBkpOuGaqPtIs+3CT1jJiwQTMPgzAcIZgwMR9Nh
7Gk24V0rwSGnj7732mFXhBz3YT7tU02JdVrEuyie2+B13UsYp3v5o4qF802emtQR
DKGLRj42CuT/Q7undXGy76QO1IwMMgEh+4XFUgNQW6mq68VU74ijZMq1tBXKoEzV
/TSBvE9l1CNWQpFk0EfzEJ3FGCxBnM5C8rZXW4eW0b9D33G37HSguThRarvL+bTv
WmeLYjS+AaN9rTXZfotCF3K7RMtd2AVAnq3gwFXQYR+QXCQwyBQZJx2yryf10qx9
zkQmTlBIQ353xDMUERd7XBb4yt2o/bran7CtUMrI9X0c12WlRZoxmYOuJXxidSY1
DfNawnUsyOv31BCUJh56Vsq/Sb0/7qD5St+DaqpWmRylWrfgSLixYBcwMe/JgDrb
sn0qKOlT12fhTZzvnawInTqzPfIvLOOg9kHu362dW/ddro6lENgjou1oYKQ5tjbH
AXvp2yAQLmS/0tv445j67GiNBl9//+oW6Y3NJ7aSUdPdqgm93LABs9pNr33eahSO
lffMFjqFwI6FsZCI6vkG/q15MTzyJkSeCMl6HZZ56DhR+lALGeEOShrOP+7QHXZU
Kvw2J01WStBejTPXGBH20yi1wdKt7DWrhPcQjx4LqZHNNdv7IMya5KCn48AQXpuZ
pHiodDrNwnLHF+gjrECyrAIK0jromQb3DcWAo+AACfMdz9kZNfp7an30JKqXjBCo
XNWuWh55w8UzOaTctQR/694rGsqOALGefEmkmqEkP/QjeW0CnN4G4dOx50Xwqc4n
d8VtRwGK+3MyryWZ8n3eHdNwDsgXui2BrpQT40wJgJItXCGyLi4+RCr3geppaDf4
m5/RmQg1QjruTXKmRCI3rORsbpHGQ8zis1g+G8kwMGgV5ZXLQu7JT72xSFOaOFUg
FBWcF5HEim2S9v5v10RWvmA24RrPFT/iMUCU6GAP5yO6Pgc24+RJ4fM7vxJZDOTm
jsz70IEUqqseUUgt7KflfcLwuoUw415mvmpYr66e3uOOJJfbfaQ+pZfh52i/ASiK
aw2OP9L1ZgCZJ64aW2z3D0ZtYCp/tp6WNUjwG8aCnVjZ63AYH9VQDe57WPlaZmYA
x59x1t/Dx2WqCb7aQsHgk3rMZvfEiRBQCWgM5cNK8cKBsVncNeOZrm6cktDpaUYu
VGoDqlvCH5gV5W96sPe5vLNlHwcLh3Vhm/01T0Z2vLtAs1xVZbgvOei0U4KLqnRb
xTJ9ptbUoCizjjfi4Q86rVYrIQTBUVu4sn5fi3HWyPytaMMv00bTf2zYns242GL8
nJPx2E8YO/cYtTPwF+1G1RF5uspye1GSDsYTFznwV8Nmy1AQdN3djMReKXMiJlYz
yQ9XQYHigTCKKEYzAmynUOVEGOuw79C7hjsPrNDIRhC7pdlkcI20mPEbwm6lkR8V
dslhJHQ5lvWP/4KBpwHKdYEMMrDm9ZBWGKaHICSXnDvq8r9OIOdYP3RmhGOYlP3e
s8R+9Wid/9D14kU8mPXQaKf7MY0VCtdacffSWaEh6KGQvifpKx7KiIz32VIlKBcx
v4MzeGV7HayIH3HN27edTtZA1VOeO55F9ADubSi0gQczaFl+bgomEgIZtuoT05ii
N8+SP/6LqR/UVw/Hy7NV61Mctuqp/hyBYjk3hA46t45sKEXJJ379R6q5vVyPLsbj
llup3HqrFg/goTFTnKZZZsbMPROelo7uUqjGi30IdsQ5pp9BwUIEFUCbIalKcm5O
tzt/bWw4w6hb75rYFkb9im5gyDro5Es2cCtzl5MO+dzKWBhFsBRZxnGEBPeFnM2r
WlvzW9N2ko90TO4aIA6iGsTIxphRJpREkHT7yVGKKQijlBWukxhrNSXuM0xNLtjD
lyFUT5VQ0lr3Vzyh+HVsJ7/5MYlCTF1jna0yNcq3Gq8asy2ctyPXhZ3ozdzQ/HEd
NeepZbEx7tt2brpGP+Aca77lnmfXx7X2NfgegQm2RrzNSFKF/LoMozUOOIiyOYBF
h09Zs98A1OxsobJ9QZosfz2ywUHWOhrQlF3ayAB6CybgONExxxTVvJhXBFLj9g5H
OQpX1Wqnn9GSGAxpq0cD7o5cS1ZoEz9TH5hIjoH22kOYPl1KK4xV7z/ISEEV8Kri
3aBXP9pCb/77Cl1ZDp+9wO7F6cWtWfpHBGRhV9l2VVnp68sQrCqWB9Q+tEVfnujt
yjzUkQaB5mujAEI4yz0BBCDTsZ3CrwWjLzWKIBRb69A4tkWBb6PVDYOESGs3xWM0
Ba4cJe4re6KjAudCZj4sQwa7bWwfvOcc/n74R1igqC5llQ1kl5jij/pz1BO0mYwf
cxNhhQF69ziU1fIDqkJuDf5QH7QnS5AeyKvKArxOLYc=
`pragma protect end_protected
