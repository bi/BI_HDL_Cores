// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:52 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
lB1NKYxw9DcOoA7h/H8VQwvnmhw/vgfmpL+ZnUbW8rls6umCVojp1PyBiyIYlXOZ
Ilk0JoV7Vg45upFnWFtO6mcbmwc1OizRx5FQutp5fvK6easvwEHXPo3nnthNC+Tl
7vd18km96NoM6dr1lj0IUk9dUwN8fdACiQVX4ALvu+A=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 7568)
8N/jaV/Gw9DdFO8nIIM0dzt37kflwd4nLTKjWiKSr82CCkY2ApgvIYye5HQKIc8d
8mz2obnCdeU4M66C/xsvl41ZswfC970z2GzeUFCNWhFIVuiOqGd81WYeAccrjnaP
X1hhbZ0CmZ/YKsAnrcEnoGNjoP1LECIoErFPVHtIbgr5z87M1GCs5d2zIttyIQSn
pfiK67i3DuxE1FEaBsFtB/Zrqq49xAc4WB1B+li92PUUPEBeQ2PczNZAs27H0zjB
fYIsEMUIpIL92qo0Y4b5Rv95LZie89hwngD4Uk1bBso1yF6oCYdLadOsE0O6Jg8l
25fCDRCsHZOiuia/di9I4LdfwuWBy899LRO4iMNUAaLRTLWj/qBUyTtqPgYb/pWQ
dM20Cnj1B6O1Lmm6IiLWOiDR1kplvM20teEenHC7Y5KBhk9iFvCYCcd+DsLiUXLx
csJ4NX4O01INGkdMvguhW4njKDGhamXdm0tBLSCA32fi4PweXBDBDuSQbtgsDnLC
vY4gm3HTub/GK4ELDCp4iRHoIifQpr9CDam+XjUHn5q/vRZqf4CHjxAnftk2TbFy
7m2Sb21rbcDbJKNGs5HsmcAP9S/7jv4PiFfOieDCyfJNdT1AUepWIUNLxW6ObNCM
AyB+z4Czepzpl66pS0x9zdKezqPfBODfP+OzFxCbdKYS5VTVfmzon61LLJFnmEia
kElZkKF0N2QzlnAKPs0C5iVR6RP78J23pyBrYMANleKZxuRkHu17Io04dYSCMVfY
im1I7eC9nnT8pmqMOEVst9fNP12zs3HF87AW883XgHJNLbOFkdSTKs1Sy11XGHU8
EGMzCe2O2TVm6a4Hq8dp24UqIneSKTXhVuM4UEXAzdva4A1RVZvMh2KyWP/txgSF
Oj8JWTOM9UvyuFopYhHv1C5HA0aMuB9Y4GogTxb/PydZhrlD0kQe3plmjXbFv/Qb
M101/vtBn1diTKvf2Ok/rrt8vZggNBjKNQhRzocTvj6I/tai8sqGMolmoLTeaLgM
x9pu24srGnXg4pVnPTTRcz2lvwCcsRpBAQQnaEJObf6gV+bVA1UW5IU8B8ea+bfZ
a3PeVAiKyjzVXCL04jb42cA0lneSu0MyEGh6X2U8UxVnQsC2q0tK4FaPRfuo9a/+
OB+802HVpXH1YUCkdUHKt39K14T5+FpFYC0qGRAtwa67PP6JbTsCNoUguUYNRWx6
0ZPe2ST7pRLcy699tWPlcvz05LQvO0zuCltKvT2eqmBYYLsE/FgOlOiMpcBl6VW7
xASNP0IrnbcEmYJ9joHUtY1hoUPMqkftlhGGl6qG8aVIuXp3sOIgqN+q1bcjXNGm
6lywnMOmMaa8inFVD/9cTMO3pUtWQSI1bIKFQyheuNKwwls6i3wcSnHQFTjspHzb
hrZCqOFlYVUJAQyDBPjOGhwqO5q5DdW4HtLJShtjJX2GLWKA02TGJbhlb6rJODlb
b2nXlbgBIquJlD7+TvGugrpag1kW2Tta/ZnnDmXhPIpIsoM/QRPZ9PIVBuemxoHo
tefsRBzIF+Sa+7vyqNbK7kAnBB2YR5ihz9v0Cma5BoVgzEgWNg46tc1YpvrFiAwL
LFExnMlIOkjqCcWCFgcWrMW+TUMpj4Rx24xl7rUQ8G6W6o6RQEdc6qq5Y4LzEyqx
OjF5CXIdfvhkjZ6ZSH+LAOFyorwK4XHjkQgNYd1KNQOwiIzFqyctOg/OaKQ/NOXX
6IGPGrBAdJ3nqXmwsG2peNtpf4Lj5j67b5dbgY1+U2D8hT20gDQcMWKjtvS6KxFg
8P38v2W+dmVejt6akpExV7sgCSsgREhGtGc6/bsg+8rWND4WtvajMdFvDlB2zg7N
dmbRuGRnhv/rDo3kekbj9StaRQqPsIXNR4AuvV0iO/kC+gcvG2jTeEN1nIJ604+o
WCoYtPITWTSCKWelJblv/b1wEG3mzVVVRMY1MW3BDkOXwLVLzMZlfbCpO2GIvgQV
78AHkgNMJC7go4fShcRL9mDHfqb9CAlyuwJd4lnDkbY5ap0kq0YDcmWVdmPf/n13
c2qtl2sqybSjAlEEUDSSlMYO8EAknb0d5+Nue3oGweSl21lzA6qYS+rqGURhkINL
lnurxToZW1V3rU+lujn7gNGHAvfBAQ5S7JjANkabMkuPCCFT2Ztvn5x4+gt23idJ
EMjvNG2kbrQ2BZL9dQkPmWw/QuwJelHNr5o+kbG9HYqkSrQvTZP6J2t0Yj4McyJI
20hOEuTkW6BuPPwhE8zU507aVbBDzxRDtbwPXZaVgqYLFvILfjMbqmQgCFVnqIFt
ABBmdnLsISuZvf6oMyboRHalCWGnOpnFTwvdN+9h5ywLEG+QeZsyaJPXD7UkmxIi
EVUSWkgQgxGm/k5EFyTfzH+YzaHHD7nlJXrcU1UMb6rXer/4QuCzvg4caXZgN6b1
TksDYoJLKAFxeOrT69gaYHpoc1fPzDmv+NDN74X0HnmrXEHnrb6SQtIJAZ2D5/Sp
QulfzAhEt4eqX3g2h1TUH9hsTGCF2lvbaSAfD7QJn7TibsZWL+UGozorfy84cx4b
9hvN71EeegDedrPSgQWp/uMJgcx1VoIGYabZCICROZ2OsddeZhuaqGZccy2D3hkx
yvJAqRAPiJeUQNlOdw0P768MrBzB+vNpmYGKhccyGYlVKE5EnSdtqCrYrvZLlG5g
ymU60qkaUxdqwMjdAQPflNmLZ8ZirZ8sEMvxif1sxcUrYDbfawQdxIPQms0url4i
FCErIhv+jQnngeHNFuphxtwySACKTRHcgs5Q6ILCCgGHN5iYd2tCGkLRKdsUi+jj
sz2o/mZhHcbh9A46qYBl12kUa8PAkG61CFWD+B7HOKI7U9qlfdpE+1NvdNY484Xm
5+JAdlFopp2HrlxllOsGStixEjyMQzrrbPBI7BMvCd1F1qgTTYUihbgeQkXAeAPN
hy7DZV5dCWR5erHdhIN4iwL4Q/k4YHIVBy+pwxLRRH/Pqu2dSnPr+TAHXqxRUd86
KMnwYob9hRDca6iAz429XO8DG7zkccvVWZlQnJGd5KHr7eux3ID6VJBaSSlGbaD0
qT4eUGej6pPSMMF7RFV1OGXCwbxDvSRHnsIO2ngCTgsWjfMR2xIc1HJyMbHILpVA
B5LWehkd0pl5pdVlV07aQahaUQvXwTHKI2FASeUSQfQcyIfWI3hlwXqdgsFknIRL
2wRHLRXb3O6SJC3Tfvlxf6IDuGeR8PdXhRA6FXOgUtjm8PQL8YBETnUmRWYvgTrZ
TD5QoaLCu718M25B8M2v63p9JUDg7yQZEL8kyoAEi6E8iGeC0meEfNbgRPvF/B9I
kqSS8bd/oDuL3Bwo3c5ChTVLWKV2ZpdDY5fvkBUoLAeqvZtf10ZkQj6dkMFAdjtS
y9jENzpzGF4dm7fOoZSzAxlKKHDn8Fyu0DLtrkAuEO0+5jBYbFF01mllo0fydQUm
HsAY7fMlUPvDlz94IJZgHY41gaqtVXZxrWEVC8DEhDYPtOoBkjkkkJ5T70/XK2xW
/5uJBFEC6DqxxIAzS1pmvYVai9YjuxhTJK92+yqGCCmLmFVYCLS/qO+A5JWLb1KS
sfeZDaqFrLEFMbIpDyA9PaypZos5QI/34swtA0GCbYNYB4aQrxJoR9fpn2vKN/av
Y8n7dzqU40Pvwjyha2wlH+gk/w0zSlPQB2F4DPU1tMzHA/O3IlIt8Jceg9B/cg0K
88o+pducvPc9JUQXFBLnzJw6vNLrGFvNO6X/H74EE0jQaoErFnB7z9cOLSA8U80W
7uLoMhfBXEhKM4sm+nmja0w2oqT07SeQyvMHcWlX/mutZJmMVYrB+v8iLcqZIUEC
Sf5hzAjIgxY3D6xBXKddp8ZbJzV24fcPD1wT766jkTtubyCM7tYnAPsjBfalsCTF
7+hnq4hKE8/QMuhSvaaDns1Q34ct8IoXbENM8aW3U7Dtdf5/TBv3uoJL0XXjFoXh
lwLEK52SVJtn+UOCHleriebRG7Ays8enA4w6MOIhAyvCqA2eG1lplmwCFgrVmstd
U7VcYX++gbDVfdme72J2XnYGFySqlqcy8MN3LhjTzOAxxNgcd4+h2h6UyQUhacxd
YLo3hkL5w2SJtNk1fR9KvKEgPMyyJ+zqYu/orvsi02x8CXKOIbGztCQjNkbCKZVK
N8gzvWkJOrkmR1POUOmjy9btMKYLgyy0ClzTZrFPjHXBOiwEBVh5g/6ovRQJFYWx
4XSHrZgg7ATNpgaAbixYRv+WdI9uh2itc6knohXFtC9lmossqeTixK4w6Rz5QGaE
DN1A4No6hhi/hJfL1wducGSLDptEy4Lyk+W5jeIkaycsBCkkPhXrlquLh9TWYf4q
XNOMPCXiIvqkHlbABtcEwtADhPR+BMHNxL558Pzf2fRdQ9sYdamsLxAxnqOlb6VD
dKoaHAUz4tY8zSaaQUrd66m8PQENwOPAedt6UnLhoHqafmX1F/ie7ihk+UHrc+jf
HK18emZfNM4l9p7NjXN+M3E1vtH8If4ymcDvlBoPjuOiv8Rbp9iBWChjnpO97TIh
2qalw/UdxOahl3tJtGHQa0IneMctUPy/K7NNB8MDGyg5WpYmrWhntk876lfmaN9/
ThkgiwCbYgbA+s42AaLCqde9McI2MmVHL4HZewQzl2izwTubJ6Tr2k8Lqly9oxiI
zmijx6YWzcqoA/LKfsItfz/C2WzXK05XlGgFhsGV6BaWl2sOxv0EYeBK/O5Tigvp
5E3WgpE++c1qnVqddrf6SsUWADEsYf9Osc0qcm/8hQ/t+5ZmPX3F8Pj023yoFgQO
AQzkwIR7UgYnpdBfYWNpknTE2t/9gFqDahXpBcE8TIGP2rNFiOv7LJEVkZmCe/cD
9lvaYWqfSeqB1NCcgho/EDB0U+Q2hGIVLiD3IsYpRK61UqJOBKW19jswy2VjZwdM
nlc9kbhxL27Ghxb7ezkJIE7n6U23qTotrSIOFhzCUj1lsI2IbEJqrbQSxLyjDC5i
qsl4O95USDaquPflXz8CpK7N4LMJqqG+4Nh2xWiuii10tj41XeS8RJdom88u0x2T
os2FRXca49U1lP6ttNXyc5XjF9lakCZVGgxng6vrNvBHI2+7DEr6FxTSVtVTKKY5
9+hKvp0Ba0LjXLnDAFfPeStSeHpDkc+1Knd8nskIu9E29aBhFGwoydTsdQTggIQL
IXk2jlqyanCnqCI8Ra4wVZS9oJqrYRdIPEAPW9thTOQvw2gutd5IyeAWt2GfiP3j
QRXMvdEGdMcz+JhGKJegnOZzkju6xsT1S7xETSAm4SOSsweOlHIZlNSg98omE3lA
ZWwsvKOkbdsuI41LJzPuxfmjq3dlCvHSO+7fUKPeOJFiRrqlF05h09aHxPZemp+D
auXxR9bSXcRaGflCienKQFH6PuEajJ1YfJ05HRfJptHVMkMz3ObR/sE6vaGDePDm
chu45PzwH3SCwpnFffDaY88EON48C+Ue8RdLW2q7EsYzmo9DX1IzQXZwPjL95uAO
T7uw4QnprDGSBdeSE2pwq3uAoHjhy70uFkKhORx9eY6VYXM4z8zwJQzzTIOUAKJS
M7XvwRfLr0n07Lhsi17mqghWYd4NB3Mob//boeWk9fg5FhvzItcdGhnbNWcvwtPZ
S83qiWCVXcCsXJuq3bhw3t9D3TuZ+2WCxpbBiDL16gdHOfjXbMWAPnl8DCKlRbl5
jhz/d8qFGBd4tmWnfRo7mSdMYCtKlc/628rZ5RQ3fSR55aOv3POO2e57uUEPAkUF
a+rIFJkzPZDTPDR532iZCKSAscOWqwbGehsnaaHp7o5eOchxRTWWAuqg3iQ5YlEP
Lttk0gWk9in0d6eElIqYYetdPAeOl5SOEgU3QVgL0AUJYS5FR9Oehte9D49gcfdD
pJpHgsd+1UPqPDdNUC6o4Rfh6iPQLZ2Ylcb5TuKWiUCHFRWRzJSjeiTq6ASsEZo7
UqBrAqomuFpFzIA/QTB7AX51zW446d0vfzjtBgz8OPUDdvV96f1k784PDzDwQtVo
LNAV/BTPWJuB497FPZLWlOLjrU3E00GqpYp/74bhNjFo9ze58sKA9vgx7CswAPTI
Sx2J1v4Mp+jnR732orNAkVKG3hejtch9wWgo5qP2ls6BkVIapbGIU/c2PM7M1MYQ
6G3QjIm/pcOAp7cuVMi4aCTS/FHGeO+o1NEAzr5o9wc9KHsiYaPki390BpsPxFDy
FB8najuqdX+m6ZhC+fri6wUzDjwy95e04dg6LRSXCTo+NMtn3axLllAfx3NQ0RvI
nunurph/RfArnRcaYiLyGstcoJWf/Pk3WVSDKCnX9u+Stqps1J9hKHoc90HdzNO6
3yHE6y6CW0N398jmo93hmISRZwY6SJVyJb8Fnxz5JoPOD0HhUE5lt2nGPpxMVkXY
pm1RP3FC7RuTNAO4/LfBiHdEfQZ9OOsBXduCbUo7b0ZJMFSy1tei/fpzq5uLgyv3
UrBe1+aLaOc8Iw6pDF5mRgtYf4lZQOORIOwt2zgMDr2ikcN9OTrcVxdzkVwJoNIN
1q/kng38tUUTBXsxyRaef+VVNuV4EVN/6OjVmjpIikK4SbudgX8s0Lb6EoCrBVst
SZ8H8jNANmLPyKBeZLyDSKbBjPh+w7J7GEAVLgsvaYRqu8WnmZFTkCJhh/lZtxU2
RLFjbJ0slPp+YXnFZl95Tj6BBKkDiBzrR9Q2x58b5vrQ3IDdmdtgYpQ+FaDoDtO0
+YKGaJAM7d4ROqLIj4GROftG8xdX4A1bCfohZnKM1uSqSet1kbrmmuhVr8Lh619K
efgrvsIHnrWy60zCyMdMby9S891EhcVYoArUQoGDwWTAHo0v43omuv4B7Ak5UBkq
b0MSyuygBn256RH8+nl6zA6fQC9MEGK1Zj+0vndu6FSZubqUCL8bJXuqlgXAfyeX
htr+keywk/ABlPRABQsZ0OoB5sSayPe5gbZcx6m6zQWZ8wR1imRN6ZUHH9cDvv0U
tPposPslbP9WU5+w8PO8CB5z9F4rDtfyA6V0eNLtlBDfqCYOzxr88WsFFlToEgzG
m/uNwXbyp1Ii8JjRVwDffo+jlD1SU7Dz6Co1O6RkCx5ZnY4NcmA0DSHTTffLCufx
zPGIyjX+oTatMild4IAUd3rXN7UYaTpyoAaeIKQ+FmSbH29onG48BKr94Yevwyw8
tDV17JR6XD/Z0NepjBriSIjmORJWr4+4I0JW3mWCuuBYvRc8ylzrmw6d2Y1nb85+
r0XlJQoPsdkJT1YQemsqsrw/EsY/lvUMK0Tz4hyqbz+dIJ06rGpkQ7yVeSCXZNjl
VXp8/yE9U+T4wOfxoZjVdWLN/vKsDKhtyI9pLFhpQWP4S4c9q19BmQIo2guLeA5N
BLI2cKLZby09qtLWmWwGk3ykQucIrZQ/jmLFAguseoMZrNMsfBmS04D14yFz8ANe
FNphDfLctPNB1mJhaeBLDrKDbkfPleBSf52Yrk/VnL54mqI6pIygmE01qSDWmir7
RoIU/jzo+UhEtjStE+FzPjgF9HlnPm7UymX2Dr2+ak7lYlS8a+DXBAn+Xfcscyc8
0jwHMUet0YS7oM43kttWlm8nPt1BasCzPctTKZKiv6IXuZab5wYy9cIaD6WDseg8
g9fhFVjlulHexfeU/5dva+0l+QYDOATiauxO0qTbV8MTuiS0dXPWQTC0sKKuSfou
36yeChhTD3Wl67enW7xt2pJUEpLpEJG8Ed+6W5nPereoBwLr9IsZCOpUmbdBf2o3
1OGDfwDNZ9C1GLvOTaD94jXp+ekpxAZNC9tXIC3AZ37BUxXqztrXAGVocHyE7ez7
J064hTko+jVHUBmcKkcGiB5VDWlSis3WR98wZ8gfDKuioOjmFznjztilDwNZ8jSR
mcvusZL+ueY4dlcnPKR3MPlGWe968GaZSODqxYslaB9AMxI+4yvKxqcsD3woLwZW
vrAzG6kAcWwrzd6To2pWBkf4zN1w27bOlNiBsaJx6Hwygww/UZWdNPdZNNVWugWG
AYu3ok9x9WUpJOBGYnfYchNkGKd3o4+tHIK9+QzTy47kGA9T1rei9R5A0ucAzypi
hiBa18AEVI8bfxpRapHb0QEskP2dDyG2PmcZoSxlzFch/bpHXRvXRn4wDe1rzua+
pGG/ECy7gZhwGoD18krpMgH9H+2Bz3gIKOaoYi1j+5XV9N4YzQJ2YoVllSyzdpqH
ZHBSC/HFFCLdx/UNwWx+QIPcMwWarg5Ory8cRlhU6u4fOzjXCWYrhf0JcS3r8rmN
oRgL0wwM4zeIRTVCXe937IBksEuwLBPslVR/RyI8G+VXjDrhRR+cAAHxp1r+7YqO
a/kynMvwpbmhKV09ZfpVWPI2v1ec5i57yuWadLeBJLH0smolk2ZyX5d7lgxkIeqw
trcsNoVVNAxyd9UDvNs6Bc0evstbzMLjzswshQ+ysdmCqQHzhHp3TxlR4sOpzYp9
174y19nPHP1W7L5LRLPAy+0lstRSS6aYM9/0rUNCnJqhCttLZP8gfTAP4X8XGWTb
10qVJ1/ij+vvSRsSyej9LdpU/CAcMT2XsRN/gHXm9SKHxsw389ZUgWtJR6BOduFX
HSDDs1SdZENtDaG6xL9mrY+u264zK+6bsp2+fpAs7Fr3NApzHUaDmdSVpYk0KjSk
t2/JOKqh2zhOp1bG07eJ7foRo6hLsfgcnloWVIPHeKCQ4uTAabk3cbbt08ajDn3L
6JK4bvCIgfffbJsWLz/REQFwU1TCYQiF2gywfWoHuqB1F5mpyyFMpPLqfvxMGh81
3NNMO/KkUHPUAiGuIrF6RFjkHRUkMYlmzi0v6b04i+e/6aLxH1bDjBgBZZaDfyqS
NQGUvRdOinY1TDg9WO+FYbWqt+aKvg/sYttFmhR63+1UMaSyIoE7LI9ex2RdA2oT
yOT7bnrLWrT8TLamBvvovg2/d8gxeiiR/xcJupFZFOFZErtmtx+ZbyMpnpQTnUfm
hI7xbT62dYElqLZQb/WIC5hZN2+nU3R5h7sjEFSEcuRNSfDyJYXIw1QkkszMrP5p
9cIZjtSURXMP9mF8dFEO8oUJxd2cpQazkQhuSLrk0qKi3Cl343xLfjgJP7mQYzCN
wohhPJothQHYQK/CXv2Xg8ZkuwdcYzYF/2uM1xIsJEPQSgXjPyN+qi0IX4GbAY/R
eodnRvF6h7NMbn4pVjpIPWcVDm/PiSgo8hn39AXodJftAJVwqR5FQsxDeX6Dzw3N
AJc9Ra+5MywGWBbqgnQAO5bgsCAkMBXrolEKb+HUJo3tdGnVgpfJAutBfPFxTTTW
Fgn1DUpFWqbEzjgTC3ty8/67XHeL6jtzXrueCDRh38QJ8VOvRoqW16o2fG6DIQ1c
61zgPRF5mWHt06RKfJrVdh6UWo19mzSlVSBi99VzL8WrD8yvsKElEgPO0vSxpVvZ
gh1dm4NwMu+WnRxsTAZgr/g20CrY5S5Zh8H9kRkW3VpDpjxCEA78RX6wI1kcyw7I
vlzXvBoFn63DnFOKdVZ2LK8xEirWZ8mISyI9omIMlGfhhXl0Dug8aavnwZoNmKzl
Rc7PZcvFpTXHVXZQyoU+cnEW4k8bORIWyW+GirRJWwCtE2Unl3h0LRb0gUVSJSNh
C4psetBiViuxOQZXnQSbU3v+JxuR4hZKRq+4iStTVhe4CpWu8QD50eXNXzCSV1bG
oxCmU49iBQ0+7HYX+yRkK5UY0cQO14OF5uU/G7vS+YM72jytUdAMIOn/tM3ZYUJw
YpWaooSrpzpkUb42GeLpSUTQ7O20+yPUMtkNlmnA19TsKBMg44botb1vuMgIKpkB
FafjwGgKyrg5q3abHiThNsdEt5Ei1sUzXLDlEwlIbA7Lq4gPpLuZ2UyRFsPIm4h8
YJo17botVZN065R3WCqZfFw24CkwRkCIcWXPuNxdXGSfKhVs79iUs8anz1WOLNXx
TM/hdCq4OPWAMGPE2VXxs0imAztEOZUApOTRTm6I75tsKZHJSS44f+bTWxIfYTEu
O93MCk04mZjHLgtFlQGkFLice1TiVEUxXjEHWcZY2z5HP8xQ7e5s/Kf6PIAxRB22
l8ajpSaYc4CFIw31nrLbaUNiPvfn3EO124+v4QpxTGA=
`pragma protect end_protected
