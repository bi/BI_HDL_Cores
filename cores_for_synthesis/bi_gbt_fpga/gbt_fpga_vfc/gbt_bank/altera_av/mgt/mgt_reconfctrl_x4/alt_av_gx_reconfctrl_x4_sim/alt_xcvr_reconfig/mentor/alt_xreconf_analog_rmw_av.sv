// Copyright (C) 2016 Intel Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Intel and is being provided
// in accordance with and subject to the protections of the
// applicable Intel Program License Subscription Agreement
// which governs its use and disclosure. Your use of Intel
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Intel Program License Subscription
// Agreement, the Intel Quartus Prime License Agreement, the Intel
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Intel and sold
// by Intel or its authorized distributors. Please refer to the
// applicable agreement for further details. Intel products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Intel assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.1
// ALTERA_TIMESTAMP:Tue Oct 25 01:50:49 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
Mb0QXAJ+0/gBFsBcZm6cK8/RVj6WtwKBxrjUXpN9DF0j0r1H95Ww6Wr1mxObNFuB
d8+PD1nnvB68FROZ7TLp5Pb9Y+n8oSBDTNVvL3+bhkvTwf2kA20foDDOD+OHd0MB
acwLG+BLW8VqBtRfaq5Cbd0/Ezr1jYo4KdnTB2xGolA=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3312)
svrvKkXGQVd/o3+tXbh5Lo+7O3K3T3KgwmgiHe1pCQGgJaRAY5NLuFH3WHxeQNHO
hC1Fuqx5WO5HxS+zEPDKsSinFGOKca+AI1guOQoYCJvHHB1VCfsig45QkdL8Y73H
0sw/DEwJXZr7HZWsEOGIrJNq4Lhe0eZJJatz4zymQDuNAmNs6e7n9JVXoNX7S+xa
TxOksVI6CguqGQ4T6G6d7J8ge1gJ6T6yyhr/cDX2FjcBI1bkVQ4JJmmRoTpEGiW/
kwB7hbRK1faduq1Ayq/PlsGsuEyKy4zs20Lm3yCmwGY1CWoF9L5fV5bCxsISra1F
ScpK292/v0R0FwGQBBzit5X+/0icBldFXzSTjvsJ1m/Q7GbeybCJIsXO9tiize/k
Va0CjBPxvqPaGGLCQpvrBgtFhTg1Df4gXr1J+7/ogdMJA+PBwAhLGBcHqVY5YhOm
QoJF1+usQfVzpQcOG8GKXWjrT8MpzBY3sbd4D+AjPpCbQQ1cMOP2kCccpfTq6bkw
reBeVQJoeAy3KJBj3ZBaS5Wz7BrMxdbFxqrbPTWgZpAwXVuuXhQk1TDtg5C4EEMP
2hkQ+cf6GrJBXrYuVfPsX8mlIT67ImwmQYcm0eMxIgvEJraCuewUiGS6UASqjZKk
6njWRSRQRlqjJV9M8O8dQHMWtO82Sok4loWwu7yEQRSk3Td9OYV3x/N5wZWRYVwm
ZrKOHzQvrtk83dDmk8GhhnmEdzaz3dvV0AhBOpWn5ZB8qKxWwrKzSK+MZe56Adf8
fNkedPa4rVv5jcsWwD5RsbfYGn740Rvw6X8xTAVKbAQzwbtO2pAG8gWxoOxXs9FJ
1TtmuSitqXBNuVxK5lKiu6jutBc+YCQ3XBeKTP1vZ5SBYZLSou3n/ByKQHe+qxbZ
rU4vti7/a8KMZ6LsPm3E0todxuG+ocAGPAUoLg0BqGYfradySR9RBD6zTpvUjmEd
t4kUfahM5RuUExUrZ2t5fo6Z4Fdl0hN27zFUDe6cEafEvZMGJ8YbXz4HsJ1j9EZO
8BPGhpBShvX8yTIibG2zMDQlpTzfj7P2yinVl/m5bV3xHB+DqvTvM4aRNLkd6JFW
KkcsPY6mgE5JOClDI6N3Ryo2roKnnVIDgB+vJDp3OuuJPXQ+AcPzF9Pst5M0AoNR
XnJuyvKppFCZKpyhIND2CtL7Vk27RrLDRxmv0glUjm4+q3IgyVVfFjGaCY/oMzIp
9vqlfivHglgoc+5hurQLAPMLgnyylFxS3RWruqbx+M2m1/TswjRUqg3i7MwTiRwT
wCeUPmMvRNzIxaUhg6dwQW5wE9INEa6P2+bpKHft32gtBBuZQuSy+KD39lnRxoxl
Nw8T22DQt4nFIQtkR7gQ1dEEQ2qIYP7+ekewTsN/tD+p39Hm8Dmi1nnOfBtP37TL
POtewjBTBKNpON8dPHKbh8rXrBdvv0k7Vf5UHm2sjrRdoM+nKR4h5ydJ9LmNZksi
H49yTJy5FnIXKPG7oOkf30h42t+EaExNC4jIf0HH8U51tD6Z5q0b2Fhcr9HdjGDu
KJFZCiNiDFQjuQw3GevHi4euaG/fQuH2N2qhpP6Dw8WA/0YIPlJuReCBwwTSKH/W
KBgCDkEyNHOqRrbbhLH3PZOji2Xj1lr8cVzObtM41nGPDs0Sy7YCsBxbOgF502R+
V1zbqMBSL4dTVaXNoNIkBc30ouEtojuz1vpsD8r6cu4qpqW8tJa/JpW0IEqERcYD
YFZ/UtMQzoEvbkzazFi5TOTG36dXiq8PQp/6C/oVGwF6zDBacEtHpfZdW70eXxmD
+a7veiWjBkhydFAGdlr1lL0C4U+0G1SdbyTe+qc1PTQnkybCL7VzjJPXdeeMUjiY
L6/v0OY8cw3pGWPXY2XvC3WhRle8elX2sPBb3KcrGWr27fRT6BIY5GiKP9fS9HUW
a8WjZA+ek9GL6wKlCrglSdeNA3jToAwIQsZH7a6GgK7yNS7LEPaiJFmqidcRtsH8
zyETfKjBnA7ENL+Sqkb9J4yU+qLlJZf+d2HOOGMZQhp4E9htg43uymuvOK8RaXn9
zVQteD1IzcDtEbPgO5NV7XsauEc4qC4oBtLukZfkqvvr/rUP2iBdGcd/6wXdbqsm
Uel9rczopVqZVtJ2Y5UGndbQJArW48/zsXPo9CVo6etDVOyQT0/bFijwLWh5pnFn
+TSAXJ7LyGeFXt3o06PeHGU2erqB4KB97iSP9Qp30tIfJZzE0AZLjhDxqTiVhJY5
V6xqAve7wpjJL8xA9zY1qrOLjfNzt8XNVD6sLkrveFDOra/aOgbzb5SVgzmVy6NI
cM66IzFzize5uD4k5HPV8YiBmXOvT28WBp6YDlzPF9FnBJ8ntg9TGBlLmxOh3322
nJdcqj7ZaM5+4Mx4cqumTlQJKnIFAzGqVr+EJjZATBcOHT+CHNWM84XemtIkv2B4
0M/mDIeo/LGI4mSzuCjsJyYCJznOU3i0F+rQSJphaHUdHdGZnwwNb3UUN8VJJSGl
oPVD6onw2dT9Lw25u+GJ3xyUywJtkQCK7zl+rdfXZID2GOvgxDalIa1bjdd6egjl
TRgKH8qZwXQpvUX+9D0fCnEH/nMU+aRtYLwwertBFXGBisRlZVINuP684OMFcX8R
faQM80Xc2ab/iHGQfaeXiIuND+W3d+LQ0IQqbAqEKwQ9/AdjDB8OxGvPPkimy53d
KDqQe/4GkjPIref1l20Ddu7tdIw547ET6BX9LoXkItVR1+9TbViOnpc9ra0GM1HP
dQK8i8PzALoEYOXSr0fo7yJm+knceQHb6aW5JKXtx1nIfB/2KAkwKDTQeuUldAzS
eH2pnjpQSbYY//kGDC5KrxnY0XCbuzD3c7QF8ftpR9d8rC8ReK3JuvbD6nvBSvS5
XPKDdiL0kwVHMV+9n676dCxdnH7o0WTCD1vIHL2Oi7EcX6wVlByTeljukMyQ34IU
RLNE/AWNArD+u4Gn0MtJ3KYDumgdWVhhUChyKRfFc9j02UmRypuF2Dd9rDrl1A+e
UYVpwICEAq2SNjFL5iRoI98bS2ITzq7zEQ8qfiG1L2KcaijNUaX0lnGXEezH7e9I
FJYu5Z5w22mSdBhgAnyBOJ4AWcc/t8RcwwXc7IS6dTSGi0pDJ0Ib4knJiJeNBweB
h81TVuF+dHZgm4EPikTk7sPRDuUnMukWLabDYJiuPkFx+4d2ZYSJO9aTDU/TznMH
zVk/KztAvZMlg7381vYDrxyBSUTvfuowmKmmdE9r/X+2TwWpMBk5jbNzNWvOIjU+
kIV2QMKPOADlsNCU7LsKsn0x93VLEdzfwZAjg7wWmkT7VJP1mMs/LZEB2H+qoA0U
HPI0rTQmJyhLzj9v9Li/zLRmDMXv/dfpWlitCWpTt3/0DKS3uFX1aNaJiLIoDsDr
JT21PkRHNNQnTpgqMrePXQQPdge6yQ8nRQ7MUWNe6sk56KeiUk3CaeHYHvltaAM7
oZeVv9kFklsjcSFS14CnZhUT6FcRuhjhbdF+0dUkqSJCWfl74XPtaU2GEtfgH/BI
ca29wDe9USUFygQnc7hLY5R0nVf2xwtwXUyD0oBovWo4cvRnDAkwvxL9KZy6a7vT
YqQC37hMkHUGVWEb7kfvpy8wvIKIvOKALo/cZFdB+zL4XbtBo6PwTNsH7HJZsC/x
ajfqenvjqOsbAT3DN9u3bTai9NfRKnlEEdNKpuBPIoJv6MBIQsIZzBu6ponnyqA3
SX9skA2R/TNkdLQ94kpoQ+Hi5MJHTaeX4BWGYQGtsNw4jrp4+G5aJ5n4l/2qUJnQ
+1nTcgzP9qwFR4714vpHE+pfO6KC6UUBI36hDUbEAG7c0zig5jJjdxgK/DcqsxF8
0m3AXo17BxnGpzIL7vJ9+BDH5UzMHpXQhlI6U5TcFPeySiewBfwE62YiJxJ4GL0e
otogx+uixtemVw9xI0+soSDt0kHV6IhrE4ddSRaS0FdHQgnLnCj1B8Q5IRajD9jg
r01dh8WaHJcpLP8Pka5vFf5Z2A9BNxvdohOwtgs6XPwUZkjXFv+VgPnm5MT6KCZ/
7yIEI+mgEhKbsh3SyFuzTlkbxbtHGeHBngevQO5/Ghl9B8qUFZcy9u66JCebiz0T
og27hn9RaWO6koBwv/t6YMBugAFVawoSAWYWrRcvFv/JBe43qrDAA0+HWy0Ks/Z0
ZLLRAyDxG4QL8rHInLS2GJcNdBTf83iIymsvN5fHH2KRZHr2fwqaBIJ/KPw+xGbJ
4N3Zo1CG+hBf2Vkl3mGIM6mxjRFtx+Dy8d6AwBqwZDmxuRpHXpvZoDivLYXievLD
lf1qMdZtL4lp824x/pUoQ0SQaCnYBfEULjIiAlxEWl6idWNRjG06Y9HNFU3sRcEs
RM0adZ4+fOmAX4cFAKKwMPPhmFpTniHhUPlIKm8ElhMlJiqNIwE+QjQ8VmJZiieK
`pragma protect end_protected
