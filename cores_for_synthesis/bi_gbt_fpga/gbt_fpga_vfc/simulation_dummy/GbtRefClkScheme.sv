// simulation dummy

`timescale 1ns/100ps

module GbtRefClkScheme 
//====================================  Global Parameters  ===================================//
#(  parameter g_BstRefClkSchemeEn = (1'b0))
//========================================  I/O ports  =======================================//
(
    //==== WishBone Interface ====//
    
    input         Clk_ik,       
    input         Rst_ir,      
    input         Cyc_i,        
    input         Stb_i,        
    input         We_i,         
    input  [ 1:0] Adr_ib2,      
    input  [31:0] Dat_ib32,     
    output [31:0] Dat_ob32,    
    output        Ack_o,     

    //==== GBT MGT Reference Clock (PllRef (Si5338)) I2C Interface ====//
    
    inout         PllRefSda_ioz,
    inout         PllRefScl_iokz,

    //==== BST Control ====//
    
    // BST SFP:
    input         BstSfpPresent_i,
    input         BstSfpTxFault_i,
    input         BstSfpLos_i,
    input         StatusBstSfpTxDisable_i, 
    input         StatusBstSfpRateSelect_i,
    output logic  BstSfpTxDisable_o = 0, 
    output logic  BstSfpRateSelect_o = 0,
    // BST Decoder:
    input         BstOn_i,
    input         BunchClkFlag_i,
    input         SelBstClk_i,
    input         SyncTxFrameClkPll_i,
    output logic  LockedFreeRunningPll_oa = 0,
    output logic  LockedGbtTxFrameClkPll_oa = 0,

    //==== Input clocks ====//
    
    input         FreeRunningClk20MHz_ik,
    input         FreeRunningClk160MHzFb_ik,
    input         BstClk_ik,
    input         BstClkFb_ik,
    input         GbtRefClk120MHz_ik,
    
    //==== Output Clock ====//
    
    output logic  FreeRunningClk160MHzFb_ok = 0,
    output logic  BstClkFb_ok = 0,
    output logic  BstOrFreeRunningMuxClk_ok = 0,        
    output logic  TxFrameClk40Mhz_ok = 0,
    
    //==== GBT Reset ====//
    
    output logic  GbtReset_or = 0
    
);

    t_WbInterface #(.g_DataWidth(32), .g_AddressWidth(2)) Wb_t(Clk_ik, Rst_ir);

    assign Wb_t.Cyc = Cyc_i;
    assign Wb_t.Stb = Stb_i;
    assign Wb_t.Adr_b = Adr_ib2;
    assign Wb_t.Sel_b = (4)'(-1);
    assign Wb_t.We = We_i;
    assign Wb_t.DatMoSi_b = Dat_ib32;
    assign Ack_o = Wb_t.Ack;
    assign Dat_ob32 = Wb_t.DatMiSo_b;

    WbDummy c_WbDummy (
        .WbSlave_iot(Wb_t)
    );

endmodule