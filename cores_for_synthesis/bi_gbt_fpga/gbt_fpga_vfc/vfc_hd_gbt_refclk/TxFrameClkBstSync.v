//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: TxFrameClkBstSync.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 29/03/17      1.0          M. Barros Marin    First Module Definition.
//
// Language: Verilog 2005
//
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     Module for synchronising the PLL that generates TX_FRAMECLK with the Bunch Clock Flag of BST.
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module TxFrameClkBstSync
//====================================  Global Parameters  ===================================//
#(  parameter g_PllRstPulseWidth_b8 = 8'hFF) // Comment: 256 BST Clock cycles
//========================================  I/O ports  =======================================//
(
    //==== Reset & Clock ====//
    
    input             Reset_ir,
    input             BstClk_ik,
    
    //==== Control ====//
    
    input             Sync_i,
    input             BunchClkFlag_i,
    output reg        ResetTxFrameClkPll_orq
    
);
//=======================================  Declarations  =====================================//

reg  [1:0] Reset_rxb2;
reg        Reset_rq;
reg  [2:0] Sync_xb3;
wire       SyncNegEdge_r;
reg  [1:0] ResetPulseGen_rxb2;
reg        ResetPulseGen_rq;
reg  [7:0] ResetWidthCntr_c8;

//=======================================  User Logic  =======================================//

//==== Synchronizers & Edge Detectors ====//

// Reset Synchronizer:
always @(posedge BstClk_ik) Reset_rxb2 <= #1 {Reset_rxb2[0], Reset_ir};
always @(posedge BstClk_ik) Reset_rq   <= #1  Reset_rxb2[1];

// Sync Synchronizer & Negative Edge Detector:
always @(posedge BstClk_ik) Sync_xb3 <= #1 Reset_rq ? 3'h0 : {Sync_xb3[1:0], Sync_i};
assign SyncNegEdge_r = Sync_xb3[2] & ~Sync_xb3[1];

//==== Reset Pulse Generator ====//

always @(posedge BstClk_ik) ResetPulseGen_rxb2 <= #1 {ResetPulseGen_rxb2[0], (Reset_rq | SyncNegEdge_r)};
always @(posedge BstClk_ik) ResetPulseGen_rq   <= #1  ResetPulseGen_rxb2[1];

//==== TX_FRAMECLK Reset Pulse Generator ====//

always @(posedge BstClk_ik)
    if (ResetPulseGen_rq) begin
        ResetWidthCntr_c8          <= #1 8'h0;
        ResetTxFrameClkPll_orq     <= #1 1'b1;
    end else if (BunchClkFlag_i) begin
        if (ResetWidthCntr_c8 < g_PllRstPulseWidth_b8) begin
            ResetWidthCntr_c8      <= #1 ResetWidthCntr_c8 + 8'h1;
        end else begin
            ResetTxFrameClkPll_orq <= #1 1'b0;
        end
    end

endmodule