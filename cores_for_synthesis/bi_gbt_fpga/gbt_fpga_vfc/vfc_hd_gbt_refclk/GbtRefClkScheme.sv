//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: GbtRefClkScheme.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 15/10/17      1.1          M. Barros Marin    - Added GBT reset output
//                                                     - Converted to SystemVerilog
//                                                     - Cosmetic modifications
//     - 22/04/17      1.0          M. Barros Marin    Firs module definition
//
// Language: SystemVerilog 2013
//
// Targeted device:
//
//     - Vendor: Intel FPGA (Altera)
//     - Model:  Arria V GX
//
// Description:
//
//     Clocks scheme of the GBT-FPGA targeted to the VFC-HD
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module GbtRefClkScheme 
//====================================  Global Parameters  ===================================//
#(  parameter g_BstRefClkSchemeEn = (1'b0))
//========================================  I/O ports  =======================================//
(
    //==== WishBone Interface ====//
    
    input         Clk_ik,       
    input         Rst_ir,      
    input         Cyc_i,        
    input         Stb_i,        
    input         We_i,         
    input  [ 1:0] Adr_ib2,      
    input  [31:0] Dat_ib32,     
    output [31:0] Dat_ob32,    
    output        Ack_o,     

    //==== GBT MGT Reference Clock (PllRef (Si5338)) I2C Interface ====//
    
    inout         PllRefSda_ioz,
    inout         PllRefScl_iokz,

    //==== BST Control ====//
    
    // BST SFP:
    input         BstSfpPresent_i,
    input         BstSfpTxFault_i,
    input         BstSfpLos_i,
    input         StatusBstSfpTxDisable_i, 
    input         StatusBstSfpRateSelect_i,
    output        BstSfpTxDisable_o, 
    output        BstSfpRateSelect_o,
    // BST Decoder:
    input         BstOn_i,
    input         BunchClkFlag_i,
    input         SelBstClk_i,
    input         SyncTxFrameClkPll_i,
    output        LockedFreeRunningPll_oa,
    output        LockedGbtTxFrameClkPll_oa,

    //==== Input clocks ====//
    
    input         FreeRunningClk20MHz_ik,
    input         FreeRunningClk160MHzFb_ik,
    input         BstClk_ik,
    input         BstClkFb_ik,
    input         GbtRefClk120MHz_ik,
    
    //==== Output Clock ====//
    
    output        FreeRunningClk160MHzFb_ok,
    output        BstClkFb_ok,
    output        BstOrFreeRunningMuxClk_ok,        
    output        TxFrameClk40Mhz_ok,
    
    //==== GBT Reset ====//
    
    output        GbtReset_or
    
);

//=======================================  Declarations  =====================================//

// WishBone Address Decoder:
wire        WbStbPllRefI2cCtrl, WbAckPllRefI2cCtrl;
wire [31:0] WbDatPllRefI2cCtrl_b32;
wire        WbStbCtrlReg, WbAckCtrlReg;
wire [31:0] WbDatCtrlReg_b32;
wire        WbStbStatReg, WbAckStatReg;
wire [31:0] WbDatStatReg_b32;
// WishBone Control Register:
reg  [31:0] CtrlReg_qb32;
wire        SelBstClk;
wire        SyncTxFrameClkPll;
// WishBone Status Register:
wire [31:0] StatReg_b32;
reg  [31:0] StatReg_qb32;
reg  [ 1:0] LockedGbtTxFrameClkPll_xb2;
reg         LockedGbtTxFrameClkPll_q;
reg  [ 1:0] LockedFreeRunningPll_xb2;
reg         LockedFreeRunningPll_q;
reg  [ 1:0] BstAsRefClk_xb2;
reg         BstAsRefClk_q;
// BST Reference Clock Scheme Generation:
wire        FreeRunningClk160Mhz_k;
wire        BstOrFreeRunningMuxClk_k;
wire        ResetTxFrameClkPll_r;
// GBT Reset:
reg [  1:0] GbtReset_xb2;
reg         GbtReset_rq;

//=======================================  User Logic  =======================================//

//==== WishBone Address Decoder ====//

AddrDecoderWbGbtRefClkSch i_AddrDecoderWbGbtRefClkSch ( 
    .Clk_ik                (Clk_ik),
    .Adr_ib2               (Adr_ib2),
    .Stb_i                 (Stb_i),
    .Dat_oqb32             (Dat_ob32),
    .Ack_oq                (Ack_o),
    //--                    
    .DatPllRefI2cCtrl_ib32 (WbDatPllRefI2cCtrl_b32),
    .AckPllRefI2cCtrl_i    (WbAckPllRefI2cCtrl),
    .StbPllRefI2cCtrl_oq   (WbStbPllRefI2cCtrl),
    //--                    
    .DatCtrlReg_ib32       (WbDatCtrlReg_b32),
    .AckCtrlReg_i          (WbAckCtrlReg),
    .StbCtrlReg_oq         (WbStbCtrlReg), 
    //--                    
    .DatStatReg_ib32       (WbDatStatReg_b32),
    .AckStatReg_i          (WbAckStatReg),
    .StbStatReg_oq         (WbStbStatReg));

//==== GBT MGT Reference Clock (PllRef (Si5338)) I2C Control ====//

I2cMasterWb #(
    .g_CycleLenght (10'd160)) 
i_PllRefI2cCtrl (   
    .Clk_ik        (Clk_ik),
    .Rst_irq       (Rst_ir),
    .Cyc_i         (Cyc_i),
    .Stb_i         (WbStbPllRefI2cCtrl),
    .We_i          (We_i),
    .Adr_ib2       (Adr_ib2),
    .Dat_ib32      (Dat_ib32),
    .Dat_oab32     (WbDatPllRefI2cCtrl_b32),
    .Ack_oa        (WbAckPllRefI2cCtrl),
    .Scl_ioz       (PllRefScl_iokz),  
    .Sda_ioz       (PllRefSda_ioz));    

//==== WishBone Control Register ====//

always @(posedge Clk_ik) CtrlReg_qb32 <= #1 Rst_ir ? 32'h00000000 : ((Cyc_i && We_i && WbStbCtrlReg) ? Dat_ib32 : (CtrlReg_qb32 & 32'hFFFFFFFD));
assign WbDatCtrlReg_b32 = CtrlReg_qb32;
assign WbAckCtrlReg     = Cyc_i && WbStbCtrlReg;

assign SelBstClk         = CtrlReg_qb32[   0] || SelBstClk_i;
assign SyncTxFrameClkPll = CtrlReg_qb32[   1] || SyncTxFrameClkPll_i; // Comment: Auto-clear
//     Reserved            CtrlReg_qb32[31:2]

//==== WishBone Status Register ====//

always @(posedge Clk_ik) StatReg_qb32 <= #1 Rst_ir ? 32'h00000000 : (WbAckStatReg ? StatReg_qb32 : StatReg_b32);
assign WbDatStatReg_b32 = StatReg_qb32;
assign WbAckStatReg     = Cyc_i && WbStbStatReg;

assign StatReg_b32[    0] = BstSfpPresent_i;
assign StatReg_b32[    1] = BstSfpTxFault_i;
assign StatReg_b32[    2] = BstSfpLos_i;
assign StatReg_b32[    3] = StatusBstSfpTxDisable_i;  // Comment: '0' -> Enabled (default) | '1' -> Disabled
assign StatReg_b32[    4] = StatusBstSfpRateSelect_i; // Comment: '0' -> Low bandwidth     | '1' -> High bandwidth (default)
assign StatReg_b32[    5] = BstOn_i;
assign StatReg_b32[ 7: 6] =  2'h0;
assign StatReg_b32[    8] = LockedGbtTxFrameClkPll_q;
assign StatReg_b32[    9] = LockedFreeRunningPll_q;
assign StatReg_b32[   10] = BstAsRefClk_q;
assign StatReg_b32[   11] =  1'b0; // Reserved
assign StatReg_b32[31:12] = 20'h0;
    
always @(posedge Clk_ik) LockedGbtTxFrameClkPll_xb2 <= #1 {LockedGbtTxFrameClkPll_xb2[0], LockedGbtTxFrameClkPll_oa};
always @(posedge Clk_ik) LockedGbtTxFrameClkPll_q   <= #1  LockedGbtTxFrameClkPll_xb2[1];

always @(posedge Clk_ik) LockedFreeRunningPll_xb2 <= #1 {LockedFreeRunningPll_xb2[0], LockedFreeRunningPll_oa};
always @(posedge Clk_ik) LockedFreeRunningPll_q   <= #1  LockedFreeRunningPll_xb2[1];

always @(posedge Clk_ik) BstAsRefClk_xb2 <= #1 {BstAsRefClk_xb2[0], (BstOn_i & SelBstClk)};
always @(posedge Clk_ik) BstAsRefClk_q   <= #1  BstAsRefClk_xb2[1];    

//==== BST Reference Clock Scheme Generation ====//
    
generate if (g_BstRefClkSchemeEn == 1'b1) begin: Gen_BstRefClkScheme

    // Comment: Note!! The PllRef must be set with input clock at 160 MHz
    
    // Free-Running PllRef Source Clock Generation:
    FreeRunningPll i_FreeRunningPll (
        .refclk   (FreeRunningClk20MHz_ik),
        .rst      (Rst_ir),     
        .outclk_0 (FreeRunningClk160Mhz_k),
        .locked   (LockedFreeRunningPll_oa));
        
    // Free-Running Clock FeedBack DDR Output:
    ObufDdr i_FreeRunningClkFbObufDdr (
        .datain_h (1'b1),
        .datain_l (1'b0),
        .outclock (FreeRunningClk160Mhz_k),    
        .dataout  (FreeRunningClk160MHzFb_ok));
    
    // BST Clock FeedBack DDR Output:
    ObufDdr i_BstClkFbObufDdr (
        .datain_h (1'b1),
        .datain_l (1'b0),
        .outclock (BstClk_ik),    
        .dataout  (BstClkFb_ok));
    
    // PllRef Source Clock Multiplexor:
    AltClkCtrl i_PllRefSrcMux (
        .inclk3x   (1'b0),  
        .inclk2x   (1'b0),  
        .inclk1x   (BstClkFb_ik),  
        .inclk0x   (FreeRunningClk160MHzFb_ik),  
        .clkselect ({1'b0, (BstOn_i & SelBstClk)}),
        .ena       (1'b1),
        .outclk    (BstOrFreeRunningMuxClk_k));
        
    // TX_FRAMECLK PLL/Bunch Clock Flag Synchronizer:
    TxFrameClkBstSync #(
        .g_PllRstPulseWidth_b8  (8'hFF))
    i_TxFrameClkBstSync (
        .Reset_ir               (Rst_ir),
        .BstClk_ik              (BstClk_ik),
        .Sync_i                 (SyncTxFrameClkPll),
        .BunchClkFlag_i         (BunchClkFlag_i),
        .ResetTxFrameClkPll_orq (ResetTxFrameClkPll_r));        

end else begin: Gen_NoBstRefClkScheme
    // Comment: Note!! The PllRef must be set with input clock at 20 MHz
    assign LockedFreeRunningPll_oa  = 1'b0;
    assign BstOrFreeRunningMuxClk_k = FreeRunningClk20MHz_ik; 
    assign ResetTxFrameClkPll_r     = Rst_ir;
end endgenerate

assign BstSfpTxDisable_o  = 1'b1; // Comment: The BST SFP TX is set to DISABLED to switch off the laser, since it is only used as RX
assign BstSfpRateSelect_o = 1'b0; // Comment: Line rate select to low bandwidth (Line Rate < 4.25 Gbps)

// PLLRef Source Clock DDR Output:
ObufDdr i_PllSourceMuxOutObufDdr (
    .datain_h (1'b1),
    .datain_l (1'b0),
    .outclock (BstOrFreeRunningMuxClk_k),    
    .dataout  (BstOrFreeRunningMuxClk_ok));
    
// GBT-FPGA TX_FRAMECLK (40 MHz) generation:
gbt_tx_frameclk_pll i_GbtTxFrameClkPll (
    .refclk   (GbtRefClk120MHz_ik),
    .rst      (ResetTxFrameClkPll_r),     
    .outclk_0 (TxFrameClk40Mhz_ok),
    .locked   (LockedGbtTxFrameClkPll_oa));
    
//==== GBT Reset ====//
    
always @(posedge TxFrameClk40Mhz_ok) GbtReset_xb2 <= #1 {GbtReset_xb2[0], ~LockedGbtTxFrameClkPll_oa};
always @(posedge TxFrameClk40Mhz_ok) GbtReset_rq  <= #1  GbtReset_xb2[1];        
assign GbtReset_or = GbtReset_rq;
    
endmodule