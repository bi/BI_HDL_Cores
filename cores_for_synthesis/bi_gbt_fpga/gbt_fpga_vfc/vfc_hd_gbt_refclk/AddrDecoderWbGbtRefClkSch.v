//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: AddrDecoderWbGbtRefClkSch.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 21/04/17      1.0          M. Barros Marin    First module definition
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     WishBone Address Decoder & CrossBar of the VFC-HD GBT-FPGA Reference Clock Scheme
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module AddrDecoderWbGbtRefClkSch
//========================================  I/O ports  =======================================//
( 
    //==== WishBone Interface ====//

    input             Clk_ik,
    input      [ 1:0] Adr_ib2,
    input             Stb_i,
    output reg [31:0] Dat_oqb32,
    output reg        Ack_oq,

    //==== WishBone Slaves Interface ====//

    input      [31:0] DatPllRefI2cCtrl_ib32,
    input             AckPllRefI2cCtrl_i,
    output reg        StbPllRefI2cCtrl_oq,    
    input      [31:0] DatCtrlReg_ib32,
    input             AckCtrlReg_i,
    output reg        StbCtrlReg_oq, 
    input      [31:0] DatStatReg_ib32,
    input             AckStatReg_i,
    output reg        StbStatReg_oq
    
); 

//=======================================  Declarations  =====================================//

reg [7:0]  SelectedModule_b8;

localparam c_SelNothing       = 8'd0,
           c_SelPllRefI2cCtrl = 8'd1, 
           c_SelCtrlReg       = 8'd2,
           c_SelStatReg       = 8'd3; 

//=======================================  User Logic  =======================================//
 
always @* 
    casez(Adr_ib2)
        2'b0?:   SelectedModule_b8 = c_SelPllRefI2cCtrl; // FROM 0 TO 1 (WB) == FROM 0 TO 4 (VME) <- 2 regs ( 8B)
        2'b10:   SelectedModule_b8 = c_SelCtrlReg;       // FROM 2 TO 2 (WB) == FROM 8 TO 8 (VME) <- 1 regs ( 4B)    
        2'b11:   SelectedModule_b8 = c_SelStatReg;       // FROM 3 TO 3 (WB) == FROM C TO C (VME) <- 1 regs ( 4B)
        default: SelectedModule_b8 = c_SelNothing;
    endcase 
            
always @(posedge Clk_ik) begin
    Ack_oq                        <= #1  1'b0;
    Dat_oqb32                     <= #1 32'h0;
    StbPllRefI2cCtrl_oq           <= #1  1'b0;
    StbCtrlReg_oq                 <= #1  1'b0;
    StbStatReg_oq                 <= #1  1'b0;
    case(SelectedModule_b8)    
        c_SelPllRefI2cCtrl: begin
            StbPllRefI2cCtrl_oq   <= #1 Stb_i;
            Dat_oqb32             <= #1 DatPllRefI2cCtrl_ib32;
            Ack_oq                <= #1 AckPllRefI2cCtrl_i;        
        end
        c_SelCtrlReg: begin
            StbCtrlReg_oq         <= #1 Stb_i;
            Dat_oqb32             <= #1 DatCtrlReg_ib32;
            Ack_oq                <= #1 AckCtrlReg_i;
        end        
        c_SelStatReg: begin
            StbStatReg_oq         <= #1 Stb_i;
            Dat_oqb32             <= #1 DatStatReg_ib32;
            Ack_oq                <= #1 AckStatReg_i;        
        end
    endcase      
end

endmodule