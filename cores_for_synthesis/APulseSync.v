//----------------------------------------------------------------------
// Title      : Asynchronous Pulse Synchroniser
// Project    : Generic
//----------------------------------------------------------------------
// File       : APulseSync.v
// Author     : A. Boccardi
// Company    : CERN BE-BI-QP
// Created    : 2017-03-29
// Last update: 2017-03-29
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
// Synchronise an asynchronous pulse of arbitrary duration into a 
// synchronous one clock period pulse.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module APulseSync (
    input      APulse_iap,
    input      Clk_ik,
    output reg Pulse_op
);

reg APulse_x = 0;
reg [2:0] PulseX_d3 = 0;

always @(posedge APulse_iap) APulse_x <= ~APulse_x;

always @(posedge Clk_ik) PulseX_d3 <= {PulseX_d3[1:0], APulse_x};
always @(posedge Clk_ik) Pulse_op  <= ^PulseX_d3[2:1];

endmodule
