-------------------------------------------------------------------------------
-- Title      : Heart beat glow generator
-- Project    : FMC DEL 1ns 2cha (FFPG)
-- URL        : http://www.ohwr.org/projects/fmc-del-1ns-2cha
-------------------------------------------------------------------------------
-- File       : HeartBeatGlow.vhd
-- Author(s)  : Jan Pospisil <j.pospisil@cern.ch>
-- Company    : CERN (BE-BI-QP)
-- Created    : 2016-07-28
-- Last update: 2016-12-14
-- Standard   : VHDL2008
-------------------------------------------------------------------------------
-- Description: Generates "glowing" signal, intended for LED signalization.
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN (BE-BI-QP)
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
-------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author        Comments
-- 2016-08-24  1.0      Jan Pospisil
-- 2016-12-14  1.1      Jan Pospisil  Changed name
--                                      HeartBeat.vhd -> HeartBeatGlow.vhd
-- 2016-12-14  1.1      Jan Pospisil  Added g_Phase
------------------------------------------------------------------------------- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity HeartBeatGlow is
    generic (
        g_ClkFrequency: positive; -- input clock frequency in Hz
        g_Phase: real := 0.0      -- starting offset from range <0, 1)
    );
    port (
        Clk_ik: in std_logic;
        Reset_ir: in std_logic;
        HeartBeat_o: out std_logic := '0'
    );
end entity;


architecture syn of HeartBeatGlow is

    --! computes least possible width of the vector to store value X
    -- http://stackoverflow.com/a/12751341/615627
    function f_log2(x: natural) return natural is
        variable i: natural;
    begin
        i := 0;
        while (2**i < x) loop
            i := i + 1;
        end loop;
        return i;
    end function;
    
    function f_min(a, b: integer) return integer is
    begin
        if a < b then
            return a;
        else
            return b;
        end if;
    end function;
    
    function f_max(a, b: integer) return integer is
    begin
        if a > b then
            return a;
        else
            return b;
        end if;
    end function;
    
    function f_CalculatePwmCountDownStartValue(Phase: real) return std_logic is
    begin
        if Phase >= 0.5 then
            return '1';
        else
            return '0';
        end if;
    end function;
    
    function f_CalculatePwmValueStartValue(Phase: real; ResultWidth: positive) return unsigned is
        variable PhaseInner: real := Phase;
    begin
        PhaseInner := PhaseInner * 2.0;
        if PhaseInner >= 1.0 then
            PhaseInner := PhaseInner - 1.0;
            return to_unsigned(integer((2.0**ResultWidth-1.0) - PhaseInner * (2.0**ResultWidth)), ResultWidth);
        else
            return to_unsigned(integer(PhaseInner * (2.0**ResultWidth)), ResultWidth);
        end if;
    end function;

    constant c_ClkFreqWidth: positive := f_log2(g_ClkFrequency);
    constant c_PwmWidth: positive := f_max(f_min(c_ClkFreqWidth, 12), 4);
    constant c_DividerWidth: natural := c_ClkFreqWidth - c_PwmWidth;
    constant c_DividerLimit: natural := g_ClkFrequency / (2**c_PwmWidth);
    -- constant c_PwmValueStartValue_b: unsigned(c_PwmWidth-1 downto 0) := to_unsigned(2000, c_PwmWidth);
    constant c_PwmValueStartValue_b: unsigned(c_PwmWidth-1 downto 0) := f_CalculatePwmValueStartValue(g_Phase, c_PwmWidth);
    constant c_PwmCountDownStartValue: std_logic := f_CalculatePwmCountDownStartValue(g_Phase);

    signal PwmCountDown: std_logic := c_PwmCountDownStartValue;
    signal UpdatePwmValue, PwmCounterOverflow: std_logic;
    signal PwmCounter_b, PwmValue_bd: unsigned(c_PwmWidth-1 downto 0) := (others => '0');
    signal PwmValue_b: unsigned(c_PwmWidth-1 downto 0) := c_PwmValueStartValue_b;
    
begin

    gNeedDivider: if c_DividerWidth > 0 generate
        cDivider: entity work.Counter(syn)
            generic map (
                g_Width => c_DividerWidth,
                g_Limit => c_DividerLimit
            )
            port map (
                Clk_ik => Clk_ik,
                Reset_ir => Reset_ir,
                Enable_i => '1',
                Set_i => '0',
                SetValue_ib => (others => '0'),
                Overflow_o => UpdatePwmValue,
                Value_ob => open
            );
    end generate;
    
    gDoesntNeedDivider: if c_DividerWidth = 0 generate
        UpdatePwmValue <= '1';
    end generate;
    
    pPwmValue: process (Clk_ik) begin
        if rising_edge(Clk_ik) then
            if Reset_ir = '1' then
                PwmValue_b <= c_PwmValueStartValue_b;
                PwmCountDown <= c_PwmCountDownStartValue;
            elsif UpdatePwmValue = '1' then
                if PwmCountDown = '1' then
                    if PwmValue_b = to_unsigned(1, c_PwmWidth) then
                        PwmCountDown <= '0';
                        PwmValue_b <= PwmValue_b + 1;
                    else
                        PwmValue_b <= PwmValue_b - 1;
                    end if;
                else
                    if PwmValue_b = unsigned(std_logic_vector(to_signed(-1, c_PwmWidth))) then
                        PwmCountDown <= '1';
                        PwmValue_b <= PwmValue_b - 1;
                    else
                        PwmValue_b <= PwmValue_b + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;
    
    cPwmCounter: entity work.Counter(syn)
        generic map (
            g_Width => c_PwmWidth
        )
        port map (
            Clk_ik => Clk_ik,
            Reset_ir => Reset_ir,
            Enable_i => '1',
            Set_i => '0',
            SetValue_ib => (others => '0'),
            Overflow_o => PwmCounterOverflow,
            Value_ob => PwmCounter_b
        );
    
    pPwmValueReg: process (Clk_ik) is begin
        if rising_edge(Clk_ik) then
            if PwmCounterOverflow = '1' then
                PwmValue_bd <= PwmValue_b;
            end if;
        end if;
    end process;

    pHearBeat: process (Clk_ik) begin
        if rising_edge(Clk_ik) then
            if Reset_ir = '1' then
                HeartBeat_o <= '0';
            else
                if PwmCounter_b = 0 then
                    HeartBeat_o <= '1';
                end if;
                if PwmCounter_b = PwmValue_bd then
                    HeartBeat_o <= '0';
                end if;
            end if;
        end if;
    end process;

end architecture;
