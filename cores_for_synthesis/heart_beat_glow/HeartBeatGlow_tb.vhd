-------------------------------------------------------------------------------
-- Title      : Testbench for HeartBeatGlow.vhd
-- Project    : FMC DEL 1ns 2cha (FFPG)
-- URL        : http://www.ohwr.org/projects/fmc-del-1ns-2cha
-------------------------------------------------------------------------------
-- File       : HeartBeatGlow_tb.vhd
-- Author(s)  : Jan Pospisil <j.pospisil@cern.ch>
-- Company    : CERN (BE-BI-QP)
-- Created    : 2016-07-28
-- Last update: 2016-12-14
-- Standard   : VHDL2008
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 CERN (BE-BI-QP)
-------------------------------------------------------------------------------
-- GNU LESSER GENERAL PUBLIC LICENSE
-------------------------------------------------------------------------------
-- This source file is free software; you can redistribute it and/or modify it
-- under the terms of the GNU Lesser General Public License as published by the
-- Free Software Foundation; either version 2.1 of the License, or (at your
-- option) any later version. This source is distributed in the hope that it
-- will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
-- of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU Lesser General Public License for more details. You should have
-- received a copy of the GNU Lesser General Public License along with this
-- source; if not, download it from http://www.gnu.org/licenses/lgpl-2.1.html
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author
-- 2016-08-24  1.0      Jan Pospisil
-- 2016-12-14  1.1      Jan Pospisil  Changed name
--                                     HeartBeat_tb.vhd -> HeartBeatGlow_tb.vhd
-- 2016-12-14  1.1      Jan Pospisil  Added g_Phase
------------------------------------------------------------------------------- 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity HeartBeatGlow_tb is
end entity;

architecture testbench of HeartBeatGlow_tb is

    constant c_ClkFrequency: positive := 300_000;
    constant c_ClkPeriod: time := (1 sec)/real(c_ClkFrequency);
    
    procedure f_Tick(ticks: in natural) is begin
        wait for ticks * c_ClkPeriod;
    end procedure;

    signal Clk_ik, Reset_ir, HeartBeat_o: std_logic;
    signal HeartBeatPwmValue: time := 0 ns;

begin

    cDUT: entity work.HeartBeatGlow(syn)
        generic map (
            g_ClkFrequency => c_ClkFrequency,
            g_Phase => 0.2
        )
        port map (
            Clk_ik,
            Reset_ir,
            HeartBeat_o
        );

    pClk: process is begin
        Clk_ik <= '0';
        wait for c_ClkPeriod/2;
        Clk_ik <= '1';
        wait for c_ClkPeriod/2;
    end process;
    
    pTest: process is begin
        Reset_ir <= '1';
        f_Tick(5);
        Reset_ir <= '0';
        f_Tick(1e6);

        assert false report "NONE. End of simulation." severity failure;
        wait;
    end process pTest;
    
    pRC: process (HeartBeat_o) is
        variable LastOn: time := 0 ns;
    begin
        if rising_edge(HeartBeat_o) then
            LastOn := now;
        else
            HeartBeatPwmValue <= now - LastOn;
        end if;
    end process;

end architecture;
