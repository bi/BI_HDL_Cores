//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: AvalonStSplitter.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  19.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      Splitter for Avalon-ST bus
//      
//      Data comming from one upstream port is splitted into multiple downstream ports. Controlling
//      signals are combined back to upstream port.
//      
//      !! Can be used only for interfaces in the same clock domain and with the same data width !!
//
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

// TODO: investigate why this warning is issued
// altera message_off 10665
module AvalonStSplitter #(
    parameter int g_DownPorts
) (
    t_AvalonStInterface UpPort_iot,
    t_AvalonStInterface DownPort_iot [0:g_DownPorts-1]
);

    logic [g_DownPorts-1:0] DownPortReady, DownPortError;

    genvar i;
    generate
        for (i = 0; i < g_DownPorts; i++) begin: generateForDownPorts
            // data, validity
            assign DownPort_iot[i].Data_b = UpPort_iot.Data_b;
            assign DownPort_iot[i].Valid = UpPort_iot.Valid;
            // control
            assign DownPortReady[i] = DownPort_iot[i].Ready;
            assign DownPortError[i] = DownPort_iot[i].Error;
        end
    endgenerate

    assign UpPort_iot.Ready = &DownPortReady;
    assign UpPort_iot.Error = |DownPortError;

endmodule
