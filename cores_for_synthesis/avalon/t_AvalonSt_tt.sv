// Description:
//
//      SV Interface for Avalon-MM bus
//      https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/manual/mnl_avalon_spec.pdf
//

interface t_AvalonSt_tt #(
    g_DataWidth
) (
    input logic Clk_k
);
    logic [g_DataWidth-1:0] DataMoSi_b;
    logic ValidMoSi;
    logic ReadyMiSo;
endinterface
