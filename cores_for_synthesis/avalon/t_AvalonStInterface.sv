//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: t_AvalonStInterface.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  19.10.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      SV Interface for Avalon-ST bus
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

interface t_AvalonStInterface #(
    g_DataWidth
) (
    input logic Clk_k
);
    logic [g_DataWidth-1:0] Data_b;
    logic Valid;
    logic Ready;
    logic Error;
endinterface
