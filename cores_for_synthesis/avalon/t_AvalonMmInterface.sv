// Description:
//
//      SV Interface for Avalon-MM bus
//      https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/manual/mnl_avalon_spec.pdf
//

interface t_AvalonMmInterface
    #(  parameter g_AddressWidth       ,
        parameter g_DataWidth          ,
        parameter g_BurstCountWidth = 1)

    (   input logic Clk_k  ,
        input logic Reset_r);

// Master to Slave
logic [g_AddressWidth-1    : 0] Address_b    ;
logic [(g_DataWidth/8)-1   : 0] ByteEnable   ;
logic                           Read         ;
logic                           Write        ;
logic [g_DataWidth-1       : 0] DataWrite_b  ;
// Slave to Master
logic [g_DataWidth-1       : 0] DataRead_b   ;
logic [g_BurstCountWidth-1 : 0] BurstCount_b ; // Optional: used in burst capable interfaces. Tie it to 'd1 if not used
logic                           WaitRequest  ; // Optional: tie it to 0 if not used
logic                           ReadDataValid; // Optional: used in pipeline and burst capable interfaces. Tie to 1 if not used

endinterface
