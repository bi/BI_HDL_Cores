#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Manoel Barros Marin (CERN BE-BI-QP) 14/12/2016
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Cleaning the transcript window:
.main clear

###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

vsim -gui -novopt work.tb_Bert
do waveforms.do
run -all

echo ""
echo ""