onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {BERT - PRBS31 x 8bits}
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/g_BusWidth
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/g_SelPrbs31
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/Clk_ik
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/Reset_ir
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/AlignPattern_i
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/SingleErrorInject_i
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/MultiErrorInject_i
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/RxDataAligned_i
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/Dv_o
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/TxData_ob
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/RxDataAligned_db2
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/SingleErrorInject_db3
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/MultiErrorInject_db3
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/c_PrbsSeed_b31
add wave -noupdate -group {Patter Generator - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x8bit/c_PrbsSeed_b7
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/g_BusWidth
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/g_SelPrbs31
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/g_Words2Sync
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/g_Words2lossOfSync
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/Clk_ik
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/Reset_ir
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/RxAligned_i
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/RxData_ib
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/CntrsReset_i
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/Synchronized_o
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/BitErrors_ob
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/WordsCntr_ocb64
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/ErrorCntr_ocb64
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/ErrorFlag_o
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/State
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/NextState
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/CntrsReset_db3
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/InitialDlyCntr_cb4
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/SeedReady_q
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/Words2SyncCntr_cb8
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/RxData_d6b
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/i
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/RxDataSr_qb
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/EnLfSr_q
add wave -noupdate -group {Patter Checker - PRBS31 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x8bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS7 x 8bits}
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x8bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/State
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/i
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 8bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x8bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS31 x 20bits}
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS31 x 20bits} /tb_Bert/i_GenPrbs31x20bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/State
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/i
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x20bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS7 x 20bits}
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS7 x 20bits} /tb_Bert/i_GenPrbs7x20bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/State
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/i
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 20bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x20bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS31 x 40bits}
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x40bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/State
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/i
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x40bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS7 x 40bits}
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x40bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/State
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/i
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 40bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x40bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS31 x 80bits}
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x80bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/State
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/i
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x80bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS7 x 80bits}
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x80bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/State
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/i
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 80bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x80bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS31 x 82bits}
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs31x82bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/State
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/i
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS31 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs31x82bit/l_InitialDly
add wave -noupdate -divider {BERT - PRBS7 x 82bits}
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/g_BusWidth
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/g_SelPrbs31
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/Clk_ik
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/Reset_ir
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/AlignPattern_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/SingleErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/MultiErrorInject_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/RxDataAligned_i
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/Dv_o
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/TxData_ob
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/RxDataAligned_db2
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/SingleErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/MultiErrorInject_db3
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/c_PrbsSeed_b31
add wave -noupdate -group {Pattern Generator - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_GenPrbs7x82bit/c_PrbsSeed_b7
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/g_BusWidth
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/g_SelPrbs31
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/g_Words2Sync
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/g_Words2lossOfSync
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/Clk_ik
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/Reset_ir
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/RxAligned_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/RxData_ib
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/CntrsReset_i
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -color Magenta -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/Synchronized_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/BitErrors_ob
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/WordsCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/ErrorCntr_ocb64
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/ErrorFlag_o
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/State
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/NextState
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/CntrsReset_db3
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/InitialDlyCntr_cb4
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/SeedReady_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/Words2SyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/LossOfSyncCntr_cb8
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/RxData_d6b
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/i
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/RxDataSr_qb
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/EnLfSr_q
add wave -noupdate -group {Pattern Checker - PRBS7 x 82bits} -radix hexadecimal /tb_Bert/i_ChkPrbs7x82bit/l_InitialDly
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {988300 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 312
configure wave -valuecolwidth 254
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {29425200 ps}
