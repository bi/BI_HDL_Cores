//============================================================================================//
//################################   Test Bench Information   ################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: tb_Bert.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 14/12/16      1.0          M. Barros Marin    First test bench definition
//
// Language: Verilog 2005
//
// Module Under Test:
//     
//     - BertPattGen.vhd
//     - BertPattChk.vhd
//
// Targeted device:
//
//     - Vendor: Agnostic
//     - Model:  Agnostic
//
// Description:
//
//     Test bench for simulation the BERT modules.
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module tb_Bert;
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

reg         Reset_rq;
reg         Clk_kq;
//--
reg         SingleErrorInject_q;
reg         MultiErrorInject_q;
reg         RxDataAligned_q;
reg         CntrsReset_q;
wire [ 7:0] Prbs31_b8;
wire [ 7:0] Prbs7_b8;
wire [19:0] Prbs31_b20;
wire [19:0] Prbs7_b20;
wire [39:0] Prbs31_b40;
wire [39:0] Prbs7_b40;
wire [79:0] Prbs31_b80;
wire [79:0] Prbs7_b80;
wire [81:0] Prbs31_b82;
wire [81:0] Prbs7_b82;

//=====================================  Status & Control  ====================================//

//==== Clocks generation ====//

always #4 Clk_kq = ~Clk_kq; // Comment: 8ns period

//==== Stimulus & Display ====//

initial begin
    Reset_rq            = 1'b1;
    Clk_kq              = 1'b1;
    SingleErrorInject_q = 1'b0;
    MultiErrorInject_q  = 1'b0;
    RxDataAligned_q     = 1'b0;
    CntrsReset_q        = 1'b0;
    //--
    $display($time, "-> Simulation Start");
    #1000
    @(posedge Clk_kq);
    Reset_rq            = 1'b0;
    $display($time, "-> Deassert Reset");
    #1000
    @(posedge Clk_kq);
    RxDataAligned_q     = 1'b1;
    $display($time, "-> Rx Data Aligned");
    #10000
    @(posedge Clk_kq);
    SingleErrorInject_q = 1'b1;
    @(posedge Clk_kq);
    SingleErrorInject_q = 1'b0;
    $display($time, "-> Single Error Inject");
    #1000
    @(posedge Clk_kq);
    MultiErrorInject_q  = 1'b1;
    @(posedge Clk_kq);
    MultiErrorInject_q  = 1'b0;
    $display($time, "-> Multi Error Inject");    
    #1000
    @(posedge Clk_kq);
    CntrsReset_q        = 1'b1;
    @(posedge Clk_kq);
    CntrsReset_q        = 1'b0;
    $display($time, "-> Error Counter Reset"); 
    #1000
    @(posedge Clk_kq);
    RxDataAligned_q     = 1'b0;
    $display($time, "-> Rx Data NOT Aligned");    
    repeat(1000)@(posedge Clk_kq);
    #5000
    $display($time, "-> Simulation Stop");
    $stop;
end

//==== DUT ====//

// PRBS31 x 8 bits:
BertPattGen #(
    .g_BusWidth          (8),
    .g_SelPrbs31         (1))
i_GenPrbs31x8bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (8'hCA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs31_b8));    
    
BertPattChk #(    
    .g_BusWidth          (8),
    .g_SelPrbs31         (1))
i_ChkPrbs31x8bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs31_b8),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());    
    
// PRBS7 x 8 bits:
BertPattGen #(
    .g_BusWidth          (8),
    .g_SelPrbs31         (0))
i_GenPrbs7x8bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (8'hCA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs7_b8));    
    
BertPattChk #(    
    .g_BusWidth          (8),
    .g_SelPrbs31         (0))
i_ChkPrbs7x8bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs7_b8),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());        
    
// PRBS31 x 20 bits:
BertPattGen #(
    .g_BusWidth          (20),
    .g_SelPrbs31         (1))
i_GenPrbs31x20bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (20'hCAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs31_b20));    
    
BertPattChk #(    
    .g_BusWidth          (20),
    .g_SelPrbs31         (1))
i_ChkPrbs31x20bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs31_b20),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());            
    
// PRBS7 x 20 bits:
BertPattGen #(
    .g_BusWidth          (20),
    .g_SelPrbs31         (0))
i_GenPrbs7x20bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (20'hCAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs7_b20));    
    
BertPattChk #(    
    .g_BusWidth          (20),
    .g_SelPrbs31         (0))
i_ChkPrbs7x20bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs7_b20),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());         
    
// PRBS31 x 40 bits:
BertPattGen #(
    .g_BusWidth          (40),
    .g_SelPrbs31         (1))
i_GenPrbs31x40bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (40'hCAAAAAAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs31_b40));    
    
BertPattChk #(    
    .g_BusWidth          (40),
    .g_SelPrbs31         (1))
i_ChkPrbs31x40bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs31_b40),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());       
    
// PRBS7 x 40 bits:
BertPattGen #(
    .g_BusWidth          (40),
    .g_SelPrbs31         (0))
i_GenPrbs7x40bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (40'hCAAAAAAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs7_b40));    
    
BertPattChk #(    
    .g_BusWidth          (40),
    .g_SelPrbs31         (0))
i_ChkPrbs7x40bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs7_b40),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());
    
// PRBS31 x 80 bits:
BertPattGen #(
    .g_BusWidth          (80),
    .g_SelPrbs31         (1))
i_GenPrbs31x80bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (80'hCAAAAAAAAAAAAAAAAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs31_b80));    
    
BertPattChk #(    
    .g_BusWidth          (80),
    .g_SelPrbs31         (1))
i_ChkPrbs31x80bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs31_b80),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());       
    
// PRBS7 x 80 bits:
BertPattGen #(
    .g_BusWidth          (80),
    .g_SelPrbs31         (0))
i_GenPrbs7x80bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (80'hCAAAAAAAAAAAAAAAAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs7_b80));    
    
BertPattChk #(    
    .g_BusWidth          (80),
    .g_SelPrbs31         (0))
i_ChkPrbs7x80bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs7_b80),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());    

// PRBS31 x 82 bits:
BertPattGen #(
    .g_BusWidth          (82),
    .g_SelPrbs31         (1))
i_GenPrbs31x82bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (82'h3AAAAAAAAAAAAAAAAAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs31_b82));    
    
BertPattChk #(    
    .g_BusWidth          (82),
    .g_SelPrbs31         (1))
i_ChkPrbs31x82bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs31_b82),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());       
    
// PRBS7 x 80 bits:
BertPattGen #(
    .g_BusWidth          (82),
    .g_SelPrbs31         (0))
i_GenPrbs7x82bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .AlignPattern_i      (82'h3AAAAAAAAAAAAAAAAAAAA),
    .SingleErrorInject_i (SingleErrorInject_q),
    .MultiErrorInject_i  (MultiErrorInject_q),
    .RxDataAligned_i     (RxDataAligned_q),
    .Dv_o                (),
    .TxData_ob           (Prbs7_b82));    
    
BertPattChk #(    
    .g_BusWidth          (82),
    .g_SelPrbs31         (0))
i_ChkPrbs7x82bit (
    .Clk_ik              (Clk_kq),
    .Reset_ir            (Reset_rq),
    .RxAligned_i         (RxDataAligned_q),
    .RxData_ib           (Prbs7_b82),
    .CntrsReset_i        (CntrsReset_q),
    .Synchronized_o      (),
    .BitErrors_ob        (),
    .WordsCntr_ocb64     (),
    .ErrorCntr_ocb64     (),
    .ErrorFlag_o         ());    
    
endmodule