-- M. Barros Marin, BE-BI-QP (CERN) - 14/12/16
-- (Based on a module from: P. Vichoudis)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BertPattGen is
    generic (
        g_BusWidth          : integer := 20;
        g_SelPrbs31         : integer range 0 to 1 := 1
    );
    port (
        Clk_ik              : in  std_logic;   
        Reset_ir            : in  std_logic;
        AlignPattern_i      : in  std_logic_vector(g_BusWidth-1 downto 0);
        SingleErrorInject_i : in  std_logic;
        MultiErrorInject_i  : in  std_logic;
        RxDataAligned_i     : in  std_logic;
        Dv_o                : out std_logic;
        TxData_ob           : out std_logic_vector(g_BusWidth-1 downto 0)
    );
end BertPattGen;

architecture behavioral of BertPattGen is 
    
   constant c_PrbsSeed_b31          : std_logic_vector(30 downto 0) := "111" & x"5ABCDEF";
   constant c_PrbsSeed_b7           : std_logic_vector( 6 downto 0) := "0101111";
   signal   RxDataAligned_db2       : std_logic_vector( 1 downto 0);
   signal   SingleErrorInject_db3   : std_logic_vector( 2 downto 0);
   signal   MultiErrorInject_db3    : std_logic_vector( 2 downto 0);   
   
begin   
    
    Main: process(Clk_ik)
        variable Lf                 : std_logic;
        variable LfSr_qb31          : std_logic_vector(30 downto 0); 
        variable LfSr_qb7           : std_logic_vector( 6 downto 0); 
        variable Prbs_qb            : std_logic_vector(g_BusWidth-1 downto 0);
    begin
        if rising_edge(Clk_ik) then        
            if (Reset_ir = '1') then
                RxDataAligned_db2        <= (others => '0');
                SingleErrorInject_db3    <= (others => '0');
                MultiErrorInject_db3     <= (others => '0');
                Lf                       := '0';
                LfSr_qb31                := c_PrbsSeed_b31;
                LfSr_qb7                 := c_PrbsSeed_b7;
                Prbs_qb                  := (others => '0');
                Dv_o                     <= '0';
                TxData_ob                <= (others => '0');
            else
                RxDataAligned_db2(1)     <= RxDataAligned_db2(0);
                RxDataAligned_db2(0)     <= RxDataAligned_i;
                SingleErrorInject_db3(2) <= SingleErrorInject_db3(1);
                SingleErrorInject_db3(1) <= SingleErrorInject_db3(0);
                SingleErrorInject_db3(0) <= SingleErrorInject_i;
                MultiErrorInject_db3(2)  <= MultiErrorInject_db3(1);
                MultiErrorInject_db3(1)  <= MultiErrorInject_db3(0);
                MultiErrorInject_db3(0)  <= MultiErrorInject_i;                      
                if (g_SelPrbs31 = 1) then
                    for i in 0 to g_BusWidth-1 loop
                        Prbs_qb((g_BusWidth-1)-i) := LfSr_qb31(30);
                        Lf                        := LfSr_qb31(30) xor LfSr_qb31(27); -- Comment: See Xilinx XAPP210
                        LfSr_qb31                 := LfSr_qb31(29 downto 0) & Lf;
                    end loop; 
                else
                    for i in 0 to g_BusWidth-1 loop   
                        Prbs_qb((g_BusWidth-1)-i) := LfSr_qb7(6);
                        Lf                        := LfSr_qb7(6) xor LfSr_qb7(5); -- Comment: See Xilinx XAPP210
                        LfSr_qb7                  := LfSr_qb7(5 downto 0) & Lf;
                    end loop;   
                end if;
                if (RxDataAligned_db2(1) = '1') then
                    Dv_o                 <= '1';
                    TxData_ob            <= Prbs_qb;
                    if ((SingleErrorInject_db3(2) = '0') and (SingleErrorInject_db3(1) = '1')) then
                        TxData_ob( 0)    <= not Prbs_qb( 0);
                    elsif ((MultiErrorInject_db3(2) = '0') and (MultiErrorInject_db3(1) = '1')) then
                        TxData_ob( 0)    <= not Prbs_qb( 0);
                        TxData_ob( 1)    <= not Prbs_qb( 1);
                        TxData_ob( 2)    <= not Prbs_qb( 2);
                        TxData_ob( 3)    <= not Prbs_qb( 3);
                    end if;
                else
                    Dv_o                 <= '0';
                    TxData_ob            <= AlignPattern_i;
                end if;
            end if;
        end if;
    end process;   
   
end behavioral;