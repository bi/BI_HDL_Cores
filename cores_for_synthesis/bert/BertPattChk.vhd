-- M. Barros Marin, BE-BI-QP (CERN) - 14/12/16
-- (Based on a module from: H. Laroussi)

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BertPattChk is
    generic (    
        g_BusWidth         : integer := 20;
        g_SelPrbs31        : integer range 0 to   1 :=   1;
        g_Words2Sync       : integer range 1 to 256 :=  64;
        g_Words2lossOfSync : integer range 1 to 256 := 128
    );
    port (
        Clk_ik             : in  std_logic;
        Reset_ir           : in  std_logic;
        RxAligned_i        : in  std_logic;
        RxData_ib          : in  std_logic_vector(g_BusWidth-1 downto 0);
        CntrsReset_i       : in  std_logic;
        Synchronized_o     : out std_logic;
        BitErrors_ob       : out std_logic_vector(g_BusWidth-1 downto 0);
        WordsCntr_ocb64    : out std_logic_vector(63 downto 0);
        ErrorCntr_ocb64    : out std_logic_vector(63 downto 0);
        ErrorFlag_o        : out std_logic
    );
end BertPattChk;

architecture behavioral of BertPattChk is
   
    --FSM:
    constant l_InitialDly     : integer range 0 to 15 := 15;
    type t_state is (s_Idle, s_Sync, s_Check);
    signal State, NextState   : t_state;
    signal CntrsReset_db3     : std_logic_vector(2 downto 0);
    signal InitialDlyCntr_cb4 : unsigned(3 downto 0);
    signal SeedReady_q        : std_logic;
    signal Words2SyncCntr_cb8 : unsigned(7 downto 0);
    signal LossOfSyncCntr_cb8 : unsigned(7 downto 0);   
    --Synchronous logic:
    type t_Array_6xBusWidth is array (0 to 5) of std_logic_vector(g_BusWidth-1 downto 0);
    signal RxData_d6b         : t_Array_6xBusWidth;
    signal i                  : integer range 0 to 3;
    signal RxDataSr_qb        : std_logic_vector((4*g_BusWidth)-1 downto 0);
    signal EnLfSr_q           : std_logic;
    
begin    
    
    -- FSM:
    FsmState: process(Clk_ik)
    begin
        if rising_edge(Clk_ik) then
            if (Reset_ir = '1') then
                State         <= s_Idle;
            else
                State         <= NextState;
            end if;
        end if;
    end process;
    
    FsmNextState: process(State, RxAligned_i, Words2SyncCntr_cb8, LossOfSyncCntr_cb8)
    begin
        NextState             <= State;
        case State is
            when s_Idle =>
                if (RxAligned_i = '1') then
                    NextState <= s_Sync;
                end if;
            when s_Sync =>
                if (Words2SyncCntr_cb8 = g_Words2Sync-1) then
                    NextState <= s_Check;
                elsif (LossOfSyncCntr_cb8 = g_Words2lossOfSync-1) then
                    NextState <= s_Idle;
                end if;
            when s_Check =>
                if (LossOfSyncCntr_cb8 = g_Words2lossOfSync-1) then
                    NextState <= s_Idle;
                end if;
            when others =>
                NextState     <= s_Idle;
        end case;
    end process;
   
    fsmOutputsAndSyncLogic: process(Clk_ik)
        -- FSM:
        variable WordsCntr_cb64 : unsigned(63 downto 0);
        variable BitErrors_b    : std_logic_vector(g_BusWidth-1 downto 0);
        variable ErrorCntr_cb64 : unsigned(63 downto 0);
        -- Synchronous logic:
        variable Lf             : std_logic;    
        variable LfSr_qb31      : std_logic_vector(30 downto 0);
        variable LfSr_qb7       : std_logic_vector( 6 downto 0);
        variable RecPattern_qb  : std_logic_vector(g_BusWidth-1 downto 0);
    begin
        if rising_edge(Clk_ik) then
            if (Reset_ir = '1') then         
                -- FSM:
                CntrsReset_db3                     <= (others => '0');
                InitialDlyCntr_cb4                 <= (others => '0');
                SeedReady_q                        <= '0';
                Synchronized_o                     <= '0';
                WordsCntr_cb64                     := (others => '0');
                WordsCntr_ocb64                    <= (others => '0');
                ErrorFlag_o                        <= '0';
                BitErrors_b                        := (others => '0');
                BitErrors_ob                       <= (others => '0');
                ErrorCntr_cb64                     := (others => '0');
                ErrorCntr_ocb64                    <= (others => '0');
                Words2SyncCntr_cb8                 <= (others => '0');
                LossOfSyncCntr_cb8                 <= (others => '0');
                -- Synchronous logic:
                i                                  <= 0;
                RxData_d6b                         <= (others => (others => '0'));
                RxDataSr_qb                        <= (others => '0');
                EnLfSr_q                           <= '0';
                Lf                                 := '0';
                LfSr_qb31                          := (others => '0');
                LfSr_qb7                           := (others => '0');
                RecPattern_qb                      := (others => '0');                
            else
                CntrsReset_db3(2)                  <= CntrsReset_db3(1);
                CntrsReset_db3(1)                  <= CntrsReset_db3(0);
                CntrsReset_db3(0)                  <= CntrsReset_i;
                case State is
                    when s_Idle =>
                        CntrsReset_db3             <= (others => '0');
                        InitialDlyCntr_cb4         <= (others => '0');
                        SeedReady_q                <= '0';
                        Synchronized_o             <= '0';
                        WordsCntr_cb64             := (others => '0');
                        WordsCntr_ocb64            <= (others => '0');
                        ErrorFlag_o                <= '0';
                        BitErrors_b                := (others => '0');
                        BitErrors_ob               <= (others => '0');
                        ErrorCntr_cb64             := (others => '0');
                        ErrorCntr_ocb64            <= (others => '0');
                        Words2SyncCntr_cb8         <= (others => '0');
                        LossOfSyncCntr_cb8         <= (others => '0');
                    when s_Sync =>
                        if (EnLfSr_q = '1') then
                            SeedReady_q            <= '1';
                        end if;
                        if (InitialDlyCntr_cb4 = l_InitialDly-1) then
                            if ((RxData_d6b(5) /= RxData_d6b(4)) and (RxData_d6b(5) = RecPattern_qb)) then
                                Words2SyncCntr_cb8 <= Words2SyncCntr_cb8+1;
                                LossOfSyncCntr_cb8 <= (others => '0');
                            else
                                Words2SyncCntr_cb8 <= (others => '0');
                                LossOfSyncCntr_cb8 <= LossOfSyncCntr_cb8+1;    
                            end if;
                        else
                            InitialDlyCntr_cb4     <= InitialDlyCntr_cb4+1;  
                        end if;
                    when s_Check =>
                        Synchronized_o             <= '1';
                        WordsCntr_cb64             := WordsCntr_cb64+1;
                        for i in 0 to g_BusWidth-1 loop
                            BitErrors_b(i)         := RxData_d6b(5)(i) xor RecPattern_qb(i);
                        end loop;
                        BitErrors_ob               <= BitErrors_b;  
                        if (RxData_d6b(5) /= RecPattern_qb) then
                            ErrorFlag_o            <= '1'; 
                            ErrorCntr_cb64         := ErrorCntr_cb64+1;
                            LossOfSyncCntr_cb8     <= LossOfSyncCntr_cb8+1;                  
                        else
                            ErrorFlag_o            <= '0'; 
                            LossOfSyncCntr_cb8     <= (others => '0');
                        end if;
                        if((CntrsReset_db3(2) = '0') and (CntrsReset_db3(1) = '1')) then
                            WordsCntr_cb64         := (others => '0');
                            ErrorCntr_cb64         := (others => '0');
                        end if;
                end case;
                WordsCntr_ocb64 <= std_logic_vector(WordsCntr_cb64);
                ErrorCntr_ocb64 <= std_logic_vector(ErrorCntr_cb64);
                -- Synchronous logic:
                RxData_d6b(0)                        <= RxData_ib;      
                for i in 0 to 4 loop RxData_d6b(i+1) <= RxData_d6b(i); end loop;                
                RxDataSr_qb((((i+1)*g_BusWidth))-1 downto (i*g_BusWidth)) <= RxData_ib;
                EnLfSr_q                             <= '0';
                if (i > 0) then i <= i-1; else i <= 3; EnLfSr_q <= '1'; end if;
                if (SeedReady_q = '1') then  
                    if (g_SelPrbs31 = 1) then
                        for i in 0 to g_BusWidth-1 loop
                            RecPattern_qb((g_BusWidth-1)-i) := LfSr_qb31(30);
                            Lf                              := LfSr_qb31(30) xor LfSr_qb31(27); -- See Xilinx XAPP210
                            LfSr_qb31                       := LfSr_qb31(29 downto 0) & Lf;            
                        end loop;
                    else
                        for i in 0 to g_BusWidth-1 loop   
                            RecPattern_qb((g_BusWidth-1)-i) := LfSr_qb7(6);
                            Lf                              := LfSr_qb7(6) xor LfSr_qb7(5); -- Comment: See Xilinx XAPP210
                            LfSr_qb7                        := LfSr_qb7(5 downto 0) & Lf;
                        end loop; 
                    end if;
                else
                    if (g_SelPrbs31 = 1) then
                        if (EnLfSr_q = '1') then
                            for i in 0 to 30 loop
                                LfSr_qb31(30-i)             := RxDataSr_qb(((4*g_BusWidth)-1)-i);
                            end loop;
                        end if;
                    else
                        if (EnLfSr_q = '1') then
                            for i in 0 to 6 loop                            
                                LfSr_qb7(6-i)               := RxDataSr_qb(((4*g_BusWidth)-1)-i);                            
                            end loop;
                        end if;
                    end if;
                    RecPattern_qb                           := (others => '0');
                    Lf                                      := '0';
                end if;
            end if;
        end if;
    end process;
      
end behavioral;