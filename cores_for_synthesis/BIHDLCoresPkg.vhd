------------------------------------------------------------------------
-- Title      : BI_HDL_Cores VHDL Package
-- Project    : BI_HDL_Cores (https://gitlab.cern.ch/bi/BI_HDL_Cores)
------------------------------------------------------------------------
-- File       : BIHDLCoresPkg.vhd
-- Author     : -
-- Company    : CERN BE-BI
------------------------------------------------------------------------
-- Description:
--
-- VHDL package with component definitions of cores in BI_HDL_Cores
-----------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package BIHDLCoresPkg is

    component SfpIdReader is
        generic (
            g_SfpWbBaseAddress : natural := 0;
            g_WbAddrWidth      : natural := 32
        );
        port (
            Clk_ik       : in  std_logic;
            SfpPlugged_i : in  std_logic;
            SfpIdValid_o : out std_logic;
            SfpPN_ob128  : out std_logic_vector(127 downto 0);
            WbCyc_o      : out std_logic;
            WbStb_o      : out std_logic;
            WbAddr_ob    : out std_logic_vector(g_WbAddrWidth-1 downto 0);
            WbData_ib8   : in  std_logic_vector(7 downto 0);
            WbAck_i      : in  std_logic
        );
    end component SfpIdReader;

    component DS18B20Emu is
        generic (
            g_ClkFrequency : real
        );
        port (
            Clk_ik         : in  std_logic;
            Rst_irq        : in  std_logic;
            UniqueId_ib64  : in  std_logic_vector(63 downto 0);
            Temp_ib16      : in  std_logic_vector(15 downto 0);
            OneWireBus_i   : in  std_logic;
            OneWireBus_oe  : out std_logic
        );
    end component DS18B20Emu;

    component Monostable is
        generic (
            g_CounterBits : natural
        );
        port (
            Input_i       : in  std_logic;
            Clk_ik        : in  std_logic;
            Width_ib      : in  std_logic_vector(g_CounterBits-1 downto 0);
            Output_oq     : out std_logic
        );
    end component Monostable;

    component Si5338LoaderWrapper is
        generic (
            g_ClkFrequency  : real;
            g_I2CFrequency  : natural;
            g_I2CSlaveAddr  : std_logic_vector(7 downto 1);
            g_RegFile       : string;
            g_NbRegs        : natural;
            g_DlyBits       : natural := 16
        );
        port (
            Clk_ik          : in  std_logic;
            Rst_ir          : in  std_logic;
            Init_i          : in  std_logic;
            InitDone_o      : out std_logic;

            WbAdr_ib3       : in  std_logic_vector(2 downto 0);
            WbDatMosi_ib8   : in  std_logic_vector(7 downto 0);
            WbDatMiso_ob8   : out std_logic_vector(7 downto 0);
            WbCyc_i         : in  std_logic;
            WbStb_i         : in  std_logic;
            WbWe_i          : in  std_logic;
            WbAck_o         : out std_logic;

            Scl_i           : in  std_logic;
            Scl_o           : out std_logic;
            SclOe_on        : out std_logic;
            Sda_i           : in  std_logic;
            Sda_o           : out std_logic;
            SdaOe_on        : out std_logic
        );
    end component Si5338LoaderWrapper;

    function f_VfcAppIdentStr2Slv (s : string) return std_logic_vector;

end BIHDLCoresPkg;

package body BIHDLCoresPkg is

    -- Convert the AppIdent string to a std_logic_vector
    function f_VfcAppIdentStr2Slv (s : string) return std_logic_vector is
        constant chr : natural := 44;
        constant len : natural := s'length;
        variable slv : std_logic_vector(chr*8-1 downto 0);
    begin
        for i in 0 to len-1 loop
            slv(slv'high-i*8 downto (slv'high-7)-i*8) := std_logic_vector(to_unsigned(character'pos(s(i+1)), 8));
        end loop;
        if len < chr then
            for i in len to chr-1 loop
                slv(slv'high-i*8 downto (slv'high-7)-i*8) := x"00";
            end loop;
        end if;
        return slv;
    end f_VfcAppIdentStr2Slv;


end BIHDLCoresPkg;
