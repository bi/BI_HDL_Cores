`timescale 1ns / 1ps

//Manoel Barros Marin, BE-BI-QP (CERN) - 18/05/15
//
//- Fixed word inverted issue (21/06/2015)

module MgtRxWordAligner40b #(

//***********   
//Parameters:
//***********

    parameter         g_AlignPattern_b20 = 40'h5CCCC3AAAA,
    parameter         g_Words2Align_b8   =  8'h10,
    parameter         g_Words2Bitslip_b8 =  8'h10) (

//*****   
//I/Os:
//*****   
   
    input             Clk_ik,
    input             Reset_ir,
    input             Enable_i,
    
    input      [39:0] RxData_ib40,
    
    output reg        RxAligned_o,
    output     [39:0] RxDataAligned_ob40
   
);

//*************   
//Declarations:
//*************   
    
    localparam  s_Idle     = 3'b001,
                s_Aligning = 3'b010,
                s_Aligned  = 3'b100;
               
    reg  [ 2:0] State_qb3, NextState_qb3;
    reg  [ 1:2] Enable_q;
    reg  [ 1:2] MgtLk2Data_q;
    reg  [39:0] PreviousData_qb40, ShiftedData_qb40;
    reg  [ 5:0] SrlAddress_qb6;
    reg  [ 7:0] Words2AlignCntr_cb8;
    reg  [ 7:0] Words2BitslipCntr_cb8;            

//***********      
//User Logic:
//***********   
   
    //Secuential logic:
    always @(posedge Clk_ik)
        if (Reset_ir) begin
            Enable_q                              <= #1  2'b0;           
            //---                                 
            PreviousData_qb40                     <= #1 40'b0;
            ShiftedData_qb40                      <= #1 40'b0;
        end else begin                            
            Enable_q[2]                           <= #1 Enable_q[1];
            Enable_q[1]                           <= #1 Enable_i;      
            //---                                 
            PreviousData_qb40                     <= #1 RxData_ib40;            
            case(SrlAddress_qb6)                  
                0: begin                          
                    ShiftedData_qb40              <= #1 RxData_ib40;
                end                               
                1: begin                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[38:0],PreviousData_qb40[39:39]};
                end                                                           
                2: begin                                                      
                    ShiftedData_qb40              <= #1 {RxData_ib40[37:0],PreviousData_qb40[39:38]};
                end                                                                               
                3: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[36:0],PreviousData_qb40[39:37]};
                end                                                                               
                4: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[35:0],PreviousData_qb40[39:36]};
                end                                                                               
                5: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[34:0],PreviousData_qb40[39:35]};
                end                                                                               
                6: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[33:0],PreviousData_qb40[39:34]};
                end                                                                               
                7: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[32:0],PreviousData_qb40[39:33]};
                end                                                                               
                8: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[31:0],PreviousData_qb40[39:32]};
                end                                                                               
                9: begin                                                                          
                    ShiftedData_qb40              <= #1 {RxData_ib40[30:0],PreviousData_qb40[39:31]};
                end                                                                               
                10: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[29:0],PreviousData_qb40[39:30]};
                end                                                                               
                11: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[28:0],PreviousData_qb40[39:29]};
                end                                                                               
                12: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[27:0],PreviousData_qb40[39:28]};
                end                                                                               
                13: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[26:0],PreviousData_qb40[39:27]};
                end                                                                               
                14: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[25:0],PreviousData_qb40[39:26]};
                end                                                                               
                15: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[24:0],PreviousData_qb40[39:25]};
                end                                                                               
                16: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[23:0],PreviousData_qb40[39:24]};
                end                                                                               
                17: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[22:0],PreviousData_qb40[39:23]};
                end                                                                               
                18: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[21:0],PreviousData_qb40[39:22]};
                end                                                                               
                19: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[20:0],PreviousData_qb40[39:21]};
                end                                                                               
                20: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[19:0],PreviousData_qb40[39:20]};
                end                                                                               
                21: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[18:0],PreviousData_qb40[39:19]};
                end                                                                               
                22: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[17:0],PreviousData_qb40[39:18]};
                end                                                                               
                23: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[16:0],PreviousData_qb40[39:17]};
                end                                                                               
                24: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[15:0],PreviousData_qb40[39:16]};
                end                                                                               
                25: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[14:0],PreviousData_qb40[39:15]};
                end                                                                               
                26: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[13:0],PreviousData_qb40[39:14]};
                end                                                                               
                27: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[12:0],PreviousData_qb40[39:13]};
                end                                                                               
                28: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[11:0],PreviousData_qb40[39:12]};
                end                                                                               
                29: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[10:0],PreviousData_qb40[39:11]};
                end                                                                               
                30: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 9:0],PreviousData_qb40[39:10]};
                end                                                                               
                31: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 8:0],PreviousData_qb40[39: 9]};
                end                                                                               
                32: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 7:0],PreviousData_qb40[39: 8]};
                end                                                                               
                33: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 6:0],PreviousData_qb40[39: 7]};
                end                                                                               
                34: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 5:0],PreviousData_qb40[39: 6]};
                end                                                                               
                35: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 4:0],PreviousData_qb40[39: 5]};
                end                                                                               
                36: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 3:0],PreviousData_qb40[39: 4]};
                end                                                                               
                37: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 2:0],PreviousData_qb40[39: 3]};
                end                                                                               
                38: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 1:0],PreviousData_qb40[39: 2]};
                end                                                                               
                39: begin                                                                         
                    ShiftedData_qb40              <= #1 {RxData_ib40[ 0:0],PreviousData_qb40[39: 1]};
                end                                                                               
                default: begin                                                                    
                   ShiftedData_qb40               <= #1 RxData_ib40;                         
                end                              
            endcase                              
        end                                      
                                                 
    assign RxDataAligned_ob40 = ShiftedData_qb40;
   
    //FSM:
    always @( posedge Clk_ik) State_qb3 <= #1 Reset_ir ? s_Idle : NextState_qb3;
    always @* begin
        NextState_qb3                              = State_qb3;
        case(State_qb3)
            s_Idle: begin
                if (Enable_q[2]) begin
                    NextState_qb3                  = s_Aligning;
                end
            end         
            s_Aligning: begin
                if (Words2AlignCntr_cb8 == g_Words2Align_b8-1) begin
                    NextState_qb3                  = s_Aligned;
                end                         
            end                            
            s_Aligned: begin 
                if (Enable_q[2]) begin    
                    NextState_qb3                  = s_Aligned;
                end else begin
                    NextState_qb3                  = s_Idle;
                end            
            end                            
            default: begin
                NextState_qb3                      = s_Idle;
            end                            
        endcase                           
    end  
    always @(posedge Clk_ik)             
        if (Reset_ir) begin               
            Words2AlignCntr_cb8                   <= #1 8'b0;
            Words2BitslipCntr_cb8                 <= #1 8'b0;
            SrlAddress_qb6                        <= #1 6'b0;
            RxAligned_o                           <= #1 1'b0;
        end else begin                    
            case(State_qb3)                
                s_Idle: begin               
                    Words2AlignCntr_cb8           <= #1 8'b0;   
                    Words2BitslipCntr_cb8         <= #1 8'b0;   
                    SrlAddress_qb6                <= #1 6'b0;
                    RxAligned_o                   <= #1 1'b0;
                end                         
                s_Aligning: begin           
                    if (ShiftedData_qb40 == g_AlignPattern_b20) begin
                        Words2AlignCntr_cb8       <= #1 Words2AlignCntr_cb8+1;                       
                    end else begin
                        Words2AlignCntr_cb8       <= #1 8'b0;
                        if (Words2BitslipCntr_cb8 == g_Words2Bitslip_b8-1) begin
                            Words2BitslipCntr_cb8 <= #1 0;
                            SrlAddress_qb6        <= #1 SrlAddress_qb6+1;
                        end else begin
                            Words2BitslipCntr_cb8 <= #1 Words2BitslipCntr_cb8+1;
                        end
                    end
                end
                s_Aligned: begin
                    RxAligned_o                   <= #1 1'b1;
                end
         endcase
      end
   
endmodule