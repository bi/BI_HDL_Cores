//----------------------------------------------------------------------
// Title      : Si5338 Loader Wrapper
// Project    : VFC-HD
//----------------------------------------------------------------------
// File       : Si5338LoaderWrapper.sv
// Author     : T. Levens
// Company    : CERN SY-BI
//----------------------------------------------------------------------
// Description:
//
// A wrapper of the Si5338Loader including the I2C Master and a
// multiplexer to provide an additional WishBone input to allow readback
// from VME.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module Si5338LoaderWrapper #(parameter
    g_ClkFrequency, // Clock frequency
    g_I2CFrequency, // I2C frequency
    g_I2CSlaveAddr, // I2C slave address
    g_RegFile,      // Registers map file
    g_NbRegs,       // Number of registers the file
    g_DlyBits = 16  // Initialise chip 2**N clocks after reset
)(
    input        Clk_ik,
    input        Rst_ir,

    input        Init_i,
    output       InitDone_o,

    input  [2:0] WbAdr_ib3,
    input  [7:0] WbDatMosi_ib8,
    output [7:0] WbDatMiso_ob8,
    input        WbCyc_i,
    input        WbStb_i,
    input        WbWe_i,
    output       WbAck_o,

    input        Scl_i,
    output       Scl_o,
    output       SclOe_on,
    input        Sda_i,
    output       Sda_o,
    output       SdaOe_on
);

wire        WbCycM2, WbStbM2, WbAckM2, WbWeM2;
wire  [2:0] WbAdrM2_b3;
wire  [7:0] WbDatMosiM2_b8, WbDatMisoM2_b8;

wire        WbCycS, WbStbS, WbAckS, WbWeS;
wire  [2:0] WbAdrS_b3;
wire  [7:0] WbDatMosiS_b8, WbDatMisoS_b8;

Si5338Loader #(
    .g_ClkFrequency (g_ClkFrequency),
    .g_I2CFrequency (g_I2CFrequency),
    .g_I2CSlaveAddr (g_I2CSlaveAddr),
    .g_I2CMasterIFR (0),
    .g_RegFile      (g_RegFile),
    .g_NbRegs       (g_NbRegs),
    .g_DlyBits      (g_DlyBits)
) i_Si5338Loader (
    .Clk_ik         (Clk_ik),
    .Rst_ir         (Rst_ir),

    .Init_i         (Init_i),
    .InitDone_o     (InitDone_o),

    .WbAdr_ob3      (WbAdrM2_b3),
    .WbDatMosi_ob8  (WbDatMosiM2_b8),
    .WbDatMiso_ib8  (WbDatMisoM2_b8),
    .WbCyc_o        (WbCycM2),
    .WbStb_o        (WbStbM2),
    .WbWe_o         (WbWeM2),
    .WbAck_i        (WbAckM2));

WbBus2M1S #(
    .g_DataWidth    (8),
    .g_AddrWidth    (3)
) i_WbBus2M1S (
    .Clk_ik         (Clk_ik),

    .CycM1_i        (WbCyc_i),
    .StbM1_i        (WbStb_i),
    .WeM1_i         (WbWe_i),
    .AddrM1_ib      (WbAdr_ib3),
    .DataM1_ib      (WbDatMosi_ib8),
    .AckM1_o        (WbAck_o),
    .DataM1_ob      (WbDatMiso_ob8),

    .CycM2_i        (WbCycM2),
    .StbM2_i        (WbStbM2),
    .WeM2_i         (WbWeM2),
    .AddrM2_ib      (WbAdrM2_b3),
    .DataM2_ib      (WbDatMosiM2_b8),
    .AckM2_o        (WbAckM2),
    .DataM2_ob      (WbDatMisoM2_b8),

    .CycS_o         (WbCycS),
    .StbS_o         (WbStbS),
    .WeS_o          (WbWeS),
    .AddrS_ob       (WbAdrS_b3),
    .DataS_ob       (WbDatMosiS_b8),
    .AckS_i         (WbAckS),
    .DataS_ib       (WbDatMisoS_b8));

i2c_master_top #(
    .g_num_interfaces (1)
) i_I2cMasterTop (
    .wb_clk_i       (Clk_ik),
    .wb_rst_i       (Rst_ir),
    .arst_i         (1'b1),
    .wb_adr_i       (WbAdrS_b3),
    .wb_dat_i       (WbDatMosiS_b8),
    .wb_dat_o       (WbDatMisoS_b8),
    .wb_we_i        (WbWeS),
    .wb_stb_i       (WbStbS),
    .wb_cyc_i       (WbCycS),
    .wb_ack_o       (WbAckS),
    .inta_o         (),
    .scl_pad_i      (Scl_i),
    .scl_pad_o      (Scl_o),
    .scl_padoen_o   (SclOe_on),
    .sda_pad_i      (Sda_i),
    .sda_pad_o      (Sda_o),
    .sda_padoen_o   (SdaOe_on));

endmodule
