#
# A (very) hacky script to convert the C header output from Skyworks
# ClockBuilder to a file which can be loaded by the FPGA loader.
#
# It expects a definition such as this:
#
#     Reg_Data const code Reg_Store[NUM_REGS_MAX] = {
#     {  0,0x00,0x00},
#     {  1,0x00,0x00},
#     ...
#     { 94,0x00,0x00},
#     {255,0x00,0xFF} };
#
# The output will be a 24-bit hex file:
#
#     06001D
#     1B7080
#     ...
#     5B000F
#     FF00FF
#
# Note that to save space in the generated ROM file, registers with
# mask equal 0 are omitted.
#
# T. Levens, CERN SY-BI
#

import os
import sys

if len(sys.argv) == 1:
    print("usage: {} filename.h".format(sys.argv[0]))
    sys.exit(1)

h_file = sys.argv[1]
dat_file = os.path.splitext(h_file)[0] + ".dat"

count = 0
lines = []

with open(h_file, "r") as f:
    for l in f.readlines():
        ls = l.strip()

        # The interesting lines should start with {
        if len(ls) == 0 or ls[0] != "{":
            continue

        end = l.find("}")
        addr, val, mask = list(map(lambda x: int(x.strip(), 0),
                                   ls[1:end].split(",")))

        # Skip lines where mask is zero
        if mask == 0x00:
            continue

        # Output in XXYYZZ format where:
        #   XX=addr
        #   YY=val
        #   ZZ=mask
        lines.append("{:02X}{:02X}{:02X}".format(addr, val, mask))
        count += 1

with open(dat_file, "w") as f:
    print("// Si5388 PLL configuration file", file=f)
    print("// Generated from {}".format(sys.argv[1]), file=f)
    print("// {} registers".format(count), file=f)

    for l in lines:
        print(l, file=f)

print("DONE: {} registers written to {}".format(count, dat_file))
