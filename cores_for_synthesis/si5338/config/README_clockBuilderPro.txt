Creating a New Configuration for RAM:
  - Any Si5338 device can be configured by writing to registers in RAM through the I2C interface. 
    A non-factory programmed device must be configured in this manner.
  - The first step is to determine all the register values for the required configuration. 
    This can be accomplished by one of two methods.
  
1. Create a device configuration (register map) using ClockBuilder Pro (v3.0 or later; 
    see "3.1.1. ClockBuilder Pro™ Software": 
    https://tools.skyworksinc.com/timingfiles/latest-tools/clockbuilder-pro-installer.zip).
    
    a. Configure the frequency plan.
    b. Configure the output driver format and supply voltage.
    c. Configure frequency and/or phase inc/dec (if desired).
    d. Configure spread spectrum (if desired).
    e. Configure for zero-delay mode (if desired).
    f. If needed go to the Advanced tab and make additional configurations.
    g. Save the configuration using the Options > Save Register Map File or Options > Save C code Header.
    
2. Create a device configuration, register by register, using the Si5338 Reference Manual.

-----------------------------------

VFC-HD case:

  Si5338N-A-GMR 
  - input single ended pin3
  - output CLK0 diff SSTL to tansceiver refClock0L (all SFPs Rx)
  - option zero delay mode: feedback CLk3 diff SSTL to pin5/6 LVPECL
  
  Configuration in ClockBuilderPro
  1. None
  2. Universal Pin Configuration = IN3(CLMIN) IN4(FDBK)
  3. I2C Address 0x70; I2C Bus Voltage 2.5/3.3V
  4. Input clock IN3 Single-ended
      (+ option to enable Zero Delay Mode External Feedback Input IN5/6)
  5. Output Frequencies CLK0 enable PLL
      (+ option CLK3 enable PLL for Feedback) 
  6. Output Driver CLK0 LVDS 3.3V Stop Low
      (+ option CLK3 LVDS 3.3V Stop Low for Feedback) 
  7. Frequency and phase offset CLK0 0ns initial offset, 0ns step size 0% 
      (+ option CLK3 0ns initial offset, 0ns step size 0%, for Feedback) 
  8. Spread Spectrum CLK0 Disable 
      (+ option CLK3 Disablefor Feedback) 
 
