//----------------------------------------------------------------------
// Title      : Si5338 Loader
// Project    : VFC-HD
//----------------------------------------------------------------------
// File       : Si5338Loader.sv
// Author     : T. Levens
// Company    : CERN SY-BI
//----------------------------------------------------------------------
// Description:
//
// This module loads the configuration into the Si5338 PLL automatically
// after reset. It is based on the I2C Programming Procedure given in
// Figure 9 of the Si5338 datasheet.
//
// The Register Map should be generated as a C header file using the
// ClockBuilder software. It should then be converted to HEX using the
// `config/ConvertHeaderToDat.py` script. Inside the `config` directory
// there are also some examples. The register map file is 24-bit with
// the format AABBCC, where AA=address, BB=data, CC=mask.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module Si5338Loader #(parameter
    g_ClkFrequency, // Clock frequency
    g_I2CFrequency, // I2C frequency
    g_I2CSlaveAddr, // I2C slave address
    g_I2CMasterIFR, // I2C master IFR
    g_RegFile,      // Registers map file
    g_NbRegs,       // Number of registers the file
    g_DlyBits = 16  // Initialise chip 2**N clocks after reset
)(
    input            Clk_ik,
    input            Rst_ir,

    input            Init_i,
    output reg       InitDone_o,

    // WB bus to (OpenCores) i2c_master_top
    output reg  [2:0] WbAdr_ob3,
    output reg  [7:0] WbDatMosi_ob8,
    input       [7:0] WbDatMiso_ib8,
    output            WbCyc_o,
    output reg        WbStb_o,
    output reg        WbWe_o,
    input             WbAck_i
);

// Configuration memory
// To avoid a Quartus critical warning, the size should be a power of 2
localparam int c_NbRegs = int'(g_NbRegs);
localparam int c_MemSize = 2**$clog2(c_NbRegs);
reg [23:0] Config_mb24 [0:c_MemSize-1];

integer i;
initial begin
    // Initialise `g_NbRegs` words from `g_RegFile`
    $readmemh(g_RegFile, Config_mb24, 0, c_NbRegs-1);

    // Zero the rest of the memory to avoid a Quartus warning
    for (i = c_NbRegs; i < c_MemSize; i = i + 1)
        Config_mb24[i] = 24'h0;
end

// Configuration register counter
reg  [$clog2(c_NbRegs)-1:0] Config_c;

// Current memory word
wire [23:0] Config_b24 = Config_mb24[Config_c];

// Delay counter for 25ms (40Hz) delay
localparam int c_Dly25ms = int'(g_ClkFrequency / 40);
localparam int c_DlyBits = $clog2(c_Dly25ms);

reg [c_DlyBits-1:0] Dly_c;
reg DlyStart;
always @(posedge Clk_ik) begin
    if (Rst_ir) begin
        Dly_c <= 1'd0;
    end else begin
        if (DlyStart) begin
            Dly_c <= (c_DlyBits)'(c_Dly25ms);
        end else if (|Dly_c) begin
            Dly_c <= Dly_c - 1'd1;
        end
    end
end

reg DlyDone;
always @(posedge Clk_ik) DlyDone = (Dly_c == 1'd1);

// Initialisation sequencer
reg [7:0] RdRegAddr_b8;
reg [7:0] WrRegAddr_b8;
reg [7:0] WrData_b8;
reg [7:0] WrMask_b8;
reg Start;
wire Done;

// State variable
typedef enum {
    s_Idle,
    s_Init0,
    s_Init1,
    s_Init2,
    s_Config0,
    s_Config1,
    s_Config2,
    s_Init3,
    s_Init4,
    s_Init5,
    s_Init6,
    s_Init7,
    s_Init8,
    s_Init9,
    s_Init10,
    s_Init11,
    s_Done
} t_State;

t_State State_l;

// Reset counter
reg  [g_DlyBits-1:0] Rst_c;
always @(posedge Clk_ik) begin
    if (Rst_ir) begin
        Rst_c <= {g_DlyBits{1'b1}};
    end else begin
        if (|Rst_c) begin
            Rst_c <= Rst_c - 1'd1;
        end
    end
end

reg RstInit;
always @(posedge Clk_ik) RstInit = (Rst_c == 1'd1);


always @(posedge Clk_ik) begin
    if (Rst_ir) begin
        State_l         <= s_Idle;
        Start           <= 1'b0;
        DlyStart        <= 1'b0;
        InitDone_o      <= 1'b0;
        RdRegAddr_b8    <= 8'h0;
        WrRegAddr_b8    <= 8'h0;
        WrData_b8       <= 8'h0;
        WrMask_b8       <= 8'h0;
        Config_c        <= 1'd0;
    end else begin
        Start           <= 1'b0;
        DlyStart        <= 1'b0;
        case (State_l)
            s_Idle: begin
                if (Init_i || RstInit) begin
                    // Reinitialise on request
                    State_l         <= s_Init0;
                    InitDone_o      <= 1'b0;
                end
            end

            s_Init0: begin
                // Set page bit to 0; reg255
                // Note: this is not specified in the programming
                // procedure but has been added for safety
                RdRegAddr_b8        <= 8'd255;
                WrRegAddr_b8        <= 8'd255;
                WrData_b8           <= 8'h0;
                WrMask_b8           <= 8'hFF;
                Start               <= 1'b1;
                State_l             <= s_Init1;
            end
            s_Init1: begin
                if (Done) begin
                    // Disable Outputs
                    // Set OEB_ALL = 1; reg230[4]
                    RdRegAddr_b8    <= 8'd230;
                    WrRegAddr_b8    <= 8'd230;
                    WrData_b8       <= 8'h10;
                    WrMask_b8       <= 8'h10;
                    Start           <= 1'b1;
                    State_l         <= s_Init2;
                end
            end
            s_Init2: begin
                if (Done) begin
                    // Pause LOL
                    // Set DIS_LOL = 1; reg241[7]
                    // Note: reg241[6:0] must be set to 0x65
                    RdRegAddr_b8    <= 8'd241;
                    WrRegAddr_b8    <= 8'd241;
                    WrData_b8       <= 8'hE5;
                    WrMask_b8       <= 8'hFF;
                    Start           <= 1'b1;
                    State_l         <= s_Config0;
                end
            end

            // Write all registers from the configuration file
            s_Config0: begin
                if (Done) begin
                    Config_c        <= 1'd0;
                    State_l         <= s_Config1;
                end
            end
            s_Config1: begin
                RdRegAddr_b8        <= Config_b24[23:16];
                WrRegAddr_b8        <= Config_b24[23:16];
                WrData_b8           <= Config_b24[15: 8];
                WrMask_b8           <= Config_b24[ 7: 0];
                Start               <= 1'b1;
                State_l             <= (Config_c == c_NbRegs-1) ? s_Init3 : s_Config2;
            end
            s_Config2: begin
                if (Done) begin
                    Config_c        <= Config_c + 1'd1;
                    State_l         <= s_Config1;
                end
            end

            // TODO: add readback of LOS_CLKIN

            s_Init3: begin
                if (Done) begin
                    // Configure PLL for locking
                    // Set FCAL_OVRD_ENA = 0; reg49[7]
                    RdRegAddr_b8        <= 8'd49;
                    WrRegAddr_b8        <= 8'd49;
                    WrData_b8           <= 8'h00;
                    WrMask_b8           <= 8'h80;
                    Start               <= 1'b1;
                    State_l             <= s_Init4;
                end
            end
            s_Init4: begin
                if (Done) begin
                    // Initiate Locking of PLL
                    // Set SOFT_RESET = 1; reg246[1]
                    RdRegAddr_b8    <= 8'd246;
                    WrRegAddr_b8    <= 8'd246;
                    WrData_b8       <= 8'h02;
                    WrMask_b8       <= 8'hFF;
                    Start           <= 1'b1;
                    State_l         <= s_Init5;
                end
            end
            s_Init5: begin
                if (Done) begin
                    // Wait 25ms
                    DlyStart        <= 1'b1;
                    State_l         <= s_Init6;
                end
            end
            s_Init6: begin
                if (DlyDone) begin
                    // Restart LOL
                    // Set DIS_LOL = 0; reg241[7]
                    // Note: reg241[6:0] must be set to 0x65
                    RdRegAddr_b8    <= 8'd241;
                    WrRegAddr_b8    <= 8'd241;
                    WrData_b8       <= 8'h65;
                    WrMask_b8       <= 8'hFF;
                    Start           <= 1'b1;
                    State_l         <= s_Init7;
                end
            end

            // TODO: add readback of PLL_LOL

            s_Init7: begin
                if (Done) begin
                    // Copy FCAL values to active registers
                    // Copy reg237[1:0] to reg47[1:0]
                    // Set reg47[7:2] = 0b000101
                    RdRegAddr_b8    <= 8'd237;
                    WrRegAddr_b8    <= 8'd47;
                    WrData_b8       <= 8'h14;
                    WrMask_b8       <= 8'hFC;
                    Start           <= 1'b1;
                    State_l         <= s_Init8;
                end
            end
            s_Init8: begin
                if (Done) begin
                    // Copy FCAL values to active registers
                    // Copy reg236[7:0] to reg46[7:0]
                    RdRegAddr_b8    <= 8'd236;
                    WrRegAddr_b8    <= 8'd46;
                    WrData_b8       <= 8'h00;
                    WrMask_b8       <= 8'h00;
                    Start           <= 1'b1;
                    State_l         <= s_Init9;
                end
            end
            s_Init9: begin
                if (Done) begin
                    // Copy FCAL values to active registers
                    // Copy reg235[7:0] to reg45[7:0]
                    RdRegAddr_b8    <= 8'd235;
                    WrRegAddr_b8    <= 8'd45;
                    WrData_b8       <= 8'h00;
                    WrMask_b8       <= 8'h00;
                    Start           <= 1'b1;
                    State_l         <= s_Init10;
                end
            end
            s_Init10: begin
                if (Done) begin
                    // Set PLL to use FCAL values
                    // Set FCAL_OVRD_ENA = 1; reg49[7]
                    RdRegAddr_b8    <= 8'd49;
                    WrRegAddr_b8    <= 8'd49;
                    WrData_b8       <= 8'h80;
                    WrMask_b8       <= 8'h80;
                    Start           <= 1'b1;
                    State_l         <= s_Init11;
                end
            end
            s_Init11: begin
                if (Done) begin
                    // Enable Outputs
                    // Set OEB_ALL = 0; reg230[4]
                    RdRegAddr_b8    <= 8'd230;
                    WrRegAddr_b8    <= 8'd230;
                    WrData_b8       <= 8'h00;
                    WrMask_b8       <= 8'h10;
                    Start           <= 1'b1;
                    State_l         <= s_Done;
                end
            end
            s_Done: begin
                if (Done) begin
                    // Initialisation complete!
                    State_l         <= s_Idle;
                    InitDone_o      <= 1'b1;
                end
            end
        endcase
    end
end

// RMW sequencer
wire  [3:0] SeqWrCnt_b4, SeqRdCnt_b4;
wire [31:0] SeqWrData_b32, SeqRdData_b32;
wire        SeqStart, SeqBusy, SeqDone, SeqNack, SeqAl;

Si5338RmwSeq i_Si5338RmwSeq (
    .Clk_ik         (Clk_ik),
    .Rst_ir         (Rst_ir),

    .RdRegAddr_ib8  (RdRegAddr_b8),
    .WrRegAddr_ib8  (WrRegAddr_b8),
    .WrData_ib8     (WrData_b8),
    .WrMask_ib8     (WrMask_b8),
    .Start_i        (Start),
    .Done_o         (Done),

    .SeqStart_o     (SeqStart),
    .SeqBusy_i      (SeqBusy),
    .SeqDone_i      (SeqDone),
    .SeqNack_i      (SeqNack),
    .SeqAl_i        (SeqAl),

    .SeqWrData_ob32 (SeqWrData_b32),
    .SeqRdData_ib32 (SeqRdData_b32),
    .SeqWrCnt_ob4   (SeqWrCnt_b4),
    .SeqRdCnt_ob4   (SeqRdCnt_b4));

// OpenCores I2C Wrapper
OcI2CWrapper #(
    .g_ClkFrequency (g_ClkFrequency),
    .g_I2CFrequency (g_I2CFrequency),
    .g_IFR          (g_I2CMasterIFR)
) i_OcI2CWrapper (
    .Clk_ik         (Clk_ik),
    .Rst_ir         (Rst_ir),

    .SlaveAddr_ib7  (g_I2CSlaveAddr),
    .Start_i        (SeqStart),
    .WrData_ib32    (SeqWrData_b32),
    .RdData_ob32    (SeqRdData_b32),
    .WrCnt_ib4      (SeqWrCnt_b4),
    .RdCnt_ib4      (SeqRdCnt_b4),
    .Busy_o         (SeqBusy),
    .Done_o         (SeqDone),
    .Nack_o         (SeqNack),
    .Al_o           (SeqAl),

    .WbAdr_ob3      (WbAdr_ob3),
    .WbDatMosi_ob8  (WbDatMosi_ob8),
    .WbDatMiso_ib8  (WbDatMiso_ib8),
    .WbCyc_o        (WbCyc_o),
    .WbStb_o        (WbStb_o),
    .WbWe_o         (WbWe_o),
    .WbAck_i        (WbAck_i));

endmodule
