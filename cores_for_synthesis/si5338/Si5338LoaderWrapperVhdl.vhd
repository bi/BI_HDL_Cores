------------------------------------------------------------------------
-- Title      : Si5338 Loader Wrapper VHDL
-- Project    : VFC-HD
------------------------------------------------------------------------
-- File       : Si5338LoaderWrapperVhdl.vhc
-- Author     : M.Ssccani
-- Company    : CERN SY-BI
------------------------------------------------------------------------
-- Description:
--
-- A VHDL wrapper calling the verilog module and implementing the tristate buffers.
--
------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Libraries definition
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-----------------------------------------------------------------------
-- Entity
-----------------------------------------------------------------------
entity Si5338LoaderWrapperVhdl is
  generic (
    g_ClkFrequency        : integer := 100E6;                                   -- Clock frequency
    g_I2CFrequency        : integer := 1E5;                                     -- I2C frequency (100kHz or 400kHz)
    g_I2CSlaveAddr        : std_logic_vector(6 downto 0) := "1110000";          -- I2C slave address (Si5338=7'h70),
    g_RegFile             : string  := "./config/PllRef_GbtFpga_BstClkSch.dat"; --Registers map file (160MHz)
    g_NbRegs              : integer := 238;                                     -- Number of registers the file
    g_DlyBits             : integer := 16                                       -- Initialise chip 2**N clocks after reset
    ); 
  port (
    -- Clock and reset
    Clk_ik                : in    std_logic;
    Rst_ir                : in    std_logic;
    --Strobe and Flags
    Init_i                : in    std_logic;
    InitDone_o            : out   std_logic;
    --WB 
    WbAdr_ib3             : in    std_logic_vector(2 downto 0); 
    WbDatMosi_ib8         : in    std_logic_vector(7 downto 0); 
    WbDatMiso_ob8         : out   std_logic_vector(7 downto 0); 
    WbCyc_i               : in    std_logic;
    WbStb_i               : in    std_logic;
    WbWe_i                : in    std_logic;
    WbAck_o               : out   std_logic;
    --I2C   
    Scl_o                 : out   std_logic;
    Sda_io                : inout std_logic
  );
end entity;




--=============================================================================
-- Architecture 
--=============================================================================
architecture rtl of Si5338LoaderWrapperVhdl is

  --SCL tristate buffers
  signal PllRefScl_kz     : std_logic;
  signal PllRefSclOutOe_n : std_logic;
  signal PllRefSclOut     : std_logic;
  --SDA tristate buffers
  signal PllRefSdaOutOe_n : std_logic;
  signal PllRefSdaOut     : std_logic;
  
  -- Si5338LoaderWrapper verilog
  component Si5338LoaderWrapper is
  generic (
    g_ClkFrequency : integer := 100E6; -- Clock frequency
    g_I2CFrequency : integer := 1E5;   -- I2C frequency
    g_I2CSlaveAddr : integer := 112;   -- I2C slave address (Si5338=7'h70=112),
    g_RegFile      : string  := "./config/PllRef_GbtFpga_BstClkSch.dat"; --Registers map file (160MHz)
    g_NbRegs       : integer := 238;   -- Number of registers the file
    g_DlyBits      : integer := 16     -- Initialise chip 2**N clocks after reset
  );
  port (
    Clk_ik         : in    std_logic;
    Rst_ir         : in    std_logic;
    --Flags         
    Init_i         : in    std_logic;
    InitDone_o     : out   std_logic;
    --WB           
    WbAdr_ib3      : in    std_logic_vector(2 downto 0);
    WbDatMosi_ib8  : in    std_logic_vector(7 downto 0);
    WbDatMiso_ob8  : out   std_logic_vector(7 downto 0);
    WbCyc_i        : in    std_logic;
    WbStb_i        : in    std_logic;
    WbWe_i         : in    std_logic;
    WbAck_o        : out   std_logic;
    --I2C          
    Scl_i          : in    std_logic;
    Scl_o          : out   std_logic;
    SclOe_on       : out   std_logic;
    Sda_i          : in    std_logic;
    Sda_o          : out   std_logic;
    SdaOe_on       : out   std_logic
  );
  end component;
  
  

begin
  
  
  -----------------------
  -- Verilog Module
  -----------------------
  i_Si5338LoaderWrapper : Si5338LoaderWrapper
    generic map(
    g_ClkFrequency => g_ClkFrequency,                       -- integer := g_FCLK*1E6,                              -- Clock frequency
    g_I2CFrequency => g_I2CFrequency,                       -- integer := 1E5,                                     -- I2C frequency
    g_I2CSlaveAddr => to_integer(unsigned(g_I2CSlaveAddr)), -- integer := to_integer(x"70"),                       -- I2C slave address (Si5338=7'h70)
    g_RegFile      => g_RegFile,                            -- string  := "./config/PllRef_GbtFpga_BstClkSch.dat", --Registers map file (160MHz)
    g_NbRegs       => g_NbRegs,                             -- integer := 238,                                     -- Number of registers the file
    g_DlyBits      => g_DlyBits                             -- integer := 16                                       -- Initialise chip 2**N clocks after reset
  )       
  port map( 
    Clk_ik         => Clk_ik,          -- in    std_logic;
    Rst_ir         => Rst_ir,          -- in    std_logic;
    --Flags      
    Init_i         => Init_i,          -- in    std_logic;
    InitDone_o     => InitDone_o,      -- out   std_logic;
    --WB        
    WbAdr_ib3      => WbAdr_ib3,       -- in    std_logic_vector(2 downto 0);
    WbDatMosi_ib8  => WbDatMosi_ib8,   -- in    std_logic_vector(7 downto 0);
    WbDatMiso_ob8  => WbDatMiso_ob8,   -- out   std_logic_vector(7 downto 0);
    WbCyc_i        => WbCyc_i,         -- in    std_logic;
    WbStb_i        => WbStb_i,         -- in    std_logic;
    WbWe_i         => WbWe_i,          -- in    std_logic;
    WbAck_o        => WbAck_o,         -- out   std_logic;
    --I2C      
    Scl_i          => PllRefScl_kz,    -- in    std_logic;
    Scl_o          => PllRefSclOut,    -- out   std_logic;
    SclOe_on       => PllRefSclOutOe_n,-- out   std_logic;
    Sda_i          => Sda_io,          -- in    std_logic;
    Sda_o          => PllRefSdaOut,    -- out   std_logic;
    SdaOe_on       => PllRefSdaOutOe_n -- out   std_logic;
  );
  
  -----------------------
  -- Tristate buffers
  -----------------------
  PllRefScl_kz <= PllRefSclOut when PllRefSclOutOe_n='0' else 'Z';
  Scl_o        <= PllRefScl_kz;
  Sda_io       <= PllRefSdaOut when PllRefSdaOutOe_n='0' else 'Z';

end architecture;

