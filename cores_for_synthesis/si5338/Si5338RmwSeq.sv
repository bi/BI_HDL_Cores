//----------------------------------------------------------------------
// Title      : Si5338 RMW Sequencer
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : Si5338RmwSeq.sv
// Author     : T. Levens
// Company    : CERN SY-BI
//----------------------------------------------------------------------
// Description:
//
// Performs a read-modify-write operation that is required to program
// the Si5338.
//
// Separate read/write register addresses are provided to also support
// register copies.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module Si5338RmwSeq (
    input             Clk_ik,
    input             Rst_ir,

    input       [7:0] RdRegAddr_ib8,
    input       [7:0] WrRegAddr_ib8,
    input       [7:0] WrData_ib8,
    input       [7:0] WrMask_ib8,
    input             Start_i,
    output reg        Done_o,

    output reg        SeqStart_o,
    input             SeqBusy_i,
    input             SeqDone_i,
    input             SeqNack_i,
    input             SeqAl_i,

    output reg [31:0] SeqWrData_ob32,
    input      [31:0] SeqRdData_ib32,
    output reg  [3:0] SeqWrCnt_ob4,
    output reg  [3:0] SeqRdCnt_ob4
);

typedef enum {
    s_Idle,
    s_StartRead,
    s_WaitRead,
    s_StartWrite,
    s_WaitWrite
} t_State;

t_State State_l;

wire [7:0] RdData_b8 = SeqRdData_ib32[7:0];
wire [7:0] WrData_b8 = (WrData_ib8 & WrMask_ib8) | (RdData_b8 & ~WrMask_ib8);

always @(posedge Clk_ik) begin
    if (Rst_ir) begin
        State_l         <= s_Idle;
        SeqStart_o      <= 1'b0;
        Done_o          <= 1'b0;
        SeqWrData_ob32  <= 32'h0;
        SeqWrCnt_ob4    <= 4'd0;
        SeqRdCnt_ob4    <= 4'd0;
    end else begin
        SeqStart_o      <= 1'b0;
        Done_o          <= 1'b0;
        case (State_l)
            s_Idle: begin
                if (Start_i) begin
                    if (WrMask_ib8 == 8'hFF) begin
                        // No need to do a read in this case as we use
                        // all of the bits of the write data
                        State_l <= s_StartWrite;
                    end else begin
                        State_l <= s_StartRead;
                    end
                end
            end
            s_StartRead: begin
                SeqStart_o      <= 1'b1;
                SeqWrCnt_ob4    <= 4'd1;
                SeqRdCnt_ob4    <= 4'd1;
                SeqWrData_ob32  <= {24'h0, RdRegAddr_ib8};
                State_l         <= s_WaitRead;
            end
            s_WaitRead: begin
                if (SeqDone_i) begin
                    State_l     <= s_StartWrite;
                end
            end
            s_StartWrite: begin
                SeqStart_o      <= 1'b1;
                SeqWrCnt_ob4    <= 4'd2;
                SeqRdCnt_ob4    <= 4'd0;
                SeqWrData_ob32  <= {16'h0, WrData_b8, WrRegAddr_ib8};
                State_l         <= s_WaitWrite;
            end
            s_WaitWrite: begin
                if (SeqDone_i) begin
                    Done_o      <= 1'b1;
                    State_l     <= s_Idle;
                end
            end
            default: begin
                // Do nothing...
            end
        endcase
    end
end

endmodule
