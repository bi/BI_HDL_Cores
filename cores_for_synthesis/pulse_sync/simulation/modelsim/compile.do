# Defining intelFPGA paths:
set QUARTUS_II_PATH C:/intelFPGA/16.1/quartus
set INTELFPGA_COMPILED_LIBRARIES_PATH "C:/Simulation_Libraries/IntelFPGA/"

# Define here the paths used for the different folders:
set DUT_PATH ../../
set TB_PATH ../

# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"

###############################################
# Verilog Compile Process
###############################################

proc compile_verilog {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vlog -work $lib $name]
	}
}

###############################################
# VHDL Compile Process
###############################################

proc compile_vhdl {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vcom -work $lib $name]
	}
}

###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
if {![info exists sc]} {
    set sc 0
    set no_sc 1;
} 

if "$no_sc == 1" {
	echo "Smart compilation not possible or not enabled. Running full compilation..."
	# Deleting pre-existing libraries
	if {[file exists work]} {vdel -all -lib work}	
	# Creating and mapping working directory:
	vlib work
    vmap work work
    # Setting no_sc variable:
	set no_sc 0;
} else {
	echo "Smart compilation enabled. Only out of date files will be compiled..."
	puts [clock format $sc -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""
    
# DUT files:
compile_verilog $sc work $DUT_PATH/cores_for_synthesis/PulseSync.v

# Test Bench files:
compile_verilog $sc work $TB_PATH/tb_PulseSync.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

set top_level work.tb_PulseSync

###############################################
# Acquiring Compilation Time
###############################################

echo ""
set sc [clock seconds];
puts [clock format $sc -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

echo ""
echo "-> Compilation Done..."
echo ""
echo ""