onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Test Bench}
add wave -noupdate /tb_PulseSync/AppReset_iqr
add wave -noupdate /tb_PulseSync/a_GbtUserClk_k
add wave -noupdate /tb_PulseSync/a_Clk_k
add wave -noupdate -divider PulseSync
add wave -noupdate /tb_PulseSync/i_GbtResetSync/ClkIn_ik
add wave -noupdate /tb_PulseSync/i_GbtResetSync/PulseIn_i
add wave -noupdate /tb_PulseSync/i_GbtResetSync/ClkOut_ik
add wave -noupdate /tb_PulseSync/i_GbtResetSync/PulseOut_o
add wave -noupdate /tb_PulseSync/i_GbtResetSync/PulseIn_d
add wave -noupdate /tb_PulseSync/i_GbtResetSync/InPosEdge
add wave -noupdate /tb_PulseSync/i_GbtResetSync/InSRFF
add wave -noupdate /tb_PulseSync/i_GbtResetSync/ClrInSRFF
add wave -noupdate /tb_PulseSync/i_GbtResetSync/CrossingIO
add wave -noupdate /tb_PulseSync/i_GbtResetSync/CrossingOI
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 133
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2144800 ps}
