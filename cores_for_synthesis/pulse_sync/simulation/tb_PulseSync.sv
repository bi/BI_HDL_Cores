//============================================================================================//
//################################   Test Bench Information   ################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: tb_PulseSync.sv
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    <author>           <description>
//
// Language: SystemVerilog 2013
//
// Module Under Test: <Module Name (ModuleName.v)>
//
// Targeted device:
//
//     - Vendor: <FPGA manufacturer if vendor specific code>
//     - Model:  <FPGA Model if model specific>
//
// Description:
//
//     <Brief description of the test bench.>
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module tb_PulseSync;
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

reg PulseIn_q;           
reg ClkOut_k;         
reg ClkIn_k; 

//=====================================  Status & Control  ====================================//

//==== Clocks generation ====//

always #6 ClkOut_k = ~ClkOut_k; // Comment: 12ns period

always #4 ClkIn_k        = ~ClkIn_k; // Comment: 8ns period

//==== Stimulus & Display ====//

initial begin
    PulseIn_q = 1'b1;
    ClkOut_k  = 1'b1;
    ClkIn_k   = 1'b1;
    //--
    $display($time, " -> Simulation Start");
    #1000
    @(posedge ClkIn_k);
    PulseIn_q = 1'b0;
    $display($time, " -> Deassert Reset");
    repeat(1000)@(posedge ClkIn_k);
    #5000
    $display($time, " -> Simulation Stop");
    $stop;
end

//==== DUT ====//

PulseSync i_GbtResetSync (
    .ClkIn_ik   (ClkIn_k),
    .PulseIn_i  (PulseIn_q),
    .ClkOut_ik  (ClkOut_k),
    .PulseOut_o ());

endmodule