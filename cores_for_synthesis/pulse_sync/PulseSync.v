//----------------------------------------------------------------------
// Title      : Clock-Crossing Pulse Synchroniser
// Project    : Generic
//----------------------------------------------------------------------
// File       : PulseSync.v
// Author     : T. Levens
// Company    : CERN BE-BI-QP
// Created    : 2015-08-20
// Last update: 2015-08-20
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
// Synchronise a pulse between two clock domains.
//----------------------------------------------------------------------

`timescale 1ns/100ps

// Avoid Quartus warnings about ASYNC_REG attribute required for Vivado
/* altera message_off 10335 */

module PulseSync (
    input  ClkIn_ik,
    input  PulseIn_i,

    input  ClkOut_ik,
    output PulseOut_o
);

// Input edge detector
reg PulseIn_d = 1'b0;
always @(posedge ClkIn_ik) PulseIn_d <= PulseIn_i;
wire InPosEdge = PulseIn_i & ~PulseIn_d;

// Input SRFF
reg  InSRFF = 1'b0;
wire ClrInSRFF;

always @(posedge ClkIn_ik) begin
    if (InPosEdge)
        InSRFF <= 'b1;
    else if (ClrInSRFF)
        InSRFF <= 'b0;
end

// Clock crossing in->out
(* ASYNC_REG = "TRUE" *)
reg [2:0] CrossingIO = 3'h0;
always @(posedge ClkOut_ik) CrossingIO <= {CrossingIO[1:0], InSRFF};

// Clock crossing out->in
(* ASYNC_REG = "TRUE" *)
reg [1:0] CrossingOI = 2'h0;
always @(posedge ClkIn_ik) CrossingOI <= {CrossingOI[0], CrossingIO[1]};

assign ClrInSRFF = CrossingOI[1];

// Output edge detector
assign PulseOut_o = CrossingIO[1] & ~CrossingIO[2];

endmodule
