//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: DpramWbToGeneric.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 22/02/17      1.3          M. Barros Marin    Renamed do to dout in dpram.
//     - 15/12/16      1.2          M. Barros Marin    Added variable data width.             
//     - 10/06/16      1.1          M. Barros Marin    Fixed bug in WB write.               
//     - 01/02/16      1.0          M. Barros Marin    First module definition.               
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     DPRAM with Wishbone Write interface and Generic Read interface.                                                                                                     
//                                                                                                   
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps 

module DpramWbToGeneric 
//====================================  Global Parameters  ===================================//
#(  parameter g_AddrWidth = 10, g_DataWidth = 32)
//========================================  I/O ports  =======================================//
(

    //==== Wishbone interface (Read) ====//
    
    input                    Rst_ir,
    input                    Clk_ik,
    input                    Cyc_i,
    input                    Stb_i,
    input                    We_i,
    input  [g_AddrWidth-1:0] Adr_ib,
    input  [g_DataWidth-1:0] Dat_ib,
    output                   Ack_o,

    //==== Generic interface  (Write) ====//
    
    input                    RdClk_ik, 
    input                    RdRst_ir, 
    input                    RdEn_i,  
    input  [g_AddrWidth-1:0] RdAddr_ib,
    output [g_DataWidth-1:0] RdData_ib
    
); 
//=======================================  Declarations  =====================================//

//==== Wires & Regs ====//

wire WriteEn;        
    
//=======================================  User Logic  =======================================//

// Wishbone interface:
assign WriteEn = Cyc_i && Stb_i && We_i;
assign Ack_o   = Cyc_i && Stb_i;
    
// Generic DPRAM:
generic_dpram_mod #(
    .aw             (g_AddrWidth),
    .dw             (g_DataWidth))
i_Dpram (    
    .rclk           (RdClk_ik),  
    .rrst           (RdRst_ir),  
    .rce            (1'b1),   
    .oe             (RdEn_i),    
    .raddr          (RdAddr_ib), 
    .dout           (RdData_ib),    
    //---
    .wclk           (Clk_ik),  
    .wrst           (Rst_ir),  
    .wce            (1'b1),    
    .we             (WriteEn),    
    .waddr          (Adr_ib), 
    .di             (Dat_ib));

endmodule