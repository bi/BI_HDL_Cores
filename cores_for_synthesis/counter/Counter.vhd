--! @file
--! @brief Counter with overflow and set possibility

-- Jan Pospisil
--   2010; KP, FEL, CVUT
--   2015; CTP, ALICE, CERN
--   2016; BE-BI-QP, CERN

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief Counter with overflow and set possibility
--!
--! Counter with overflow. Width of the counter is specified by generic g_Width. Limit is specified
--! by generic g_Limit. If g_Limit is not set, 2**g_Width is used as a limit. Overflow_o is set when
--! value of the counter is limit-1 (i.e. signalizing that counter has reached its maximal value).
entity Counter is
    generic (
        --! width of the counter in bits
        g_Width: natural := 8;
        --! length of counting sequence
        g_Limit: integer := 0
    );
    port (
        --! clock input
        Clk_ik: in std_logic;
        --! synchronous reset
        Reset_ir: in std_logic;
        --! enables counting
        Enable_i: in std_logic;
        --! set actual value to the value presented at SetValue_ib
        Set_i: in std_logic;
        --! value to be set by Set_i
        SetValue_ib: in unsigned(g_Width-1 downto 0);
        --! overflow flag, set when Value_ob is maximal
        Overflow_o: out std_logic;
        --! actual value of the counter
        Value_ob: out unsigned(g_Width-1 downto 0)
    );
end entity;

architecture syn of Counter is

    function f_CalculateStopValue(g_Limit: integer; g_Width: natural) return unsigned is
        variable tmp: unsigned(g_Width downto 0);
    begin
        tmp := unsigned(to_signed(g_Limit-1, g_Width+1));
        -- to not display "vector truncated" warning
        return tmp(g_Width-1 downto 0);
    end function;

    --! overflow limit
    constant c_StopValue: unsigned(g_Width-1 downto 0) := f_CalculateStopValue(g_Limit, g_Width);

    --! actual value of the counter
    signal Value_b: unsigned(g_Width-1 downto 0) := (others => '0');
    
begin

    --! synchronous counter with synchronous reset
    pCounter: process (Clk_ik) is begin
        if rising_edge(Clk_ik) then
            if Reset_ir = '1' then
                Value_b <= (others => '0');
            else
                if Set_i = '1' then
                    Value_b <= SetValue_ib;
                elsif Enable_i = '1' then
                    Value_b <= Value_b + 1;
                    if Value_b = c_StopValue then
                        Value_b <= (others => '0');
                    end if;
                end if;
            end if;
        end if;
    end process pCounter;

    Value_ob <= Value_b;
    
    Overflow_o <=
        '1' when Value_b = c_StopValue else
        '0';

end architecture;