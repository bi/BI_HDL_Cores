--! @file
--! @brief Counter with overflow and directly specified limit

-- Jan Pospisil
--   2010; KP, FEL, CVUT
--   2015; CTP, ALICE, CERN
--   2016; BE-BI-QP, CERN

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! @brief Counter with overflow and directly specified limit
--!
--! Counter with overflow limit specified by generic g_Length (counts from 0 to g_Length-1). Counter
--! width is calculated automatically. Actual value is not outputted and set feature cannot be used.

-- Why value isn't outputted? We don't know WIDTH when instantiation CounterLength, thus output port
-- with value has to be unconstrained port. We also cannot connect anything to this port, because we
-- don't know its size. And "open" unconstrained port is not allowed in VHDL.
-- Same goes for set feature.
entity CounterLength is
    generic (
        --! length of counting sequence
        g_Length: positive := 256
    );
    port (
        --! clock input
        Clk_ik: in std_logic;
        --! synchronous reset
        Reset_ir: in std_logic;
        --! enables counting
        Enable_i: in std_logic;
        --! overflow flag, set when value of the counter is maximal
        Overflow_o: out std_logic := '0'
    );
end entity;

architecture syn of CounterLength is

    --! computes least possible width of the vector to store value X
    -- http://stackoverflow.com/a/12751341/615627
    function f_log2(x: natural) return natural is
        variable i: natural;
    begin
        i := 0;
        while (2**i < x) loop
            i := i + 1;
        end loop;
        return i;
    end function;
    
    --! width of the vector needed for storing actual counter value
    constant c_Width: natural := f_log2(g_Length);
    
begin

    cCounter: entity work.Counter(syn)
        generic map (
            g_Width => c_Width,
            g_Limit => g_Length
        )
        port map (
            Clk_ik => Clk_ik,
            Reset_ir => Reset_ir,
            Enable_i => Enable_i,
            Set_i => '0',
            SetValue_ib => (others => '0'),
            Overflow_o => Overflow_o,
            Value_ob => open
        );

end architecture;