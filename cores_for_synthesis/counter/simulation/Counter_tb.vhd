library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all; 

library std;
use std.env.all;

entity Counter_tb is
end entity;

architecture testbench_width of Counter_tb is

    constant c_ClkPeriod: time := 50 ns;
    constant c_Width: natural := 8;
    
    signal Clk, Reset, Enable, Overflow: std_logic;
    signal Value_b: unsigned(c_Width-1 downto 0);
    
    procedure f_Tick(ticks: in natural) is begin
        wait for ticks * c_ClkPeriod;
    end procedure;

begin

    cDUT: entity work.Counter(syn)
        generic map (
            g_Width => c_Width
        )
        port map (
            Clk_ik => Clk,
            Reset_ir => Reset,
            Enable_i => Enable,
            Set_i => '0',
            SetValue_ib => (others => '0'),
            Overflow_o => Overflow,
            Value_ob => Value_b
        );

    pClk: process is begin
        Clk <= '0';
        wait for c_ClkPeriod/2;
        Clk <= '1';
        wait for c_ClkPeriod/2;
    end process;
    
    pTest: process is begin
        Reset <= '1';
        Enable <= '0';
        f_Tick(5);
        Reset <= '0';
        f_Tick(1);
        assert Overflow = '0'
            report "OVERFLOW asserted at the beginning!"
            severity failure;
        assert Value_b = 0
            report "Not zero at the beginning!"
            severity failure;

        Enable <= '1';
        for i in 1 to (2**c_Width)-3 loop
            f_Tick(1);
            assert Overflow = '0'
                report "OVERFLOW asserted!"
                severity failure;
            assert Value_b = i
                report "Bad VALUE!"
                severity failure;
        end loop;
        
        f_Tick(1);
        assert Overflow = '0'
            report "OVERFLOW asserted at the end!"
            severity failure;
        assert Value_b = 2**c_Width-2
            report "Bad VALUE near the end!"
            severity failure;

        f_Tick(1);
        assert Overflow = '1'
            report "OVERFLOW not asserted at the overflow!"
            severity failure;
        assert Value_b = 2**c_Width-1
            report "Bad VALUE at the end!"
            severity failure;

        f_Tick(1);
        assert Overflow = '0'
            report "OVERFLOW too long!"
            severity failure;
        assert Value_b = 0
            report "VALUE didn't overflow correctly!"
            severity failure;

        assert false report "NONE. End of simulation." severity failure;
        wait;
    end process;

end architecture;

architecture testbench_length of Counter_tb is

    constant c_ClkPeriod: time := 50 ns;
    constant c_Width: natural := 5;
    constant c_Length: natural := 2**c_Width -0; -- can be minus something to test more cases
    
    signal Clk, Reset, Enable, Overflow: std_logic;

    procedure f_Tick(ticks: in natural) is begin
        wait for ticks * c_ClkPeriod;
    end procedure;

begin

    cDUT: entity work.CounterLength(syn)
        generic map (
            g_Length => c_Length
        )
        port map (
            Clk_ik => Clk,
            Reset_ir => Reset,
            Enable_i => Enable,
            Overflow_o => Overflow
        );

    pClk: process is begin
        Clk <= '0';
        wait for c_ClkPeriod/2;
        Clk <= '1';
        wait for c_ClkPeriod/2;
    end process;
    
    pTest: process is begin
        Reset <= '1';
        Enable <= '0';
        f_Tick(5);
        Reset <= '0';
        f_Tick(1);
        assert Overflow = '0'
            report "OVERFLOW asserted at the beginning!"
            severity failure;

        Enable <= '1';
        for i in 1 to c_Length-3 loop
            f_Tick(1);
            assert Overflow = '0'
                report "OVERFLOW asserted!"
                severity failure;
        end loop;
        
        f_Tick(1);
        assert Overflow = '0'
            report "OVERFLOW asserted at the end!"
            severity failure;

        f_Tick(1);
        assert Overflow = '1'
            report "OVERFLOW not asserted at the overflow!"
            severity failure;

        f_Tick(1);
        assert Overflow = '0'
            report "OVERFLOW too long!"
            severity failure;

        assert false report "NONE. End of simulation." severity failure;
        wait;
    end process;

end architecture;