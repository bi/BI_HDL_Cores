//----------------------------------------------------------------------
// Title      : IIR of order 1 with runtime Programmable Bandwidth
//----------------------------------------------------------------------
// File       : IirOrder1Prog.v
// Author     : A. Boccardi
// Standard   : SystemVerilog
//----------------------------------------------------------------------
// Description:
// This module implements the following formula:
//
//              X
//   Y = -----------------
//        2^N-(2^N-1)Z^-1
//
// After each reset the filter is load with the 1st input received
// Y and X are considered signed in 2's complements
// N can be changed at run time
// The equivalent analogue BW is Fs/((2*Pi)*(2**N)) where Fs is
// the sampling frequency
//----------------------------------------------------------------------

`timescale 1ns/100ps

module IirOrder1Prog
// Parameters
#(  int  g_InputBits             =             8  ,
    int  g_OutputBits            =            16  ,
    int  g_Lg2ProgBws            =             3  ,
    int  g_N_m [2**g_Lg2ProgBws] = '{0, 3, 7, 10, 11, 12, 13, 14} )
// Interface
(   input                               Clk_ik        ,
    input                               Rst_ir        ,
    input         [g_Lg2ProgBws - 1: 0] BwSelector_ib ,
    input                               NewDataIn_i   ,
    input                               Restart_i     ,
    input signed  [g_InputBits  - 1: 0] Input_ib      ,
    output reg                          NewDataOut_o  ,
    output signed [g_OutputBits - 1: 0] Output_ob     );

// Declarations: Constants
localparam c_MaxShift   = g_N_m[2**g_Lg2ProgBws-1],
           c_FilterBits = g_OutputBits + c_MaxShift + 1; // +1 for the intermediate stages

// Declarations: Signals
logic signed [c_FilterBits - 1 : 0] FilterValue_qb = 'h0;
logic signed [c_FilterBits - 1 : 0] ExtendedInput_b;
logic                               FirstSample = 1'b1;

// Logic
assign ExtendedInput_b = (Input_ib <<<(c_FilterBits-g_InputBits))>>>1;

always_ff @(posedge Clk_ik)
    if (Rst_ir) begin
        NewDataOut_o      <= #1 1'b0;
        FilterValue_qb    <= #1 'h0;
        FirstSample       <= #1 1'b1;
    end else begin
        NewDataOut_o <= #1 NewDataIn_i;
        if (NewDataIn_i) begin
            FirstSample <= #1 1'b0;
            if (FirstSample || Restart_i)
                FilterValue_qb <= #1 ExtendedInput_b;
            else
                FilterValue_qb <= #1 FilterValue_qb+((ExtendedInput_b-FilterValue_qb)>>>g_N_m[BwSelector_ib]);
        end
    end

assign Output_ob = FilterValue_qb[c_FilterBits - 2 : c_MaxShift];

endmodule




