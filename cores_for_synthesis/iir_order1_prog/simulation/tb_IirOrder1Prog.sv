//-------------------   Test Bench Information   -----------------------
//
// Company: CERN (BE-BI)
//
// File Name: tb_IirOrder1Prog.v
//
// Language: SystemVerilog 2009
//
//----------------------------------------------------------------------

`timescale 1ns/100ps

module tb_IirOrder1Prog;

parameter int c_Lg2ProgBws = 2;
parameter int c_N_m [2**c_Lg2ProgBws] = '{10, 12, 14, 16};
parameter int c_InputBits = 11;
parameter int c_OutputBits = 11;

reg Clk_k = 0;
logic Rst_r = 0;
logic [c_Lg2ProgBws-1 :0] BwSelector_b = 0;
logic DavIn = 0;
logic [c_InputBits-1 :0] Input_b = 0;
logic DavOut;
logic [c_OutputBits-1 :0] Output_b;
longint RiseTime = 0;
int ExpectedRiseTime;
int RelativeError;

always # 1 Clk_k = ~Clk_k;

initial begin
    $display("-----------------------------------");
    $display("Starting the test");
    $display("-----------------------------------");
    for (int BwIndex = 0; BwIndex <2**c_Lg2ProgBws; BwIndex++) begin
        $display("Testing the BW of index %d: N = %d).", BwIndex, 2**c_N_m[BwIndex]);
        RiseTime=0;
        BwSelector_b = BwIndex;
        @(posedge Clk_k) Rst_r = 1'b1;
        @(posedge Clk_k) Rst_r = 1'b0;
        @(posedge Clk_k) begin
            Input_b = 'd5;
            DavIn   = 1'b1;
        end
        @(posedge Clk_k) DavIn   = 1'b0;
        repeat(10) @(posedge Clk_k);
        @(posedge Clk_k) begin
            Input_b = 'd1000;
            DavIn   = 1'b1;
        end
        do begin
            @(posedge Clk_k);
            if (DavOut && (Output_b>100) && (Output_b<900))
                RiseTime = RiseTime + 1;
        end while(Output_b<990);
        @(posedge Clk_k) DavIn   = 1'b0;
        repeat(10) @(posedge Clk_k);
        ExpectedRiseTime = 22*2**c_N_m[BwIndex]/10; // It should be ~2.2*2**N
        RelativeError = (ExpectedRiseTime-RiseTime)*1000/ExpectedRiseTime;
        if (RelativeError<0) RelativeError = -RelativeError;
        if (RelativeError>10)
            $error("Rise time relative error %d per 1000: expected rise time %d, measured %d", RelativeError, ExpectedRiseTime, RiseTime);
        else
            $display("OK: relative error %d per 1000", RelativeError);
    end
    $stop();
end

//==== DUT ====//

IirOrder1Prog
#(  .g_InputBits(c_InputBits),
    .g_OutputBits(c_OutputBits),
    .g_Lg2ProgBws(c_Lg2ProgBws),
    .g_N_m(c_N_m))
    i_DUT
    (   .Clk_ik        (Clk_k),
        .Rst_ir        (Rst_r),
        .BwSelector_ib (BwSelector_b),
        .NewDataIn_i   (DavIn),
        .Restart_i     (1'b0),
        .Input_ib      (Input_b),
        .NewDataOut_o  (DavOut),
        .Output_ob     (Output_b));

endmodule
