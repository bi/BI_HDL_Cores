`timescale 1ns/100ps 

// Added by M. Barros Marin (11/04/17)

module Generic32InputRegs (
	input	          Clk_ik,
    input             Rst_irq,
    input             Cyc_i,
    input             Stb_i,
    input      [ 4:0] Adr_ib5,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,
    input      [31:0] Reg0Value_ib32,
    input      [31:0] Reg1Value_ib32,
    input      [31:0] Reg2Value_ib32,
    input      [31:0] Reg3Value_ib32,
    input      [31:0] Reg4Value_ib32,
    input      [31:0] Reg5Value_ib32,
    input      [31:0] Reg6Value_ib32,
    input      [31:0] Reg7Value_ib32,
    input      [31:0] Reg8Value_ib32,
    input      [31:0] Reg9Value_ib32,
    input      [31:0] Reg10Value_ib32,
    input      [31:0] Reg11Value_ib32,
    input      [31:0] Reg12Value_ib32,
    input      [31:0] Reg13Value_ib32,
    input      [31:0] Reg14Value_ib32,
    input      [31:0] Reg15Value_ib32,
    input      [31:0] Reg16Value_ib32,
    input      [31:0] Reg17Value_ib32,
    input      [31:0] Reg18Value_ib32,
    input      [31:0] Reg19Value_ib32,
    input      [31:0] Reg20Value_ib32,
    input      [31:0] Reg21Value_ib32,
    input      [31:0] Reg22Value_ib32,
    input      [31:0] Reg23Value_ib32,
    input      [31:0] Reg24Value_ib32,
    input      [31:0] Reg25Value_ib32,
    input      [31:0] Reg26Value_ib32,
    input      [31:0] Reg27Value_ib32,
    input      [31:0] Reg28Value_ib32,
    input      [31:0] Reg29Value_ib32,
    input      [31:0] Reg30Value_ib32,
    input      [31:0] Reg31Value_ib32
);
	
always @(posedge Clk_ik)
    if (Rst_irq) begin
        Dat_oab32                  <= #1 32'h0;
        Ack_oa                     <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib5)
                5'b00000: Dat_oab32 <= #1 Reg0Value_ib32;
                5'b00001: Dat_oab32 <= #1 Reg1Value_ib32;
                5'b00010: Dat_oab32 <= #1 Reg2Value_ib32;
                5'b00011: Dat_oab32 <= #1 Reg3Value_ib32;
                5'b00100: Dat_oab32 <= #1 Reg4Value_ib32;
                5'b00101: Dat_oab32 <= #1 Reg5Value_ib32;
                5'b00110: Dat_oab32 <= #1 Reg6Value_ib32;
                5'b00111: Dat_oab32 <= #1 Reg7Value_ib32;
                5'b01000: Dat_oab32 <= #1 Reg8Value_ib32;
                5'b01001: Dat_oab32 <= #1 Reg9Value_ib32;
                5'b01010: Dat_oab32 <= #1 Reg10Value_ib32;
                5'b01011: Dat_oab32 <= #1 Reg11Value_ib32;
                5'b01100: Dat_oab32 <= #1 Reg12Value_ib32;
                5'b01101: Dat_oab32 <= #1 Reg13Value_ib32;
                5'b01110: Dat_oab32 <= #1 Reg14Value_ib32;
                5'b01111: Dat_oab32 <= #1 Reg15Value_ib32;
                5'b10000: Dat_oab32 <= #1 Reg16Value_ib32;
                5'b10001: Dat_oab32 <= #1 Reg17Value_ib32;
                5'b10010: Dat_oab32 <= #1 Reg18Value_ib32;
                5'b10011: Dat_oab32 <= #1 Reg19Value_ib32;
                5'b10100: Dat_oab32 <= #1 Reg20Value_ib32;
                5'b10101: Dat_oab32 <= #1 Reg21Value_ib32;
                5'b10110: Dat_oab32 <= #1 Reg22Value_ib32;
                5'b10111: Dat_oab32 <= #1 Reg23Value_ib32;
                5'b11000: Dat_oab32 <= #1 Reg24Value_ib32;
                5'b11001: Dat_oab32 <= #1 Reg25Value_ib32;
                5'b11010: Dat_oab32 <= #1 Reg26Value_ib32;
                5'b11011: Dat_oab32 <= #1 Reg27Value_ib32;
                5'b11100: Dat_oab32 <= #1 Reg28Value_ib32;
                5'b11101: Dat_oab32 <= #1 Reg29Value_ib32;
                5'b11110: Dat_oab32 <= #1 Reg30Value_ib32;
                5'b11111: Dat_oab32 <= #1 Reg31Value_ib32;
                default: Dat_oab32 <= #1 Reg0Value_ib32;
            endcase
        end
        Ack_oa                     <= #1 Stb_i && Cyc_i;
    end

endmodule