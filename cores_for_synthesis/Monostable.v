//----------------------------------------------------------------------
// Title      : Monostable
// Project    : Generic
//----------------------------------------------------------------------
// File       : Monostable.v
// Author     : T. Levens
// Company    : CERN BE-BI-QP
// Created    : 2015-06-24
// Last update: 2016-06-21
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
// Monostable output.
//
// Based on the BTF monostable but updated to be fully synchronous and
// to remove a spurious pulse generated at reset.
//
// Now can change output pulse width with port Width_ib.
//----------------------------------------------------------------------

`timescale 1ns/1ns

module Monostable #(parameter g_CounterBits = 32) (
    input   Input_i,
    input   Clk_ik,
    input   [g_CounterBits-1:0] Width_ib,
    output  reg Output_oq
);

localparam dly = 1;

reg [g_CounterBits-1:0] Counter_c = 'b0;

always @(posedge Clk_ik) begin
    if (Input_i)
        Counter_c <= #dly Width_ib;
    else if (|Counter_c)
        Counter_c <= #dly Counter_c - 1'b1;
end

always @(posedge Clk_ik) Output_oq <= #dly |Counter_c;

endmodule
