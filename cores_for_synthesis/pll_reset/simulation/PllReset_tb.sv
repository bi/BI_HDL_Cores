module PllReset_tb;

    timeunit 1ns / 1ps;

    localparam int g_PllCount = 1;
    localparam longint unsigned g_ClkFrequency = 125e6; // in [Hz]
    localparam real g_PllTimeout[g_PllCount] = {1.5e-6}; // in [s]

    logic Clk_ik;
    logic [g_PllCount-1:0] PllLocked_iab;
    logic [g_PllCount-1:0] PllReset_ob;
    
    logic ClockEnable;
    
    ClockGenerator #(
        .g_Frequency(g_ClkFrequency)
    ) i_ClkGenerator (
        .Clk_ok(Clk_ik),
        .Enable_i(ClockEnable)
    );

    PllReset #(
        .g_PllCount(g_PllCount),
        .g_ClkFrequency(g_ClkFrequency),
        .g_PllTimeout(g_PllTimeout)
    ) i_Dut (
        .*
    );
    
    initial begin
        // initial reset
        PllLocked_iab = 0;
        ClockEnable = 1;
        @(posedge Clk_ik);
        assert(PllReset_ob == 1);
        #1400;
        assert(PllReset_ob == 1);
        #200
        assert(PllReset_ob == 0);
        PllLocked_iab = 1;
        #500;
        assert(PllReset_ob == 0);
        // PLL locked lost
        PllLocked_iab = 0;
        #200
        assert(PllReset_ob == 1);
        #1200
        assert(PllReset_ob == 1);
        #200
        assert(PllReset_ob == 0);
        PllLocked_iab = 1;
        #500;
        // clock lost
        ClockEnable = 0;
        #100
        PllLocked_iab = 0;
        #100
        ClockEnable = 1;
        #200
        assert(PllReset_ob == 1);
        #1200
        assert(PllReset_ob == 1);
        #200
        assert(PllReset_ob == 0);
        PllLocked_iab = 1;
        #500;
        $stop();
    end
       
endmodule