set CORES_SYN ../../..
set CORES_SIM ../../../../cores_for_simulation
set TB PllReset_tb

if {[file exists work]} {vdel -all -lib work} 
vlib work

vcom -reportprogress 300 -work work $CORES_SYN/SignalSyncer.vhd 
vcom -reportprogress 300 -work work $CORES_SYN/EdgeDetector.vhd 
vcom -reportprogress 300 -work work $CORES_SYN/counter/Counter.vhd 
vcom -reportprogress 300 -work work $CORES_SYN/counter/CounterLength.vhd 
vlog -reportprogress 300 -work work $CORES_SYN/pll_reset/PllReset.sv 
vlog -reportprogress 300 -work work $CORES_SIM/ClockGenerator.sv
vlog -reportprogress 300 -work work ../$TB.sv

vopt work.$TB +acc -o _design_optimized

vsim -gui _design_optimized

add wave -noupdate sim:/$TB/*
add wave -noupdate -expand -group DUT sim:/$TB/i_Dut/*

configure wave -signalnamewidth 1

run -all

wave zoom full