//============================================================================================
//##################################   Module Information   ##################################
//============================================================================================
//
// Company: CERN (BE-BI)
//
// File Name: PllReset.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                            DESCRIPTION
//  05.12.2017  1        J. Pospisil <j.pospisil@cern.ch>  first version
//
// Description:
//
//  Automatic PLL Reseting Logic
//  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//
//  Parameters:
//    g_PllCount - number of PLLs to be guarded
//    g_ClkFrequency - frequency of Clk_ik clock, in [Hz]
//    g_PllCount - array of PLL reset timeouts, in [s] (one for each PLL)
//                 i.e. minimal reset pulse width
//
//  Clock supplied to Clk_ik pin has to be independent of the PLL to be guarded, so PLL output
//  clocks CANNOT be used. You can use PLL input clock, though.
//
//============================================================================================
//############################################################################################
//============================================================================================

module PllReset #(
    parameter int g_PllCount,
    parameter longint unsigned g_ClkFrequency,
    parameter real g_PllTimeout[g_PllCount]
) (
    input Clk_ik,
    input [g_PllCount-1:0] PllLocked_iab,
    output [g_PllCount-1:0] PllReset_ob
);

    function integer rtoi(integer x); // to supplement Quartus not fully supporting SV 
        rtoi = x;
    endfunction

    logic [g_PllCount-1:0] PllLocked_b; // in Clk_ik clock domain

    SignalSyncer #(
        .g_Width(g_PllCount),
        .g_Delay(2)
    ) i_PllLockedSyncer (
        .Clk_ik(Clk_ik),
        .Data_ib(PllLocked_iab),
        .Data_ob(PllLocked_b)
    );

    genvar pll;
    
    generate
        for (pll = 0; pll < g_PllCount; pll++) begin: gen_Pllreset
            localparam real c_R = g_PllTimeout[pll]; // to supplement Quartus not fully supporting SV 
            
            logic PllLockedLost;
            logic TimeoutOverflow;
            logic Reset;
            
            EdgeDetector i_PllLockedDetector (
                .Clk_ik(Clk_ik),
                .Signal_i(PllLocked_b[pll]),
                .Edge_o(),
                .RisingEdge_o(),
                .FallingEdge_o(PllLockedLost)
            );
        
            CounterLength #(
                .g_Length(rtoi(c_R*g_ClkFrequency))
            ) i_TimeoutCounter (
                .Clk_ik(Clk_ik),
                .Reset_ir(1'b0),
                .Enable_i(PllLockedLost | ~TimeoutOverflow),
                .Overflow_o(TimeoutOverflow)
            );
            
            assign Reset = ~TimeoutOverflow;
            assign PllReset_ob[pll] = Reset;
        end
    endgenerate

endmodule