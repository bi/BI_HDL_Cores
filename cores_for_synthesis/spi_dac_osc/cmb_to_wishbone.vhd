--============================================================================================
--##################################   Module Information   ##################################
--============================================================================================
--
-- Company: CERN (BE-BI-BL)
--
-- Language: VHDL 2008
--
-- Description:
--
--     CMB to Wishbone bridge
--
--============================================================================================


-------------------------------------------------------------------------------
-- Libraries 
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-----------------------------------------------------------------------
-- Entity
-----------------------------------------------------------------------
entity cmb_to_wishbone is
  port (
    -- Clock and reset
    clk_ik              : in  std_logic;
    rst_ir              : in  std_logic;
    cen_ie              : in  std_logic;
    -- CMB input
    cmb_frame_i         : in  std_logic;
    cmb_addr_ib         : in  std_logic_vector(32-1 downto 0);
    cmb_enable_ib       : in  std_logic_vector(32-1 downto 0);
    cmb_wr_i            : in  std_logic;
    cmb_wr_data_ib      : in  std_logic_vector(32-1 downto 0);
    cmb_rd_i            : in  std_logic;
    cmb_rd_data_ob      : out std_logic_vector(32-1 downto 0); 
    cmb_rd_valid_o      : out std_logic;
    cmb_busy_o          : out std_logic;
    -- WB output
    wb_cyc_o            : out std_logic;
    wb_stb_o            : out std_logic;
    wb_addr_ob          : out std_logic_vector(32-1 downto 0);
    wb_we_o             : out std_logic;
    wb_data_mosi_ob     : out std_logic_vector(32-1 downto 0);
    wb_data_miso_ib     : in  std_logic_vector(32-1 downto 0);
    wb_ack_i            : in  std_logic
  );
end entity;




-----------------------------------------------------------------------
-- Architecture
-----------------------------------------------------------------------
architecture rtl of cmb_to_wishbone is
  
  -- CMB 
  signal cmb_rd_data       : std_logic_vector(32-1 downto 0); 
  signal cmb_rd_valid      : std_logic;
  signal cmb_busy          : std_logic;
  
  -- WB 
  signal wb_cyc            : std_logic;
  signal wb_stb            : std_logic;
  signal wb_addr           : std_logic_vector(32-1 downto 0);
  signal wb_we             : std_logic;
  signal wb_data_mosi      : std_logic_vector(32-1 downto 0);
  
  -- Status
  signal ongoing_access    : std_logic;

begin


--=============================================================================
-- CMB <-> Wishbone
--=============================================================================  
p_bridge: process(clk_ik, rst_ir)
  begin
    if rst_ir = '1'then
      --CMB       
      cmb_rd_data      <= (others=>'0');
      cmb_rd_valid     <= '0';   
      cmb_busy         <= '0'; 
      --WB
      wb_cyc           <= '0';             
      wb_stb           <= '0';          
      wb_addr          <= (others=>'0');
      wb_we            <= '0'; 
      wb_data_mosi     <= (others=>'0');
      --status
      ongoing_access   <= '0';
      
    elsif rising_edge(clk_ik) then
      if cen_ie='1' then
        --========================
        --read request
        if cmb_rd_i='1' and cmb_busy='0' then
          --CMB
          cmb_rd_data      <= (others=>'0');
          cmb_rd_valid     <= '0';   
          cmb_busy         <= '1';
          --WB
          wb_cyc           <= '1';             
          wb_stb           <= '1';          
          wb_addr          <= cmb_addr_ib;
          wb_we            <= '0'; 
          wb_data_mosi     <= (others=>'0');
          --status
          ongoing_access   <= '1';
        --========================
        --write request
        elsif cmb_wr_i='1' and cmb_busy='0' then
          --CMB
          cmb_rd_data      <= (others=>'0');
          cmb_rd_valid     <= '0';   
          cmb_busy         <= '1';
          --WB
          wb_cyc           <= '1';             
          wb_stb           <= '1';          
          wb_addr          <= cmb_addr_ib;
          wb_we            <= '1'; 
          wb_data_mosi     <= cmb_wr_data_ib;
          --status
          ongoing_access   <= '1';
        --========================
        --access ongoing
        elsif ongoing_access='1' and cmb_busy='1' then
          if wb_ack_i='1' then 
            --CMB
            cmb_rd_data      <= wb_data_miso_ib;
            cmb_rd_valid     <= wb_cyc and wb_stb and not(wb_we);   
            cmb_busy         <= '1'; --access not released
            --WB
            wb_cyc           <= '0';             
            wb_stb           <= '0';          
            wb_addr          <= (others=>'0');
            wb_we            <= '0'; 
            wb_data_mosi     <= (others=>'0');
            --status
            ongoing_access   <= '0';
          end if;
        --========================
        --no access
        else
          --CMB       
          cmb_rd_data      <= (others=>'0');
          cmb_rd_valid     <= '0'; 
          --end of the access
          if wb_ack_i='0' then 
            cmb_busy       <= '0'; 
          end if;
          --WB
          wb_cyc           <= '0';             
          wb_stb           <= '0';          
          wb_addr          <= (others=>'0');
          wb_we            <= '0'; 
          wb_data_mosi     <= (others=>'0');
        end if;
      end if;
    end if;
  end process;
     
  
  
--=============================================================================
-- Output assignments
--=============================================================================  +
  --CMB       
  cmb_rd_data_ob     <= cmb_rd_data;
  cmb_rd_valid_o     <= cmb_rd_valid;
  cmb_busy_o         <= cmb_busy    ;
  --WB
  wb_cyc_o           <= wb_cyc      ;
  wb_stb_o           <= wb_stb      ;
  wb_addr_ob         <= wb_addr     ;
  wb_we_o            <= wb_we       ;
  wb_data_mosi_ob    <= wb_data_mosi;

  
end architecture;
