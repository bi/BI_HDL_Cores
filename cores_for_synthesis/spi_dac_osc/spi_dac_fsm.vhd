--============================================================================================
--##################################   Module Information   ##################################
--============================================================================================
--
-- Company: CERN (BE-BI-BL)
--
-- Language: VHDL 2008
--
-- Description:
--
--     FSM to program oscillator through SPI
--
--============================================================================================



-------------------------------------------------------------------------------
-- Libraries definition
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


--=============================================================================
-- Entity declaration 
--=============================================================================
entity spi_dac_fsm is
  port (                
    clk_ik              : in std_logic;
    rst_ir              : in std_logic;
    cen_ie              : in std_logic;
    
    new_val_i           : in std_logic;
    --0V0 is the midscale ( 20.000MHz) for the OSC3
    dac20_val_i         : in std_logic_vector(15 downto 0); --0x0000=0d00000=0V0 for midrange
    --1V6 is the midscale (125.000MHz) for the OSC2
    dac25_val_i         : in std_logic_vector(15 downto 0); --0x51eb=0d20971=1V6 for midrange
    
    -- CMB sequence to SPI protocol 
    cmb_frame_o         : out std_logic;
    cmb_addr_ob         : out std_logic_vector(32-1 downto 0);
    cmb_enable_ob       : out std_logic_vector(32-1 downto 0);
    cmb_wr_o            : out std_logic;
    cmb_wr_data_ob      : out std_logic_vector(32-1 downto 0);
    cmb_rd_o            : out std_logic;
    cmb_rd_data_ib      : in  std_logic_vector(32-1 downto 0); 
    cmb_rd_valid_i      : in  std_logic;
    cmb_busy_i          : in  std_logic
  );
end entity;




--=============================================================================
-- Architecture declaration
--=============================================================================
architecture rtl of spi_dac_fsm is

-----------------------------------------------------------------------
-- Type declaration
-----------------------------------------------------------------------
type vadc_array is array(7 downto 0) of std_logic_vector(11 downto 0);  


-----------------------------------------------------------------------
-- Constant declaration
-----------------------------------------------------------------------

-----------------------------------------------------------------------
-- Signals declaration
-----------------------------------------------------------------------
  --wait
  signal first_prog   : std_logic;
  
  --cmb
  signal cmb_frame    : std_logic;
  signal cmb_addr     : std_logic_vector(32-1 downto 0);
  signal cmb_enable   : std_logic_vector(32-1 downto 0);
  signal cmb_wr       : std_logic;
  signal cmb_wr_data  : std_logic_vector(32-1 downto 0);
  signal cmb_rd       : std_logic;
  
  -- Fsm  
  Type t_state is (
    s_IDLE,
    s_SET_DAC25,   --1V6 is the midscale (125.000MHz) for the OSC2
    s_SET_DAC20    --0V0 is the midscale ( 20.000MHz) for the OSC3
  );
  attribute SYN_ENCODING : STRING;
  attribute SYN_ENCODING of t_state : type is "one-hot, safe";
  signal curr_state: t_state;

  
begin


--=============================================================================
-- FSM 
--=============================================================================
  --state flow process (synchronous)
  p_protocol_fsm_flow: process (rst_ir, clk_ik)
  begin
    if rst_ir = '1' then
      curr_state   <= s_IDLE;
      
      first_prog   <='1';
      
      --CMB    
      cmb_frame    <= '0';
      cmb_addr     <= (others=>'0');
      cmb_enable   <= (others=>'1');
      cmb_wr       <= '0';
      cmb_wr_data  <= (others=>'0');
      cmb_rd       <= '0';
            
    elsif rising_edge(clk_ik) then
      if cen_ie = '1' then
      
        --default
        cmb_frame       <= '0';
        cmb_enable      <= (others=>'1');
        
        case curr_state is
                  
          -- START or STOP the programming sequence
          when s_IDLE =>
            if new_val_i='1' or first_prog='1' then 
              curr_state <= s_SET_DAC25;
              first_prog <='0';
            end if;
                    
          -- set DAC25 to 1V6
          when s_SET_DAC25 =>
            if cmb_busy_i='0' then              --bus is free
              if cmb_wr='0' and cmb_rd='0' then --no command on-going
                cmb_addr     <= x"00000000";    --DAC25
                cmb_wr       <= '1';                  
                cmb_wr_data  <= x"0000"& dac25_val_i;
                cmb_rd       <= '0';
              elsif cmb_wr='1' then             --write is acknowledged
                cmb_addr     <= (others=>'0');
                cmb_wr       <= '0';
                cmb_wr_data  <= (others=>'0');
                cmb_rd       <= '0';
                curr_state   <= s_SET_DAC20;
              end if;
            end if;
            
          -- set DAC20 to 0V
          when s_SET_DAC20 =>
            if cmb_busy_i='0' then              --bus is free
              if cmb_wr='0' and cmb_rd='0' then --no command on-going
                cmb_addr     <= x"00000001";    --DAC20
                cmb_wr       <= '1';                    
                cmb_wr_data  <= x"0000"& dac20_val_i;
                cmb_rd       <= '0';
              elsif cmb_wr='1' then             --write is acknowledged
                cmb_addr     <= (others=>'0');
                cmb_wr       <= '0';
                cmb_wr_data  <= (others=>'0');
                cmb_rd       <= '0';
                curr_state   <= s_IDLE;
              end if;
            end if;
                  
          -- OTHERS
          when others =>
            curr_state    <= s_IDLE;
            
        end case;
      end if;
    end if;
  end process;

--cmb
cmb_frame_o    <= cmb_frame   ;
cmb_addr_ob    <= cmb_addr    ;
cmb_enable_ob  <= cmb_enable  ;
cmb_wr_o       <= cmb_wr      ;
cmb_wr_data_ob <= cmb_wr_data ;
cmb_rd_o       <= cmb_rd      ;
  
end architecture;
