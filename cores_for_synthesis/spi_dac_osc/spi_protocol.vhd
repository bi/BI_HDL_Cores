--============================================================================================
--##################################   Module Information   ##################################
--============================================================================================
--
-- Company: CERN (BE-BI-BL)
--
-- Language: VHDL 2008
--
-- Description:
--
--     Simple SPI protocol
--
--============================================================================================


-------------------------------------------------------------------------------
-- Libraries definition
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

--=============================================================================
-- Entity declaration 
--=============================================================================
entity spi_protocol is
  generic (
    g_CLOCK_FREQ        : integer   := 100000000
    );
  port (
    -- Clock and reset
    clk_ik                  : in  std_logic;
    rst_ir                  : in  std_logic;
    cen_ie                  : in  std_logic;
    -- CMB sequence input
    cmb_in_frame_i          : in  std_logic;
    cmb_in_addr_ib          : in  std_logic_vector(32-1 downto 0);
    cmb_in_enable_ib        : in  std_logic_vector(32-1 downto 0);
    cmb_in_wr_i             : in  std_logic;
    cmb_in_wr_data_ib       : in  std_logic_vector(32-1 downto 0);
    cmb_in_rd_i             : in  std_logic;
    cmb_in_rd_data_ob       : out std_logic_vector(32-1 downto 0); 
    cmb_in_rd_valid_o       : out std_logic;
    cmb_in_busy_o           : out std_logic;
    -- CMB commands output  
    cmb_out_frame_o         : out std_logic;
    cmb_out_addr_ob         : out std_logic_vector(32-1 downto 0);
    cmb_out_enable_ob       : out std_logic_vector(32-1 downto 0);
    cmb_out_wr_o            : out std_logic;
    cmb_out_wr_data_ob      : out std_logic_vector(32-1 downto 0);
    cmb_out_rd_o            : out std_logic;
    cmb_out_rd_data_ib      : in  std_logic_vector(32-1 downto 0); 
    cmb_out_rd_valid_i      : in  std_logic;
    cmb_out_busy_i          : in  std_logic
  );
end entity;




--=============================================================================
-- Architecture declaration
--=============================================================================
architecture rtl of spi_protocol is

--------------------------
--CONSTANT
constant c_DAC_HALFPER : integer := g_CLOCK_FREQ/(1000000*20*2); --DAC SPI 20MHz (30MHz max)

--------------------------
--SIGNALS
signal wait_valid      : std_logic;
                     
--CMB in               
signal cmb_in_rd_data   : std_logic_vector(32-1 downto 0);
signal cmb_in_rd_valid  : std_logic;
signal cmb_in_busy      : std_logic;
signal cmb_in_addr      : std_logic_vector(32-1 downto 0);
signal cmb_in_wr        : std_logic;
signal cmb_in_wr_data   : std_logic_vector(32-1 downto 0);

--CMB output
signal cmb_out_frame    : std_logic;
signal cmb_out_addr     : std_logic_vector(32-1 downto 0);
signal cmb_out_enable   : std_logic_vector(32-1 downto 0);
signal cmb_out_wr       : std_logic;
signal cmb_out_wr_data  : std_logic_vector(32-1 downto 0);
signal cmb_out_rd       : std_logic;
                     
--PSI parameters
signal conf1        : std_logic_vector(31 downto 0);
signal spi_pol      : std_logic;
signal spi_pha      : std_logic;
signal spi_lsb1st   : std_logic;
signal spi_cs       : std_logic_vector(4 downto 0);
signal spi_length   : std_logic_vector(11 downto 0);
signal conf2        : std_logic_vector(31 downto 0);
signal spi_halfper  : std_logic_vector(15 downto 0);
signal spi_waittime : std_logic_vector(15 downto 0);
signal spi_shout    : std_logic_vector(31 downto 0);

-- Fsm  
  Type t_state is (
    s_IDLE,
    s_WAIT_SPI_FREE,
    s_WR_CONF1,
    s_WR_CONF2,
    s_WR_SHOUT,
    s_WAIT_SPI_FREE_2,
    s_RD_SHIN
  );
  attribute SYN_ENCODING : STRING;
  attribute SYN_ENCODING of t_state : type is "one-hot, safe";
  signal curr_state: t_state;

begin


--=============================================================================
-- FSM 
--=============================================================================
  --state flow process (synchronous)
  p_protocol_fsm_flow: process (rst_ir, clk_ik)
  begin
    if rst_ir = '1' then
      curr_state        <= s_IDLE;
                        
      wait_valid        <= '0';
      
      --CMB input
      cmb_in_rd_data    <= (others=>'0');
      cmb_in_rd_valid   <= '0';
      cmb_in_busy       <= '0';
      cmb_in_wr         <= '0';
      cmb_in_wr_data    <= (others=>'0');
      cmb_in_addr       <= (others=>'0');
            
      --CMB output
      cmb_out_frame     <= '0';
      cmb_out_addr      <= (others=>'0');
      cmb_out_enable    <= (others=>'1');
      cmb_out_wr        <= '0';
      cmb_out_wr_data   <= (others=>'0');
      cmb_out_rd        <= '0';
         
    elsif rising_edge(clk_ik) then
      if cen_ie = '1' then
        
        --CMB input
        cmb_in_rd_data    <= (others=>'0');
        cmb_in_rd_valid   <= '0';
              
        --CMB output
        cmb_out_frame     <= '0';
        cmb_out_addr      <= (others=>'0');
        cmb_out_enable    <= (others=>'1');
        cmb_out_wr        <= '0';
        cmb_out_wr_data   <= (others=>'0');
        cmb_out_rd        <= '0';
        
        case curr_state is
                  
          --=============================
          when s_IDLE =>
          --=============================
            if cmb_in_busy='0' then
              if cmb_in_wr_i='1' or cmb_in_rd_i='1' then
                curr_state     <= s_WAIT_SPI_FREE;
                --latch address and data
                cmb_in_wr      <= cmb_in_wr_i;
                cmb_in_wr_data <= cmb_in_wr_data_ib;
                cmb_in_addr    <= cmb_in_addr_ib;
                cmb_in_busy    <= '1';
              end if;
            end if;

          --=============================
          when s_WAIT_SPI_FREE =>
          --=============================
            if cmb_out_busy_i = '0' and wait_valid = '0' then 
              if cmb_out_wr='0' and cmb_out_rd='0' then 
                --read status: @xC0 SPI free data(17..16)='00' 
                cmb_out_addr     <= x"00000000"; --STATUS REG
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= x"00000000"; 
                cmb_out_rd       <= '1';                    
              elsif cmb_out_rd = '1' then           
                wait_valid       <='1';
                cmb_out_addr     <= (others=>'0');
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= (others=>'0');
                cmb_out_rd       <= '0';
              end if;
            end if;    
            
            if cmb_out_rd_valid_i = '1' then 
              wait_valid   <='0';
              if cmb_out_rd_data_ib(17 downto 16)="00" then --SPI bus is not busy
                curr_state   <= s_WR_CONF1;
              end if;
            end if;    
  
          
          --=============================
          when s_WR_CONF1 =>
          --=============================
            if cmb_out_busy_i = '0' then             
              if cmb_out_wr='0' and cmb_out_rd='0' then 
                cmb_out_addr     <= x"00000001"; --CONF1 REG
                cmb_out_wr       <= '1';                     
                cmb_out_wr_data  <= conf1;
                cmb_out_rd       <= '0';
              elsif cmb_out_wr = '1' then           
                cmb_out_addr     <= (others=>'0');
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= (others=>'0');
                cmb_out_rd       <= '0';
                curr_state       <= s_WR_CONF2;
              end if;
            end if;
            
          --=============================
          when s_WR_CONF2 =>
          --=============================
            if cmb_out_busy_i = '0' then             
              if cmb_out_wr='0' and cmb_out_rd='0' then 
                cmb_out_addr     <= x"00000002"; --CONF2 REG
                cmb_out_wr       <= '1';                     
                cmb_out_wr_data  <= conf2;
                cmb_out_rd       <= '0';
              elsif cmb_out_wr = '1' then           
                cmb_out_addr     <= (others=>'0');
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= (others=>'0');
                cmb_out_rd       <= '0';
                curr_state       <= s_WR_SHOUT;
              end if;
            end if;

          --=============================
          when s_WR_SHOUT =>
          --=============================
            if cmb_out_busy_i = '0' then             
              if cmb_out_wr='0' and cmb_out_rd='0' then 
                cmb_out_addr     <= x"00000003"; --SHIFT OUT REG
                cmb_out_wr       <= '1';                     
                cmb_out_wr_data  <= spi_shout;
                cmb_out_rd       <= '0';
              elsif cmb_out_wr = '1' then           
                cmb_out_addr     <= (others=>'0');
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= (others=>'0');
                cmb_out_rd       <= '0';
                curr_state       <= s_WAIT_SPI_FREE_2; --read access
              end if;
            end if;
            
          --=============================
          when s_WAIT_SPI_FREE_2 =>
          --=============================
            if cmb_out_busy_i = '0' and wait_valid = '0' then 
              if cmb_out_wr='0' and cmb_out_rd='0' then 
                --read status: @xC0 SPI free data(17..16)='00' 
                cmb_out_addr     <= x"00000000"; --STATUS REG
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= x"00000000"; 
                cmb_out_rd       <= '1';                    
              elsif cmb_out_rd = '1' then           
                wait_valid       <='1';
                cmb_out_addr     <= (others=>'0');
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= (others=>'0');
                cmb_out_rd       <= '0';
              end if;
            end if;   
            
            if cmb_out_rd_valid_i = '1' then 
              wait_valid   <='0';
              if cmb_out_rd_data_ib(17 downto 16)="00" then --SPI bus is not busy
                if cmb_in_wr='1' then
                  curr_state   <= s_IDLE; --end of write access
                  cmb_in_busy  <= '0';
                else 
                  curr_state   <= s_RD_SHIN; --read access
                end if;
              end if;
            end if;    
            
            
          --=============================
          when s_RD_SHIN =>
          --=============================
            if cmb_out_busy_i='0' and wait_valid ='0' then 
              if cmb_out_wr='0' and cmb_out_rd='0' then 
                cmb_out_addr     <= x"00000004"; --SHIFT IN REG
                cmb_out_wr       <= '0';        
                cmb_out_wr_data  <= x"00000000"; 
                cmb_out_rd       <= '1';                
              elsif cmb_out_rd='1' then             
                wait_valid       <='1';
                cmb_out_addr     <= (others=>'0');
                cmb_out_wr       <= '0';
                cmb_out_wr_data  <= (others=>'0');
                cmb_out_rd       <= '0';
              end if;
            end if;
            
            if cmb_out_rd_valid_i='1' then 
              curr_state      <= s_IDLE;
              cmb_in_busy     <= '0';
              cmb_in_rd_data  <= cmb_out_rd_data_ib; --12 lsb useful only
              cmb_in_rd_valid <= '1';
              wait_valid      <= '0';
            end if;  
            
          --=============================
          when others =>
          --=============================
            curr_state    <= s_IDLE;
            
        end case;
      end if;
    end if;
  end process;
  
---------------------------------------
--SPI parameters
---------------------------------------
--@xC4 (CPol_1b, CPha_1b, LSB1st_1b, 8b=00000000, CS_5b, 4b=0000, len_12b) 
conf1        <= spi_pol & spi_pha & spi_lsb1st & x"00" & spi_cs & x"0" & spi_length;
spi_pol      <= '1'; 
spi_pha      <= '0'; 
spi_lsb1st   <= '0'; 
spi_cs       <=      "00000"  when cmb_in_addr(3 downto 0)=x"0" --DAC25 (IC29 OSC1 125MHz)
                else "00001" ;--   cmb_in_addr(3 downto 0)=x"1" --DAC20 (IC30 OSC2 20MHz)
spi_length   <=      x"018"   when cmb_in_addr(3 downto 0)=x"0"; --24bits
--@xC8 (HalfPeriod_16b, WaitTime_16b)
conf2        <= spi_halfper & spi_waittime;
spi_halfper  <= std_logic_vector(to_unsigned(c_DAC_HALFPER,16));--  when cmb_in_addr(3 downto 0)=x"0"; --DAC (30MHz max)
spi_waittime <= x"0002" ; --2 cycles
--@xCC (Value_24b= x"00" & dac_data_16b)
spi_shout    <= x"00" & cmb_in_wr_data(15 downto 0) & x"00";  --unsused 8 LSB

  
---------------------------------------
--output assignment
---------------------------------------
--in
cmb_in_rd_data_ob  <= cmb_in_rd_data ;
cmb_in_rd_valid_o  <= cmb_in_rd_valid;
cmb_in_busy_o      <= cmb_in_busy    ;
--out
cmb_out_frame_o    <= cmb_out_frame  ;
cmb_out_addr_ob    <= cmb_out_addr   ;
cmb_out_enable_ob  <= cmb_out_enable ;
cmb_out_wr_o       <= cmb_out_wr     ;
cmb_out_wr_data_ob <= cmb_out_wr_data;
cmb_out_rd_o       <= cmb_out_rd     ;

  
end architecture;
