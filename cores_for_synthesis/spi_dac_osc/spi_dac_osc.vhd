--============================================================================================
--##################################   Module Information   ##################################
--============================================================================================
--
-- Company: CERN 
--
-- Language: VHDL 2008
--
-- Description:
--
--
----------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- Libraries definition
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library altera_mf;
use altera_mf.all;
use altera_mf.altera_mf_components.all;

-----------------------------------------------------------------------
-- Entity
-----------------------------------------------------------------------
entity spi_dac_osc is
generic(
  g_CLOCK_FREQ        : integer := 100000000  --MHz
);
port (
  -- Clock:
  clk_ik              : in  std_logic;
  rst_ir              : in  std_logic;
  cen_ie              : in  std_logic;
  --input val
  new_val_i           : in std_logic;
  --0V0 is the midscale ( 20.000MHz) for the OSC3
  dac20_val_i         : in std_logic_vector(15 downto 0); --0x0000=0d00000=0V0 for midrange
  --1V6 is the midscale (125.000MHz) for the OSC2
  dac25_val_i         : in std_logic_vector(15 downto 0); --0x51eb=0d20971=1V6 for midrange
  --SPI IO
  PllDacSclk_ok       : out std_logic;
  PllDacDin_o         : out std_logic;
  PllDac25Sync_o      : out std_logic;
  PllDac20Sync_o      : out std_logic
);
end entity;


--=============================================================================
-- Architecture declaration
--=============================================================================
architecture rtl of spi_dac_osc is
    

  --CMB sequence from FSM to protocol
  signal cmb_seq_frame              : std_logic;
  signal cmb_seq_addr               : std_logic_vector(32-1 downto 0);
                                    
  signal cmb_seq_enable             : std_logic_vector(32-1 downto 0);
  signal cmb_seq_wr                 : std_logic;
  signal cmb_seq_wr_data            : std_logic_vector(32-1 downto 0);
  signal cmb_seq_rd                 : std_logic;
  signal cmb_seq_rd_data            : std_logic_vector(32-1 downto 0); 
  signal cmb_seq_rd_valid           : std_logic;
  signal cmb_seq_busy               : std_logic;
  
  --CMB command from protocol to SPI master
  signal cmb_cmd_frame              : std_logic;
  signal cmb_cmd_addr               : std_logic_vector(32-1 downto 0);
  signal cmb_cmd_enable             : std_logic_vector(32-1 downto 0);
  signal cmb_cmd_wr                 : std_logic;
  signal cmb_cmd_wr_data            : std_logic_vector(32-1 downto 0);
  signal cmb_cmd_rd                 : std_logic;
  signal cmb_cmd_rd_data            : std_logic_vector(32-1 downto 0); 
  signal cmb_cmd_rd_valid           : std_logic;
  signal cmb_cmd_busy               : std_logic;
    
  -- SPI DAC OSC
  signal WbLocSpiDacCtrlCyc         : std_logic;
  signal WbLocSpiDacCtrlStb         : std_logic;
  signal WbLocSpiDacCtrlAdr_b32     : std_logic_vector(32-1 downto 0);
  signal WbLocSpiDacCtrlWr          : std_logic;
  signal WbLocSpiDacCtrlDatmosi_b32 : std_logic_vector(32-1 downto 0);
  signal WbLocSpiDacCtrlDatmiso_b32 : std_logic_vector(32-1 downto 0);
  signal WbLocSpiDacCtrlAck         : std_logic;
  
  --internal Sleave select (32 available, but only 2 are mapped)
  signal PllDacSs_nb32              : std_logic_vector(31 downto 0);

  --SPI Master WB
  component SpiMasterWb is
  port (
    Clk_ik                : in   std_logic;
    Rst_irq               : in   std_logic;
    Cyc_i                 : in   std_logic;
    Stb_i                 : in   std_logic;
    We_i                  : in   std_logic;
    Adr_ib3               : in   std_logic_vector(2 downto 0);
    Dat_ib32              : in   std_logic_vector(31 downto 0);
    Dat_oab32             : out  std_logic_vector(31 downto 0);
    Ack_oa                : out  std_logic;
    WaitingNewData_o      : out  std_logic;
    ModuleIdle_o          : out  std_logic;
    SClk_o                : out  std_logic;
    MoSi_o                : out  std_logic;
    MiSo_ib32             : in   std_logic_vector(31 downto 0);
    SS_onb32              : out  std_logic_vector(31 downto 0)
  );
  end component;


begin
        
  --=============================================================================
  -- Sequencing FSM
  --=============================================================================
    i_dac_fsm : entity work.spi_dac_fsm     
    port map(   
      clk_ik              => clk_ik, -- in  std_logic;
      rst_ir              => rst_ir, -- in  std_logic;
      cen_ie              => cen_ie, -- in  std_logic;
      
      new_val_i           => new_val_i, -- in std_logic;
      --0V0 is the midscale ( 20.000MHz) for the OSC3
      dac20_val_i         => dac20_val_i, -- in std_logic_vector(15 downto 0); --0x0000=0d00000=0V0 for midrange
      --1V6 is the midscale (125.000MHz) for the OSC2
      dac25_val_i         => dac25_val_i, -- in std_logic_vector(15 downto 0); --0x51eb=0d20971=1V6 for midrange
  
      -- CMB sequence to SPI protocol 
      cmb_frame_o         => cmb_seq_frame    , -- out std_logic;
      cmb_addr_ob         => cmb_seq_addr     , -- out std_logic_vector(g_RAM_ADDRSIZE-1 downto 0);
      cmb_enable_ob       => cmb_seq_enable   , -- out std_logic_vector(g_RAM_DATASIZE-1 downto 0);
      cmb_wr_o            => cmb_seq_wr       , -- out std_logic;
      cmb_wr_data_ob      => cmb_seq_wr_data  , -- out std_logic_vector(g_RAM_DATASIZE-1 downto 0);
      cmb_rd_o            => cmb_seq_rd       , -- out std_logic;
      cmb_rd_data_ib      => cmb_seq_rd_data  , -- in  std_logic_vector(g_RAM_DATASIZE-1 downto 0); 
      cmb_rd_valid_i      => cmb_seq_rd_valid , -- in  std_logic;
      cmb_busy_i          => cmb_seq_busy       -- in  std_logic
    );
    
  
  
    
  --=============================================================================
  -- SPI Protocol
  --=============================================================================
  i_dac_protocol : entity work.spi_protocol    
    generic map(
      g_CLOCK_FREQ           => g_CLOCK_FREQ      --Hz
      )                      
    port map(                
      -- Clock and reset     
      clk_ik                 => clk_ik,           -- in  std_logic;
      rst_ir                 => rst_ir,           -- in  std_logic;
      cen_ie                 => cen_ie,           -- in  std_logic;
      -- CMB sequence input
      cmb_in_frame_i         => cmb_seq_frame   , -- in  std_logic;
      cmb_in_addr_ib         => cmb_seq_addr    , -- in  std_logic_vector(g_ADDR_LENGTH-1 downto 0);
      cmb_in_enable_ib       => cmb_seq_enable  , -- in  std_logic_vector(g_DATA_LENGTH-1 downto 0);
      cmb_in_wr_i            => cmb_seq_wr      , -- in  std_logic;
      cmb_in_wr_data_ib      => cmb_seq_wr_data , -- in  std_logic_vector(g_DATA_LENGTH-1 downto 0);
      cmb_in_rd_i            => cmb_seq_rd      , -- in  std_logic;
      cmb_in_rd_data_ob      => cmb_seq_rd_data , -- out std_logic_vector(g_DATA_LENGTH-1 downto 0); 
      cmb_in_rd_valid_o      => cmb_seq_rd_valid, -- out std_logic;
      cmb_in_busy_o          => cmb_seq_busy    , -- out std_logic;
      -- CMB commands output
      cmb_out_frame_o         => cmb_cmd_frame   , -- out std_logic;
      cmb_out_addr_ob         => cmb_cmd_addr    , -- out std_logic_vector(g_ADDR_LENGTH-1 downto 0);
      cmb_out_enable_ob       => cmb_cmd_enable  , -- out std_logic_vector(g_DATA_LENGTH-1 downto 0);
      cmb_out_wr_o            => cmb_cmd_wr      , -- out std_logic;
      cmb_out_wr_data_ob      => cmb_cmd_wr_data , -- out std_logic_vector(g_DATA_LENGTH-1 downto 0);
      cmb_out_rd_o            => cmb_cmd_rd      , -- out std_logic;
      cmb_out_rd_data_ib      => cmb_cmd_rd_data , -- in  std_logic_vector(g_DATA_LENGTH-1 downto 0); 
      cmb_out_rd_valid_i      => cmb_cmd_rd_valid, -- in  std_logic;
      cmb_out_busy_i          => cmb_cmd_busy      -- in  std_logic;
    );
    
    
  --=============================================================================
  -- CMB to WB bridge
  --=============================================================================
  i_cmb_to_wb: entity work.cmb_to_wishbone    
    port map(
      -- Clock and reset
      clk_ik              => clk_ik,                     -- in  std_logic;
      rst_ir              => rst_ir,                     -- in  std_logic;
      cen_ie              => cen_ie,                     -- in  std_logic;
      -- CMB input                                       
      cmb_frame_i         => cmb_cmd_frame   ,           -- in  std_logic;
      cmb_addr_ib         => cmb_cmd_addr    ,           -- in  std_logic_vector(g_ADDR_LENGTH-1 downto 0);
      cmb_enable_ib       => cmb_cmd_enable  ,           -- in  std_logic_vector(g_DATA_LENGTH-1 downto 0);
      cmb_wr_i            => cmb_cmd_wr      ,           -- in  std_logic;
      cmb_wr_data_ib      => cmb_cmd_wr_data ,           -- in  std_logic_vector(g_DATA_LENGTH-1 downto 0);
      cmb_rd_i            => cmb_cmd_rd      ,           -- in  std_logic;
      cmb_rd_data_ob      => cmb_cmd_rd_data ,           -- out std_logic_vector(g_DATA_LENGTH-1 downto 0); 
      cmb_rd_valid_o      => cmb_cmd_rd_valid,           -- out std_logic;
      cmb_busy_o          => cmb_cmd_busy    ,           -- out std_logic;
      -- WB output
      wb_cyc_o            => WbLocSpiDacCtrlCyc,         -- out std_logic;
      wb_stb_o            => WbLocSpiDacCtrlStb,         -- out std_logic;
      wb_addr_ob          => WbLocSpiDacCtrlAdr_b32,     -- out std_logic_vector(g_ADDR_LENGTH-1 downto 0);
      wb_we_o             => WbLocSpiDacCtrlWr,          -- out std_logic;       
      wb_data_mosi_ob     => WbLocSpiDacCtrlDatmosi_b32, -- out std_logic_vector(g_DATA_LENGTH-1 downto 0);
      wb_data_miso_ib     => WbLocSpiDacCtrlDatmiso_b32, -- in  std_logic_vector(g_DATA_LENGTH-1 downto 0);
      wb_ack_i            => WbLocSpiDacCtrlAck          -- in  std_logic
    );
    
    
    
  --=============================================================================
  -- SPI Master Wb
  --=============================================================================
  i_DacSpiMaster : SpiMasterWb  
  port map(
    Rst_irq          => rst_ir,
    Clk_ik           => clk_ik,
    --WB         
    Cyc_i            => WbLocSpiDacCtrlCyc,
    Stb_i            => WbLocSpiDacCtrlStb,
    Adr_ib3          => WbLocSpiDacCtrlAdr_b32(2 downto 0),
    We_i             => WbLocSpiDacCtrlWr,
    Dat_ib32         => WbLocSpiDacCtrlDatmosi_b32,
    Dat_oab32        => WbLocSpiDacCtrlDatmiso_b32,
    Ack_oa           => WbLocSpiDacCtrlAck,
    --SPI   
    SClk_o           => PllDacSclk_ok,
    MoSi_o           => PllDacDin_o,
    MiSo_ib32        => (others=>'0'),
    SS_onb32         => PllDacSs_nb32,
    WaitingNewData_o => open,
    ModuleIdle_o     => open
  );
  
  --Slave select outputs
  PllDac25Sync_o <= PllDacSs_nb32(0); 
  PllDac20Sync_o <= PllDacSs_nb32(1); 
    
end architecture;

