`timescale 1ns/1ns

module I2CSlave
#(  parameter g_Address = 7'h55,
    parameter g_MinPerDown = 3_000,
    parameter g_Display = 0,
    parameter g_Initialise = 0)
(
    inout Scl_io,
    inout Sda_io
);

integer ShCount = 0;

reg [3:0] State=1;

reg [7:0] Mem_m [255:0];

reg [7:0] ByteAddr_b8;

reg RdWrBit;

reg Scl_q = 1'bz, 
    Sda_q = 1'bz;

assign Scl_io = Scl_q;
assign Sda_io = Sda_q;

localparam  s_Idle          = 1,
            s_Addressing    = 2,
            s_WrRdSel       = 3,
            s_AckAddress    = 4,
            s_ByteAddr      = 5,
            s_AckByteAddr   = 6,
            s_GetByte       = 7,
            s_SendAckData   = 8,
            s_SendByte      = 9,
            s_GetAckByte    = 10;


integer i;


initial begin
    if (g_Initialise)
        for (i=0; i<256; i = i+1)
            Mem_m[i] = 0;//{4'ha, i[3:0]};
end

always@(negedge Sda_io) if (Scl_io) begin
    State = s_Addressing;  //Start Bit
    ShCount = 'h0;
end

always@(posedge Sda_io) if (Scl_io) begin
    State = s_Idle; //Stop Bit
end

reg [7:0] ShReg;

always@(posedge Scl_io) case (State)
    s_Addressing: begin
        ShReg = {ShReg[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 7) begin
            State = (ShReg[6:0]==g_Address) ? s_WrRdSel : s_Idle;
        end
    end
    s_WrRdSel: begin
        RdWrBit = Sda_io;
        State = s_AckAddress;
    end
    s_AckAddress: begin
        ShCount = 0;
        if (RdWrBit) begin
            State = s_SendByte;
            ShReg = Mem_m[ByteAddr_b8];
        end else State = s_ByteAddr;
    end
    s_ByteAddr: begin
        ByteAddr_b8 = {ByteAddr_b8[6:0], Sda_io};   
        ShCount = ShCount + 1;
        if (ShCount == 8) State = s_AckByteAddr;
    end
    s_AckByteAddr: begin
        State = s_GetByte;
        ShCount = 0;
    end
    s_GetByte: begin
        ShReg = {ShReg[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 8) begin
            if (g_Display)
                $display("I2C@%02h WR %02h=%02h", g_Address, ByteAddr_b8, ShReg);
            Mem_m[ByteAddr_b8] = ShReg;
            State = s_SendAckData;         
        end
    end
    s_SendAckData:begin
        ShCount = 0;
        State = s_GetByte;
        ByteAddr_b8 = ByteAddr_b8 + 1;
    end
    s_SendByte: begin
        if (g_Display && ShCount == 0)
            $display("I2C@%02h RD %02h=%02h", g_Address, ByteAddr_b8, ShReg);
        ShReg = {ShReg[6:0], 1'b0};
        ShCount = ShCount + 1;
        if (ShCount == 8) begin
            State = s_GetAckByte;
            ByteAddr_b8 = ByteAddr_b8 + 1;
            ShReg = Mem_m[ByteAddr_b8];
        end
    end
    s_GetAckByte: begin       
        ShCount = 0;
        State = Sda_io ? s_Idle : s_SendByte;
    end
endcase
    
        
always@(negedge Scl_io) begin
    Scl_q = 1'b0;
    #g_MinPerDown;
    case (State)
    s_Addressing: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_WrRdSel: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_AckAddress: begin
       Sda_q = 1'b0; 
       #100;
       Scl_q = 1'bz;
    end
    s_ByteAddr: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_AckByteAddr: begin
       Sda_q = 1'b0; 
       #100;
       Scl_q = 1'bz;
    end
    s_GetByte: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_SendAckData:begin
       Sda_q = 1'b0; 
       #100;
       Scl_q = 1'bz;    
    end
    s_SendByte: begin
       Sda_q = ShReg[7] ? 1'bz : 1'b0; 
       #100;
       Scl_q = 1'bz;
    end
    s_GetAckByte: begin       
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    default: begin
        Sda_q = 1'bz; 
       #100;
        Scl_q = 1'bz;
    end   
    endcase    
end    
             
endmodule
