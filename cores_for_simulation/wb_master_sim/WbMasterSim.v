`timescale 1ns/100ps 

module WbMasterSim (
    output  reg        Rst_orq,
    input              Clk_ik,
    output  reg [31:0] Adr_obq32,
    output  reg [31:0] Dat_obq32,
    input       [31:0] Dat_ib32,
    output  reg        We_oq,
    output  reg        Cyc_oq,
    output  reg        Stb_oq,
    input              Ack_i);

initial begin 
  We_oq = 0;
  Cyc_oq = 0;
  Stb_oq = 0;
  Rst_orq = 0;
  $warning("This core is deprecated! Please use its SystemVerilog variant.");
end


task WbWrite;
    input [31:0] Address_ib32; 
    input [31:0] Data_ib32;
begin
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Cyc_oq      = 1'b1;
    Stb_oq      = 1'b1;
    We_oq       = 1'b1;
    Adr_obq32   = Address_ib32;
    Dat_obq32   = Data_ib32;
    if (~Ack_i) @(posedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Cyc_oq      = 1'b0;
    Stb_oq      = 1'b0;
    Dat_obq32   = 'hx;
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
end
endtask

task WbRead;
    input [31:0] Address_ib32; 
    output [31:0] Data_ob32;
begin
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Cyc_oq      = 1'b1;
    Stb_oq      = 1'b1;
    We_oq       = 1'b0;
    Adr_obq32   = Address_ib32;
    Dat_obq32   = 'hx;
    if (~Ack_i) @(posedge Ack_i);
    @(posedge Clk_ik);
    #1;
    Data_ob32   = Dat_ib32;
    Cyc_oq      = 1'b0;
    Stb_oq      = 1'b0;
    Dat_obq32   = 'hx;
    if (Ack_i) @(negedge Ack_i);
    @(posedge Clk_ik);   
end
endtask

task Reset;
begin
	@(posedge Clk_ik) Rst_orq = #1 1;
	@(posedge Clk_ik) Rst_orq = #1 0;
end
endtask

endmodule