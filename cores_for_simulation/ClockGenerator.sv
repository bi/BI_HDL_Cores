module ClockGenerator #(
    parameter longint unsigned g_Frequency // in Hz
) (
    output Clk_ok,
    input Enable_i = 1'b1
);

    timeunit 1ps / 1ps;

    time Period = 1s/g_Frequency;
    logic Clock_k = 'b1;

    initial begin
        $timeformat(-9, 3, " ns");
        $display("ClockGenerator (%m):");
        $display("  frequency = %.3f MHz", g_Frequency/1_000_000.0);
        $display("  period = %0t", Period);
        assert (Period > 0ns) else begin
            $error("No clock period!");
            $stop();
        end
    end

    initial
        forever #(Period/2.0) Clock_k = ~Clock_k;

    assign Clk_ok = Clock_k & Enable_i;

endmodule