`timescale 1ns/10ps

module VmeBusModule #(
    parameter g_NSlots = 20,
    parameter g_VerboseDefault = 0,
    parameter g_Timeout = 132000 // in [ns] (has to be higher than timeout in VME-WB interface)
) (
    inout wire As_n,
    inout wire [5:0] AM_b6,
    inout wire [31:1] A_b31,
    inout wire LWord,
    inout wire [1:0] Ds_nb2,
    inout wire Wr_n,
    inout wire [31:0] D_b32,
    inout wire DtAck_n,
    inout wire [7:1] Irq_nb7,
    inout wire Iack_n,
    inout wire [g_NSlots:1] IackIn_nb,
    inout wire [g_NSlots:1] IackOut_nb,
    inout wire SysResetN_irn,
    inout wire SysClk_k,
    inout wire [4:0] Ga_nmb5 [g_NSlots:1],
    inout wire [g_NSlots:1] Gap_nbm //I use a inout wire to detect conflicts (the logic type doesn't have a resolver)
);

logic TaskAs_n = 'bz;
logic [5:0] TaskAM_b6 = 'bz;
logic [31:1] TaskA_b31 = 'bz;
logic TaskLWord = 'bz;
logic [1:0] TaskDs_nb2 = 'bz;
logic TaskWr_n = 'bz;
logic [31:0] TaskD_b32 = 'bz;
logic TaskIack_n = 'bz;
logic TaskSysResetN_irn = 'bz;

logic InternalClk_k = 0;

logic TclVmeWr_p    = 0,
      TclVmeRd_p    = 0,
      TclVmeReset_p = 0,
      TclVerbose    = 0;
logic [31:0] TclVmeAddress_b32, TclVmeData_b32;
logic [31:0] TclVmeResetTime = 10;
logic  [7:0] TclVmeTrExitCode_b8;

// constants for ExitCode_b8 values
// TODO: change this to enum
localparam logic [7:0] c_ExitCode_OK      = 8'h00;
localparam logic [7:0] c_ExitCode_TIMEOUT = 8'h01;

// PULL UPS AND IACK CHAIN

assign (pull1, pull0) As_n = 1'b1;
assign (pull1, pull0) Ds_nb2 = 2'b11;
assign (pull1, pull0) DtAck_n = 1'b1;
assign (pull1, pull0) Irq_nb7 = 7'b111_1111;
assign (pull1, pull0) Iack_n = 1'b1;
assign (pull1, pull0) IackOut_nb = IackIn_nb;
assign IackIn_nb = {IackOut_nb[g_NSlots-1:1], Iack_n};
assign (pull1, pull0) SysResetN_irn = 1'b1;

// VME SYSCLK

always #31.25ns ++InternalClk_k; // 16 MHz
assign SysClk_k = InternalClk_k;

// LINKS BETWEEN TASK LOCIC AND inout wireS

assign As_n          = TaskAs_n;
assign AM_b6         = TaskAM_b6;
assign A_b31         = TaskA_b31;
assign LWord         = TaskLWord;
assign Ds_nb2        = TaskDs_nb2;
assign Wr_n          = TaskWr_n;
assign D_b32         = TaskD_b32;
assign Iack_n        = TaskIack_n;
assign SysResetN_irn = TaskSysResetN_irn;

// GA

for (genvar SlotNumber=1; SlotNumber<g_NSlots+1; SlotNumber++) begin
  assign (highz1, supply0) Ga_nmb5[SlotNumber] = ~SlotNumber[4:0];
  assign (highz1, supply0) Gap_nbm[SlotNumber] = ~^(~SlotNumber[4:0]);
end

// TASKS

task ReleaseVmeBus();
    TaskAs_n = 'bz;
    TaskAM_b6 = 'bz;
    TaskA_b31 = 'bz;
    TaskLWord = 'bz;
    TaskDs_nb2 = 'bz;
    TaskWr_n = 'bz;
    TaskD_b32 = 'bz;
    TaskIack_n = 'bz;
    TaskSysResetN_irn = 'bz;
endtask

task ReadA32D32 (input [31:0] Address_b32, output [31:0] Data_b32, output [7:0] ExitCode_b8, input VerboseAccesses = g_VerboseDefault);
    fork
        // transaction
        begin
            //Making sure everything is 'free' before the task
            TaskAs_n = 'b1;
            TaskAM_b6 = 'b1;
            TaskA_b31 = 'b1;
            TaskLWord = 'b1;
            TaskDs_nb2 = 'b1;
            TaskWr_n = 'b1;
            TaskD_b32 = 'bz;
            TaskIack_n = 'b1;
            TaskSysResetN_irn = 'b1;
            //The actual read sequence
            {TaskA_b31, TaskLWord} = {Address_b32[31:2], 2'b0};
            TaskAM_b6 = 6'h09;
            #10ns TaskAs_n = 1'b0;
            #10ns TaskDs_nb2 = 2'b00;
            @(negedge DtAck_n);
            Data_b32 = D_b32;
            #10ns TaskDs_nb2 = 'bz;
            TaskAs_n = 'bz;
            @(posedge DtAck_n);
            ExitCode_b8 = c_ExitCode_OK;
            #10ns;
            if (VerboseAccesses) $display("VME read  cycle OK: d = 32'h%h at address 32'h%h of A32 address space at %t", Data_b32, Address_b32, $time);
        end
        // timeout
        begin
            #g_Timeout;
            Data_b32 = 'hx;
            ExitCode_b8 = c_ExitCode_TIMEOUT;
            if (DtAck_n)
                if (VerboseAccesses) $display("VME read cycle ERROR (time out with no reply): address 32'h%h at %t", Address_b32, $time);
            else if (~DtAck_n)
                if (VerboseAccesses) $display("VME read cycle ERROR (time out with DTACK never released): address 32'h%h at %t", Address_b32, $time);
        end
    join_any;
    disable fork;  //not really needed except for saving memory and CPU
    ReleaseVmeBus();
endtask:ReadA32D32

task automatic ReadA32D32M(
    input [31:0] Address_b32, inout [31:0] Words [], output [7:0] ExitCode_b8, input VerboseAccesses = g_VerboseDefault);

    fork
        // transaction
        begin:TrnBlk
            //Making sure everything is 'free' before the task
            TaskAs_n = 'b1;
            TaskDs_nb2 = 'b11;
            TaskWr_n = 'b1;
            TaskD_b32 = 'bz ;
            TaskIack_n = 'b1;
            TaskSysResetN_irn = 'b1;
            //The actual read sequence
            {TaskA_b31, TaskLWord} = {Address_b32[31:3], 3'b0};
            TaskAM_b6 = 6'h08;
            #100ns TaskAs_n = 1'b0;
            #140ns TaskDs_nb2 = 2'b00 ;
            @(negedge DtAck_n);
            #40ns TaskDs_nb2 = 2'b11 ;
            TaskA_b31 = 'bz ;
            TaskLWord = 'bz ;
            @(posedge DtAck_n);
            for (int i = 0 ; i < Words.size() ; i = i +2)
            begin
                #40ns TaskDs_nb2 = 2'b00;
                @(negedge DtAck_n);
                Words[i+1] = D_b32 ;
                Words[i] = {A_b31, LWord} ;
                #40ns TaskDs_nb2 = 2'b11 ;
                @(posedge DtAck_n);
            end

            #40ns ;
            TaskAs_n = 1'b1 ;
            TaskDs_nb2 = 'bz ;
            ExitCode_b8 = c_ExitCode_OK;
            #10ns;
            if (VerboseAccesses) $display("VME block read cycle OK: Address 32'h%h, words = %0d, at %t", Address_b32, Words.size(), $time);
        end
        // timeout
        begin:TimeBlk
            #(Words.size() *g_Timeout);
            ExitCode_b8 = c_ExitCode_TIMEOUT;
            if (DtAck_n)
                if (VerboseAccesses) $display("VME read cycle ERROR (time out with no reply): address 32'h%h at %t", Address_b32, $time);
            else if (~DtAck_n)
                if (VerboseAccesses) $display("VME read cycle ERROR (time out with DTACK never released): address 32'h%h at %t", Address_b32, $time);
        end
    join_any
    disable fork ;
    ReleaseVmeBus();
endtask:ReadA32D32M


task WriteA32D32 (input [31:0] Address_b32, input [31:0] Data_b32, output [7:0] ExitCode_b8, input VerboseAccesses = g_VerboseDefault);
    fork
        // transaction
        begin
            //Making sure everything is 'free' before the task
            TaskAs_n = 'b1;
            TaskAM_b6 = 'b1;
            TaskA_b31 = 'b1;
            TaskLWord = 'b1;
            TaskDs_nb2 = 2'b11;
            TaskWr_n = 'b1;
            TaskD_b32 = 'bz;
            TaskIack_n = 'b1;
            TaskSysResetN_irn = 'b1;
            //The actual write sequence
            {TaskA_b31, TaskLWord} = {Address_b32[31:2], 2'b0};
            TaskAM_b6 = 6'h09;
            #10ns TaskAs_n = 1'b0;
            TaskWr_n = 1'b0;
            TaskD_b32 = Data_b32;
            #10ns TaskDs_nb2 = 2'b00;
            @(negedge DtAck_n);
            #10ns TaskDs_nb2 = 'bz;
            TaskAs_n = 'bz;
            #10ns TaskD_b32 = 'hz;
            @(posedge DtAck_n);
            ExitCode_b8 = c_ExitCode_OK;
            #10ns;
            if (VerboseAccesses) $display("VME write cycle OK: d = 32'h%h at address 32'h%h of A32 address space at %t", Data_b32, Address_b32, $time);
        end
        // timeout
        begin
            #g_Timeout;
            ExitCode_b8 = c_ExitCode_TIMEOUT;
            if (DtAck_n)
                if (VerboseAccesses) $display("VME write cycle ERROR (time out with no reply): d = 32'h%h at address 32'h%h at %t", Data_b32, Address_b32, $time);
            else if (~DtAck_n)
                if (VerboseAccesses) $display("VME write cycle ERROR (time out with DTACK never released): d = 32'h%h at address 32'h%h at %t", Data_b32, Address_b32, $time);
        end
    join_any;
    disable fork;  //not really needed except for saving memory and CPU
    ReleaseVmeBus();
endtask:WriteA32D32

task VmeReset(integer ResetTime, input VerboseAccesses = g_VerboseDefault);
    TaskSysResetN_irn = 1'b0;
    if (VerboseAccesses) begin
       $display("VME reset asserted at          %t", $time);
    end
    #ResetTime TaskSysResetN_irn = 1'bz;
    if (VerboseAccesses) $display("VME reset released at          %t", $time);
endtask:VmeReset

// Logic to be used via TCL scripts

always @(posedge TclVmeRd_p) begin
    ReadA32D32(TclVmeAddress_b32, TclVmeData_b32, TclVmeTrExitCode_b8, TclVerbose);
    $stop();
end

always @(posedge TclVmeWr_p) begin
    WriteA32D32(TclVmeAddress_b32, TclVmeData_b32, TclVmeTrExitCode_b8, TclVerbose);
    $stop();
end

always @(posedge TclVmeReset_p) begin
    VmeReset(TclVmeResetTime, TclVerbose);
    $stop();
end

endmodule

