#!ruby

require 'erb'
require 'fileutils'

# Phase width, data width (sin,cos), steps = total number of iterations, rpc = maximum rotations per cycle, cps = maximum cycles per sample.
# Default settings generate fully pipelined code (1 sample per cycle, 1 rotation per cycle).
def gen_vars(phase_w:, data_w:, steps:, rpc:1, cps:1, mod_name:"Cordic", flags_w:0)
    stages = (steps/rpc/cps).floor()
    if ((stages*rpc*cps) != steps)
        puts("Error, please select steps, rpc and cps such that steps/rpc/cps is integral number!")
    end

    gain = 1.0
    (0...steps).each() {|s|
        gain = gain *Math.sqrt(1.0 + 2.0**(-2.0*s))
    }

    if ((Math.atan(2.0**(-(steps -1)))/(2.0*Math::PI) *2.0**phase_w).to_i() == 0)
        puts("Please check number of steps (steps) and phase width (phase_w) as higher order rotations become 0.")
    end

    if (steps >= data_w)
        puts("data_w <= steps does not make sense, as calculations will shift data to 0x0.")
    end

    return binding
end

# Read RTL and testbench "template" files
template_rtl = ERB.new(File.read("Cordic.sv.erb"), nil, '-')
template_tb = ERB.new(File.read("tb_cordic.sv.erb"), nil, '-')

# Current test set for cordic generator. Should be expanded as more use cases are gathered
testset = {
    "BbqSlow" => {:phase_w=>32, :data_w=>33, :steps=>30, :rpc=>1, :cps=>6, :flags_w=>1},
    "Bbq" => {:phase_w=>32, :data_w=>33, :steps=>30, :rpc=>2, :cps=>3, :flags_w=>1},
    "Pipelined" => {:phase_w=>24, :data_w=>16, :steps=>15, :rpc=>1, :cps=>1},
    "Iterative" => {:phase_w=>24, :data_w=>32, :steps=>22, :rpc=>1, :cps=>22},
    "R2C5" => {:phase_w=>34, :data_w=>38, :steps=>30, :rpc=>2, :cps=>5},
    "R8C3" => {:phase_w=>64, :data_w=>56, :steps=>48, :rpc=>8, :cps=>3}
}

# Generate RTL and test file
# Check if directory exists
gen_dir = "./generated"
if (File.exist?(gen_dir))
    if (!Dir.empty?(gen_dir))
        puts("#{gen_dir} should be empty directory!")
        exit 1
    end
else
    FileUtils.mkdir_p(gen_dir)
end

gen_dir = gen_dir +"/"
testset.each_pair() {|k, v|
    # Open RTL and test files
    rtl = File.open(gen_dir + k + "Cordic.sv", 'w')
    tst = File.open(gen_dir + "Test" +k + "Cordic.sv", 'w')

    # Construct default module name, if it is not in settings
    v[:mod_name] = v.fetch(:mod_name, k + "Cordic")
    # Create binding for parameters
    parameters = gen_vars(**v)

    # Output RTL
    rtl.puts(template_rtl.result(parameters))
    # Output bench into test file
    tst.puts(template_tb.result(parameters))

    rtl.close()
    tst.close()
}

