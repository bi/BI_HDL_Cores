# First parameter provides filename for RTL you would like to run simulation for
# Testbench file is then extracted from this

dir=$(dirname $1)
file=$(basename $1)
testfile="${dir}/Test${file}"
vlog $1 $testfile && vopt +acc -o _tb tb && vsim -batch _tb -do "run -all ; quit"

