//============================================================================================//
//################################   Test Bench Information   ################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: <tb_ModuleName>.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    <author>           <description>
//
// Language: Verilog 2005
//
// Module Under Test: <Module Name (ModuleName.v)>
//
// Targeted device:
//
//     - Vendor: <FPGA manufacturer if vendor specific code>
//     - Model:  <FPGA Model if model specific>
//
// Description:
//
//     <Brief description of the test bench.>
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module tb_VfcTemplate;
//=======================================  Declarations  =====================================//

//==== Local parameters ====//
localparam c_MyParam   = 16'hcaca,
           c_YourParam = 16'ac1d;
//==== Wires & Regs ====//
reg        Reset_rq;
reg        Clk125MHz_kq;
reg        ControlReg1_q;
wire       StatusWire;
reg  [7:0] MyCounter_cb8;

//=====================================  Status & Control  ====================================//

//==== Clocks generation ====//
always #4 Clk125MHz_kq     = ~Clk125MHz_kq; // Comment: 8ns period

//==== Stimulus & Display ====//
initial begin
    Reset_rq            = 1'b1;
    Clk125MHz_kq        = 1'b1;
    ControlReg1_q       = 1'b0;
    //--
    $display($time, " -> Simulation Start");
    #1000
    @(posedge Clk125MHz_kq);
    Reset_rq            = 1'b0;
    $display($time, " -> Deassert Reset");
    #1000
    @(posedge Clk125MHz_kq);
    ControlReg1_q       = 1'b1;
    $display($time, " -> Trigger");
    repeat(10000)@(posedge Clk125MHz_kq);
    #50000
    $display($time, " -> Simulation Stop");
    $stop;
end

//==== Monitors ====//
initial begin
    $monitor($time, " -> StatusWire = %b",StatusWire);
end

//=== Test logic ====//
// Example of synchronous logic with synchronous reset:
always @(posedge Clk125MHz_kq)
    if (Reset_rq) begin
        MyCounter_cb8        <= #1 8'h0;
    end else begin
        MyCounter_cb8        <= #1 MyCounter_cb8 + 1'b1;
    end

//==== DUT ====//
TestModule # (
    .g_BusWidth  (80))
Dut (
    .Reset_ir    (Reset_rq),
    .Clk_ik      (Clk125MHz_kq),
    .InputPort_i (ControlReg1_q),
    .OutpuPort_o (StatusWire));

endmodule
