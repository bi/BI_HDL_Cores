//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: <ModuleName>.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    <author>           <description>
//
// Language: Verilog 2005
//
// Targeted device:
//
//     - Vendor:  <FPGA manufacturer if vendor specific code>
//     - Model:   <FPGA Model if model specific>
//
// Description:
//
//     <Brief description of the module.>
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module ModuleName
//====================================  Global Parameters  ===================================//
#(  parameter g_Parameter1 = 32'hfac3_babe,
              g_Parameter2 = 16'hcafe)
//========================================  I/O ports  =======================================//

(
    //==== Clocks & Resets ====//
    input             Clock_ik,
    input             Reset_iran,   //Comment: Just a comment about the functionality
    input             Reset_ir,
    //==== <Section> ====//
    input             Input1_i,
    output     [31:0] Output_ob8,   //Comment: (a) Another comment about the functionality
                                    //         (b) Second comment about the inout
    output reg [31:0] Output_oqb32,
    // <Sub section>:
    inout             InOut_io,
    //==== <Section> ====//
    output            Output3_oa
);

//=======================================  Declarations  =====================================//

//==== Local parameters ====//
localparam c_MyParam   = 16'hcaca,
           c_YourParam = 16'ac1d;
// FSM:
localparam s_Idle      = 3'b001,
           s_State1    = 3'b010,
           s_State2    = 3'b100;
//==== Wires & Regs ====//
wire       Wire1;
reg  [7:0] MyCounter_c8;
reg  [7:0] YourCounter_c8;
// FSM:
reg [ 2:0] State_qb3, NextState_ab3;

//=======================================  User Logic  =======================================//

//==== Example of Sequential logic ====//

// Example of synchronous logic with asynchronous reset:
always @(posedge Clock_ik or negedge Reset_iran)
    if (~Reset_iran) begin
        MyCounter_c8          <= #1 8'h0;
    end else if (Reset_ir) begin
        MyCounter_c8          <= #1 8'h0;
    end else begin
        MyCounter_c8          <= #1 MyCounter_c8 + 1'b1;
    end

// Example of synchronous logic with synchronous reset:
always @(posedge Clock_ik)
    if (Reset_ir) begin
        YourCounter_c8        <= #1 8'h0;
    end else begin
        YourCounter_c8        <= #1 YourCounter_c8 + 1'b1;
    end

//==== Example of FSM ====//

always @(posedge Clock_ik) State_qb3 <= #1 Reset_ir ? s_Idle : NextState_ab3;
always @* begin
    NextState_ab3              = State_qb3;
    case(State_qb3)
        s_Idle: begin
            if (Condition1) begin
                NextState_ab3  = s_State1;
            end
        end
        s_State1: begin
            if (Condition2) begin
            NextState_ab3      = s_State2;
            end else if (Condition0) begin
                NextState_ab3  = s_Idle;
            end
        end
        s_State2: begin
            if (Condition0) begin
                NextState_ab3  = s_Idle;
            end
        end
        default: begin
            NextState_ab3      = s_Idle;
        end
    endcase
end
always @(posedge Clock_ik)
    if (Reset_ir) begin
        IdleAction_q           <= #1 4'b0;
        State1Action_q         <= #1 4'b0;
        State2Action_q         <= #1 4'b0;
    end else begin
        case(State_qb3)
            s_Idle: begin
                IdleAction_q   <= #1 4'b0;
                State1Action_q <= #1 4'b0;
                State2Action_q <= #1 4'b0;
            end
            s_State1: begin
                State1Action_q <= #1 4'b0;
            end
            s_State2: begin
                State2Action_q <= #1 4'b0;
            end
        endcase
    end

endmodule
